(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-product-module"],{

/***/ "./src/app/core/models/image.ts":
/*!**************************************!*\
  !*** ./src/app/core/models/image.ts ***!
  \**************************************/
/*! exports provided: Image */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Image", function() { return Image; });
var Image = /** @class */ (function () {
    function Image() {
    }
    return Image;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-description/product-description.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-description/product-description.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div itemprop=\"description\" class=\"col-12\">\n  <div innerHTML=\"{{description}}\"> </div>\n</div>\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-description/product-description.component.scss":
/*!***************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-description/product-description.component.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-description/product-description.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-description/product-description.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: ProductDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDescriptionComponent", function() { return ProductDescriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductDescriptionComponent = /** @class */ (function () {
    function ProductDescriptionComponent() {
    }
    ProductDescriptionComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductDescriptionComponent.prototype, "description", void 0);
    ProductDescriptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-description',
            template: __webpack_require__(/*! ./product-description.component.html */ "./src/app/product/components/product-detail-page/product-description/product-description.component.html"),
            styles: [__webpack_require__(/*! ./product-description.component.scss */ "./src/app/product/components/product-detail-page/product-description/product-description.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], ProductDescriptionComponent);
    return ProductDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-detail-page.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-detail-page.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"product$ | async; let product\" class=\"container\">\n  <app-product-details [product]=\"product\" ></app-product-details>\n</div>\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-detail-page.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-detail-page.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-detail-page.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-detail-page.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ProductDetailPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPageComponent", function() { return ProductDetailPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../core/services/product.service */ "./src/app/core/services/product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductDetailPageComponent = /** @class */ (function () {
    function ProductDetailPageComponent(productService, route) {
        var _this = this;
        this.productService = productService;
        this.route = route;
        this.product$ = null;
        /**On Init
         * 1. Parse route params
         * 2. Retrive product id
         * 3. Ask for the product detail based on product id
         * */
        this.actionsSubscription$ = this.route.params.subscribe(function (params) {
            _this.productId = params['id'];
            _this.product$ = _this.productService.getProduct(_this.productId);
        });
    }
    ;
    ProductDetailPageComponent.prototype.ngOnInit = function () {
    };
    ProductDetailPageComponent.prototype.ngOnDestroy = function () {
        this.actionsSubscription$.unsubscribe();
    };
    /**
     * Action To be dispatched
     * when added to cart
     */
    ProductDetailPageComponent.prototype.addToCart = function () {
        return;
    };
    ProductDetailPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-detail-page',
            template: __webpack_require__(/*! ./product-detail-page.component.html */ "./src/app/product/components/product-detail-page/product-detail-page.component.html"),
            styles: [__webpack_require__(/*! ./product-detail-page.component.scss */ "./src/app/product/components/product-detail-page/product-detail-page.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_core_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ProductDetailPageComponent);
    return ProductDetailPageComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-details/product-details.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-details/product-details.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div itemscope itemtype=\"https://schema.org/Product\" *ngIf=\"product != null\" class=\"row pt-5\">\n  <div class=\"col-12 col-md-5\">\n    <app-image-container [images]=\"images\" class=\"col-12\" [selectedImage]=\"images[0]\">\n    </app-image-container>\n  </div>\n  <div class=\"col-md-7 col-12\">\n    <app-product-price-info [product]=\"product\" (onAddToCart)=\"addToCart($event)\" (onMarkAsFavorites)=\"markAsFavorite()\" (selectedVariant)=\"selectedVariant($event)\"\n      class=\"row\">\n    </app-product-price-info>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"col-12\">\n    <h1 class=\"mt-2\">\n      Description\n    </h1>\n    <app-product-description [description]=\"product.description\" class=\"row\">\n    </app-product-description>\n  </div>\n</div>\n<div *ngIf=\"similarProducts$ | async; let similarProducts\">\n  <div class=\"slider\" *ngIf=\"similarProducts.length >0\">\n    <h2>\n      Similar products\n    </h2>\n    <app-product-slider [productlist]=\"similarProducts\"></app-product-slider>\n  </div>\n</div>\n<div *ngIf=\"relatedProducts$ | async; let relatedProducts\">\n  <div class=\"slider\" *ngIf=\"relatedProducts.length >0\">\n    <h2>\n      Related products\n    </h2>\n    <app-product-slider [productlist]=\"relatedProducts\"></app-product-slider>\n  </div>\n</div>\n<app-product-review [product]=\"product\" [reviewList]='reviewProducts$ | async'></app-product-review>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-details/product-details.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-details/product-details.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padding-top {\n  padding-top: 50px; }\n\nh1 {\n  font-size: 1.7em;\n  color: #212529; }\n\n.pdp-add-to-bag {\n  width: 60%;\n  text-align: center;\n  padding: 10px 0;\n  background-color: #e9ecef;\n  border: 1px solid #e9ecef;\n  color: #fff; }\n\n.pdp-button {\n  border-radius: 3px;\n  outline: 0;\n  margin-top: 10px;\n  padding: 10px 30px;\n  font-size: 15px;\n  min-height: 22px;\n  margin-bottom: 10px; }\n\n.fav-icon-sontainer {\n  position: relative; }\n\n.fav-icon-sontainer .favorite {\n    position: absolute;\n    right: 8px;\n    top: -24px;\n    background-color: #e9ecef;\n    color: #fff;\n    padding: 10px 13px;\n    border-radius: 50%;\n    box-shadow: 0px 0px 8px #212529;\n    z-index: 20; }\n\n.slider {\n  margin-top: 1vw;\n  padding-top: 1vw; }\n\n.slider h2 {\n    font-size: 1.7em;\n    color: #212529; }\n\n@media screen and (min-width: 310px) and (max-width: 768px) {\n  .fav-icon-sontainer .favorite {\n    top: 50px; } }\n\n.cust-reviews .avg-review {\n  padding: 1rem 0 3rem; }\n\n.cust-reviews .avg-review .out-review {\n    border-left: 1px solid #ccc; }\n\n.cust-reviews .rataingpercentage {\n  font-size: 1.2em;\n  margin-bottom: 10px; }\n\n.cust-reviews .rataingpercentage .progress-container .progress {\n    display: flex;\n    height: 1.6rem; }\n\n.cust-reviews .review-display .rating-inner {\n  padding-bottom: 1rem;\n  border-bottom: 1px solid #ccc;\n  margin-bottom: 1rem; }\n\n.cust-reviews .review-display .rating-inner .rating-head {\n    font-weight: 700;\n    color: #212529;\n    position: relative;\n    left: -5px;\n    text-transform: capitalize; }\n\n.cust-reviews .review-display .rating-by {\n  font-size: 1em;\n  color: #7b8289; }\n\n.cust-reviews .review-display .description {\n  padding-top: 1rem;\n  text-align: justify; }\n\n.cust-reviews .review-sort-filter {\n  border-top: 1px solid #ccc;\n  border-top: 1px solid #ccc;\n  background-color: #e9ecef;\n  padding: 1rem; }\n\n.product-box {\n  border-radius: 5px;\n  margin-top: 1rem;\n  border: 1px solid transparent; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n    .product-box {\n      margin-right: 1rem;\n      border: 1px solid #ccc; } }\n\n.product-box:hover {\n    border-color: black; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-details/product-details.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-details/product-details.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: ProductDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsComponent", function() { return ProductDetailsComponent; });
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../reducers/selectors */ "./src/app/product/reducers/selectors.ts");
/* harmony import */ var _actions_product_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var _home_reducers_selectors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../home/reducers/selectors */ "./src/app/home/reducers/selectors.ts");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../../../checkout/actions/checkout.actions */ "./src/app/checkout/actions/checkout.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../../../../core/services/product.service */ "./src/app/core/services/product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ProductDetailsComponent = /** @class */ (function () {
    function ProductDetailsComponent(checkoutActions, store, productService, router, toastrService, searchActions, productsActions) {
        this.checkoutActions = checkoutActions;
        this.store = store;
        this.productService = productService;
        this.router = router;
        this.toastrService = toastrService;
        this.searchActions = searchActions;
        this.productsActions = productsActions;
    }
    ProductDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.product.has_variants) {
            var product = this.product.variants[0];
            this.description = product.description;
            this.images = product.images;
            this.variantId = product.id;
            this.productID = this.product.id;
            this.product.display_price = product.display_price;
            this.product.price = product.price;
            this.product.master.is_orderable = product.is_orderable;
            this.product.master.cost_price = product.cost_price;
        }
        else {
            this.description = this.product.description;
            this.images = this.product.master.images;
            this.variantId = this.product.master.id;
            this.productID = this.product.id;
        }
        this.productService.getRelatedProducts(this.productID)
            .subscribe(function (productdata) {
            _this.productdata = productdata;
        });
        if (this.product.taxon_ids[0]) {
            this.store.dispatch(this.searchActions.getProductsByTaxon("id=" + this.product.taxon_ids[0]));
            this.similarProducts$ = this.store.select(_home_reducers_selectors__WEBPACK_IMPORTED_MODULE_2__["getProductsByKeyword"]);
        }
        this.store.dispatch(this.productsActions.getRelatedProduct(this.productID));
        this.relatedProducts$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_0__["relatedProducts"]);
        this.store.dispatch(this.productsActions.getProductReviews(this.productID));
        this.reviewProducts$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_0__["productReviews"]);
    };
    ProductDetailsComponent.prototype.ngOnChanges = function () { };
    ProductDetailsComponent.prototype.addToCart = function (quantitiy) {
        this.store.dispatch(this.checkoutActions.addToCart(this.variantId, quantitiy));
    };
    ProductDetailsComponent.prototype.markAsFavorite = function () {
        var _this = this;
        this.productService.markAsFavorite(this.product.id).subscribe(function (res) {
            _this.toastrService.info(res['message'], 'info');
        });
    };
    ProductDetailsComponent.prototype.showReviewForm = function () {
        this.router.navigate([this.product.slug, 'write_review'], {
            queryParams: { prodId: this.productID }
        });
    };
    ProductDetailsComponent.prototype.selectedVariant = function (variant) {
        this.images = variant.images;
        this.variantId = variant.id;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_8__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_9__["Product"])
    ], ProductDetailsComponent.prototype, "product", void 0);
    ProductDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_8__["Component"])({
            selector: 'app-product-details',
            template: __webpack_require__(/*! ./product-details.component.html */ "./src/app/product/components/product-detail-page/product-details/product-details.component.html"),
            styles: [__webpack_require__(/*! ./product-details.component.scss */ "./src/app/product/components/product-detail-page/product-details/product-details.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_8__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_7__["CheckoutActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_6__["Store"],
            _core_services_product_service__WEBPACK_IMPORTED_MODULE_10__["ProductService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_3__["SearchActions"],
            _actions_product_actions__WEBPACK_IMPORTED_MODULE_1__["ProductActions"]])
    ], ProductDetailsComponent);
    return ProductDetailsComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-images/product-images.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-images/product-images.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"image-container\" *ngIf=\"images\">\n  <div class=\"row\">\n    <div class=\"col-1\">\n      <div class=\"thumbnails-vertical-container d-none d-lg-block\">\n        <button class=\"thumbnails-thumbnail-button\" *ngFor=\"let image of images\">\n          <img class=\"thumbnails-thumbnail img-fluid\" [src]=\"getProductImageUrl(image.small_url)\" [alt]=\"image.alt\" (mouseover)=\"onMouseOver(image)\">\n        </button>\n      </div>\n    </div>\n    <div class=\"col-11\" *ngIf=\"selectedImage\">\n      <!-- <img itemprop=\"image\" [zoomable]='zoomOptions' [src]=\"getProductImageUrl(selectedImage.large_url)\" [alt]=\"selectedImage.alt\" class=\"thumbnails-selected-image img-fluid\" -->\n      <img itemprop=\"image\" [zoomable]='zoomOptions' [zoomableSrc]='getProductImageUrl(selectedImage.large_url)' [src]=\"getProductImageUrl(selectedImage.large_url)\"\n        [alt]=\"selectedImage.alt\" class=\"thumbnails-selected-image img-fluid\">\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-images/product-images.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-images/product-images.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image-container .thumbnails-selected-image {\n  background: #fff none repeat scroll 0% 0%/auto padding-box border-box;\n  max-width: 100%;\n  width: 100%;\n  margin: auto;\n  height: 67%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down; }\n\n.image-container .thumbnails-vertical-container {\n  width: 60px;\n  float: left;\n  margin-bottom: 10px; }\n\n.image-container .thumbnails-vertical-container .thumbnails-thumbnail-button {\n    outline: 0;\n    margin-right: 15px;\n    background-color: #fff;\n    border: 0;\n    padding: 0;\n    position: relative;\n    cursor: pointer;\n    margin-bottom: 10px; }\n\nimg {\n  display: block; }\n\n.img1 {\n  width: 400px;\n  height: 274px; }\n\n.img2 {\n  width: 200px;\n  height: 020px; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-images/product-images.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-images/product-images.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ProductImagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductImagesComponent", function() { return ProductImagesComponent; });
/* harmony import */ var _core_models_image__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../core/models/image */ "./src/app/core/models/image.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var img1 = __webpack_require__(/*! ../../../../../assets/thumbnail.jpg */ "./src/assets/thumbnail.jpg");
var img2 = __webpack_require__(/*! ../../../../../assets/thumbnail2.jpg */ "./src/assets/thumbnail2.jpg");
var ProductImagesComponent = /** @class */ (function () {
    function ProductImagesComponent() {
        this.imageSource1 = img1;
        this.imageSource2 = img2;
        this.images = null;
        this.selectedImage = null;
    }
    ProductImagesComponent.prototype.ngOnInit = function () { };
    ProductImagesComponent.prototype.getProductImageUrl = function (url) {
        return url;
    };
    ProductImagesComponent.prototype.onMouseOver = function (image) {
        this.selectedImage = image;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Array)
    ], ProductImagesComponent.prototype, "images", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", _core_models_image__WEBPACK_IMPORTED_MODULE_0__["Image"])
    ], ProductImagesComponent.prototype, "selectedImage", void 0);
    ProductImagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-image-container',
            template: __webpack_require__(/*! ./product-images.component.html */ "./src/app/product/components/product-detail-page/product-images/product-images.component.html"),
            styles: [__webpack_require__(/*! ./product-images.component.scss */ "./src/app/product/components/product-detail-page/product-images/product-images.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], ProductImagesComponent);
    return ProductImagesComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.html":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.html ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"bordered\">\n  <div class=\"buybox__purchase\">\n    <form>\n      <div>\n        <div class=\"buy-quantity\">\n          <a class=\"btn  quantity-minus\" (click)=\"decreaseCount()\">-</a>\n          <span class=\"quantity-input\">\n            <span class=\"quantity-number\">{{count}}</span>\n            <span>Quantity</span>\n          </span>\n          <a class=\"btn  quantity-plus\" (click)=\"increseCount()\">+</a>\n        </div>\n      </div>\n      <div class=\"buttons row\" *ngIf=\"isOrderable\">\n        <input type=\"submit\" class=\"btn themebtnprimary js-add-cart\" value=\"Add to Cart\" (click)=\"addToCart(count)\">\n        <button (click)=\"buyNow(count)\" class=\"btn themebtnprimary themebtnprimarysade mt-1\">Buy Now</button>\n      </div>\n      <div class=\"buttons row\" *ngIf=\"!isOrderable\">\n        <input type=\"submit\" class=\" btn themebtnprimary themebtndisable\" value=\"Out of Stock\" (click)=\"addToCart(count)\"\n          disabled>\n      </div>\n    </form>\n  </div>\n  <a class=\"buybox__favorite\" (click)=\"markAsFavorites()\">\n    <span>\n      <i class=\"fa fa-heart\"></i> Add to Favorites</span>\n  </a>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.scss":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.scss ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#buybox {\n  float: left;\n  width: 40%; }\n  @media only screen and (min-width: 320px) and (max-width: 767px) {\n    #buybox {\n      width: 100%; } }\n  #buybox .bordered {\n    background-color: #f8f9fa;\n    border: 1px solid #ccc;\n    border-radius: 5px; }\n  #buybox .bordered .buybox__favorite {\n      cursor: pointer;\n      font-size: 1.1rem;\n      display: block;\n      text-align: center;\n      padding: 16px 20px;\n      background: #fff;\n      border-top: 1px solid #ccc;\n      border-radius: 0 0 5px 5px; }\n  #buybox .bordered .buybox__purchase {\n      padding: 20px; }\n  #buybox .bordered .buybox__purchase .buttons {\n        padding: 0 1.2rem; }\n  #buybox .bordered .buybox__purchase .buy-quantity {\n        display: block;\n        margin-bottom: 15px;\n        border: 1px solid #ccc;\n        box-shadow: 0 1px 0 #ccc;\n        border-radius: 4px;\n        overflow: hidden; }\n  #buybox .bordered .buybox__purchase .buy-quantity .quantity-input {\n          width: 33.33333%;\n          float: left;\n          border: 1px solid;\n          border-color: #fff;\n          background-color: #fff;\n          position: relative; }\n  #buybox .bordered .buybox__purchase .buy-quantity .quantity-input .quantity-number {\n            top: 0;\n            bottom: auto;\n            font-size: 1.2em; }\n  #buybox .bordered .buybox__purchase .buy-quantity .quantity-input span {\n            display: block;\n            text-align: center;\n            font-size: .8em;\n            color: #343a40; }\n  #buybox .bordered .buybox__purchase .buy-quantity a.quantity-minus {\n          font-size: 2.4em;\n          line-height: 1.3em; }\n  #buybox .bordered .buybox__purchase .buy-quantity a.disabled {\n          cursor: default;\n          background: #e9ecef;\n          color: #ccc; }\n  #buybox .bordered .buybox__purchase .buy-quantity a {\n          width: 33.33333%;\n          float: left;\n          height: 50px;\n          padding: 0;\n          margin: 0;\n          line-height: 50px;\n          font-weight: 400;\n          font-size: 1.7em;\n          border: none;\n          box-shadow: none;\n          border-radius: 0;\n          -webkit-user-select: none;\n          -moz-user-select: none;\n          -ms-user-select: none;\n          user-select: none; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.ts ***!
  \********************************************************************************************************************/
/*! exports provided: ProductCountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCountComponent", function() { return ProductCountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductCountComponent = /** @class */ (function () {
    function ProductCountComponent(router) {
        this.router = router;
        this.onAddToCart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMarkAsFavorites = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.count = 1;
    }
    ProductCountComponent.prototype.ngOnInit = function () {
    };
    ProductCountComponent.prototype.increseCount = function () {
        this.count += 1;
    };
    /**
     *
     *
     * @memberof ProductcountComponent
     */
    ProductCountComponent.prototype.decreaseCount = function () {
        this.count -= 1;
        if (this.count <= 1) {
            this.count = 1;
        }
    };
    ProductCountComponent.prototype.addToCart = function (count) {
        this.onAddToCart.emit(count);
    };
    ProductCountComponent.prototype.buyNow = function (count) {
        this.onAddToCart.emit(count);
        this.router.navigate(['checkout', 'address']);
    };
    ProductCountComponent.prototype.markAsFavorites = function () {
        this.onMarkAsFavorites.emit();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_1__["Product"])
    ], ProductCountComponent.prototype, "product", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductCountComponent.prototype, "isOrderable", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductCountComponent.prototype, "onAddToCart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductCountComponent.prototype, "onMarkAsFavorites", void 0);
    ProductCountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-count',
            template: __webpack_require__(/*! ./product-count.component.html */ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.html"),
            styles: [__webpack_require__(/*! ./product-count.component.scss */ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ProductCountComponent);
    return ProductCountComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 \">\n  <h1 itemprop=\"name\" class=\"title\">{{product.name}}</h1>\n  <!-- <div class=\"product-subtitle\">\n    <a href=\"#\">\n      By\n      #To-do read from product properties.\n      <span itemprop=\"brand\"></span>\n    </a>\n  </div> -->\n</div>\n<div class=\"col-12\">\n  <div class=\"product-header-extras\">\n    <div class=\"ugc pr-3\">\n      <a class=\"hide-phone js-revScrollTo\">\n        <ngx-input-star-rating disabled=\"true\" value=\"{{product.avg_rating }}\" required></ngx-input-star-rating>\n        {{product.reviews_count }} Reviews\n      </a>\n    </div>\n  </div>\n</div>\n<div class=\"col-12\">\n  <div class=\"buybox-wrapper\">\n    <div id=\"product-vitals\">\n      <div id=\"pricing\">\n        <ul class=\"product-pricing\">\n          <li class=\"list-price\" *ngIf=\"discount > 0\">\n            <p class=\"title\">\n              List Price:</p>\n            <p class=\"price\">{{product.currency}}{{product.master.cost_price}}</p>\n          </li>\n          <li class=\"our-price\">\n            <p class=\"title\">Price:</p>\n            <p class=\"price\">\n              <span class=\"ga-eec__price\">\n                {{product.display_price}}</span>\n              <span class=\"free-shipping  d-xs-none d-sm-none d-none d-lg-block\">FREE 1-2 Day\n                <span>Shipping on this item</span>\n              </span>\n            </p>\n          </li>\n          <li class=\"you-save\" *ngIf=\"discount > 0\">\n            <p class=\"title\">You Save:</p>\n            <p class=\"price\">\n              {{product.currency}}{{discount}} ({{discountPercent}})\n            </p>\n          </li>\n        </ul>\n      </div>\n      <div id=\"featured-promotions\" class=\"pt-3\" *ngIf=\"customOptionTypesHash\">\n        <app-product-variants [customOptionTypesHash]=\"customOptionTypesHash\" [currentSelectedOptions]=\"currentSelectedOptions\" (onOptionClickEvent)=\"onOptionClick($event)\"\n          [correspondingOptions]=\"correspondingOptions\" [mainOptions]=\"mainOptions\" class=\"row m-0\"></app-product-variants>\n        <hr class=\"\">\n      </div>\n\n    </div>\n    <div id=\"buybox\" class=\"\">\n      <app-product-count \n        (onAddToCart)=\"addToCart($event)\" \n        (onMarkAsFavorites)=\"markAsFavorites()\" \n        [isOrderable]=\"isOrderable\"></app-product-count>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".title {\n  color: #343a40;\n  padding-top: 5px;\n  font-weight: 600;\n  font-size: 2em;\n  line-height: 1.5em;\n  font-family: \"Whitney\";\n  display: inline; }\n\n.product-subtitle {\n  display: inline-block;\n  font-size: 1.5em;\n  margin-left: 10px; }\n\n.product-subtitle a {\n    color: black; }\n\n.selling-price {\n  font-size: 16px;\n  margin: 10px 0; }\n\n.vatInfo {\n  color: #343a40;\n  font-size: 13px; }\n\n.product-header-extras .ugc {\n  float: left;\n  margin: 3px 0;\n  color: #495057;\n  font-family: Roboto, sans-serif;\n  font-weight: 400;\n  font-size: 10px;\n  -webkit-font-smoothing: antialiased;\n  font-size: 1em; }\n\n.product-header-extras .ugc a {\n    color: black; }\n\n.product-header-extras .product-header-extras__qna {\n  float: left;\n  color: #495057;\n  font-size: 1em;\n  line-height: 1em;\n  border-left: 1px solid #ccc;\n  margin-top: 5px; }\n\n.product-header-extras .product-header-extras__qna a {\n    color: black; }\n\n.buybox-wrapper {\n  color: #343a40; }\n\n.buybox-wrapper #product-vitals {\n    float: left;\n    width: 60%;\n    padding-right: 15px;\n    margin-bottom: 10px; }\n\n@media only screen and (min-width: 320px) and (max-width: 767px) {\n      .buybox-wrapper #product-vitals {\n        width: 100%; } }\n\n.buybox-wrapper #product-vitals .product-pricing {\n      margin-top: 0;\n      margin-bottom: 0;\n      padding-right: 0;\n      padding-left: 0;\n      list-style-type: none;\n      border-bottom: 1px solid #ccc;\n      padding: 5px 0 15px;\n      overflow: hidden; }\n\n.buybox-wrapper #product-vitals .product-pricing li:first-child,\n      .buybox-wrapper #product-vitals .product-pricing li:last-child {\n        padding-bottom: 0; }\n\n.buybox-wrapper #product-vitals .product-pricing li {\n        margin: 0;\n        padding-bottom: 25px; }\n\n.buybox-wrapper #product-vitals .product-pricing .title {\n        font-size: 1em;\n        line-height: 1;\n        color: #6c757d;\n        font-weight: normal;\n        width: auto;\n        width: 85px;\n        float: left;\n        margin: 0; }\n\n.buybox-wrapper #product-vitals .product-pricing .list-price .price {\n        font-size: 1.4rem;\n        line-height: 1;\n        color: #000;\n        text-decoration: line-through; }\n\n.buybox-wrapper #product-vitals .product-pricing .price {\n        margin-left: 85px;\n        display: block; }\n\n.buybox-wrapper #product-vitals .product-pricing p {\n        margin-top: 0;\n        margin-bottom: 10px; }\n\n.buybox-wrapper #product-vitals .product-pricing .our-price .price {\n        font-size: 1.6em;\n        font-weight: 600;\n        color: #eb7f00 -10;\n        line-height: 12px;\n        padding-top: 11px; }\n\n.buybox-wrapper #product-vitals .product-pricing .our-price .price span {\n          float: left;\n          display: -ms-grid;\n          display: grid; }\n\n.buybox-wrapper #product-vitals .product-pricing .our-price .title {\n        padding-top: 10px; }\n\n.buybox-wrapper #product-vitals .product-pricing .free-shipping {\n        font-size: 12px;\n        line-height: 13px;\n        font-weight: 500;\n        color: #343a40;\n        margin: -8px 0 0 10px;\n        text-transform: uppercase;\n        width: 8rem; }\n\n.buybox-wrapper #product-vitals .product-pricing .you-save .title {\n        margin-bottom: 0; }\n\n.buybox-wrapper #product-vitals .product-pricing .you-save .price {\n        font-size: 1.4rem;\n        margin-bottom: 0;\n        line-height: 1;\n        color: #eb7f00 -10; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion {\n      margin: 5px 0;\n      text-align: left; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion .title {\n        font-size: 1em;\n        font-weight: 600;\n        text-align: right;\n        color: #d76b00;\n        padding: 11px 5px 0 0;\n        line-height: 1;\n        width: auto;\n        width: 85px;\n        float: left; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion .value {\n        margin-left: 85px;\n        display: block; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion .autoship-pricing {\n        font-weight: 600;\n        color: #eb7f00 -10;\n        font-size: 1.714em;\n        margin: 0; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion .autoship-pricing .autoship-percent {\n          font-size: 14px;\n          font-weight: 400;\n          padding-left: 5px; }\n\n.buybox-wrapper #product-vitals #featured-promotions .promotion .title span {\n        color: #ffe364;\n        font-size: 0.8em; }\n\n.buybox-wrapper #buybox {\n    float: left;\n    width: 40%; }\n\n@media only screen and (min-width: 320px) and (max-width: 767px) {\n      .buybox-wrapper #buybox {\n        width: 100%; } }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: ProductPriceInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPriceInfoComponent", function() { return ProductPriceInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_variant_retriver_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../core/services/variant-retriver.service */ "./src/app/core/services/variant-retriver.service.ts");
/* harmony import */ var _core_services_variant_parser_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../core/services/variant-parser.service */ "./src/app/core/services/variant-parser.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductPriceInfoComponent = /** @class */ (function () {
    function ProductPriceInfoComponent(variantParser) {
        this.variantParser = variantParser;
        this.onAddToCart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMarkAsFavorites = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectedVariant = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.currentSelectedOptions = {};
    }
    ProductPriceInfoComponent.prototype.ngOnInit = function () {
        this.images = this.product.master.images;
        this.variantId = this.product.master.id;
        this.customOptionTypesHash = this.variantParser
            .getOptionsToDisplay(this.product.variants, this.product.option_types);
        this.mainOptions = this.makeGlobalOptinTypesHash(this.customOptionTypesHash);
        this.correspondingOptions = this.mainOptions;
        this.isOrderable = this.product.master.is_orderable;
    };
    ProductPriceInfoComponent.prototype.onOptionClick = function (option) {
        var result = new _core_services_variant_retriver_service__WEBPACK_IMPORTED_MODULE_1__["VariantRetriverService"]().getVariant(this.currentSelectedOptions, this.customOptionTypesHash, option, this.product);
        this.createNewCorrespondingOptions(result.newCorrespondingOptions, option.value.optionValue.option_type_name);
        this.currentSelectedOptions = result.newSelectedoptions;
        var newVariant = result.variant;
        this.variantId = newVariant.id;
        this.description = newVariant.description;
        this.images = newVariant.images;
        this.product.display_price = result.variant.display_price;
        this.getSelectedVariant(result.variant);
        this.isOrderable = newVariant.is_orderable;
        this.product.master.cost_price = newVariant.cost_price;
        this.product.price = newVariant.price;
    };
    ProductPriceInfoComponent.prototype.makeGlobalOptinTypesHash = function (customOptionTypes) {
        var temp = {};
        for (var key in customOptionTypes) {
            if (customOptionTypes.hasOwnProperty(key)) {
                temp[key] = Object.keys(customOptionTypes[key]);
            }
        }
        return temp;
    };
    ProductPriceInfoComponent.prototype.createNewCorrespondingOptions = function (newOptions, optionKey) {
        for (var key in this.correspondingOptions) {
            if (this.correspondingOptions.hasOwnProperty(key) && key !== optionKey) {
                this.correspondingOptions[key] = newOptions[key];
            }
        }
    };
    ProductPriceInfoComponent.prototype.addToCart = function (event) {
        this.onAddToCart.emit(event);
    };
    ProductPriceInfoComponent.prototype.markAsFavorites = function () {
        this.onMarkAsFavorites.emit();
    };
    ProductPriceInfoComponent.prototype.getSelectedVariant = function (variant) {
        this.selectedVariant.emit(variant);
    };
    Object.defineProperty(ProductPriceInfoComponent.prototype, "discount", {
        get: function () {
            return this.product.master.cost_price - this.product.price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProductPriceInfoComponent.prototype, "discountPercent", {
        get: function () {
            return Math.ceil(this.discount / this.product.master.cost_price * 100) + "%";
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductPriceInfoComponent.prototype, "product", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductPriceInfoComponent.prototype, "onAddToCart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductPriceInfoComponent.prototype, "onMarkAsFavorites", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductPriceInfoComponent.prototype, "selectedVariant", void 0);
    ProductPriceInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-price-info',
            template: __webpack_require__(/*! ./product-price-info.component.html */ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.html"),
            styles: [__webpack_require__(/*! ./product-price-info.component.scss */ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_services_variant_parser_service__WEBPACK_IMPORTED_MODULE_2__["VariantParserService"]])
    ], ProductPriceInfoComponent);
    return ProductPriceInfoComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-review/product-review.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-review/product-review.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cust-reviews\">\n  <h1>Customer Reviews</h1>\n  <div class=\"row\">\n    <div class=\"col-12 col-md-6 pl-4\" *ngIf=\"reviewList\">\n      <div class=\"avg-review\">\n        <div class=\"row\">\n          <div class=\"review col-12 col-md-5 col-lg-5 p-0\">\n            <ngx-input-star-rating disabled=\"true\" value=\"{{product.avg_rating }}\" required></ngx-input-star-rating>\n            {{product.reviews_count }} Reviews\n          </div>\n          <div class=\"out-review col-12 col-md-5 col-lg-5 \">\n            {{product.avg_rating }} out of 5 star\n          </div>\n        </div>\n      </div>\n      <div class=\"write-review\">\n      </div>\n      <div class=\"row rataingpercentage\" *ngFor=\"let rating of reviewList.rating_summery; let i = index\">\n        <div class=\"col-3 col-md-2 col-lg-2 pl-2 pr-0\">\n          {{rating.rating}} Stars\n        </div>\n        <div class=\"col-7 col-md-8 col-lg-8 pr-0 pl-0\">\n          <div class=\"mb-2 progress-container\">\n            <progressbar [value]=\"rating.percentage\" type=\"warning\"></progressbar>\n          </div>\n        </div>\n        <div class=\"col-2  col-md-2 col-lg-2 pl-2\">\n          <b>{{rating.percentage | number: '1.0-0'}}% </b>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-12 col-md-3 offset-md-2 mt-5 pt-5\">\n      <div class=\"row\">\n        <button (click)=\"showReviewForm()\" class=\"btn themebtnprimary col-10 m-auto\">Write Review</button>\n      </div>\n    </div>\n  </div>\n  <div class=\"review-display\">\n    <div class=\"row\">\n      <div class=\"col-md-8\">\n        <div class=\"review-sort-filter mt-5 mb-4\">\n          <div class=\"row\">\n            <div class=\"float-left col-md-4 pt-2\">Showing {{product.reviews_count }} Reviews</div>\n          </div>\n        </div>\n        <div *ngIf=\"reviewList\">\n          <div *ngFor=\"let item of reviewList.reviews\" class=\"rating-inner\">\n            <div class=\"rating-head\">\n              <ngx-input-star-rating disabled=\"true\" value=\"{{item.rating }}\" required></ngx-input-star-rating>\n              <span class=\"pl-3\">{{item.title}}</span>\n            </div>\n            <div class=\"rating-by\">\n              By {{item.name}} on {{item.created_at | date}}\n            </div>\n            <p class=\"description\">\n              {{item.review}}\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-review/product-review.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-review/product-review.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padding-top {\n  padding-top: 50px; }\n\nh1 {\n  font-size: 1.7em;\n  color: #212529; }\n\n.pdp-add-to-bag {\n  width: 60%;\n  text-align: center;\n  padding: 10px 0;\n  background-color: #e9ecef;\n  border: 1px solid #e9ecef;\n  color: #fff; }\n\n.pdp-button {\n  border-radius: 3px;\n  outline: 0;\n  margin-top: 10px;\n  padding: 10px 30px;\n  font-size: 15px;\n  min-height: 22px;\n  margin-bottom: 10px; }\n\n.fav-icon-sontainer {\n  position: relative; }\n\n.fav-icon-sontainer .favorite {\n    position: absolute;\n    right: 8px;\n    top: -24px;\n    background-color: #e9ecef;\n    color: #fff;\n    padding: 10px 13px;\n    border-radius: 50%;\n    box-shadow: 0px 0px 8px #212529;\n    z-index: 20; }\n\n.slider {\n  margin-top: 1vw;\n  padding-top: 1vw; }\n\n.slider h2 {\n    font-size: 1.7em;\n    color: #212529; }\n\n@media screen and (min-width: 310px) and (max-width: 768px) {\n  .fav-icon-sontainer .favorite {\n    top: 50px; } }\n\n.cust-reviews .avg-review {\n  padding: 1rem 0 3rem; }\n\n.cust-reviews .avg-review .out-review {\n    border-left: 1px solid #ccc; }\n\n@media screen and (min-width: 310px) and (max-width: 768px) {\n      .cust-reviews .avg-review .out-review {\n        border-left: 0 solid #ccc;\n        margin-top: 10px;\n        padding: 10px; } }\n\n.cust-reviews .rataingpercentage {\n  font-size: 1.2em;\n  margin-bottom: 10px; }\n\n.cust-reviews .rataingpercentage .progress-container .progress {\n    display: flex;\n    height: 1.6rem; }\n\n.cust-reviews .review-display .rating-inner {\n  padding-bottom: 1rem;\n  border-bottom: 1px solid #ccc;\n  margin-bottom: 1rem; }\n\n.cust-reviews .review-display .rating-inner .rating-head {\n    font-weight: 700;\n    color: #212529;\n    position: relative;\n    left: -5px;\n    text-transform: capitalize; }\n\n.cust-reviews .review-display .rating-by {\n  font-size: 1em;\n  color: #7b8289; }\n\n.cust-reviews .review-display .description {\n  padding-top: 1rem;\n  text-align: justify;\n  color: #212529; }\n\n.cust-reviews .review-sort-filter {\n  border-top: 1px solid #ccc;\n  border-top: 1px solid #ccc;\n  background-color: #e9ecef;\n  padding: 1rem; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-review/product-review.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-review/product-review.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ProductReviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductReviewComponent", function() { return ProductReviewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../auth/reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProductReviewComponent = /** @class */ (function () {
    function ProductReviewComponent(router, store, toastrService) {
        var _this = this;
        this.router = router;
        this.store = store;
        this.toastrService = toastrService;
        this.store.select(_auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__["getAuthStatus"]).subscribe(function (auth) {
            _this.isAuthenticated = auth;
        });
    }
    ProductReviewComponent.prototype.ngOnInit = function () {
        this.productID = this.product.id;
    };
    ProductReviewComponent.prototype.showReviewForm = function () {
        if (this.isAuthenticated) {
            this.router.navigate([this.product.slug, 'write_review'], { queryParams: { 'prodId': this.productID } });
        }
        else {
            this.toastrService.info('Please Login to write review.', 'Login');
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductReviewComponent.prototype, "reviewList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_1__["Product"])
    ], ProductReviewComponent.prototype, "product", void 0);
    ProductReviewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-review',
            template: __webpack_require__(/*! ./product-review.component.html */ "./src/app/product/components/product-detail-page/product-review/product-review.component.html"),
            styles: [__webpack_require__(/*! ./product-review.component.scss */ "./src/app/product/components/product-detail-page/product-review/product-review.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], ProductReviewComponent);
    return ProductReviewComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-variants/product-variants.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"col-9\">\n  <div *ngFor=\"let variant of (customOptionTypesHash | keys)\">\n    <div class=\"size-buttons-size-header\">\n      <h6>{{ variant.key }}\n        <span *ngIf=\"selectedItem\">{{selectedItem.key }}</span>\n      </h6>\n    </div>\n    <ul>\n      <li class=\"option-buttons\" [ngClass]=\"[selectedItem == options ? 'active':'',isDisabled(correspondingOptions[variant.key], options.value['optionValue'].name) ? 'disabled':'']\"\n        (click)=\"listClick($event, options)\" *ngFor=\"let options of (variant.value | keys)\" [class.selected]=\"options.key === currentSelectedOptions[variant.key]\">\n        <p>\n          {{ options.value[\"optionValue\"].presentation }}\n          <span class=\"strike\"></span>\n        </p>\n      </li>\n    </ul>\n  </div>\n</div> -->\n<div *ngFor=\"let variant of (customOptionTypesHash | keys)\">\n  <div class=\"size-buttons-size-header\">\n    <h6>{{ variant.key }}</h6>\n  </div>\n  <button class=\"option-buttons\" *ngFor=\"let options of (variant.value | keys)\" (click)=\"onOptionClick(options)\" [class.selected]=\"options.key === currentSelectedOptions[variant.key]\"\n    [ngClass]=\"{'disabled': isDisabled(correspondingOptions[variant.key], options.value['optionValue'].name)}\">\n    <p>\n      {{ options.value[\"optionValue\"].presentation }}\n      <span class=\"strike\"></span>\n    </p>\n  </button>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-variants/product-variants.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".size-buttons-size-header {\n  overflow: auto;\n  color: #212529; }\n  .size-buttons-size-header span {\n    font-weight: bold;\n    font-size: 1em; }\n  .option-buttons {\n  display: inline-block;\n  background-color: #fff;\n  border: 1px solid #212529;\n  border-radius: 4px;\n  padding: .5rem 1rem;\n  text-align: center;\n  margin-right: 10px;\n  margin-bottom: 15px;\n  cursor: pointer;\n  color: #212529; }\n  .option-buttons.disabled {\n    position: relative;\n    color: #212529;\n    border: 1px solid #ccc;\n    cursor: #dc3545;\n    pointer-events: none; }\n  .option-buttons.disabled .strike {\n      position: absolute;\n      top: 50%;\n      left: 0;\n      width: 100%;\n      height: 1px;\n      background-color: #ccc; }\n  .option-buttons p {\n    margin: 0; }\n  .option-buttons:hover, .option-buttons.active {\n    color: #fff;\n    font-weight: 500;\n    background-color: #ffe364;\n    border: 1px solid #ffe364; }\n  .selected {\n  color: #fff;\n  font-weight: 500;\n  background-color: #212529; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/product-variants/product-variants.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ProductVariantsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductVariantsComponent", function() { return ProductVariantsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductVariantsComponent = /** @class */ (function () {
    function ProductVariantsComponent() {
        this.currentSelectedOptions = {};
        this.onOptionClickEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ProductVariantsComponent.prototype.ngOnInit = function () {
    };
    ProductVariantsComponent.prototype.onOptionClick = function (option) {
        this.onOptionClickEvent.emit(option);
    };
    ProductVariantsComponent.prototype.isDisabled = function (arrayTocheck, value) {
        return (arrayTocheck.indexOf(value) === -1);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductVariantsComponent.prototype, "customOptionTypesHash", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductVariantsComponent.prototype, "currentSelectedOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductVariantsComponent.prototype, "mainOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductVariantsComponent.prototype, "correspondingOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductVariantsComponent.prototype, "onOptionClickEvent", void 0);
    ProductVariantsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-variants',
            template: __webpack_require__(/*! ./product-variants.component.html */ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.html"),
            styles: [__webpack_require__(/*! ./product-variants.component.scss */ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProductVariantsComponent);
    return ProductVariantsComponent;
}());



/***/ }),

/***/ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container pt-5 pb-5\" *ngIf=\"product$ | async; let product\">\n  <h3 *ngIf=\"!showThanks\">Write Review</h3>\n  <h3 *ngIf=\"showThanks\">Review Submitted!</h3>\n  <div class=\"write-review\">\n    <div class=\"row\">\n      <div class=\"col-2 pl-4\">\n        <img [src]=\"getProductImageUrl(product.master.images[0].product_url)\" class=\"img-fluid\">\n      </div>\n      <div class=\"col-7\">\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-12\" *ngIf=\"!showThanks\">\n              <h2>{{product.name}}</h2>\n              <form [formGroup]=\"reviewForm\" (ngSubmit)=\"onSubmit(product.id)\" data-toggle=\"validator\">\n                <div class=\"form-group\">\n                  <label for=\"exampleInputEmail1\">Rate this product</label>\n                  <div>\n                    <ngx-input-star-rating formControlName=\"rating\" required></ngx-input-star-rating>\n                  </div>\n                </div>\n                <div class=\"form-group\">\n                  <label for=\"exampleInputEmail1\">Title</label>\n                  <input type=\"text\" class=\"form-control\" aria-describedby=\"emailHelp\" placeholder=\"Headline for your review\" formControlName=\"title\"\n                    required>\n                </div>\n                <div class=\"form-group\">\n                  <label for=\"exampleFormControlTextarea1\">Content</label>\n                  <textarea class=\"form-control\" rows=\"3\" placeholder=\"Your review\" formControlName=\"review\" required></textarea>\n                </div>\n                <button type=\"submit\" class=\"btn themebtnprimary col-5\">Submit Review</button>\n              </form>\n            </div>\n            <div class=\"col-12\" *ngIf=\"showThanks\">\n              <h3>Thanks for your review!</h3>\n              <div class=\"float-left\">\n                <i class=\"fa fa-check-circle fa-2x text-success\"></i>\n              </div>\n              <div class=\"float-right\">\n                <p class=\"w-75 text-success\">Your review has been submitted.Please note that your review may take up to 48 hours to appear.</p>\n              </div>\n              <button type=\"normal\" class=\"btn themebtnprimary col-5 mt-4\" (click)=\"goToProduct(product.id)\">Continue Shopping</button>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-3\" *ngIf=\"!showThanks\">\n        <div class=\"guideline\">\n          <h4>Writing Guidelines</h4>\n          <ul>\n            <li>\n              Please refrain from mentioning competitors or the specific price you paid for the product\n            </li>\n            <li>\n              Do not include any personally identifiable information, such as full names\n            </li>\n          </ul>\n          <h4>Media Guidelines</h4>\n          <ul>\n            <li>\n              Confirm you hold the copyright for the media\n            </li>\n          </ul>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.scss":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.scss ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".write-review {\n  margin: 0 auto;\n  padding: 2em 0;\n  border-radius: 4px;\n  border: 1px solid #ccc; }\n  .write-review label {\n    font-size: 1em;\n    color: #212529; }\n  .write-review .guideline {\n    background-color: #f8f9fa;\n    border-radius: 4px;\n    padding: 1em;\n    margin: 0 15px; }\n  .write-review h4 {\n    color: #212529;\n    font-size: 1.2em; }\n  .write-review h2 {\n    color: #212529;\n    font-size: 1.7em; }\n  .write-review ul {\n    list-style: inside;\n    text-indent: -22px;\n    padding: 1rem 0rem 1rem 1.4rem;\n    line-height: 1.5em; }\n  .write-review ul li {\n      padding-bottom: .5rem;\n      font-size: 1em;\n      color: #212529; }\n"

/***/ }),

/***/ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.ts ***!
  \***************************************************************************************************************/
/*! exports provided: WriteProductReviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WriteProductReviewComponent", function() { return WriteProductReviewComponent; });
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../core/services/product.service */ "./src/app/core/services/product.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../auth/reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var WriteProductReviewComponent = /** @class */ (function () {
    function WriteProductReviewComponent(fb, productService, activeRoute, toastrService, router, store) {
        var _this = this;
        this.fb = fb;
        this.productService = productService;
        this.activeRoute = activeRoute;
        this.toastrService = toastrService;
        this.router = router;
        this.store = store;
        this.showThanks = false;
        this.submitReview = true;
        this.store.select(_auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_7__["getAuthStatus"]).subscribe(function (auth) {
            _this.isAuthenticated = auth;
        });
    }
    WriteProductReviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm();
        this.product$ = this.activeRoute.queryParams
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (params) {
            return _this.productService.getProduct(params.prodId);
        }));
    };
    WriteProductReviewComponent.prototype.initForm = function () {
        var rating = '';
        var name = '';
        var title = '';
        var review = '';
        if (this.isAuthenticated) {
            this.reviewForm = this.fb.group({
                rating: [rating, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                name: [JSON.parse(localStorage.getItem('user')).email],
                title: [title, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                review: [review, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            });
        }
        else {
            this.router.navigate(['auth', 'login']);
        }
    };
    WriteProductReviewComponent.prototype.getProductImageUrl = function (url) {
        return url;
    };
    WriteProductReviewComponent.prototype.parse = function (formData) {
        return {
            review: {
                rating: formData.rating.toString(),
                name: formData.name,
                title: formData.title,
                review: formData.review,
                user_id: JSON.parse(localStorage.getItem('user')).id
            }
        };
    };
    WriteProductReviewComponent.prototype.onSubmit = function (prodId) {
        var _this = this;
        if (this.reviewForm.valid) {
            var values = this.reviewForm.value;
            var params = this.parse(values);
            this.productService.submitReview(prodId, params)
                .subscribe(function (res) {
                _this.result = res;
                if (_this.result === 'info') {
                    _this.goToProduct(prodId);
                }
                else if (_this.result === 'success') {
                    _this.showThanks = true;
                    _this.submitReview = false;
                }
                else {
                    _this.goToProduct(prodId);
                }
            });
        }
        else {
            this.toastrService.error('All fields are rquired', 'Invalid!');
        }
    };
    WriteProductReviewComponent.prototype.goToProduct = function (prodId) {
        this.router.navigate([prodId]);
    };
    WriteProductReviewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-write-product-review',
            template: __webpack_require__(/*! ./write-product-review.component.html */ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.html"),
            styles: [__webpack_require__(/*! ./write-product-review.component.scss */ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.scss")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _core_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_0__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
    ], WriteProductReviewComponent);
    return WriteProductReviewComponent;
}());



/***/ }),

/***/ "./src/app/product/product-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/product/product-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ProductRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductRoutingModule", function() { return ProductRoutingModule; });
/* harmony import */ var _components_product_detail_page_write_product_review_write_product_review_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/product-detail-page/write-product-review/write-product-review.component */ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.ts");
/* harmony import */ var _components_product_detail_page_product_detail_page_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/product-detail-page/product-detail-page.component */ "./src/app/product/components/product-detail-page/product-detail-page.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: ':id', component: _components_product_detail_page_product_detail_page_component__WEBPACK_IMPORTED_MODULE_1__["ProductDetailPageComponent"] },
    { path: ':id/write_review', component: _components_product_detail_page_write_product_review_write_product_review_component__WEBPACK_IMPORTED_MODULE_0__["WriteProductReviewComponent"] }
];
var ProductRoutingModule = /** @class */ (function () {
    function ProductRoutingModule() {
    }
    ProductRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
        })
    ], ProductRoutingModule);
    return ProductRoutingModule;
}());



/***/ }),

/***/ "./src/app/product/product.component.css":
/*!***********************************************!*\
  !*** ./src/app/product/product.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/product/product.component.html":
/*!************************************************!*\
  !*** ./src/app/product/product.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  product works!\n</p>\n"

/***/ }),

/***/ "./src/app/product/product.component.ts":
/*!**********************************************!*\
  !*** ./src/app/product/product.component.ts ***!
  \**********************************************/
/*! exports provided: ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductComponent = /** @class */ (function () {
    function ProductComponent() {
    }
    ProductComponent.prototype.ngOnInit = function () {
    };
    ProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product',
            template: __webpack_require__(/*! ./product.component.html */ "./src/app/product/product.component.html"),
            styles: [__webpack_require__(/*! ./product.component.css */ "./src/app/product/product.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "./src/app/product/product.module.ts":
/*!*******************************************!*\
  !*** ./src/app/product/product.module.ts ***!
  \*******************************************/
/*! exports provided: ProductModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductModule", function() { return ProductModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/index.js");
/* harmony import */ var _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-lite/input-star-rating */ "./node_modules/@ngx-lite/input-star-rating/fesm5/ngx-lite-input-star-rating.js");
/* harmony import */ var _product_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-routing.module */ "./src/app/product/product-routing.module.ts");
/* harmony import */ var _components_product_detail_page_product_detail_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/product-detail-page/product-detail-page.component */ "./src/app/product/components/product-detail-page/product-detail-page.component.ts");
/* harmony import */ var _components_product_detail_page_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/product-detail-page/product-details/product-details.component */ "./src/app/product/components/product-detail-page/product-details/product-details.component.ts");
/* harmony import */ var _components_product_detail_page_product_description_product_description_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/product-detail-page/product-description/product-description.component */ "./src/app/product/components/product-detail-page/product-description/product-description.component.ts");
/* harmony import */ var _components_product_detail_page_product_images_product_images_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/product-detail-page/product-images/product-images.component */ "./src/app/product/components/product-detail-page/product-images/product-images.component.ts");
/* harmony import */ var _components_product_detail_page_product_price_info_product_price_info_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/product-detail-page/product-price-info/product-price-info.component */ "./src/app/product/components/product-detail-page/product-price-info/product-price-info.component.ts");
/* harmony import */ var _components_product_detail_page_product_price_info_product_count_product_count_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/product-detail-page/product-price-info/product-count/product-count.component */ "./src/app/product/components/product-detail-page/product-price-info/product-count/product-count.component.ts");
/* harmony import */ var _components_product_detail_page_product_variants_product_variants_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/product-detail-page/product-variants/product-variants.component */ "./src/app/product/components/product-detail-page/product-variants/product-variants.component.ts");
/* harmony import */ var _product_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./product.component */ "./src/app/product/product.component.ts");
/* harmony import */ var _components_product_detail_page_write_product_review_write_product_review_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/product-detail-page/write-product-review/write-product-review.component */ "./src/app/product/components/product-detail-page/write-product-review/write-product-review.component.ts");
/* harmony import */ var _components_product_detail_page_product_review_product_review_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/product-detail-page/product-review/product-review.component */ "./src/app/product/components/product-detail-page/product-review/product-review.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// Components










var ProductModule = /** @class */ (function () {
    function ProductModule() {
    }
    ProductModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                // components
                _components_product_detail_page_product_detail_page_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailPageComponent"],
                _product_component__WEBPACK_IMPORTED_MODULE_12__["ProductComponent"],
                _components_product_detail_page_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsComponent"],
                _components_product_detail_page_product_images_product_images_component__WEBPACK_IMPORTED_MODULE_8__["ProductImagesComponent"],
                _components_product_detail_page_product_price_info_product_price_info_component__WEBPACK_IMPORTED_MODULE_9__["ProductPriceInfoComponent"],
                _components_product_detail_page_product_description_product_description_component__WEBPACK_IMPORTED_MODULE_7__["ProductDescriptionComponent"],
                _components_product_detail_page_product_variants_product_variants_component__WEBPACK_IMPORTED_MODULE_11__["ProductVariantsComponent"],
                _components_product_detail_page_product_price_info_product_count_product_count_component__WEBPACK_IMPORTED_MODULE_10__["ProductCountComponent"],
                _components_product_detail_page_write_product_review_write_product_review_component__WEBPACK_IMPORTED_MODULE_13__["WriteProductReviewComponent"],
                _components_product_detail_page_product_review_product_review_component__WEBPACK_IMPORTED_MODULE_14__["ProductReviewComponent"]
                // pipes
            ],
            exports: [
                // components
                _components_product_detail_page_product_detail_page_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailPageComponent"],
                _components_product_detail_page_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsComponent"],
                _components_product_detail_page_product_images_product_images_component__WEBPACK_IMPORTED_MODULE_8__["ProductImagesComponent"],
                _components_product_detail_page_product_price_info_product_price_info_component__WEBPACK_IMPORTED_MODULE_9__["ProductPriceInfoComponent"],
                _components_product_detail_page_product_description_product_description_component__WEBPACK_IMPORTED_MODULE_7__["ProductDescriptionComponent"],
                _components_product_detail_page_product_variants_product_variants_component__WEBPACK_IMPORTED_MODULE_11__["ProductVariantsComponent"],
                _components_product_detail_page_product_price_info_product_count_product_count_component__WEBPACK_IMPORTED_MODULE_10__["ProductCountComponent"]
            ],
            imports: [
                _shared_index__WEBPACK_IMPORTED_MODULE_1__["SharedModule"],
                _product_routing_module__WEBPACK_IMPORTED_MODULE_4__["ProductRoutingModule"],
                _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_3__["NgxInputStarRatingModule"],
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_2__["ProgressbarModule"].forRoot()
            ],
            providers: []
        })
    ], ProductModule);
    return ProductModule;
}());



/***/ }),

/***/ "./src/assets/thumbnail.jpg":
/*!**********************************!*\
  !*** ./src/assets/thumbnail.jpg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "thumbnail.jpg";

/***/ }),

/***/ "./src/assets/thumbnail2.jpg":
/*!***********************************!*\
  !*** ./src/assets/thumbnail2.jpg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "thumbnail2.jpg";

/***/ })

}]);
//# sourceMappingURL=product-product-module.js.map