(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"auth-auth-module"
	],
	"./checkout/checkout.module": [
		"./src/app/checkout/checkout.module.ts",
		"checkout-checkout-module"
	],
	"./home/index": [
		"./src/app/home/index.ts"
	],
	"./landing/landing.module": [
		"./src/app/landing/landing.module.ts",
		"landing-landing-module"
	],
	"./product/product.module": [
		"./src/app/product/product.module.ts",
		"product-product-module"
	],
	"./user/index": [
		"./src/app/user/index.ts",
		"user-index"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"selected-theme\">\n  <section>\n    <ng-progress [color]=\"'red'\" [spinner]=\"false\"></ng-progress>\n\n    <app-header *ngIf=\"currentUrl\"></app-header>\n    \n    <main class=\"main-body\">\n      <router-outlet></router-outlet>\n    </main>   \n    <app-footer *ngIf=\"currentUrl\"></app-footer>\n    \n  </section>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  min-height: 100vh;\n  display: block; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n  .issearchopen :host /deep/ .main-body {\n    margin-top: 48px; } }\n\n:host /deep/ .ngx-input-star-rating__btn {\n  background-color: transparent;\n  border: 0;\n  cursor: pointer;\n  position: relative;\n  width: 26px;\n  height: 8px; }\n\n:host /deep/ .ngx-input-star-rating__btn svg {\n  position: absolute;\n  top: -12px;\n  left: 5px; }\n\n:host /deep/ .ngx-input-star-rating__btn {\n  background-color: transparent;\n  border: 0;\n  cursor: pointer;\n  position: relative;\n  width: 26px;\n  height: 8px; }\n\n:host /deep/ .ngx-input-star-rating__btn svg {\n  position: absolute;\n  top: -12px;\n  left: 5px; }\n\n:host /deep/ ngx-input-star-rating {\n  display: inline-block;\n  --color-star-default: #ddd;\n  --color-star-highlight: #fe9700; }\n\n:host /deep/ ngx-input-star-rating {\n  display: inline-block;\n  --color-star-default: #ddd;\n  --color-star-highlight: #fe9700; }\n\n:host /deep/ .themebtnprimary {\n  background: #ffa728;\n  background-image: linear-gradient(to bottom, #ffe364 0%, #ffbb3c 45%);\n  background-repeat: repeat-x;\n  color: #fff;\n  border: 1px solid #ff9314;\n  border-radius: 0.4rem;\n  padding: 0.5rem;\n  font-weight: 300 !important;\n  box-shadow: 0px 2px 4.6px #ffcf50;\n  width: 100%;\n  color: #fff;\n  font-family: \"Roboto Regular\";\n  font-size: 1.4em;\n  margin: 10px auto;\n  text-transform: capitalize; }\n\n:host /deep/ .themebtnprimary:focus {\n    background-image: linear-gradient(to bottom, #ffe364 0%, #ffbb3c 45%);\n    background-repeat: repeat-x; }\n\n:host /deep/ .themebtnprimarysade {\n  background-image: linear-gradient(to bottom, #ffe364 0%, #ffa728 95%);\n  background-repeat: repeat-x; }\n\n:host /deep/ .themebtndisable {\n  background-image: linear-gradient(to bottom, #e9ecef 0%, #6c757d 95%);\n  background-repeat: repeat-x;\n  box-shadow: 0px 2px 4.6px #586169;\n  border: 1px solid #1c252d; }\n\n.btn:not(:disabled):not(.disabled):active,\n.btn:not(:disabled):not(.disabled).active {\n  background-image: linear-gradient(to bottom, #ffe364 0%, #ffb132 80%);\n  background-repeat: repeat-x; }\n\n.btn:not(:disabled):not(.disabled):active,\n.btn:not(:disabled):not(.disabled).active {\n  background-image: auto !important; }\n\n:host /deep/ .page-item.active .page-link {\n  z-index: 1;\n  color: #fff;\n  background-color: #ff9314;\n  border-color: #ff9314; }\n\n:host /deep/ .page-link {\n  color: #212529; }\n\n:host /deep/ .page-link:hover {\n    background-color: #eb7f00;\n    border-color: #eb7f00; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth/reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_services_checkout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/services/checkout.service */ "./src/app/core/services/checkout.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppComponent = /** @class */ (function () {
    function AppComponent(router, checkoutService, store) {
        var _this = this;
        this.router = router;
        this.checkoutService = checkoutService;
        this.store = store;
        this.checkoutUrls = ['/checkout/cart', '/checkout/address', '/checkout/payment'];
        router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["filter"])(function (e) { return e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"]; }))
            .subscribe(function (e) {
            _this.currentUrl = e.url;
            _this.findCurrentStep(_this.currentUrl);
            window.scrollTo(0, 0);
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.store.select(_auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getAuthStatus"]).subscribe(function () {
            _this.orderSub$ = _this.checkoutService.fetchCurrentOrder().subscribe();
        });
    };
    AppComponent.prototype.isCheckoutRoute = function () {
        if (!this.currentUrl) {
            return false;
        }
        var index = this.checkoutUrls.indexOf(this.currentUrl);
        if (index >= 0) {
            return true;
        }
        else {
            return false;
        }
    };
    AppComponent.prototype.findCurrentStep = function (currentRoute) {
        var currRouteFragments = currentRoute.split('/');
        var length = currRouteFragments.length;
        this.currentStep = currentRoute.split('/')[length - 1];
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.orderSub$.unsubscribe();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _core_services_checkout_service__WEBPACK_IMPORTED_MODULE_3__["CheckoutService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _app_preloading_strategy__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app_preloading_strategy */ "./src/app/app_preloading_strategy.ts");
/* harmony import */ var _oauth_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./oauth_config */ "./src/app/oauth_config.ts");
/* harmony import */ var ng2_ui_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-ui-auth */ "./node_modules/ng2-ui-auth/fesm5/ng2-ui-auth.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngrx/store-devtools */ "./node_modules/@ngrx/store-devtools/fesm5/store-devtools.js");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/service-worker */ "./node_modules/@angular/service-worker/fesm5/service-worker.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _home_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./home/index */ "./src/app/home/index.ts");
/* harmony import */ var _layout_index__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./layout/index */ "./src/app/layout/index.ts");
/* harmony import */ var _core_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./core/index */ "./src/app/core/index.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _app_reducers__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./app.reducers */ "./src/app/app.reducers.ts");
/* harmony import */ var _layout_checkout_header_checkout_header_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./layout/checkout-header/checkout-header.component */ "./src/app/layout/checkout-header/checkout-header.component.ts");
/* harmony import */ var _layout_checkout_footer_checkout_footer_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./layout/checkout-footer/checkout-footer.component */ "./src/app/layout/checkout-footer/checkout-footer.component.ts");
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-drag-scroll */ "./node_modules/ngx-drag-scroll/index.js");
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// Components

// Routes

// Modules












var config = { url: 'http://localhost:8080/', options: {} };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"],
                _layout_checkout_header_checkout_header_component__WEBPACK_IMPORTED_MODULE_20__["CheckoutHeaderComponent"],
                _layout_checkout_footer_checkout_footer_component__WEBPACK_IMPORTED_MODULE_21__["CheckoutFooterComponent"]
            ],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(_app_routes__WEBPACK_IMPORTED_MODULE_13__["routes"], { preloadingStrategy: _app_preloading_strategy__WEBPACK_IMPORTED_MODULE_0__["AppPreloadingStrategy"] }),
                _ngrx_store__WEBPACK_IMPORTED_MODULE_18__["StoreModule"].forRoot(_app_reducers__WEBPACK_IMPORTED_MODULE_19__["reducers"], { metaReducers: _app_reducers__WEBPACK_IMPORTED_MODULE_19__["metaReducers"] }),
                /**
                 * Store devtools instrument the store retaining past versions of state
                 * and recalculating new states. This enables powerful time-travel
                 * debugging.
                 *
                 * To use the debugger, install the Redux Devtools extension for either
                 * Chrome or Firefox
                 *
                 * See: https://github.com/zalmoxisus/redux-devtools-extension
                 */
                !_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].production ? _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_9__["StoreDevtoolsModule"].instrument() : [],
                /**
                 * EffectsModule.forRoot() is imported once in the root module and
                 * sets up the effects class to be initialized immediately when the
                 * application starts.
                 *
                 * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
                 */
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["EffectsModule"].forRoot([]),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["BrowserAnimationsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_7__["HttpModule"],
                _home_index__WEBPACK_IMPORTED_MODULE_15__["HomeModule"],
                _layout_index__WEBPACK_IMPORTED_MODULE_16__["LayoutModule"],
                ng2_ui_auth__WEBPACK_IMPORTED_MODULE_2__["Ng2UiAuthModule"].forRoot(_oauth_config__WEBPACK_IMPORTED_MODULE_1__["myAuthConfig"]),
                ngx_socket_io__WEBPACK_IMPORTED_MODULE_25__["SocketIoModule"].forRoot(config),
                ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_22__["DragScrollModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_24__["ToastrModule"].forRoot({
                    timeOut: 1500,
                    positionClass: 'toast-top-right',
                    preventDuplicates: true,
                    progressBar: true,
                    progressAnimation: 'increasing'
                }),
                _core_index__WEBPACK_IMPORTED_MODULE_17__["CoreModule"],
                _shared_index__WEBPACK_IMPORTED_MODULE_14__["SharedModule"],
                _angular_service_worker__WEBPACK_IMPORTED_MODULE_10__["ServiceWorkerModule"].register('/ngsw-worker.js', { enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].production })
            ],
            providers: [_app_preloading_strategy__WEBPACK_IMPORTED_MODULE_0__["AppPreloadingStrategy"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.reducers.ts":
/*!*********************************!*\
  !*** ./src/app/app.reducers.ts ***!
  \*********************************/
/*! exports provided: reducers, logger, metaReducers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducers", function() { return reducers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logger", function() { return logger; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "metaReducers", function() { return metaReducers; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _product_reducers_product_reducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product/reducers/product-reducer */ "./src/app/product/reducers/product-reducer.ts");
/* harmony import */ var _user_reducers_user_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user/reducers/user.reducer */ "./src/app/user/reducers/user.reducer.ts");
/* harmony import */ var _checkout_reducers_checkout_reducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./checkout/reducers/checkout.reducer */ "./src/app/checkout/reducers/checkout.reducer.ts");
/* harmony import */ var _auth_reducers_auth_reducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/reducers/auth.reducer */ "./src/app/auth/reducers/auth.reducer.ts");





var reducers = {
    products: _product_reducers_product_reducer__WEBPACK_IMPORTED_MODULE_1__["reducer"],
    auth: _auth_reducers_auth_reducer__WEBPACK_IMPORTED_MODULE_4__["reducer"],
    checkout: _checkout_reducers_checkout_reducer__WEBPACK_IMPORTED_MODULE_3__["reducer"],
    users: _user_reducers_user_reducer__WEBPACK_IMPORTED_MODULE_2__["reducer"]
};
// console.log all actions
function logger(reducer) {
    return function (state, action) {
        // console.log('state', state);
        // console.log('action', action);
        return reducer(state, action);
    };
}
/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
var metaReducers = !_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].production
    ? [logger]
    : [];


/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var _core_guards_auth_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/guards/auth.guard */ "./src/app/core/guards/auth.guard.ts");

var routes = [
    {
        path: '',
        loadChildren: './landing/landing.module#LandingModule',
        data: { preload: true, delay: true },
    },
    {
        path: 'search',
        loadChildren: './home/index#HomeModule',
        data: { preload: false, delay: false },
    },
    {
        path: 'checkout',
        loadChildren: './checkout/checkout.module#CheckoutModule',
        data: { preload: true, delay: true },
    },
    {
        path: 'user',
        loadChildren: './user/index#UserModule',
        canActivate: [_core_guards_auth_guard__WEBPACK_IMPORTED_MODULE_0__["CanActivateViaAuthGuard"]],
        data: { preload: false, delay: false },
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule',
        data: { preload: false, delay: false },
    },
    {
        path: '',
        loadChildren: './product/product.module#ProductModule',
        data: { preload: false, delay: false },
    }
];


/***/ }),

/***/ "./src/app/app_preloading_strategy.ts":
/*!********************************************!*\
  !*** ./src/app/app_preloading_strategy.ts ***!
  \********************************************/
/*! exports provided: AppPreloadingStrategy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPreloadingStrategy", function() { return AppPreloadingStrategy; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");


var AppPreloadingStrategy = /** @class */ (function () {
    function AppPreloadingStrategy() {
    }
    AppPreloadingStrategy.prototype.preload = function (route, load) {
        var loadRoute = function (delay) { return delay
            ? Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["timer"])(150).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["flatMap"])(function (_) { return load(); }))
            : load(); };
        return route.data && route.data.preload
            ? loadRoute(route.data.delay)
            : Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(null);
    };
    return AppPreloadingStrategy;
}());



/***/ }),

/***/ "./src/app/auth/actions/auth.actions.ts":
/*!**********************************************!*\
  !*** ./src/app/auth/actions/auth.actions.ts ***!
  \**********************************************/
/*! exports provided: AuthActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthActions", function() { return AuthActions; });
var AuthActions = /** @class */ (function () {
    function AuthActions() {
    }
    AuthActions.prototype.authorize = function () {
        return { type: AuthActions.AUTHORIZE };
    };
    AuthActions.prototype.login = function () {
        return { type: AuthActions.LOGIN };
    };
    AuthActions.prototype.oAuthLogin = function (provider) {
        return {
            type: AuthActions.O_AUTH_LOGIN,
            payload: provider
        };
    };
    AuthActions.prototype.forgetPasswordSuccess = function () {
        return { type: AuthActions.FORGET_PASSWORD_SUCCESS };
    };
    AuthActions.prototype.forgetPassword = function () {
        return { type: AuthActions.FORGET_PASSWORD };
    };
    AuthActions.prototype.updatePasswordSuccess = function () {
        return { type: AuthActions.UPDATE_PASSWORD_SUCCESS };
    };
    AuthActions.prototype.updatePassword = function () {
        return { type: AuthActions.UPDATE_PASSWORD };
    };
    AuthActions.prototype.loginSuccess = function () {
        return { type: AuthActions.LOGIN_SUCCESS };
    };
    AuthActions.prototype.logout = function () {
        return { type: AuthActions.LOGOUT };
    };
    AuthActions.prototype.logoutSuccess = function () {
        return { type: AuthActions.LOGOUT_SUCCESS };
    };
    AuthActions.prototype.noOp = function () {
        return { type: AuthActions.NO_OP };
    };
    AuthActions.LOGIN = 'LOGIN';
    AuthActions.LOGIN_SUCCESS = 'LOGIN_SUCCESS';
    AuthActions.LOGOUT = 'LOGOUT';
    AuthActions.LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
    AuthActions.AUTHORIZE = 'AUTHORIZE';
    AuthActions.O_AUTH_LOGIN = 'O_AUTH_LOGIN';
    AuthActions.NO_OP = 'NO_OPERATION';
    AuthActions.FORGET_PASSWORD = 'FORGET_PASSWORD';
    AuthActions.FORGET_PASSWORD_SUCCESS = 'FORGET_PASSWORD_SUCCESS';
    AuthActions.UPDATE_PASSWORD = 'UPDATE_PASSWORD';
    AuthActions.UPDATE_PASSWORD_SUCCESS = 'UPDATE_PASSWORD_SUCCESS';
    return AuthActions;
}());



/***/ }),

/***/ "./src/app/auth/effects/auth.effects.ts":
/*!**********************************************!*\
  !*** ./src/app/auth/effects/auth.effects.ts ***!
  \**********************************************/
/*! exports provided: AuthenticationEffects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationEffects", function() { return AuthenticationEffects; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthenticationEffects = /** @class */ (function () {
    function AuthenticationEffects(actions$, authService, authActions) {
        var _this = this;
        this.actions$ = actions$;
        this.authService = authService;
        this.authActions = authActions;
        // tslint:disable-next-line:member-ordering
        this.Authorized$ = this.actions$
            .ofType(_actions_auth_actions__WEBPACK_IMPORTED_MODULE_4__["AuthActions"].AUTHORIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function () { return _this.authService.authorized(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["filter"])(function (data) { return data.status !== 'unauthorized'; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function () { return _this.authActions.loginSuccess(); }));
        // tslint:disable-next-line:member-ordering
        this.OAuthLogin = this.actions$
            .ofType(_actions_auth_actions__WEBPACK_IMPORTED_MODULE_4__["AuthActions"].O_AUTH_LOGIN)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.authService.socialLogin(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["filter"])(function (data) { return data !== null; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            if (typeof data === typeof 'string') {
                return _this.authActions.noOp();
            }
            else {
                return _this.authActions.loginSuccess();
            }
        }));
    }
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"])
    ], AuthenticationEffects.prototype, "Authorized$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"])
    ], AuthenticationEffects.prototype, "OAuthLogin", void 0);
    AuthenticationEffects = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Actions"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _actions_auth_actions__WEBPACK_IMPORTED_MODULE_4__["AuthActions"]])
    ], AuthenticationEffects);
    return AuthenticationEffects;
}());



/***/ }),

/***/ "./src/app/auth/reducers/auth.reducer.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/reducers/auth.reducer.ts ***!
  \***********************************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var _auth_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.state */ "./src/app/auth/reducers/auth.state.ts");


var initialState = new _auth_state__WEBPACK_IMPORTED_MODULE_1__["AuthStateRecord"]();
function reducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__["AuthActions"].LOGIN_SUCCESS:
            return state.merge({ isAuthenticated: true });
        case _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__["AuthActions"].LOGOUT_SUCCESS:
            return state.merge({ isAuthenticated: false });
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/app/auth/reducers/auth.state.ts":
/*!*********************************************!*\
  !*** ./src/app/auth/reducers/auth.state.ts ***!
  \*********************************************/
/*! exports provided: AuthStateRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthStateRecord", function() { return AuthStateRecord; });
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);

var AuthStateRecord = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Record"])({
    isAuthenticated: false
});


/***/ }),

/***/ "./src/app/auth/reducers/selectors.ts":
/*!********************************************!*\
  !*** ./src/app/auth/reducers/selectors.ts ***!
  \********************************************/
/*! exports provided: getAuthStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthStatus", function() { return getAuthStatus; });
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reselect */ "./node_modules/reselect/lib/index.js");
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reselect__WEBPACK_IMPORTED_MODULE_0__);

// Base state function
function getAuthState(state) {
    return state.auth;
}
// ******************** Individual selectors ***************************
var fetchAuthStatus = function (state) {
    return state.isAuthenticated;
};
// *************************** PUBLIC API's ****************************
var getAuthStatus = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getAuthState, fetchAuthStatus);


/***/ }),

/***/ "./src/app/checkout/actions/checkout.actions.ts":
/*!******************************************************!*\
  !*** ./src/app/checkout/actions/checkout.actions.ts ***!
  \******************************************************/
/*! exports provided: CheckoutActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutActions", function() { return CheckoutActions; });
var CheckoutActions = /** @class */ (function () {
    function CheckoutActions() {
    }
    CheckoutActions.prototype.fetchCurrentOrder = function () {
        return { type: CheckoutActions.FETCH_CURRENT_ORDER };
    };
    CheckoutActions.prototype.fetchCurrentOrderSuccess = function (order) {
        return {
            type: CheckoutActions.FETCH_CURRENT_ORDER_SUCCESS,
            payload: order
        };
    };
    CheckoutActions.prototype.addToCart = function (variant_id, quantity) {
        return {
            type: CheckoutActions.ADD_TO_CART,
            payload: { variant_id: variant_id, quantity: quantity }
        };
    };
    CheckoutActions.prototype.addToCartSuccess = function (lineItem) {
        return {
            type: CheckoutActions.ADD_TO_CART_SUCCESS,
            payload: lineItem
        };
    };
    CheckoutActions.prototype.removeLineItem = function (lineItemId) {
        return {
            type: CheckoutActions.REMOVE_LINE_ITEM,
            payload: lineItemId
        };
    };
    CheckoutActions.prototype.removeLineItemSuccess = function (lineItem) {
        return {
            type: CheckoutActions.REMOVE_LINE_ITEM_SUCCESS,
            payload: lineItem
        };
    };
    CheckoutActions.prototype.changeLineItemQuantity = function (quantity, lineItemId) {
        return {
            type: CheckoutActions.CHANGE_LINE_ITEM_QUANTITY,
            payload: { quantity: quantity, lineItemId: lineItemId }
        };
    };
    CheckoutActions.prototype.placeOrder = function () {
        return { type: CheckoutActions.PLACE_ORDER };
    };
    CheckoutActions.prototype.changeOrderState = function () {
        return { type: CheckoutActions.CHANGE_ORDER_STATE };
    };
    CheckoutActions.prototype.changeOrderStateSuccess = function (order) {
        return {
            type: CheckoutActions.CHANGE_ORDER_STATE_SUCCESS,
            payload: order
        };
    };
    CheckoutActions.prototype.updateOrder = function () {
        return { type: CheckoutActions.UPDATE_ORDER };
    };
    CheckoutActions.prototype.updateOrderSuccess = function (order) {
        return {
            type: CheckoutActions.UPDATE_ORDER_SUCCESS,
            payload: order
        };
    };
    CheckoutActions.prototype.orderCompleteSuccess = function () {
        return { type: CheckoutActions.ORDER_COMPLETE_SUCCESS };
    };
    CheckoutActions.prototype.getOrderDetails = function (order_number) {
        return {
            type: CheckoutActions.GET_ORDER_DETAILS,
            payload: order_number
        };
    };
    CheckoutActions.prototype.getOrderDetailsSuccess = function (order) {
        return {
            type: CheckoutActions.GET_ORDER_DETAILS,
            payload: order
        };
    };
    CheckoutActions.FETCH_CURRENT_ORDER = 'FETCH_CURRENT_ORDER';
    CheckoutActions.FETCH_CURRENT_ORDER_SUCCESS = 'FETCH_CURRENT_ORDER_SUCCESS';
    CheckoutActions.ADD_TO_CART = 'ADD_TO_CART';
    CheckoutActions.ADD_TO_CART_SUCCESS = 'ADD_TO_CART_SUCCESS';
    CheckoutActions.REMOVE_LINE_ITEM = 'REMOVE_LINE_ITEM';
    CheckoutActions.REMOVE_LINE_ITEM_SUCCESS = 'REMOVE_LINE_ITEM_SUCCESS';
    CheckoutActions.CHANGE_LINE_ITEM_QUANTITY = 'CHANGE_LINE_ITEM_QUANTITY';
    CheckoutActions.PLACE_ORDER = 'PLACE_ORDER';
    CheckoutActions.CHANGE_ORDER_STATE = 'CHANGE_ORDER_STATE';
    CheckoutActions.CHANGE_ORDER_STATE_SUCCESS = 'CHANGE_ORDER_STATE_SUCCESS';
    CheckoutActions.UPDATE_ORDER = 'UPDATE_ORDER';
    CheckoutActions.UPDATE_ORDER_SUCCESS = 'UPDATE_ORDER_SUCCESS';
    CheckoutActions.ORDER_COMPLETE_SUCCESS = 'ORDER_COMPLETE_SUCCESS';
    CheckoutActions.GET_ORDER_DETAILS = 'GET_ORDER_DETAILS';
    CheckoutActions.GET_ORDER_DETAILS_SUCCESS = 'GET_ORDER_DETAILS_SUCCESS';
    return CheckoutActions;
}());



/***/ }),

/***/ "./src/app/checkout/reducers/checkout.reducer.ts":
/*!*******************************************************!*\
  !*** ./src/app/checkout/reducers/checkout.reducer.ts ***!
  \*******************************************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../actions/checkout.actions */ "./src/app/checkout/actions/checkout.actions.ts");
/* harmony import */ var _checkout_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./checkout.state */ "./src/app/checkout/reducers/checkout.state.ts");


var initialState = new _checkout_state__WEBPACK_IMPORTED_MODULE_1__["CheckoutStateRecord"]();
function reducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    var _lineItems, _lineItemEntities, _lineItemIds, _lineItem, _lineItemEntity, _lineItemId, _totalCartItems = 0, _totalCartValue, _ship_address, _bill_address, _orderState, _shipTotal = 0, _itemTotal, _adjustmentTotal;
    switch (type) {
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].FETCH_CURRENT_ORDER_SUCCESS:
            var _orderNumber = payload.number;
            _lineItems = payload.line_items;
            _lineItemIds = _lineItems.map(function (lineItem) { return lineItem.id; });
            _totalCartItems = payload.total_quantity;
            _totalCartValue = parseFloat(payload.total);
            _ship_address = payload.ship_address;
            _bill_address = payload.bill_address;
            _orderState = payload.state;
            _shipTotal = payload.ship_total;
            _itemTotal = parseFloat(payload.item_total);
            _adjustmentTotal = payload.display_adjustment_total;
            _lineItemEntities = _lineItems.reduce(function (lineItems, lineItem) {
                return Object.assign(lineItems, (_a = {},
                    _a[lineItem.id] = lineItem,
                    _a));
                var _a;
            }, {});
            return state.merge({
                orderNumber: _orderNumber,
                orderState: _orderState,
                lineItemIds: _lineItemIds,
                lineItemEntities: _lineItemEntities,
                totalCartItems: _totalCartItems,
                totalCartValue: _totalCartValue,
                shipAddress: _ship_address,
                billAddress: _bill_address,
                shipTotal: _shipTotal,
                itemTotal: _itemTotal,
                adjustmentTotal: _adjustmentTotal
            });
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].ADD_TO_CART_SUCCESS:
            _lineItem = payload;
            _lineItemId = _lineItem.id;
            // TODO : @Refactor this code later
            // return the same state if the item is already included.
            if (state.lineItemIds.includes(_lineItemId)) {
                _totalCartItems = state.totalCartItems + _lineItem.quantity - state.lineItemEntities.toJS()[_lineItemId].quantity;
                _totalCartValue = state.totalCartValue + parseFloat(_lineItem.total) - state.lineItemEntities.toJS()[_lineItemId].total;
                _itemTotal = state.itemTotal + parseFloat(_lineItem.total) - state.lineItemEntities.toJS()[_lineItemId].total;
                _lineItemEntity = (_b = {}, _b[_lineItemId] = _lineItem, _b);
                _shipTotal = state.shipTotal;
                return state.merge({
                    lineItemEntities: state.lineItemEntities.merge(_lineItemEntity),
                    totalCartItems: _totalCartItems,
                    totalCartValue: _totalCartValue,
                    itemTotal: _itemTotal,
                    shipTotal: _shipTotal
                });
            }
            _totalCartItems = state.totalCartItems + _lineItem.quantity;
            _totalCartValue = state.totalCartValue + parseFloat(_lineItem.total);
            _itemTotal = state.itemTotal + parseFloat(_lineItem.total);
            _lineItemEntity = (_c = {}, _c[_lineItemId] = _lineItem, _c);
            _lineItemIds = state.lineItemIds.push(_lineItemId);
            _shipTotal = state.shipTotal;
            return state.merge({
                lineItemIds: _lineItemIds,
                lineItemEntities: state.lineItemEntities.merge(_lineItemEntity),
                totalCartItems: _totalCartItems,
                totalCartValue: _totalCartValue,
                shipTotal: _shipTotal,
                itemTotal: _itemTotal
            });
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].REMOVE_LINE_ITEM_SUCCESS:
            _lineItem = payload;
            _lineItemId = _lineItem.id;
            var index = state.lineItemIds.indexOf(_lineItemId);
            if (index >= 0) {
                _lineItemIds = state.lineItemIds.splice(index, 1);
                _lineItemEntities = state.lineItemEntities.delete(_lineItemId);
                _totalCartItems = state.totalCartItems - _lineItem.quantity;
                _totalCartValue = state.totalCartValue - parseFloat(_lineItem.total);
                _itemTotal = state.itemTotal - parseFloat(_lineItem.total);
                _shipTotal = state.shipTotal;
            }
            return state.merge({
                lineItemIds: _lineItemIds,
                lineItemEntities: _lineItemEntities,
                totalCartItems: _totalCartItems,
                totalCartValue: _totalCartValue,
                itemTotal: _itemTotal,
                shipTotal: _shipTotal
            });
        // case CheckoutActions.CHANGE_LINE_ITEM_QUANTITY:
        //   const quantity = payload.quantity;
        //   lineItemId = payload.lineItemId;
        //   _lineItemEntities = state.lineItemEntities;
        //   _lineItemEntities[lineItemId][quantity] = quantity;
        //   return state.merge({
        //     lineItemEntities: _lineItemEntities
        //   }) as CheckoutState;
        // case CheckoutActions.CHANGE_ORDER_STATE:
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].CHANGE_ORDER_STATE_SUCCESS:
            _orderState = payload.state;
            _totalCartItems = payload.total_quantity;
            _totalCartValue = parseFloat(payload.total);
            _ship_address = payload.ship_address;
            _bill_address = payload.bill_address;
            _orderState = payload.state;
            _shipTotal = payload.ship_total;
            _itemTotal = parseFloat(payload.item_total);
            _adjustmentTotal = payload.display_adjustment_total;
            _lineItems = payload.line_items;
            _lineItemIds = _lineItems.map(function (lineItem) { return lineItem.id; });
            _lineItemEntities = _lineItems.reduce(function (lineItems, lineItem) {
                return Object.assign(lineItems, (_a = {},
                    _a[lineItem.id] = lineItem,
                    _a));
                var _a;
            }, {});
            return state.merge({
                orderState: _orderState,
                totalCartItems: _totalCartItems,
                totalCartValue: _totalCartValue,
                shipAddress: _ship_address,
                billAddress: _bill_address,
                shipTotal: _shipTotal,
                itemTotal: _itemTotal,
                adjustmentTotal: _adjustmentTotal,
                lineItemIds: _lineItemIds,
                lineItemEntities: _lineItemEntities,
            });
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].UPDATE_ORDER_SUCCESS:
            _orderState = payload.state;
            _totalCartItems = payload.total_quantity;
            _totalCartValue = parseFloat(payload.total);
            _ship_address = payload.ship_address;
            _bill_address = payload.bill_address;
            _orderState = payload.state;
            _shipTotal = payload.ship_total;
            _itemTotal = parseFloat(payload.item_total);
            _adjustmentTotal = payload.display_adjustment_total;
            _lineItems = payload.line_items;
            _lineItemIds = _lineItems.map(function (lineItem) { return lineItem.id; });
            _lineItemEntities = _lineItems.reduce(function (lineItems, lineItem) {
                return Object.assign(lineItems, (_a = {},
                    _a[lineItem.id] = lineItem,
                    _a));
                var _a;
            }, {});
            return state.merge({
                orderState: _orderState,
                totalCartItems: _totalCartItems,
                totalCartValue: _totalCartValue,
                shipAddress: _ship_address,
                billAddress: _bill_address,
                shipTotal: _shipTotal,
                itemTotal: _itemTotal,
                adjustmentTotal: _adjustmentTotal,
                lineItemIds: _lineItemIds,
                lineItemEntities: _lineItemEntities,
            });
        case _actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"].ORDER_COMPLETE_SUCCESS:
            return initialState;
        default:
            return state;
    }
    var _b, _c;
}
;


/***/ }),

/***/ "./src/app/checkout/reducers/checkout.state.ts":
/*!*****************************************************!*\
  !*** ./src/app/checkout/reducers/checkout.state.ts ***!
  \*****************************************************/
/*! exports provided: CheckoutStateRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutStateRecord", function() { return CheckoutStateRecord; });
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);

var CheckoutStateRecord = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Record"])({
    orderNumber: null,
    orderState: null,
    lineItemIds: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    lineItemEntities: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])({}),
    totalCartItems: 0,
    totalCartValue: 0,
    billAddress: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["fromJS"])({}),
    shipAddress: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["fromJS"])({}),
    shipTotal: 0,
    itemTotal: 0,
    adjustmentTotal: 0,
});


/***/ }),

/***/ "./src/app/checkout/reducers/selectors.ts":
/*!************************************************!*\
  !*** ./src/app/checkout/reducers/selectors.ts ***!
  \************************************************/
/*! exports provided: getCheckoutState, fetchLineItems, fetchOrderNumber, fetchTotalCartItems, fetchTotalCartValue, fetchShipAddress, fetchBillAddress, fetchOrderState, fetchShipTotal, fetchItemTotal, fetchAdjustmentTotal, getLineItems, getOrderNumber, getTotalCartItems, getTotalCartValue, getShipAddress, getBillAddress, getOrderState, getShipTotal, getItemTotal, getAdjustmentTotal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCheckoutState", function() { return getCheckoutState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchLineItems", function() { return fetchLineItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchOrderNumber", function() { return fetchOrderNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTotalCartItems", function() { return fetchTotalCartItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTotalCartValue", function() { return fetchTotalCartValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchShipAddress", function() { return fetchShipAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchBillAddress", function() { return fetchBillAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchOrderState", function() { return fetchOrderState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchShipTotal", function() { return fetchShipTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchItemTotal", function() { return fetchItemTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchAdjustmentTotal", function() { return fetchAdjustmentTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLineItems", function() { return getLineItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOrderNumber", function() { return getOrderNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalCartItems", function() { return getTotalCartItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalCartValue", function() { return getTotalCartValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getShipAddress", function() { return getShipAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBillAddress", function() { return getBillAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOrderState", function() { return getOrderState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getShipTotal", function() { return getShipTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getItemTotal", function() { return getItemTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAdjustmentTotal", function() { return getAdjustmentTotal; });
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reselect */ "./node_modules/reselect/lib/index.js");
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reselect__WEBPACK_IMPORTED_MODULE_0__);

// Base Cart State function
function getCheckoutState(state) {
    return state.checkout;
}
// ******************** Individual selectors ***************************
function fetchLineItems(state) {
    var ids = state.lineItemIds.toJS();
    var lineItemEntitites = state.lineItemEntities.toJS();
    return ids.map(function (id) { return lineItemEntitites[id]; });
}
function fetchOrderNumber(state) {
    return state.orderNumber;
}
function fetchTotalCartItems(state) {
    return state.totalCartItems;
}
function fetchTotalCartValue(state) {
    return state.totalCartValue;
}
function fetchShipAddress(state) {
    return state.shipAddress ? state.shipAddress.toJS() : state.shipAddress;
}
function fetchBillAddress(state) {
    return state.billAddress ? state.billAddress.toJS() : state.billAddress;
}
function fetchOrderState(state) {
    return state.orderState;
}
function fetchShipTotal(state) {
    return state.shipTotal;
}
function fetchItemTotal(state) {
    return state.itemTotal;
}
function fetchAdjustmentTotal(state) {
    return state.adjustmentTotal;
}
// *************************** PUBLIC API's ****************************
var getLineItems = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchLineItems);
var getOrderNumber = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchOrderNumber);
var getTotalCartItems = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchTotalCartItems);
var getTotalCartValue = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchTotalCartValue);
var getShipAddress = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchShipAddress);
var getBillAddress = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchBillAddress);
var getOrderState = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchOrderState);
var getShipTotal = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchShipTotal);
var getItemTotal = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchItemTotal);
var getAdjustmentTotal = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getCheckoutState, fetchAdjustmentTotal);


/***/ }),

/***/ "./src/app/core/guards/auth.guard.ts":
/*!*******************************************!*\
  !*** ./src/app/core/guards/auth.guard.ts ***!
  \*******************************************/
/*! exports provided: CanActivateViaAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanActivateViaAuthGuard", function() { return CanActivateViaAuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../auth/reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CanActivateViaAuthGuard = /** @class */ (function () {
    function CanActivateViaAuthGuard(store, router) {
        this.store = store;
        this.router = router;
    }
    CanActivateViaAuthGuard.prototype.canActivate = function (route, state) {
        var _this = this;
        this.subscription = this.store
            .select(_auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__["getAuthStatus"])
            .subscribe(function (isAuthenticated) {
            _this.isAuthenticated = isAuthenticated;
            if (!isAuthenticated) {
                _this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
            }
        });
        return this.isAuthenticated;
    };
    CanActivateViaAuthGuard.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    CanActivateViaAuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CanActivateViaAuthGuard);
    return CanActivateViaAuthGuard;
}());



/***/ }),

/***/ "./src/app/core/index.ts":
/*!*******************************!*\
  !*** ./src/app/core/index.ts ***!
  \*******************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../checkout/actions/checkout.actions */ "./src/app/checkout/actions/checkout.actions.ts");
/* harmony import */ var _services_checkout_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/checkout.service */ "./src/app/core/services/checkout.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_progressbar_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-progressbar/core */ "./node_modules/@ngx-progressbar/core/fesm5/ngx-progressbar-core.js");
/* harmony import */ var _ngx_progressbar_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-progressbar/http */ "./node_modules/@ngx-progressbar/http/fesm5/ngx-progressbar-http.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/product.service */ "./src/app/core/services/product.service.ts");
/* harmony import */ var _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../auth/actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var _services_variant_retriver_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/variant-retriver.service */ "./src/app/core/services/variant-retriver.service.ts");
/* harmony import */ var _services_variant_parser_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/variant-parser.service */ "./src/app/core/services/variant-parser.service.ts");
/* harmony import */ var _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./interceptors/token.interceptor */ "./src/app/core/interceptors/token.interceptor.ts");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _auth_effects_auth_effects__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../auth/effects/auth.effects */ "./src/app/auth/effects/auth.effects.ts");
/* harmony import */ var _product_effects_product_effects__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../product/effects/product.effects */ "./src/app/product/effects/product.effects.ts");
/* harmony import */ var _user_actions_user_actions__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../user/actions/user.actions */ "./src/app/user/actions/user.actions.ts");
/* harmony import */ var _user_effects_user_effects__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../user/effects/user.effects */ "./src/app/user/effects/user.effects.ts");
/* harmony import */ var _user_services_user_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../user/services/user.service */ "./src/app/user/services/user.service.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/core/guards/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// Components
// Services






// import { ProductDummyService } from './services/product-dummy.service';







var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [],
            exports: [
                // components
                // DummyService
                _ngx_progressbar_core__WEBPACK_IMPORTED_MODULE_4__["NgProgressModule"]
            ],
            imports: [
                // Were not working on modules sice update to rc-5
                // TO BE moved to respective modules.
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_12__["EffectsModule"].forFeature([
                    _auth_effects_auth_effects__WEBPACK_IMPORTED_MODULE_13__["AuthenticationEffects"],
                    _product_effects_product_effects__WEBPACK_IMPORTED_MODULE_14__["ProductEffects"],
                    // CheckoutEffects,
                    _user_effects_user_effects__WEBPACK_IMPORTED_MODULE_16__["UserEffects"]
                ]),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _ngx_progressbar_core__WEBPACK_IMPORTED_MODULE_4__["NgProgressModule"].forRoot({
                    meteor: false
                }),
                _ngx_progressbar_http__WEBPACK_IMPORTED_MODULE_5__["NgProgressHttpModule"],
            ],
            providers: [
                _services_variant_parser_service__WEBPACK_IMPORTED_MODULE_10__["VariantParserService"],
                _services_variant_retriver_service__WEBPACK_IMPORTED_MODULE_9__["VariantRetriverService"],
                _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
                _services_checkout_service__WEBPACK_IMPORTED_MODULE_1__["CheckoutService"],
                // ProductDummyService,
                _services_product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"],
                _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_8__["AuthActions"],
                _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"],
                _user_actions_user_actions__WEBPACK_IMPORTED_MODULE_15__["UserActions"],
                _user_services_user_service__WEBPACK_IMPORTED_MODULE_17__["UserService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_18__["CanActivateViaAuthGuard"],
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"], useClass: _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_11__["TokenInterceptor"], multi: true },
            ]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/core/interceptors/token.interceptor.ts":
/*!********************************************************!*\
  !*** ./src/app/core/interceptors/token.interceptor.ts ***!
  \********************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/core/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(injector) {
        this.injector = injector;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        var auth = this.injector.get(_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]);
        var clonedRequest = request.clone({
            headers: auth.getTokenHeader(request),
            url: this.fixUrl(request.url)
        });
        return next.handle(clonedRequest);
    };
    TokenInterceptor.prototype.fixUrl = function (url) {
        if (url.indexOf('http://') >= 0 || url.indexOf('https://') >= 0) {
            return url;
        }
        else {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiEndpoint + url;
        }
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/core/models/jsonapi.ts":
/*!****************************************!*\
  !*** ./src/app/core/models/jsonapi.ts ***!
  \****************************************/
/*! exports provided: CJsonApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CJsonApi", function() { return CJsonApi; });
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var CJsonApi = /** @class */ (function () {
    function CJsonApi() {
    }
    CJsonApi.prototype.toModel = function () {
        return __assign({ id: this.id }, this.attributes, this.loadRelationShips());
    };
    CJsonApi.prototype.loadRelationShips = function () {
        var _this = this;
        var keys = Object.keys(this.relationships || {});
        var newRelationShips = Object.assign({});
        if (keys.length) {
            keys.forEach(function (relationKey) {
                var includedRelation = _this.included[relationKey];
                var formatedIncludedRelation;
                if (_this.included[relationKey] instanceof Array) {
                    formatedIncludedRelation = [];
                    includedRelation.forEach(function (subIncludedRelation) {
                        var _subJson = Object.assign.apply(Object, [new CJsonApi()].concat(subIncludedRelation.data)).toModel();
                        formatedIncludedRelation.push(_subJson);
                    });
                }
                else {
                    formatedIncludedRelation = Object.assign.apply(Object, [new CJsonApi()].concat(_this.included[relationKey].data)).toModel();
                }
                newRelationShips = __assign({}, newRelationShips, (_a = {}, _a[relationKey] = formatedIncludedRelation, _a));
                var _a;
            });
        }
        return newRelationShips;
    };
    return CJsonApi;
}());



/***/ }),

/***/ "./src/app/core/models/product.ts":
/*!****************************************!*\
  !*** ./src/app/core/models/product.ts ***!
  \****************************************/
/*! exports provided: Product */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Product", function() { return Product; });
/*
 * Product model
 * Detailed info http://guides.spreecommerce.org/developer/products.html
 * Public API's http://guides.spreecommerce.org/api/products.html
 */
var Product = /** @class */ (function () {
    function Product() {
    }
    return Product;
}());



/***/ }),

/***/ "./src/app/core/pipes/humanize.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/core/pipes/humanize.pipe.ts ***!
  \*********************************************/
/*! exports provided: HumanizePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HumanizePipe", function() { return HumanizePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// tslint:disable-next-line:use-pipe-transform-interface
var HumanizePipe = /** @class */ (function () {
    function HumanizePipe() {
    }
    /**
     *
     *
     * @param {any} value
     * @returns
     *
     * @memberof HumanizePipe
     */
    HumanizePipe.prototype.transform = function (value) {
        var updated_val = value;
        if (typeof (value) === 'string') {
            updated_val = value.replace(/\_/g, ' ');
        }
        return updated_val;
    };
    HumanizePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'humanize' })
    ], HumanizePipe);
    return HumanizePipe;
}());

;


/***/ }),

/***/ "./src/app/core/services/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../auth/actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var ng2_ui_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-ui-auth */ "./node_modules/ng2-ui-auth/fesm5/ng2-ui-auth.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AuthService = /** @class */ (function () {
    /**
     * Creates an instance of AuthService.
     * @param {HttpService} http
     * @param {AuthActions} actions
     * @param {Store<AppState>} store
     *
     * @memberof AuthService
     */
    function AuthService(http, actions, store, oAuthService, toastrService, router, activatedRoute) {
        this.http = http;
        this.actions = actions;
        this.store = store;
        this.oAuthService = oAuthService;
        this.toastrService = toastrService;
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    /**
     *
     *
     * @param {Authenticate} { email, password }
     * @returns {Observable<User>}
     * @memberof AuthService
     */
    AuthService.prototype.login = function (_a) {
        var _this = this;
        var email = _a.email, password = _a.password;
        var params = { spree_user: { email: email, password: password } };
        return this.http.post('login.json', params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (user) {
            _this.setTokenInLocalStorage(user);
            _this.store.dispatch(_this.actions.loginSuccess());
            return user;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (_) { return _this.router.navigate(['/']); }, function (user) { return _this.toastrService.error(user.error.error, 'ERROR!'); }));
        // catch should be handled here with the http observable
        // so that only the inner obs dies and not the effect Observable
        // otherwise no further login requests will be fired
        // MORE INFO https://youtu.be/3LKMwkuK0ZE?t=24m29s
    };
    /**
     *
     *
     * @param {User} data
     * @returns {Observable<User>}
     *
     * @memberof AuthService
     */
    AuthService.prototype.register = function (data) {
        var _this = this;
        var params = { spree_user: data };
        return this.http.post('auth/accounts', params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (user) {
            _this.setTokenInLocalStorage(user);
            _this.store.dispatch(_this.actions.loginSuccess());
            return user;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (_) { return _; }, function (_) { return _this.toastrService.error('Invalid/Existing data', 'ERROR!!'); }));
        // catch should be handled here with the http observable
        // so that only the inner obs dies and not the effect Observable
        // otherwise no further login requests will be fired
        // MORE INFO https://youtu.be/3LKMwkuK0ZE?t=24m29s
    };
    /**
     *
     *
     * @param {anyUser} data
     * @returns {Observable<any>}
     * @memberof AuthService
     */
    AuthService.prototype.forgetPassword = function (data) {
        var _this = this;
        return this.http
            .post('auth/passwords', { spree_user: data })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_) {
            return _this.toastrService.success('Password reset link has be sent to your email.', 'Success');
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (_) { return _; }, function (_) { return _this.toastrService.error('Not a valid email/user', 'ERROR!!'); }));
    };
    /**
     *
     *
     * @param {User} data
     * @returns {Observable<any>}
     * @memberof AuthService
     */
    AuthService.prototype.updatePassword = function (data) {
        var _this = this;
        return this.http
            .put("auth/passwords/" + data.id, { spree_user: data })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_) {
            return _this.toastrService.success('Password updated success fully!', 'Success');
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (_) { return _; }, function (_) { return _this.toastrService.error('Unable to update password', 'ERROR!'); }));
    };
    /**
     *
     *
     * @returns {Observable<any>}
     *
     * @memberof AuthService
     */
    AuthService.prototype.authorized = function () {
        //return this.http.get('auth/authenticated').pipe(map((res: Response) => res));
        return new rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"]();
        // catch should be handled here with the http observable
        // so that only the inner obs dies and not the effect Observable
        // otherwise no further login requests will be fired
        // MORE INFO https://youtu.be/3LKMwkuK0ZE?t=24m29s
    };
    /**
     *
     *
     * @returns
     *
     * @memberof AuthService
     */
    AuthService.prototype.logout = function () {
        var _this = this;
        return this.http.get('logout.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (res) {
            // Setting token after login
            localStorage.removeItem('user');
            _this.store.dispatch(_this.actions.logoutSuccess());
            return res;
        }));
    };
    /**
     *
     *
     * @returns {{}}
     * @memberof AuthService
     */
    AuthService.prototype.getTokenHeader = function (request) {
        var user = ['undefined', null]
            .indexOf(localStorage.getItem('user')) === -1 ?
            JSON.parse(localStorage.getItem('user')) : {};
        return new _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpHeaders"]({
            'Content-Type': request.headers.get('Content-Type') || 'application/json',
            'token-type': 'Bearer',
            'access_token': user.access_token || [],
            'client': user.client || [],
            'uid': user.uid || [],
            'Auth-Token': user.spree_api_key || [],
            'ng-api': 'true'
        });
    };
    /**
     *
     *
     * @private
     * @param {any} user_data
     *
     * @memberof AuthService
     */
    AuthService.prototype.setTokenInLocalStorage = function (user_data) {
        var jsonData = JSON.stringify(user_data);
        localStorage.setItem('user', jsonData);
    };
    AuthService.prototype.socialLogin = function (provider) {
        var _this = this;
        return this.oAuthService.authenticate(provider).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (user) {
            _this.setTokenInLocalStorage(user);
            return user;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(function (_) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])('Social login failed');
        }));
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"],
            _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_5__["AuthActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            ng2_ui_auth__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/core/services/checkout.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/checkout.service.ts ***!
  \***************************************************/
/*! exports provided: CheckoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutService", function() { return CheckoutService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _checkout_reducers_selectors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../checkout/reducers/selectors */ "./src/app/checkout/reducers/selectors.ts");
/* harmony import */ var _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../checkout/actions/checkout.actions */ "./src/app/checkout/actions/checkout.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CheckoutService = /** @class */ (function () {
    /**
     * Creates an instance of CheckoutService.
     * @param {HttpService} http
     * @param {CheckoutActions} actions
     * @param {Store<AppState>} store
     *
     * @memberof CheckoutService
     */
    function CheckoutService(http, actions, store, toastyService) {
        var _this = this;
        this.http = http;
        this.actions = actions;
        this.store = store;
        this.toastyService = toastyService;
        this.store.select(_checkout_reducers_selectors__WEBPACK_IMPORTED_MODULE_2__["getOrderNumber"])
            .subscribe(function (number) { return (_this.orderNumber = number); });
    }
    /**
     *
     *
     * @param {number} variant_id
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.createNewLineItem = function (variant_id, quantity) {
        var _this = this;
        var params = {
            line_item: { variant_id: variant_id, quantity: quantity }
        }, url = "api/v1/orders/" + this.orderNumber + "/line_items?order_token=" + this.getOrderToken();
        return this.http.post(url, params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (lineItem) {
            _this.toastyService.success('Success!', 'Cart updated!');
            return lineItem;
        }, function (_) { return _this.toastyService.error('Something went wrong!', 'Failed'); }));
    };
    /**
     *
     *
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.fetchCurrentOrder = function () {
        /*return this.http.get<Order>('api/v1/orders/current').pipe(
          map(order => {
            if (order) {
              const token = order.token;
              this.setOrderTokenInLocalStorage({ order_token: token });
              return this.store.dispatch(
                this.actions.fetchCurrentOrderSuccess(order)
              );
            } else {
              this.createEmptyOrder().subscribe();
            }
          })
        );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_8__["Observable"]();
    };
    /**
     *
     *
     * @param {string} orderNumber
     * @returns
     * @memberof CheckoutService
     */
    CheckoutService.prototype.getOrder = function (orderNumber) {
        var url = "api/v1/orders/" + orderNumber + ".json";
        return this.http.get(url);
    };
    /**
     *
     *
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.createEmptyOrder = function () {
        var _this = this;
        var user = JSON.parse(localStorage.getItem('user'));
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'text/plain');
        return this.http
            .post('api/v1/orders.json', null, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (order) {
            _this.setOrderTokenInLocalStorage({ order_token: order.token });
            return _this.store.dispatch(_this.actions.fetchCurrentOrderSuccess(order));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (_) { return _; }, function (_) {
            return _this.toastyService.error('Unable to create empty order', 'ERROR!!');
        }));
    };
    /**
     *
     *
     * @param {LineItem} lineItem
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.deleteLineItem = function (lineItem) {
        var _this = this;
        var url = "api/v1/orders/" + this.orderNumber + "/line_items/" + lineItem.id + "?order_token=" + this.getOrderToken();
        return this.http
            .delete(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function () {
            return _this.store.dispatch(_this.actions.removeLineItemSuccess(lineItem));
        }));
    };
    /**
     *
     *
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.changeOrderState = function () {
        var _this = this;
        var url = "api/v1/checkouts/" + this.orderNumber + "/next.json?order_token=" + this.getOrderToken();
        return this.http
            .put(url, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (order) {
            return _this.store.dispatch(_this.actions.changeOrderStateSuccess(order));
        }));
    };
    /**
     *
     *
     * @param {any} params
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.updateOrder = function (params) {
        var _this = this;
        var url = "api/v1/checkouts/" + this.orderNumber + ".json?order_token=" + this.getOrderToken();
        return this.http
            .put(url, params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (order) {
            return _this.store.dispatch(_this.actions.updateOrderSuccess(order));
        }));
    };
    /**
     *
     *
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.availablePaymentMethods = function () {
        var url = "api/v1/orders/" + this.orderNumber + "/payments/new?order_token=" + this.getOrderToken();
        return this.http.get(url);
    };
    /**
     *
     *
     * @param {number} paymentModeId
     * @param {number} paymentAmount
     * @returns
     * @memberof CheckoutService
     */
    CheckoutService.prototype.createNewPayment = function (paymentModeId, paymentAmount) {
        var _this = this;
        return this.http
            .post("api/v1/orders/" + this.orderNumber + "/payments?order_token=" + this.getOrderToken(), {
            payment: {
                payment_method_id: paymentModeId,
                amount: paymentAmount
            }
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (_) { return _this.changeOrderState().subscribe(); }));
    };
    CheckoutService.prototype.makePayment = function (params) {
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/x-www-form-urlencoded');
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        body = body.set('key', params.key);
        body = body.set('txnid', params.txnid);
        body = body.set('amount', params.amount);
        body = body.set('productinfo', params.productinfo);
        body = body.set('firstname', params.firstname);
        body = body.set('email', params.email);
        body = body.set('phone', params.phone);
        body = body.set('surl', params.surl);
        body = body.set('furl', params.furl);
        body = body.set('hash', params.hash);
        return this.http.post("" + _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].config.payuBizUrl, body, { headers: header, responseType: 'text', observe: 'response' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (resp) {
            return resp;
        }), function (error) { return error; });
    };
    /**
     *
     *
     * @private
     * @returns
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.getOrderToken = function () {
        var order = JSON.parse(localStorage.getItem('order'));
        var token = order.order_token;
        return token;
    };
    /**
     *
     *
     * @private
     * @param {any} token
     *
     * @memberof CheckoutService
     */
    CheckoutService.prototype.setOrderTokenInLocalStorage = function (token) {
        var jsonData = JSON.stringify(token);
        localStorage.setItem('order', jsonData);
    };
    CheckoutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_3__["CheckoutActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"]])
    ], CheckoutService);
    return CheckoutService;
}());



/***/ }),

/***/ "./src/app/core/services/json-api-parser.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/core/services/json-api-parser.service.ts ***!
  \**********************************************************/
/*! exports provided: JsonApiParserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JsonApiParserService", function() { return JsonApiParserService; });
/* harmony import */ var _models_jsonapi__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../models/jsonapi */ "./src/app/core/models/jsonapi.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JsonApiParserService = /** @class */ (function () {
    function JsonApiParserService() {
    }
    JsonApiParserService.prototype.parseSingleObj = function (data) {
        return Object.assign(new _models_jsonapi__WEBPACK_IMPORTED_MODULE_0__["CJsonApi"], data).toModel();
    };
    JsonApiParserService.prototype.parseArrayofObject = function (array) {
        return array.map(function (obj) { return Object.assign(new _models_jsonapi__WEBPACK_IMPORTED_MODULE_0__["CJsonApi"], obj).toModel(); });
    };
    JsonApiParserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], JsonApiParserService);
    return JsonApiParserService;
}());



/***/ }),

/***/ "./src/app/core/services/product.service.ts":
/*!**************************************************!*\
  !*** ./src/app/core/services/product.service.ts ***!
  \**************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var _json_api_parser_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./json-api-parser.service */ "./src/app/core/services/json-api-parser.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProductService = /** @class */ (function () {
    /**
     * Creates an instance of ProductService.
     * @param {HttpService} http
     *
     * @memberof ProductService
     */
    function ProductService(http, toastrService, apiParser) {
        this.http = http;
        this.toastrService = toastrService;
        this.apiParser = apiParser;
    }
    /**
     *
     *
     * @param {string} id
     * @returns {Observable<Product>}
     *
     * @memberof ProductService
     */
    ProductService.prototype.getProduct = function (id) {
        var _this = this;
        return this.http
            .get("api/v1/products/" + id + "?data_set=large")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) { return _this.apiParser.parseSingleObj(resp.data); }));
    };
    ProductService.prototype.getProductReviews = function (products) {
        return this.http.get("products/" + products + "/reviews");
    };
    /**
     *
     *
     * @returns {Array<Taxonomy>}
     *
     * @memberof ProductService
     */
    ProductService.prototype.getTaxonomies = function () {
        //return this.http.get<Array<Taxonomy>>(`api/v1/taxonomies?set=nested`);
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    /**
     *
     *
     * @returns {Array<Product>}
     *
     * @memberof ProductService
     */
    ProductService.prototype.getProducts = function (pageNumber) {
        /*return this.http
          .get<{ data: CJsonApi[] }>(
            `api/v1/products?page=${pageNumber}&per_page=20&data_set=small`
          )
          .pipe(
            map(
              resp => this.apiParser.parseArrayofObject(resp.data) as Array<Product>
            )
          );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    ProductService.prototype.markAsFavorite = function (id) {
        return this.http.post("favorite_products", { id: id });
    };
    ProductService.prototype.removeFromFavorite = function (id) {
        return this.http.delete("favorite_products/" + id);
    };
    ProductService.prototype.getFavoriteProducts = function () {
        /*return this.http
          .get<{ data: CJsonApi[] }>(
            `favorite_products.json?per_page=20&data_set=small`
          )
          .pipe(
            map(
              resp => this.apiParser.parseArrayofObject(resp.data) as Array<Product>
            )
          );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    ProductService.prototype.getUserFavoriteProducts = function () {
        /*return this.http
          .get<{ data: CJsonApi[] }>(
            `spree/user_favorite_products.json?data_set=small`
          )
          .pipe(
            map(
              resp => this.apiParser.parseArrayofObject(resp.data) as Array<Product>
            )
          );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    // tslint:disable-next-line:max-line-length
    ProductService.prototype.getProductsByTaxon = function (id) {
        var _this = this;
        return this.http
            .get("api/v1/taxons/products?" + id + "&per_page=20&data_set=small")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) {
            return {
                pagination: resp.pagination,
                products: _this.apiParser.parseArrayofObject(resp.data)
            };
        }));
    };
    ProductService.prototype.getProductsByTaxonNP = function (id) {
        var _this = this;
        return this.http
            .get("api/v1/taxons/products?id=" + id + "&per_page=20&data_set=small")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) { return _this.apiParser.parseArrayofObject(resp.data); }));
    };
    ProductService.prototype.getTaxonByName = function (name) {
        /*return this.http.get<Array<Taxonomy>>(
          `api/v1/taxonomies?q[name_cont]=${name}&set=nested`
        );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    ProductService.prototype.getproductsByKeyword = function (keyword) {
        var _this = this;
        return this.http
            .get("api/v1/products?" + keyword + "&per_page=20&data_set=small")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) {
            return {
                pagination: resp.pagination,
                products: _this.apiParser.parseArrayofObject(resp.data)
            };
        }));
    };
    ProductService.prototype.getChildTaxons = function (taxonomyId, taxonId) {
        /*return this.http.get<Array<Taxonomy>>(
          `/api/v1/taxonomies/${taxonomyId}/taxons/${taxonId}`
        );*/
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"]();
    };
    ProductService.prototype.submitReview = function (productId, params) {
        var _this = this;
        return this.http.post("products/" + productId + "/reviews", params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (success) {
            _this.success = success;
            if (_this.success.type === 'info') {
                _this.toastrService.info(_this.success.message, _this.success.type);
                return _this.success.type;
            }
            else {
                _this.toastrService.success(_this.success.message, _this.success.type);
                return _this.success.type;
            }
        }, function (error) {
            _this.error = error;
            _this.toastrService.error(_this.error.message, _this.error.type);
            return _this.error.type;
        }));
    };
    ProductService.prototype.getRelatedProducts = function (productId) {
        var _this = this;
        return this.http
            .get("api/products/" + productId + "/relations")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) { return _this.apiParser.parseArrayofObject(resp.data); }));
    };
    ProductService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"],
            _json_api_parser_service__WEBPACK_IMPORTED_MODULE_0__["JsonApiParserService"]])
    ], ProductService);
    return ProductService;
}());



/***/ }),

/***/ "./src/app/core/services/variant-parser.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/core/services/variant-parser.service.ts ***!
  \*********************************************************/
/*! exports provided: VariantParserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariantParserService", function() { return VariantParserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VariantParserService = /** @class */ (function () {
    function VariantParserService() {
    }
    /**
     *
     *
     * @param {Variant[]} variants
     * @param {OptionType[]} optionTypes
     * @returns
     *
     * @memberof VariantParserService
     */
    VariantParserService.prototype.getOptionsToDisplay = function (variants, optionTypes) {
        var _this = this;
        var optionTypesHash = {};
        /**Iterate over optionTypes say [tsize, tcolor] */
        optionTypes.forEach(function (optionType) {
            /**For each optionType iterate over each variant in varaints */
            variants.forEach(function (variant) {
                /**For option values like [small, Red] etc in varaint iterate over each option value */
                _this.currVariantOptionValues = variant.option_values;
                variant.option_values.forEach(function (optionValue) {
                    /**
                    * This loop runs for 750 times for 2 optiontypes and optionsvalues 3 and 5
                    * Refactor this latter;
                    */
                    /**Check if optionvalue's type i.e smalls type is tsize and then procced else not
                     * i.e for tsize option type color option value like green will be ignored.
                     */
                    if (optionValue.option_type_name === optionType.name) {
                        Object.assign(optionTypesHash, _this.singleOptionTypeHashMaker(optionValue, optionTypesHash, optionType, variant));
                    }
                });
            });
        });
        return optionTypesHash;
    };
    /**Create a single custom option type
     *
     * @param: optionValue, optionTypesHash(final hash to return), optionType(i.e tsize, tcolor, etc),
     * variant(i.e current variant from which option value is to retrived e.g: (s-small, green))
     *
     * @return: {tsize: {small: {etc etc etc}}};
     */
    VariantParserService.prototype.singleOptionTypeHashMaker = function (optionValue, optionTypesHash, optionType, variant) {
        var optionTypeName = optionType.name;
        if (optionTypesHash[optionTypeName] != null) {
            // This will become value of op["tsize"] i.e {small: {etc, etc}};
            optionTypesHash[optionTypeName] = Object.assign({}, optionTypesHash[optionTypeName], this.optionMaker(optionValue, optionTypesHash, optionType, variant));
            return optionTypesHash;
        }
        else {
            var singleOption = {};
            // e.g: singleOption["tsize"] = {small: {etc, etc}};
            singleOption[optionTypeName] = this.optionMaker(optionValue, optionTypesHash, optionType, variant);
            return singleOption;
        }
    };
    /**
     * Here we make optionvalue of option Type
     * say optionType is tsize  i.e key then here we making value of that option OptionType
     * like { small: {optionvalue: {}, variant_ids: [1,2,3,4]}};
     */
    VariantParserService.prototype.optionMaker = function (optionValue, optionTypesHash, optionType, variant) {
        var name = optionValue.name;
        var optionInnerValue = {};
        // e.g: optionInnverValue['small'] = {option_value: {etc ,etc}, variant_ids: [1,2,3,4]}
        optionInnerValue[name] = this.optionInnerValueMaker(optionValue, optionTypesHash, optionType, variant);
        return optionInnerValue;
    };
    /**
     * Creates Inner Values of optionValue
     * like { option_value: {}, varaint_ids: [1,2,3,4]};
     */
    VariantParserService.prototype.optionInnerValueMaker = function (optionValue, optionTypesHash, optionType, variant) {
        return Object.assign({}, {
            optionValue: optionValue,
            variantIds: this.variantIdsMaker(optionValue, optionTypesHash, optionType, variant)
        });
    };
    /**
     * Checks if the optionType  and the optionvalue of that type exist in OptionTypesHash
     * i.e "tsize" exists in the main hash that we are creating and corresponding "small" value exists too
     * then take arr of the variant ids and push a new id in it and return;
     * else create a new array of the varaint id and return;
     */
    VariantParserService.prototype.variantIdsMaker = function (optionValue, optionTypesHash, optionType, variant) {
        var currespondingOptionValues = this.getOtherOptionValues(optionValue, optionType);
        if (optionTypesHash[optionType.name] != null && optionTypesHash[optionType.name][optionValue.name] != null) {
            var variantArr = optionTypesHash[optionType.name][optionValue.name].variantIds;
            variantArr.push((_a = {}, _a[variant.id] = currespondingOptionValues, _a));
            return variantArr;
        }
        else {
            return Array.of((_b = {}, _b[variant.id] = currespondingOptionValues, _b));
        }
        var _a, _b;
    };
    /**
     *
     *
     * @param {any} optionValue
     * @param {any} currOptionType
     * @returns
     *
     * @memberof VariantParserService
     */
    VariantParserService.prototype.getOtherOptionValues = function (optionValue, currOptionType) {
        var correspondingOptionValues = [];
        for (var i = 0; i < this.currVariantOptionValues.length; i++) {
            if (this.currVariantOptionValues[i].option_type_name !== currOptionType.name) {
                correspondingOptionValues.push((_a = {}, _a[this.currVariantOptionValues[i].option_type_name] = this.currVariantOptionValues[i].name, _a));
            }
        }
        return correspondingOptionValues;
        var _a;
    };
    VariantParserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], VariantParserService);
    return VariantParserService;
}());



/***/ }),

/***/ "./src/app/core/services/variant-retriver.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/core/services/variant-retriver.service.ts ***!
  \***********************************************************/
/*! exports provided: VariantRetriverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariantRetriverService", function() { return VariantRetriverService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VariantRetriverService = /** @class */ (function () {
    function VariantRetriverService() {
        this.customSelectedOptions = {};
        this.currentVariantIds = [];
        this.variantId = null;
        this.variant = null;
    }
    /**
     * Note: Params could have been taken in constructor
     * due to prod-build error for constructor,
     * currently taking params from function;
     * TODO: fix this issue
     * @param:
     * currentSelectedOptions: { tsize: "small", tcolor: "red" }
     * customOptionTypesHash: {tshirt-size: Object, tshirt-color: Object}
     * currSelectedOption: { key: "Small", value: Object } => One that is recently selected;
     * product: Product
     */
    VariantRetriverService.prototype.getVariant = function (currentSelectedOptions, customOptionTypesHash, currSelectedOption, product) {
        // Set Variables
        this.currentSelectedOptions = currentSelectedOptions;
        this.customOptionTypesHash = customOptionTypesHash;
        this.currSelectedOption = currSelectedOption;
        this.product = product;
        this.setCurrentSelectedOptions();
        this.createCustomSelectedOptions();
        this.setCombinedVariantIds();
        this.getVariantId();
        this.parseVariantId();
        this.getVariantFromProduct();
        this.setCorrespondingOptions();
        return {
            newSelectedoptions: this.currentSelectedOptions,
            variant: this.variant,
            newCorrespondingOptions: this.newCorrespondingOptions
        };
    };
    /**
     * This sets current selected Options by user say (small, red, full-sleves)
     * { tsize: 'small', tcolor: 'red' }
     * if new currSelectedOptionType is tcolor and the value is blue the
     * previous one will get overidden
     * like { tsize: 'small', tcolor: 'red' } => { tsize: 'small', tcolor: 'blue' }
     *
     */
    VariantRetriverService.prototype.setCurrentSelectedOptions = function () {
        var currSelectedOptionType = this.currSelectedOption.value
            .optionValue
            .option_type_name;
        this.currentSelectedOptions[currSelectedOptionType] = this.currSelectedOption.key;
    };
    /**
     * It creates Custom selected Options extracting the options selected by user from
     *  customOptionTypesHash
     * say user selected {tsize: 'small'} this will extract the red option from
     * the global option types hash {'tsize': [small: {etc..}, large: {etc..}, medium: {etc..}]}
     *
     */
    VariantRetriverService.prototype.createCustomSelectedOptions = function () {
        /**
         * currentSelectedOptions: {tsize: 'small', tcolor: 'red'}
         * currentSelectedOptions: {} at first.. keeps
         * filling up on iteration of currentSelectedOptions
         *
         * this.customOptionTypesHash[key][this.currentSelectedOptions[key]] =
         * { optionValue: SomeObject, varaintIds: {etc etc} }
         *
         *
         ** */
        for (var key in this.currentSelectedOptions) {
            if (this.currentSelectedOptions.hasOwnProperty(key)) {
                this.customSelectedOptions[this.currentSelectedOptions[key]] =
                    this.customOptionTypesHash[key][this.currentSelectedOptions[key]];
            }
        }
        ;
    };
    /**
     * Makes a currentVaraintIds from the set of customSelectedOptions
     * {'small': {OptionValue: Object, variantIds: [etc etc]}, 'red': {...}}
     * @return: array of arrays of varaintIds
     * e.g: [[1,2,3,4], [7,8,9,1]]
     */
    VariantRetriverService.prototype.setCombinedVariantIds = function () {
        var temp = [];
        for (var key in this.customSelectedOptions) {
            // First key may be 'small' so varaiant Ids of small should be.
            // inside temp  = [vIds of small];
            if (this.customSelectedOptions.hasOwnProperty(key)) {
                // Make temp empty for each key;
                temp = [];
                this.customSelectedOptions[key].variantIds.forEach(function (obj) {
                    temp.push(Object.keys(obj)[0]);
                });
                this.currentVariantIds
                    .push(temp);
            }
        }
    };
    /**
     * Gets a Unique variantId from the selectedOptions by user
     * by intersecting the arrays in currentvaraintIds
     * Example: [[1,2,3,4], [1,5,6,7]]
     * returns [1] from which we take first index so it is 1;
     * if the array only contains [[2,3,4]] then it will return
     * the first element i.e 2;
     */
    VariantRetriverService.prototype.getVariantId = function () {
        // As scoped variable is not accessible
        // inside filter function hence the tempArr declaration;
        var tempArr = this.currentVariantIds;
        this.variantId = tempArr.shift().filter(function (v) {
            return tempArr.every(function (a) {
                return a.indexOf(v) !== -1;
            });
        })[0];
    };
    /**
     * Parses Varaint Id if null then sets the first
     * variantId in the array as the varaintId to return;
     */
    VariantRetriverService.prototype.parseVariantId = function () {
        if (this.variantId === null || this.variantId === undefined) {
            this.variantId = this.currSelectedOption.value.variantIds[0];
        }
    };
    /**
     * Gets the Varaint From the Product by using the varaintId;
     * else returns null if not present;
     */
    VariantRetriverService.prototype.getVariantFromProduct = function () {
        var _this = this;
        var result = this.product.variants
            .filter(function (v) { return v.id === parseInt(_this.variantId, 10); });
        this.variant = result ? result[0] : null;
    };
    /**
     *
     *
     *
     * @memberof VariantRetriverService
     */
    VariantRetriverService.prototype.setCorrespondingOptions = function () {
        var vIds = this.currSelectedOption.value.variantIds;
        var newObj = {};
        vIds.forEach(function (obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    obj[key].forEach(function (oType) {
                        for (var jkey in oType) {
                            if (newObj[jkey] !== undefined) {
                                newObj[jkey].push(oType[jkey]);
                            }
                            else {
                                newObj[jkey] = Array.of(oType[jkey]);
                            }
                        }
                    });
                }
            }
        });
        this.newCorrespondingOptions = newObj;
    };
    VariantRetriverService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], VariantRetriverService);
    return VariantRetriverService;
}());



/***/ }),

/***/ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"bread_crumb\">\n  <li *ngFor=\"let breadcrumb of breadcrumbs; let i = index;\">\n    <a *ngIf=\"i != breadcrumbs.length - 1; else elseBlock\" href=\"#\" class=\"crumb\">\n      <span>{{breadcrumb}}</span>\n    </a>\n    <ng-template #elseBlock>{{breadcrumb}}</ng-template>\n  </li>\n</ul>"

/***/ }),

/***/ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bread_crumb {\n  color: red;\n  position: relative;\n  margin: 25px 0;\n  font-size: 13px;\n  background-color: white !important; }\n  .bread_crumb li {\n    font-size: 13px;\n    display: inline-block;\n    float: left;\n    margin-right: 5px;\n    color: #343a40;\n    text-transform: capitalize; }\n  .bread_crumb li a {\n      color: #343a40; }\n  .bread_crumb li:after {\n    font-size: 10px;\n    content: '/';\n    margin-left: 5px; }\n  .bread_crumb li:last-child {\n    font-weight: 500;\n    margin-right: 0;\n    font-family: \"Whitney\"; }\n  .bread_crumb li:last-child:after {\n    content: '';\n    margin-right: 0; }\n  ul.bread_crumb:before, ul.bread_crumb:after {\n  content: \"\";\n  display: table; }\n"

/***/ }),

/***/ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.ts ***!
  \*******************************************************************************/
/*! exports provided: BreadcrumbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbComponent", function() { return BreadcrumbComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BreadcrumbComponent = /** @class */ (function () {
    function BreadcrumbComponent() {
        // breadcrumbs: string[] = ['Home', 'Clothing', 'Shirts', 'Men Casual Shirts']
        this.breadcrumbs = ['Home', 'Categories'];
    }
    BreadcrumbComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BreadcrumbComponent.prototype, "taxonomies", void 0);
    BreadcrumbComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-breadcrumb',
            template: __webpack_require__(/*! ./breadcrumb.component.html */ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.html"),
            styles: [__webpack_require__(/*! ./breadcrumb.component.scss */ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
}());



/***/ }),

/***/ "./src/app/home/category-page/category-page.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/home/category-page/category-page.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"selectedTaxons$ | async; let selectedTaxons\">\n  <div class=\"row\" *ngIf=\"productList$ | async; let productlist\">\n\n    <div class=\"col-md-12\">\n      <img src={{category_banner.image_link}} style=\"display: block; width: 100%;\">\n    </div>\n\n    <div class=\"col-md-2\" *ngIf=\"productlist[0].length > 0\">\n      <ul class=\"list-group\">\n        <li *ngFor=\"let taxon of selectedTaxons.taxons\">\n          <a class=\"list-group-item list-group-item-action\" [routerLink]=\"['/','search']\" [queryParams]=\"{'q[name_cont]': taxon.name, id: taxon.id}\">{{taxon.name}}</a>\n        </li>\n      </ul>\n    </div>\n    <div class=\"col-md-10 container\">\n      <div *ngFor=\"let taxon of selectedTaxons.taxons; index as taxonIndex\">\n        <div>\n          <div *ngIf=\"productlist[taxonIndex]?.length\">\n            <p>{{taxon.name}}</p>\n            <a [routerLink]=\"['/','search']\" [queryParams]=\"{'q[name_cont]': taxon.name, id: taxon.id}\">All</a>\n            <app-product-slider [productlist]=\"productlist[taxonIndex]\"></app-product-slider>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-12\" *ngIf=\"productlist[0].length === 0\">\n      <h1>No Product found for this category!</h1>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/home/category-page/category-page.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/home/category-page/category-page.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/category-page/category-page.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/home/category-page/category-page.component.ts ***!
  \***************************************************************/
/*! exports provided: CategoryPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageComponent", function() { return CategoryPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _product_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../product/reducers/selectors */ "./src/app/product/reducers/selectors.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/services/product.service */ "./src/app/core/services/product.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CategoryPageComponent = /** @class */ (function () {
    function CategoryPageComponent(route, store, productService) {
        this.route = route;
        this.store = store;
        this.productService = productService;
        this.banners = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].config.category_banner;
    }
    CategoryPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedTaxons$ =
            this.store.select(_product_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__["getTaxonomies"])
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(function (taxonomies) {
                var taxonomy = taxonomies[0];
                return _this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (params) {
                    var categeory_number = params.number;
                    _this.category_banner = _this.banners[categeory_number];
                    var taxons = [];
                    if (taxonomy) {
                        taxons = taxonomy.root.taxons;
                    }
                    ;
                    return taxons.find(function (taxon) { return taxon.name === categeory_number; });
                    ;
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (selectedTaxon) {
                    if (typeof selectedTaxon === 'undefined') {
                        return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])({});
                    }
                    ;
                    var taxons = selectedTaxon.taxons;
                    _this.productList$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["forkJoin"])(taxons.map(function (taxon) { return _this.productService.getProductsByTaxonNP(taxon.id); }));
                    return selectedTaxon;
                }));
            }));
    };
    CategoryPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category-page',
            template: __webpack_require__(/*! ./category-page.component.html */ "./src/app/home/category-page/category-page.component.html"),
            styles: [__webpack_require__(/*! ./category-page.component.scss */ "./src/app/home/category-page/category-page.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"],
            _core_services_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"]])
    ], CategoryPageComponent);
    return CategoryPageComponent;
}());



/***/ }),

/***/ "./src/app/home/content/content-header/content-header.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/home/content/content-header/content-header.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container content mb-4\" *ngIf=\"!screenWidth\">\n  <div class=\"row \">\n\n    <div itemprop=\"name\" class=\"queried-for col-7 pl-1 \">\n      <h4 title=\"men casual shirts\">Showing {{paginationInfo.count}} of {{paginationInfo.total_count}} products.\n      </h4>\n      <span>&nbsp;</span>\n    </div>\n    <div class=\"options col-5 p-0 pr-2\">\n\n      <div class=\"form-group row mb-0 \">\n        <label for=\"inputEmail3 \" class=\"col-sm-5 col-md-5 col-form-label \">Sort by</label>\n        <div class=\"btn-group\" dropdown>\n          <button dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle\" aria-controls=\"dropdown-basic\">\n            {{selectedOption}}\n          </button>\n          <ul id=\"dropdown-basic\" *dropdownMenu class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"button-basic\">\n            <li role=\"menuitem\">\n              <a class=\"dropdown-item\" (click)=sortFilter(opt.name) *ngFor=\"let opt of options\">{{opt.name}}</a>\n            </li>\n          </ul>\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"container content \" *ngIf=\"screenWidth\">\n  <div class=\"row\">\n    <div class=\"col-6 pl-1 pr-1  text-right\">\n      <button (click)=\"showModal()\" type=\"button\" class=\"search-btns col-12 p-0 px-1 py-3 shadow-sm\">\n        <i class=\"fa fa-sort mr-2\"></i>\n        <span *ngIf=\"!selectedEntry\">{{defaultselectedEntry}}</span>\n        <span *ngIf=\"selectedEntry\">{{selectedEntry.name}}</span>\n      </button>\n      <div class=\"modal fade\" bsModal #modal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-events-name\">\n        <div class=\"modal-dialog sort-mobile\">\n          <div class=\"modal-content\">\n            <div class=\"modal-body\">\n              <ul>\n                <li>\n                  <span class=\"sort\">Sort</span>\n                  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modal.hide()\">\n                    <span aria-hidden=\"true\">&times;</span>\n                  </button>\n                </li>\n                <li *ngFor=\"let entry of optionsMobile; let idx = index\">\n                  <input type=\"radio\" name=\"radiogroup\" id=\"test{{idx}}\" [checked]=\"idx === 0\" [value]=\"entry.id\" (change)=\"onSelectionChange(entry)\">\n                  <label for=\"test{{idx}}\">{{ entry.name }}</label>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-6 pr-1 pl-1 text-left\">\n      <button type=\"button\" class=\"col-12 px-1 py-3 search-btns shadow-sm\" (click)=\"staticModal.show()\">\n        <i class=\"fa fa-filter mr-2\"></i>Filter By</button>\n      <div class=\"modal fade\" bsModal #staticModal=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-static-name\">\n        <div class=\"modal-dialog sort-mobile\">\n          <div class=\"modal-content\">\n            <div class=\"modal-body\">\n              <ul>\n                <li>\n                  <span class=\"sort\">Filter</span>\n                  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"staticModal.hide()\">\n                    <span aria-hidden=\"true\">&times;</span>\n                  </button>\n                </li>\n              </ul>\n              <app-filter-mobile-menu [fillterList]=\"fillterList\"></app-filter-mobile-menu>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/home/content/content-header/content-header.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/home/content/content-header/content-header.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content .search-btns {\n  text-transform: uppercase;\n  padding: 10px 10px;\n  font-size: 1em;\n  line-height: 1em;\n  border-radius: 7px;\n  color: #000;\n  border: 1px solid #6c757d;\n  background-image: linear-gradient(to bottom, #f8f9fa 0%, #dee2e6 100%);\n  background-repeat: repeat-x; }\n\n.content .queried-for {\n  overflow: hidden;\n  font-size: 18px;\n  font-weight: 500;\n  text-transform: capitalize;\n  color: #212529;\n  display: flex;\n  align-items: baseline; }\n\n.content .queried-for .q {\n    font-weight: 900;\n    display: inline-block;\n    text-overflow: ellipsis;\n    vertical-align: top;\n    overflow: hidden;\n    white-space: nowrap;\n    max-width: 67%; }\n\n.content .queried-for span {\n    line-height: 39px;\n    color: #212529;\n    font-weight: 400;\n    font-size: 18px;\n    font-style: normal; }\n\n.content .options {\n  position: relative;\n  text-align: right;\n  display: inline-block; }\n\n.content .options .form-control,\n  .content .options .form-control:focus {\n    box-shadow: none;\n    border-color: #ced4da; }\n\n.content .options ul.img-size {\n    display: inline-block; }\n\n.content .options ul.img-size label {\n      color: #212529;\n      font-weight: 400; }\n\n.content .options ul.img-size li {\n      list-style: none;\n      display: inline-block;\n      cursor: pointer;\n      margin: 0 4px -2px;\n      width: 19px;\n      height: 13px;\n      background: url('search-sprite.png') no-repeat transparent; }\n\n.content .options ul.img-size li.big {\n        background-position: -284px -160px; }\n\n.content .options ul.img-size li.big.selected {\n          cursor: default;\n          background-position: -284px -180px; }\n\n.content .options ul.img-size li.small {\n        background-position: -311px -160px; }\n\n.content .options ul.img-size li.small.selected {\n          background-position: -311px -180px;\n          cursor: default; }\n\n.content .options ul.sort {\n    display: inline-block;\n    position: relative;\n    color: #212529; }\n\n.content .options ul.sort label {\n      margin-left: 5px;\n      font-weight: 400; }\n\n.content .options ul.sort label:first-child {\n      color: #212529; }\n\n.content .options ul.sort li {\n      display: inline-block;\n      margin-left: 5px;\n      text-decoration: none;\n      cursor: pointer; }\n\n.content .options ul.sort li:after {\n      content: ' ';\n      display: inline-block;\n      height: 11px;\n      width: 1px;\n      border-left: 1px solid #ced4da;\n      margin-left: 5px;\n      margin-bottom: -1px; }\n\n.content .options ul.sort label {\n      font-weight: 400;\n      margin-left: 5px; }\n\n.content .options ul.sort label li:first-child {\n        margin-left: 0; }\n\n.content .options ul.sort label li:after {\n        content: none; }\n\n.content .options ul.sort:before,\n  .content .options ul.sort:after {\n    content: \"\";\n    display: table; }\n\n.content .options:before,\n.content .options:after {\n  content: \"\";\n  display: table; }\n\n.sort-mobile {\n  width: 100.3%;\n  max-width: 300%;\n  margin: -1px 0px 0px -0.6px;\n  height: 100vh; }\n\n.sort-mobile .modal-content {\n    border-radius: 0px;\n    box-shadow: none;\n    height: 100vh; }\n\n[type=\"radio\"]:checked,\n[type=\"radio\"]:not(:checked) {\n  position: absolute;\n  left: -9999px; }\n\n[type=\"radio\"]:checked + label,\n[type=\"radio\"]:not(:checked) + label {\n  position: relative;\n  padding-left: 40px;\n  cursor: pointer;\n  line-height: 20px;\n  display: inline-block;\n  color: #212529; }\n\n[type=\"radio\"]:checked + label:before,\n[type=\"radio\"]:not(:checked) + label:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  top: -4px;\n  width: 25px;\n  height: 25px;\n  border: 1px solid #ccc;\n  border-radius: 100%;\n  background: #fff; }\n\n[type=\"radio\"]:checked + label:after,\n[type=\"radio\"]:not(:checked) + label:after {\n  content: \"\";\n  content: \"\";\n  width: 12px;\n  height: 12px;\n  background: #ffe364;\n  position: absolute;\n  top: 2.7px;\n  left: 7px;\n  border-radius: 100%;\n  transition: all 0.2s ease; }\n\n[type=\"radio\"]:not(:checked) + label:after {\n  opacity: 0;\n  -webkit-transform: scale(0);\n  transform: scale(0); }\n\n[type=\"radio\"]:checked + label:after {\n  opacity: 1;\n  -webkit-transform: scale(1);\n  transform: scale(1); }\n\nul {\n  list-style: none;\n  padding: 0 0 0.5rem 0;\n  margin: 0px; }\n\nul li {\n    padding: 0.8rem 1.1rem;\n    border-bottom: 1px solid #ccc;\n    color: #212529;\n    cursor: pointer;\n    text-align: left; }\n\nul li .sort {\n      font-size: 1.5em;\n      font-weight: bold;\n      color: #212529; }\n"

/***/ }),

/***/ "./src/app/home/content/content-header/content-header.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/home/content/content-header/content-header.component.ts ***!
  \*************************************************************************/
/*! exports provided: ContentHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentHeaderComponent", function() { return ContentHeaderComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContentHeaderComponent = /** @class */ (function () {
    function ContentHeaderComponent(routernomal) {
        this.routernomal = routernomal;
        this.toggleSize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.options = [
            { name: 'Newest', value: 1 },
            { name: 'A To Z', value: 2 },
            { name: 'Z To A', value: 3 },
            { name: 'Relevence', value: 4 }
        ];
        this.optionsMobile = [
            { name: 'Newest', value: 1 },
            { name: 'A To Z', value: 2 },
            { name: 'Z To A', value: 3 },
            { name: 'Relevence', value: 4 }
        ];
        this.queryMap = {
            Newest: 'updated_at+asc',
            'A To Z': 'name+asc',
            'Z To A': 'name+desc',
            Relevance: '',
        };
        this.selectedOption = 'Relevance';
        this.selectedSize = 'COZY';
        this.searchKeyword = '';
        this.defaultselectedEntry = 'Relevance';
    }
    ContentHeaderComponent.prototype.showModal = function () {
        this.modal.show();
    };
    ContentHeaderComponent.prototype.onSelectionChange = function (entry) {
        this.selectedEntry = entry;
        this.sortFilter(this.selectedEntry.name);
        this.modal.hide();
    };
    ContentHeaderComponent.prototype.ngOnInit = function () {
        if (window.screen.width <= 768) {
            this.screenWidth = window.screen.width;
        }
    };
    ContentHeaderComponent.prototype.toggleView = function (view) {
        this.selectedSize = view;
        this.toggleSize.emit({ size: view });
    };
    ContentHeaderComponent.prototype.isSmallSelected = function () {
        return this.selectedSize === 'COZY';
    };
    ContentHeaderComponent.prototype.isBigSelected = function () {
        return this.selectedSize === 'COMPACT';
    };
    ContentHeaderComponent.prototype.sortFilter = function (i) {
        var urlTree = this.routernomal.createUrlTree([], {
            queryParams: { 'q[s]': this.queryMap[i] },
            queryParamsHandling: 'merge',
            preserveFragment: true,
        });
        this.routernomal.navigateByUrl(urlTree);
        this.selectedOption = i;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        __metadata("design:type", Object)
    ], ContentHeaderComponent.prototype, "toggleSize", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ContentHeaderComponent.prototype, "paginationInfo", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ContentHeaderComponent.prototype, "fillterList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["ModalDirective"]),
        __metadata("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["ModalDirective"])
    ], ContentHeaderComponent.prototype, "modal", void 0);
    ContentHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-content-header',
            template: __webpack_require__(/*! ./content-header.component.html */ "./src/app/home/content/content-header/content-header.component.html"),
            styles: [__webpack_require__(/*! ./content-header.component.scss */ "./src/app/home/content/content-header/content-header.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]])
    ], ContentHeaderComponent);
    return ContentHeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/content/content.ts":
/*!*****************************************!*\
  !*** ./src/app/home/content/content.ts ***!
  \*****************************************/
/*! exports provided: ContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentComponent", function() { return ContentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContentComponent = /** @class */ (function () {
    function ContentComponent() {
        this.toggleLayout = { size: 'COZY' };
    }
    ContentComponent.prototype.ngOnInit = function () {
    };
    ContentComponent.prototype.toggleSize = function (layoutInfo) {
        this.toggleLayout = layoutInfo;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ContentComponent.prototype, "productsList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ContentComponent.prototype, "paginationData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ContentComponent.prototype, "taxonIds", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ContentComponent.prototype, "fillterList", void 0);
    ContentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-content',
            template: "\n   <div *ngIf='paginationData.total_count > 0'>\n    <app-content-header (toggleSize)=\"toggleSize($event)\"\n     [paginationInfo]=\"paginationData\" [fillterList]=\"fillterList\"></app-content-header>\n    <app-product-list [(toggleLayout)]='toggleLayout' [products]='productsList' [paginationData]='paginationData' [taxonIds]=\"taxonIds\"></app-product-list>\n    </div>\n  ",
        }),
        __metadata("design:paramtypes", [])
    ], ContentComponent);
    return ContentComponent;
}());



/***/ }),

/***/ "./src/app/home/content/customize/customize.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/home/content/customize/customize.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"customize\">\n  <!--<ul class=\"properties\">\n    <li data-order=\"0\" data-filter=\"size_facet\" style=\"\" class=\"selected\">Sizes</li>\n    <li data-order=\"2\" data-filter=\"tag_coupon\" style=\"display: none\">Offers</li>\n    <li data-order=\"3\" data-filter=\"age_facet\" style=\"display: none\">Age</li>\n    <li data-order=\"4\" data-filter=\"Shirt_Length_article_attr\" style=\"\">Shirt Length</li>\n    <li data-order=\"5\" data-filter=\"Sleeve_Length_article_attr\" style=\"\">Sleeve Length</li>\n    <li data-order=\"6\" data-filter=\"Hemline_article_attr\" style=\"\">Hemline</li>\n    <li data-order=\"7\" data-filter=\"Pattern_article_attr\" style=\"\">Pattern</li>\n    <li data-order=\"8\" data-filter=\"Collar_article_attr\" style=\"\">Collar</li>\n    <li data-order=\"9\" data-filter=\"Fabric_article_attr\" style=\"\">Fabric</li>\n    <li data-order=\"10\" data-filter=\"Pattern_Size_article_attr\" style=\"\">Pattern Size</li>\n    <li data-order=\"11\" data-filter=\"Weave_Pattern_article_attr\" style=\"\">Weave Pattern</li>\n    <li data-order=\"12\" data-filter=\"Print_or_Pattern_Type_article_attr\" style=\"\">Print or Pattern Type</li>\n    <li data-order=\"13\" data-filter=\"Occasion_article_attr\" style=\"\">Occasion</li>\n    <li data-order=\"14\" data-filter=\"Fit_article_attr\" style=\"\">Fit</li>\n  </ul>-->\n  <div class=\"allOptions\">\n    <!--<ul data-filter=\"size_facet\" style=\"\" class=\"options\">\n      <li data-filter=\"size_facet\" data-option=\"38\" data-num=\"456\" data-colorhex=\"38\" class=\"option\">\n        <label title=\"38\" class=\"selected\">\n          <input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"38\" class=\"checkbox\">38<span class=\"num\">(456)</span>\n          </label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"39\" data-num=\"387\" data-colorhex=\"39\" class=\"option\">\n        <label title=\"39\" class=\"selected\">\n          <input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"39\" class=\"checkbox\">39<span class=\"num\">(387)</span>\n        </label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"40\" data-num=\"1020\" data-colorhex=\"40\" class=\"option\">\n        <label title=\"40\">\n          <input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"40\" class=\"checkbox\">40<span class=\"num\">(1020)</span>\n        </label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"42\" data-num=\"1082\" data-colorhex=\"42\" class=\"option\">\n        <label title=\"42\">\n          <input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"42\" class=\"checkbox\">42<span class=\"num\">(1082)</span>\n        </label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"43\" data-num=\"1\" data-colorhex=\"43\" class=\"option\">\n        <label title=\"43\">\n          <input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"43\" class=\"checkbox\">43<span class=\"num\">(1)</span>\n        </label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"44\" data-num=\"1318\" data-colorhex=\"44\" class=\"option\"><label title=\"44\"><input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"44\" class=\"checkbox\">44<span class=\"num\">(1318)</span></label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"45\" data-num=\"1\" data-colorhex=\"45\" class=\"option\"><label title=\"45\"><input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"45\" class=\"checkbox\">45<span class=\"num\">(1)</span></label>\n      </li>\n      <li data-filter=\"size_facet\" data-option=\"46\" data-num=\"184\" data-colorhex=\"46\" class=\"option\"><label title=\"46\"><input type=\"checkbox\" data-filter=\"size_facet\" data-option=\"46\" class=\"checkbox\">46<span class=\"num\">(184)</span></label>\n      </li>\n    </ul>-->\n    <!-- Add provision for more filter options later after this -->\n    <!--<ul data-filter=\"Shirt_Length_article_attr\" style=\"display:none;\" class=\"options\">\n      <li data-filter=\"Shirt_Length_article_attr\" data-option=\"regular\" data-num=\"1662\" data-colorhex=\"regular\" class=\"option\">\n        <label title=\"regular\">\n          <input type=\"checkbox\" data-filter=\"Shirt_Length_article_attr\" data-option=\"regular\" class=\"checkbox\">regular<span class=\"num\">(1662)</span>\n        </label>\n      </li>\n    </ul>-->\n     \n  </div>    \n</div>"

/***/ }),

/***/ "./src/app/home/content/customize/customize.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/home/content/customize/customize.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".customize ul.properties {\n  margin-left: -40px;\n  display: block;\n  position: relative; }\n  .customize ul.properties li {\n    position: relative;\n    cursor: pointer;\n    font-size: 14px;\n    float: left;\n    margin-right: 32px;\n    margin-bottom: 5px;\n    border-bottom: 2px solid #fff;\n    display: inline-block;\n    color: #343a40;\n    list-style: none; }\n  .customize ul.properties li.selected:after {\n      height: 2px;\n      width: 100%;\n      position: absolute;\n      background-color: #ffe364;\n      content: ' ';\n      top: 108%;\n      left: 0; }\n  .customize ul.properties li:not(.selected):hover:after {\n    height: 2px;\n    width: 100%;\n    position: absolute;\n    background-color: #343a40;\n    content: ' ';\n    top: 108%;\n    left: 0; }\n  .customize ul.properties:before, .customize ul.properties:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n  .customize .allOptions {\n  display: block;\n  margin-top: 12px;\n  position: relative;\n  padding-top: 10px;\n  border-top: 1px dotted #ccc; }\n  .customize .allOptions ul.options {\n    padding-left: 0px;\n    text-align: right;\n    margin: 5px 0 0 5px;\n    text-transform: capitalize;\n    display: block;\n    border-bottom: 1px dotted #ccc;\n    max-height: 76px;\n    position: relative;\n    padding-bottom: 5px;\n    margin-bottom: 15px;\n    overflow: hidden;\n    float: none; }\n  .customize .allOptions ul.options li {\n      margin: 0 2px 2px 0;\n      display: inline-block;\n      width: 120px;\n      font-size: 14px;\n      text-align: left;\n      color: #495057;\n      float: left;\n      list-style: none; }\n  .customize .allOptions ul.options li label {\n        overflow: hidden;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        position: relative;\n        cursor: pointer;\n        display: block;\n        font-weight: 400;\n        height: 19px;\n        line-height: 19px;\n        vertical-align: top; }\n  .customize .allOptions ul.options li label.selected:before {\n          background-position: -237px -138px; }\n  .customize .allOptions ul.options li label input.checkbox, .customize .allOptions ul.options li label input.radio {\n          display: inline-block !important;\n          visibility: hidden;\n          margin: 0 12px 0 0;\n          width: 8px;\n          height: 8px; }\n  .customize .allOptions ul.options li label .num {\n          display: none; }\n  .customize .allOptions ul.options li label:before {\n        background: url('search-sprite.png') no-repeat -237px -110px transparent;\n        content: ' ';\n        height: 13px;\n        width: 13px;\n        display: block;\n        visibility: visible;\n        top: 4px;\n        position: absolute; }\n  .customize .allOptions ul.options li label:not(.selected):hover:before {\n        background-position: -237px -124px; }\n  .customize .allOptions ul.options:before, .customize .allOptions ul.options:after {\n    content: \"\";\n    display: table; }\n"

/***/ }),

/***/ "./src/app/home/content/customize/customize.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/home/content/customize/customize.component.ts ***!
  \***************************************************************/
/*! exports provided: CustomizeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomizeComponent", function() { return CustomizeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CustomizeComponent = /** @class */ (function () {
    function CustomizeComponent() {
    }
    CustomizeComponent.prototype.ngOnInit = function () {
    };
    CustomizeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customize',
            template: __webpack_require__(/*! ./customize.component.html */ "./src/app/home/content/customize/customize.component.html"),
            styles: [__webpack_require__(/*! ./customize.component.scss */ "./src/app/home/content/customize/customize.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], CustomizeComponent);
    return CustomizeComponent;
}());



/***/ }),

/***/ "./src/app/home/content/filter-summary/filter-summary.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/home/content/filter-summary/filter-summary.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"filter-summary\">\n  <div class=\"filter\">\n    <div \n      *ngFor=\"let filter of filters$ | async\" \n      class=\"option\"\n      (click)=\"removeFilter(filter)\">{{ filter.name }}\n    </div>\n    <!--<div data-filter=\"brands_filter_facet\" data-option=\"Wills Lifestyle\" data-colorhex=\"\" class=\"option\">Wills Lifestyle</div>-->\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/home/content/filter-summary/filter-summary.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/home/content/filter-summary/filter-summary.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".filter-summary {\n  margin: 10px 0 5px;\n  height: 20px; }\n  .filter-summary .filter {\n    margin-right: 10px;\n    float: left; }\n  .filter-summary .filter .option {\n      display: block;\n      float: left;\n      position: relative;\n      background-color: #ccc;\n      text-transform: capitalize;\n      color: #495057;\n      padding: 2px 10px 2px 25px;\n      margin: 0 5px 5px 0;\n      cursor: pointer;\n      transition: all .2s ease-out; }\n  .filter-summary .filter .option:hover {\n      background: #ccc;\n      text-decoration: line-through; }\n  .filter-summary .filter .option:before {\n      content: ' ';\n      background: url('search-sprite.png') no-repeat -281px -70px;\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 10px;\n      height: 10px;\n      top: 7px;\n      left: 8px; }\n  .filter-summary .filter:before,\n  .filter-summary .filter:after {\n    content: \"\";\n    display: table; }\n  .filter-summary:before,\n.filter-summary:after {\n  content: \"\";\n  display: table; }\n"

/***/ }),

/***/ "./src/app/home/content/filter-summary/filter-summary.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/home/content/filter-summary/filter-summary.component.ts ***!
  \*************************************************************************/
/*! exports provided: FilterSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterSummaryComponent", function() { return FilterSummaryComponent; });
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../reducers/selectors */ "./src/app/home/reducers/selectors.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterSummaryComponent = /** @class */ (function () {
    function FilterSummaryComponent(store, search) {
        this.store = store;
        this.search = search;
        this.filters$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getFilters"]);
    }
    FilterSummaryComponent.prototype.ngOnInit = function () {
    };
    FilterSummaryComponent.prototype.removeFilter = function (removedFilter) {
        this.store.dispatch(this.search.removeFilter(removedFilter));
    };
    FilterSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-filter-summary',
            template: __webpack_require__(/*! ./filter-summary.component.html */ "./src/app/home/content/filter-summary/filter-summary.component.html"),
            styles: [__webpack_require__(/*! ./filter-summary.component.scss */ "./src/app/home/content/filter-summary/filter-summary.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"]])
    ], FilterSummaryComponent);
    return FilterSummaryComponent;
}());



/***/ }),

/***/ "./src/app/home/content/product-list/product-filter.pipe.ts":
/*!******************************************************************!*\
  !*** ./src/app/home/content/product-list/product-filter.pipe.ts ***!
  \******************************************************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Filter the products based on selected taxons in the sidebar
 * @name filter
 * @param selectedTaxonids
 */
var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (products, selectedTaxonIds) {
        var selectedIds = selectedTaxonIds;
        if (!products) {
            return [];
        }
        if (!selectedIds || selectedIds.length === 0) {
            return products;
        }
        return products.filter(function (product) {
            var productPresent = false;
            selectedIds.forEach(function (id) {
                if (product.taxon_ids.findIndex(function (taxon_id) { return taxon_id === id; }) !== -1) {
                    productPresent = true;
                }
            });
            return productPresent;
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/home/content/product-list/product-list-item/product-list-item.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list-item/product-list-item.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"product-tile\">\n  <p> Product id: {{ product.id }} </p>\n  <p> Product Name : {{ product.name }} </p>\n  <p> Product Price : {{ product.price }} </p>\n  <img [src]=\"getProductImageUrl(product.master.images[0]?.product_url)\">\n</div>"

/***/ }),

/***/ "./src/app/home/content/product-list/product-list-item/product-list-item.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list-item/product-list-item.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".product-tile {\n  margin: 10px;\n  border: solid 1px gray; }\n  .product-tile img {\n    -o-object-fit: cover;\n       object-fit: cover;\n    width: 100%; }\n"

/***/ }),

/***/ "./src/app/home/content/product-list/product-list-item/product-list-item.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list-item/product-list-item.component.ts ***!
  \********************************************************************************************/
/*! exports provided: ProductListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListItemComponent", function() { return ProductListItemComponent; });
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductListItemComponent = /** @class */ (function () {
    function ProductListItemComponent() {
    }
    ProductListItemComponent.prototype.ngOnInit = function () {
    };
    ProductListItemComponent.prototype.getProductImageUrl = function (url) {
        return url;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_0__["Product"])
    ], ProductListItemComponent.prototype, "product", void 0);
    ProductListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-list-item',
            template: __webpack_require__(/*! ./product-list-item.component.html */ "./src/app/home/content/product-list/product-list-item/product-list-item.component.html"),
            styles: [__webpack_require__(/*! ./product-list-item.component.scss */ "./src/app/home/content/product-list/product-list-item/product-list-item.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], ProductListItemComponent);
    return ProductListItemComponent;
}());



/***/ }),

/***/ "./src/app/home/content/product-list/product-list.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row results-base m-0\" *ngIf=\"!isMobile\">\n  <div class=\"col-12 col-md-3 col-sm-3 col-lg-3 product-base\" *ngFor=\"let product of products | filter : selectedTaxonIds; trackBy: trackByFn\"\n    [style.margin]=\"getMargin()\" itemscope itemtype=\"https://schema.org/Product\">\n    <a itemprop=\"url\" [routerLink]=\"['/', product.slug]\">\n      <img itemprop=\"image\" class=\"product-thumb\" alt=\"{{product.name}}\" [src]=\"product.product_url\">\n      <div class=\"product-productMetaInfo\">\n        <div class=\"morechoices\">\n          More Choices Available\n        </div>\n        <div itemprop=\"name\" class=\"product-brand\">{{ product.name }}</div>\n        <div itemprop=\"offers\" itemscope itemtype=\"https://schema.org/Offer\" class=\"product-price\">\n          <span>\n            <span itemprop=\"price\" class=\"product-discountedPrice\">{{product.currency}} {{ product.price }}</span>\n            <span *ngIf=\"product.cost_price; let cost_price\">\n              <span itemprop=\"price\" class=\"disableprize\">{{product.currency}}{{cost_price}}</span>\n            </span>\n          </span>\n        </div>\n        <p class=\"shipping\">\n          FREE 1-2 day shipping on this item\n        </p>\n        <div class=\"ngrating my-3\">\n          <ngx-input-star-rating disabled=\"true\" value=\"{{product.avg_rating }}\"></ngx-input-star-rating>\n        </div>\n      </div>\n    </a>\n  </div>\n</div>\n\n\n<div class=\"col-12\">\n  <div class=\"row\">\n    <pagination *ngIf=\"paginationData.total_count > paginationData.per_page\" [totalItems]=\"paginationData.total_count\" [itemsPerPage]=\"paginationData.per_page\"\n      [(ngModel)]=\"paginationData.current_page\" (pageChanged)=\"pageChanged($event)\" class=\"m-auto\"></pagination>\n  </div>\n\n</div>\n\n\n<div class=\"row results-base m-0\" *ngIf=\"isMobile\">\n  <div class=\"col-12 product-base\" *ngFor=\"let product of products | filter : selectedTaxonIds; trackBy: trackByFn\" [style.margin]=\"getMargin()\"\n    itemscope itemtype=\"https://schema.org/Product\">\n    <a itemprop=\"url\" [routerLink]=\"['/', product.slug]\">\n      <div class=\"row\">\n        <div class=\"col-5 p-0 searchimg\">\n          <img itemprop=\"image\" class=\"product-thumb img-fluid\" alt=\"{{product.name}}\" [src]=\"product.product_url\">\n          <div class=\"morechoices\">\n            More Choices Available\n          </div>\n        </div>\n        <div class=\"col-7 p-0\">\n          <div class=\"product-productMetaInfo\">\n            <div itemprop=\"name\" class=\"product-brand\">{{ product.name }}</div>\n            <div itemprop=\"offers\" itemscope itemtype=\"https://schema.org/Offer\" class=\"product-price\">\n              <span>\n                <span itemprop=\"price\" class=\"product-discountedPrice\">{{product.currency}} {{ product.price }}</span>\n                <span *ngIf=\"product.cost_price; let cost_price\">\n                  <span itemprop=\"price\" class=\"disableprize\">{{product.currency}}{{cost_price}}</span>\n                </span>\n              </span>\n            </div>\n            <p class=\"shipping\">\n              FREE 1-2 day shipping on this item\n            </p>\n            <div class=\"ngrating\">\n              <ngx-input-star-rating disabled=\"true\" value=\"{{product.avg_rating }}\"></ngx-input-star-rating>\n            </div>\n          </div>\n        </div>\n      </div>\n    </a>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/home/content/product-list/product-list.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".product-thumbShim {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 101%;\n  height: 100%;\n  background-color: #212529;\n  transition: visibility 0s, opacity .5s linear;\n  visibility: hidden;\n  opacity: 1; }\n\n.product-base {\n  flex: 0 0 23.3%;\n  max-width: 23.3%;\n  text-align: left;\n  position: relative;\n  background-color: #fff;\n  border: 1px solid #ccc;\n  box-shadow: 0 1px 0 #d4d4d4;\n  border-radius: 5px;\n  transition: border .2s linear; }\n\n@media screen and (min-width: 320px) and (max-width: 767px) {\n    .product-base {\n      margin: 0px !important;\n      flex: 0 0 100%;\n      max-width: 100%;\n      border: 0;\n      border-radius: 0;\n      border-bottom: 1px solid #ccc;\n      display: block;\n      padding: 1em; } }\n\n@media screen and (width: 768px) {\n    .product-base {\n      flex: 0 0 30.5%;\n      max-width: 30.5%; } }\n\n.product-base:hover {\n    box-shadow: 0px 0px 3px #ccc; }\n\n.product-base a {\n    text-decoration: none;\n    background-color: transparent;\n    color: #212529; }\n\n.product-base a .searchimg {\n      height: 220px; }\n\n.product-base a .product-thumb {\n      height: 240px;\n      display: block;\n      background-color: transparent;\n      opacity: 1;\n      width: 100%;\n      -o-object-fit: contain;\n         object-fit: contain; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n        .product-base a .product-thumb {\n          height: 80%; } }\n\n.product-base a .product-productMetaInfo {\n      position: relative;\n      background: #fff;\n      padding: 0 5px; }\n\n.product-base a .product-productMetaInfo .ngrating {\n        position: relative;\n        left: -10px; }\n\n.product-base a .product-productMetaInfo .product-brand,\n      .product-base a .product-productMetaInfo .product-product {\n        overflow: hidden;\n        font-size: 1em;\n        font-weight: 500;\n        line-height: 1.5em;\n        color: #343a40;\n        text-transform: capitalize;\n        margin-top: 10px;\n        height: 4.6vw; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n          .product-base a .product-productMetaInfo .product-brand,\n          .product-base a .product-productMetaInfo .product-product {\n            height: auto; } }\n\n.product-base a .product-productMetaInfo .product-price {\n        font-size: 1.3em;\n        line-height: 1.2em;\n        color: #cd6100;\n        margin: 10px 0 0;\n        white-space: nowrap;\n        font-weight: 500; }\n\n.morechoices {\n  background-color: #495057;\n  color: #fff;\n  border-radius: 5px;\n  margin-top: 10px;\n  padding: .2rem;\n  text-align: center; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n    .morechoices {\n      background-color: #fff;\n      color: #343a40;\n      font-size: 1em;\n      margin-top: 0px; } }\n\n.shipping {\n  font-size: 0.8em;\n  font-weight: 400;\n  text-align: left;\n  margin-top: 15px;\n  color: #000;\n  margin-bottom: 0px; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n    .shipping {\n      margin-bottom: 10px; } }\n\n.disableprize {\n  font-size: 1rem;\n  line-height: 1;\n  color: #495057;\n  text-decoration: line-through;\n  margin-left: 10px; }\n"

/***/ }),

/***/ "./src/app/home/content/product-list/product-list.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/home/content/product-list/product-list.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProductListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListComponent", function() { return ProductListComponent; });
/* harmony import */ var _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../checkout/actions/checkout.actions */ "./src/app/checkout/actions/checkout.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductListComponent = /** @class */ (function () {
    function ProductListComponent(store, checkoutActions, router, routernomal) {
        var _this = this;
        this.store = store;
        this.checkoutActions = checkoutActions;
        this.router = router;
        this.routernomal = routernomal;
        this.router.queryParams
            .subscribe(function (params) {
            _this.queryParams = params;
        });
    }
    ProductListComponent.prototype.ngOnInit = function () {
        this.screenwidth = window.innerWidth;
        this.calculateInnerWidth();
    };
    ProductListComponent.prototype.calculateInnerWidth = function () {
        if (this.screenwidth <= 1000) {
            this.isMobile = this.screenwidth;
        }
    };
    ProductListComponent.prototype.getMargin = function () {
        return this.toggleLayout.size === 'COZY' ? '0 7.5px 20px 7.5px' : '0 80px 20px 0';
    };
    ProductListComponent.prototype.trackByFn = function (index, item) {
        return index;
    };
    ProductListComponent.prototype.pageChanged = function (event) {
        this.page = event.page;
        var urlTree = this.routernomal.createUrlTree([], {
            queryParams: { page: this.page },
            queryParamsHandling: 'merge',
            preserveFragment: true
        });
        this.routernomal.navigateByUrl(urlTree);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        __metadata("design:type", Object)
    ], ProductListComponent.prototype, "products", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        __metadata("design:type", Object)
    ], ProductListComponent.prototype, "paginationData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])('taxonIds'),
        __metadata("design:type", Object)
    ], ProductListComponent.prototype, "selectedTaxonIds", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        __metadata("design:type", Object)
    ], ProductListComponent.prototype, "toggleLayout", void 0);
    ProductListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-product-list',
            template: __webpack_require__(/*! ./product-list.component.html */ "./src/app/home/content/product-list/product-list.component.html"),
            styles: [__webpack_require__(/*! ./product-list.component.scss */ "./src/app/home/content/product-list/product-list.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"],
            _checkout_actions_checkout_actions__WEBPACK_IMPORTED_MODULE_0__["CheckoutActions"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProductListComponent);
    return ProductListComponent;
}());



/***/ }),

/***/ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/home/filter-mobile-menu/filter-mobile-menu.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex  my-flex-container\" *ngIf=\"fillterList[0]; let catgeory_taxonomy\">\n\n  <div class=\"p-0 no-gutters\" class=\"parrent-catgory\">\n    <ul class=\"filter\">\n      <li *ngFor=\"let taxon of catgeory_taxonomy.root.taxons\" [ngClass]=\"{'active': selectedItem == taxon.name}\" (click)=\"listClick($event, taxon.name)\">\n        <a>{{taxon.name}}</a>\n        <ul>\n          <li *ngFor=\"let subtaxon of taxon.taxons\">\n            <a>{{subtaxon.name}}</a>\n            <ul>\n              <li *ngFor=\"let subsubtaxon of subtaxon.taxons\">\n                <a>{{ subsubtaxon.name }}</a>\n              </li>\n            </ul>\n          </li>\n        </ul>\n      </li>\n    </ul>\n  </div>\n  <span class=\"badge badge-secondary center\"></span>\n</div>"

/***/ }),

/***/ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/home/filter-mobile-menu/filter-mobile-menu.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\n  list-style: none;\n  padding: 0.5rem 0 0.5rem 0;\n  margin: 0px; }\n  ul li a {\n    padding: 0.8rem 1.1rem;\n    border-bottom: 1px solid #ccc;\n    color: #212529;\n    display: block;\n    cursor: pointer; }\n  ul li a::after {\n      font-family: 'FontAwesome';\n      content: \"\\f105\";\n      width: 20px;\n      height: 57px;\n      margin-right: 12px;\n      right: 0px;\n      position: absolute;\n      background-size: 10px; }\n  ul li ul {\n    display: none; }\n  ul li ul ul {\n      display: none; }\n  ul .active ul {\n    display: block; }\n  .parrent-catgory, .sub-catgory, .sub-sub-catgory {\n  width: 100vw;\n  left: 0vw;\n  position: relative; }\n  .sub-catgory {\n  left: -100vw; }\n  .radio-pink [type=\"radio\"]:checked + label:after {\n  border-color: #ff9314;\n  background-color: #ff9314; }\n  /*Gap*/\n  .radio-pink-gap [type=\"radio\"].with-gap:checked + label:before {\n  border-color: #ff9314; }\n  .radio-pink-gap [type=\"radio\"]:checked + label:after {\n  border-color: #ff9314;\n  background-color: #ff9314; }\n"

/***/ }),

/***/ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/home/filter-mobile-menu/filter-mobile-menu.component.ts ***!
  \*************************************************************************/
/*! exports provided: FilterMobileMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterMobileMenuComponent", function() { return FilterMobileMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterMobileMenuComponent = /** @class */ (function () {
    function FilterMobileMenuComponent() {
        this.showParrent = false;
        this.showChild = false;
        this.backBtnShow = false;
    }
    FilterMobileMenuComponent.prototype.showCategory = function (taxon) {
        this.menuTaxons = taxon.taxons;
    };
    Object.defineProperty(FilterMobileMenuComponent.prototype, "stateName", {
        get: function () {
            return this.showParrent ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilterMobileMenuComponent.prototype, "stateName1", {
        get: function () {
            return this.showChild ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    FilterMobileMenuComponent.prototype.showSubCategory = function (i) {
        this.showChild = !this.showChild;
        this.subChild = this.menuTaxons.taxons[i];
    };
    FilterMobileMenuComponent.prototype.parrentBack = function () {
        this.showParrent = !this.showParrent;
    };
    FilterMobileMenuComponent.prototype.childBack = function () {
        this.showChild = !this.showChild;
    };
    FilterMobileMenuComponent.prototype.listClick = function (event, newValue) {
        this.selectedItem = newValue;
    };
    FilterMobileMenuComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FilterMobileMenuComponent.prototype, "fillterList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FilterMobileMenuComponent.prototype, "isScrolled", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FilterMobileMenuComponent.prototype, "screenwidth", void 0);
    FilterMobileMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filter-mobile-menu',
            template: __webpack_require__(/*! ./filter-mobile-menu.component.html */ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.html"),
            styles: [__webpack_require__(/*! ./filter-mobile-menu.component.scss */ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.scss")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('popOverState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -100 + 'vw'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: 0
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in'))
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('subCatgory', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -200 + 'vw'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -100
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [])
    ], FilterMobileMenuComponent);
    return FilterMobileMenuComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pagination$ | async; let paginationData\">\n  <div  class=\"container col-12 home\" *ngIf=\"paginationData.total_count > 0\">\n    <div class=\"contaier\" *ngIf=\"taxonomies$ | async; let taxonomies\">\n      <div class=row>\n        <div class=\"col-lg-3 sidebar d-sm-none d-xs-none d-none d-lg-block\">\n          <h1>Searched Results</h1>\n          <!-- <app-taxons [taxonomies]=\"taxonomies$ | async\"></app-taxons> -->\n          <app-categories [taxonomiList]=\"taxonomies\" (onSelected)=\"OnCategeorySelected($event)\" (showAll)=\"showAll()\" [isFilterOn]=\"isFilterOn$ | async\"\n            [categoryLevel]=\"categoryLevel$ | async\">\n          </app-categories>\n          <app-brand-filter [taxonomiList]=\"brands$ | async\" [isFilterOn]=\"isFilterOn$ | async\"></app-brand-filter>\n        </div>\n        <div class=\"col-lg-9 col-12 col-sm-12\" [ngClass]=\"[isMobile ? 'p-0' : '']\">\n          <app-content [productsList]=\"products$ | async\" [paginationData]=\"paginationData\" [fillterList]=\"taxonomies\" class=\"col-12\">\n            <!-- [taxonIds]=\"selectedTaxonIds$ | async\"  -->\n          </app-content>\n        </div>\n      </div>\n    </div>\n  </div>\n  <!-- Need to change it  -->\n  <div *ngIf='paginationData.total_count <= 0'>\n    <h2 style=\"padding-top: 100px;\n    /* padding-right: 14px; */padding-left: 500px;\">No prodcuts found</h2>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".home {\n  background-color: #e9ecef; }\n  .home .sidebar {\n    background-color: #f8f9fa;\n    padding: 3.5vh 2vw; }\n  @media screen and (width: 768px) {\n      .home .sidebar {\n        padding: 2.5vh 2vw; } }\n  .home h1 {\n    font-size: 1.7em;\n    color: #212529; }\n  .filter-mobile-menu {\n  width: 100Vw;\n  margin: 0px;\n  margin-top: 20vh; }\n  .filter-mobile-menu .modal-content {\n    border: 0px solid #f8f9fa;\n    border-radius: 0px; }\n  .filter-mobile-menu accordion-group {\n    cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reducers/selectors */ "./src/app/home/reducers/selectors.ts");
/* harmony import */ var _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../product/actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var _product_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../product/reducers/selectors */ "./src/app/product/reducers/selectors.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomeComponent = /** @class */ (function () {
    function HomeComponent(store, actions, searchActions) {
        var _this = this;
        this.store = store;
        this.actions = actions;
        this.searchActions = searchActions;
        this.isBrandOpen = false;
        this.isCategoryOpen = true;
        // tslint:disable-next-line:member-ordering
        this.isModalShown = false;
        this.store.dispatch(this.actions.getAllProducts(1));
        this.store.dispatch(this.actions.getAllTaxonomies());
        this.taxonomies$ = this.store.select(_product_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__["getTaxonomies"]);
        this.brands$ = this.store.select(_product_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__["getTaxonomies"]);
        this.selectedTaxonIds$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getSelectedTaxonIds"]);
        this.products$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getProductsByKeyword"]);
        this.pagination$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getPaginationData"]);
        this.isFilterOn$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["searchFilterStatus"]);
        this.store.select(_product_reducers_selectors__WEBPACK_IMPORTED_MODULE_3__["rootTaxonomyId"])
            .subscribe(function (id) { return _this.rootTaxonomyId = id; });
    }
    HomeComponent.prototype.showModal = function () {
        this.isModalShown = true;
    };
    HomeComponent.prototype.calculateInnerWidth = function () {
        if (this.screenwidth <= 1000) {
            this.isMobile = this.screenwidth;
        }
    };
    HomeComponent.prototype.hideModal = function () {
        this.autoShownModal.hide();
    };
    HomeComponent.prototype.onHidden = function () {
        this.isModalShown = false;
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.screenwidth = window.innerWidth;
        this.calculateInnerWidth();
    };
    HomeComponent.prototype.OnCategeorySelected = function (category) {
        this.store.dispatch(this.searchActions.getChildTaxons(this.rootTaxonomyId, category.id));
        this.taxonomies$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getChildTaxons"]);
        this.categoryLevel$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["categeoryLevel"]);
        // ToDo: Here Brands are hardcoded For now.
        this.store.dispatch(this.searchActions.getTaxonomiesByName('Brands', category.name));
        this.brands$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["taxonomiByName"]);
        this.store.dispatch(this.searchActions.setSearchFilterOn());
    };
    HomeComponent.prototype.showAll = function () {
        this.store.dispatch(this.searchActions.setSearchFilterOff());
    };
    HomeComponent.prototype.isOpenChangeaccourdian = function () {
        this.isCategoryOpen = !this.isCategoryOpen;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ViewChild"])('autoShownModal'),
        __metadata("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalDirective"])
    ], HomeComponent.prototype, "autoShownModal", void 0);
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"],
            _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/home.routes.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.routes.ts ***!
  \*************************************/
/*! exports provided: HomeRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutes", function() { return HomeRoutes; });
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _category_page_category_page_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-page/category-page.component */ "./src/app/home/category-page/category-page.component.ts");


var HomeRoutes = [
    { path: 'search', component: _home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"] },
    { path: 'c/:number', component: _category_page_category_page_component__WEBPACK_IMPORTED_MODULE_1__["CategoryPageComponent"] }
];


/***/ }),

/***/ "./src/app/home/index.ts":
/*!*******************************!*\
  !*** ./src/app/home/index.ts ***!
  \*******************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var _product_effects_product_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../product/effects/product.effects */ "./src/app/product/effects/product.effects.ts");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/index.js");
/* harmony import */ var _sidebar_brand_filter_brand_filter_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sidebar/brand-filter/brand-filter.component */ "./src/app/home/sidebar/brand-filter/brand-filter.component.ts");
/* harmony import */ var _sidebar_categories_categories_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar/categories/categories.component */ "./src/app/home/sidebar/categories/categories.component.ts");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../product/actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-lite/input-star-rating */ "./node_modules/@ngx-lite/input-star-rating/fesm5/ngx-lite-input-star-rating.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _breadcrumb_components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./breadcrumb/components/breadcrumb/breadcrumb.component */ "./src/app/home/breadcrumb/components/breadcrumb/breadcrumb.component.ts");
/* harmony import */ var _content_product_list_product_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./content/product-list/product-list.component */ "./src/app/home/content/product-list/product-list.component.ts");
/* harmony import */ var _content_product_list_product_list_item_product_list_item_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./content/product-list/product-list-item/product-list-item.component */ "./src/app/home/content/product-list/product-list-item/product-list-item.component.ts");
/* harmony import */ var _content_filter_summary_filter_summary_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./content/filter-summary/filter-summary.component */ "./src/app/home/content/filter-summary/filter-summary.component.ts");
/* harmony import */ var _content_customize_customize_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./content/customize/customize.component */ "./src/app/home/content/customize/customize.component.ts");
/* harmony import */ var _content_content_header_content_header_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./content/content-header/content-header.component */ "./src/app/home/content/content-header/content-header.component.ts");
/* harmony import */ var _content_content__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./content/content */ "./src/app/home/content/content.ts");
/* harmony import */ var _sidebar_taxons_taxons_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./sidebar/taxons/taxons.component */ "./src/app/home/sidebar/taxons/taxons.component.ts");
/* harmony import */ var _sidebar_filter_filter_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./sidebar/filter/filter.component */ "./src/app/home/sidebar/filter/filter.component.ts");
/* harmony import */ var _filter_mobile_menu_filter_mobile_menu_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./filter-mobile-menu/filter-mobile-menu.component */ "./src/app/home/filter-mobile-menu/filter-mobile-menu.component.ts");
/* harmony import */ var _home_routes__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./home.routes */ "./src/app/home/home.routes.ts");
/* harmony import */ var _content_product_list_product_filter_pipe__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./content/product-list/product-filter.pipe */ "./src/app/home/content/product-list/product-filter.pipe.ts");
/* harmony import */ var _reducers_index__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./reducers/index */ "./src/app/home/reducers/index.ts");
/* harmony import */ var _category_page_category_page_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./category-page/category-page.component */ "./src/app/home/category-page/category-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














// Components

// Breadcrumb components

// Content components






// Sidebar components



// Routes




var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["NgModule"])({
            declarations: [
                // components
                _home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
                _content_product_list_product_list_component__WEBPACK_IMPORTED_MODULE_15__["ProductListComponent"],
                _content_product_list_product_list_item_product_list_item_component__WEBPACK_IMPORTED_MODULE_16__["ProductListItemComponent"],
                _sidebar_taxons_taxons_component__WEBPACK_IMPORTED_MODULE_21__["TaxonsComponent"],
                _sidebar_filter_filter_component__WEBPACK_IMPORTED_MODULE_22__["FilterComponent"],
                _breadcrumb_components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_14__["BreadcrumbComponent"],
                _content_content_header_content_header_component__WEBPACK_IMPORTED_MODULE_19__["ContentHeaderComponent"],
                _content_customize_customize_component__WEBPACK_IMPORTED_MODULE_18__["CustomizeComponent"],
                _content_filter_summary_filter_summary_component__WEBPACK_IMPORTED_MODULE_17__["FilterSummaryComponent"],
                _content_content__WEBPACK_IMPORTED_MODULE_20__["ContentComponent"],
                _sidebar_categories_categories_component__WEBPACK_IMPORTED_MODULE_3__["CategoriesComponent"],
                _sidebar_brand_filter_brand_filter_component__WEBPACK_IMPORTED_MODULE_2__["BrandFilterComponent"],
                _filter_mobile_menu_filter_mobile_menu_component__WEBPACK_IMPORTED_MODULE_23__["FilterMobileMenuComponent"],
                _category_page_category_page_component__WEBPACK_IMPORTED_MODULE_27__["CategoryPageComponent"],
                // pipes
                _content_product_list_product_filter_pipe__WEBPACK_IMPORTED_MODULE_25__["FilterPipe"]
            ],
            exports: [],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(_home_routes__WEBPACK_IMPORTED_MODULE_24__["HomeRoutes"]),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_1__["PaginationModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_11__["ModalModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_11__["AccordionModule"].forRoot(),
                _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_12__["NgxInputStarRatingModule"],
                /**
                 * StoreModule.forFeature is used for composing state
                 * from feature modules. These modules can be loaded
                 * eagerly or lazily and will be dynamically added to
                 * the existing state.
                 */
                _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["StoreModule"].forFeature('home', _reducers_index__WEBPACK_IMPORTED_MODULE_26__["reducers"]),
                /**
                 * Effects.forFeature is used to register effects
                 * from feature modules. Effects can be loaded
                 * eagerly or lazily and will be started immediately.
                 *
                 * All Effects will only be instantiated once regardless of
                 * whether they are registered once or multiple times.
                 */
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["EffectsModule"].forFeature([_product_effects_product_effects__WEBPACK_IMPORTED_MODULE_0__["ProductEffects"]]),
                _shared_index__WEBPACK_IMPORTED_MODULE_10__["SharedModule"],
            ],
            providers: [
                _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_8__["ProductActions"],
                _reducers_search_actions__WEBPACK_IMPORTED_MODULE_9__["SearchActions"]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/home/reducers/index.ts":
/*!****************************************!*\
  !*** ./src/app/home/reducers/index.ts ***!
  \****************************************/
/*! exports provided: reducers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducers", function() { return reducers; });
/* harmony import */ var _search_reducers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search.reducers */ "./src/app/home/reducers/search.reducers.ts");

var reducers = {
    search: _search_reducers__WEBPACK_IMPORTED_MODULE_0__["reducer"]
};


/***/ }),

/***/ "./src/app/home/reducers/search.actions.ts":
/*!*************************************************!*\
  !*** ./src/app/home/reducers/search.actions.ts ***!
  \*************************************************/
/*! exports provided: SearchActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchActions", function() { return SearchActions; });
var SearchActions = /** @class */ (function () {
    function SearchActions() {
    }
    /**
     * @method getAllFtilers
     * Fetches all the filters that have been getSelectedProduct
     * Used in filterSummaryComponent
     */
    SearchActions.prototype.getAllFiltes = function () {
        return { type: SearchActions.GET_ALL_FILTERS };
    };
    /**
     * @method addFilter
     * @param taxon Class Taxon
     * Get's triggered on checking the checkboxes in TaxonsComponent.
     */
    SearchActions.prototype.addFilter = function (taxon) {
        return {
            type: SearchActions.ADD_FILTER,
            payload: taxon
        };
    };
    /**
     * @method removeFilter
     * @param taxon
     * Get's triggered at 2 places:-
     * 1. When user unchecks the checkbox.
     * 2. When user clears the selected filtes in filterSummaryComponent
     */
    SearchActions.prototype.removeFilter = function (taxon) {
        return {
            type: SearchActions.REMOVE_FILTER,
            payload: taxon
        };
    };
    SearchActions.prototype.getproductsByKeyword = function (keyword) {
        return {
            type: SearchActions.GET_PRODUCTS_BY_KEYWORD,
            payload: keyword
        };
    };
    SearchActions.prototype.getProducsByKeywordSuccess = function (products) {
        return {
            type: SearchActions.GET_PRODUCTS_BY_KEYWORD_SUCCESS,
            payload: products
        };
    };
    SearchActions.prototype.getProductsByTaxon = function (taxonId) {
        return {
            type: SearchActions.GET_PRODUCTS_BY_TAXON,
            payload: taxonId
        };
    };
    SearchActions.prototype.getChildTaxons = function (taxonomiesId, taxonId) {
        return {
            type: SearchActions.GET_CHILD_TAXONS,
            payload: { taxonomiesId: taxonomiesId, taxonId: taxonId }
        };
    };
    SearchActions.prototype.getChildTaxonsSuccess = function (taxonList) {
        return {
            type: SearchActions.GET_CHILD_TAXONS_SUCCESS,
            payload: taxonList
        };
    };
    SearchActions.prototype.clearCategeoryLevel = function () {
        return {
            type: SearchActions.CLEAR_SELECTED_CATAGEORY
        };
    };
    SearchActions.prototype.getTaxonomiesByName = function (taxonomyName, categoryName) {
        this.category = categoryName;
        return {
            type: SearchActions.GET_TAXONOMIES_BY_NAME,
            payload: taxonomyName
        };
    };
    SearchActions.prototype.getTaxonomiesByNameSuccess = function (taxonomiList) {
        var category = this.category;
        return {
            type: SearchActions.GET_TAXONOMIES_BY_NAME_SUCCESS,
            payload: { taxonomiList: taxonomiList, category: category }
        };
    };
    SearchActions.prototype.setSearchFilterOn = function () {
        return {
            type: SearchActions.SET_SEARCH_FILTER_ON
        };
    };
    SearchActions.prototype.setSearchFilterOff = function () {
        return {
            type: SearchActions.SET_SEARCH_FILTER_OFF
        };
    };
    SearchActions.GET_ALL_FILTERS = 'GET_ALL_FILTERS';
    SearchActions.ADD_FILTER = 'ADD_FILTER';
    SearchActions.REMOVE_FILTER = 'REMOVE_FILTER';
    SearchActions.GET_PRODUCTS_BY_KEYWORD = 'GET_PRODUCTS_BY_KEYWORD';
    SearchActions.GET_PRODUCTS_BY_KEYWORD_SUCCESS = 'GET_PRODUCTS_BY_KEYWORD_SUCCESS';
    SearchActions.GET_PRODUCTS_BY_TAXON = 'GET_PRODUCTS_BY_TAXON';
    SearchActions.GET_CHILD_TAXONS = 'GET_CHILD_TAXONS';
    SearchActions.GET_CHILD_TAXONS_SUCCESS = 'GET_CHILD_TAXONS_SUCCESS';
    SearchActions.CLEAR_SELECTED_CATAGEORY = 'CLEAR_SELECTED_CATAGEORY';
    SearchActions.GET_TAXONOMIES_BY_NAME = 'GET_TAXONOMIES_BY_NAME';
    SearchActions.GET_TAXONOMIES_BY_NAME_SUCCESS = 'GET_TAXONOMIES_BY_NAME_SUCCESS';
    SearchActions.SET_SEARCH_FILTER_ON = 'SET_SEARCH_FILTER_ON';
    SearchActions.SET_SEARCH_FILTER_OFF = 'SET_SEARCH_FILTER_OFF';
    return SearchActions;
}());



/***/ }),

/***/ "./src/app/home/reducers/search.reducers.ts":
/*!**************************************************!*\
  !*** ./src/app/home/reducers/search.reducers.ts ***!
  \**************************************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _search_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _search_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search.state */ "./src/app/home/reducers/search.state.ts");


var initialState = new _search_state__WEBPACK_IMPORTED_MODULE_1__["SearchStateRecord"]();
var isUpdated;
var updatedCategory;
function reducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].ADD_FILTER:
            var filterAlreadyPresent_1 = false;
            state.selectedFilters.forEach(function (filter) {
                var filterId = filter['id'];
                if (filterId === payload.id) {
                    filterAlreadyPresent_1 = true;
                }
            });
            if (filterAlreadyPresent_1) {
                return state;
            }
            else {
                var selectedFilters = state.selectedFilters.concat([payload]);
                var selectedTaxonIds = state.selectedTaxonIds.concat(payload.id);
                return state.merge({
                    selectedFilters: selectedFilters,
                    selectedTaxonIds: selectedTaxonIds,
                });
            }
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].REMOVE_FILTER:
            var removeIndex_1 = -1;
            state.selectedFilters.forEach(function (filter, index) {
                var filterId = filter['id'];
                if (filterId === payload.id) {
                    removeIndex_1 = index;
                }
            });
            var _selectedFilters = state.selectedFilters.remove(removeIndex_1);
            var taxonRemoveIndex = state.selectedTaxonIds.findIndex(function (filterId) { return payload.id === filterId; });
            var _selectedTaxonIds = state.selectedTaxonIds.remove(taxonRemoveIndex);
            return state.merge({
                selectedFilters: _selectedFilters,
                selectedTaxonIds: _selectedTaxonIds
            });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].GET_PRODUCTS_BY_KEYWORD_SUCCESS:
            var productsByKeyword = payload.products;
            var paginationData = payload.pagination;
            return state.merge({
                productsByKeyword: productsByKeyword,
                paginationData: paginationData || {}
            });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].GET_CHILD_TAXONS_SUCCESS:
            var selectedCategory_1 = { 'id': payload.taxonList.id, 'name': payload.taxonList.name };
            var _categeoryLevel_1 = state.categeoryLevel;
            state.categeoryLevel.forEach(function (categeory, i) {
                if (categeory.id === selectedCategory_1.id) {
                    isUpdated = true;
                    updatedCategory = _categeoryLevel_1.slice(0, i + 1);
                    _categeoryLevel_1 = updatedCategory;
                }
                else {
                    isUpdated = false;
                }
            });
            if (isUpdated !== true) {
                _categeoryLevel_1 = _categeoryLevel_1.push(selectedCategory_1);
            }
            var _getChildTaxons = payload.taxonList;
            return state.merge({
                getChildTaxons: _getChildTaxons,
                categeoryLevel: _categeoryLevel_1
            });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].CLEAR_SELECTED_CATAGEORY:
            return state.merge({
                categeoryLevel: []
            });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].GET_TAXONOMIES_BY_NAME_SUCCESS:
            var _taxonomiByName = payload.taxonomiList.taxonomiList.taxonomies;
            var brandArray = [];
            var brandsRoot = _taxonomiByName[0].root;
            var lengthBrands = brandsRoot.taxons.length;
            for (var i = 0; i < lengthBrands; i++) {
                var lengthCategory = brandsRoot.taxons[i].taxons.length;
                for (var j = 0; j < lengthCategory; j++) {
                    if (brandsRoot.taxons[i].taxons[j].name === payload.category) {
                        brandArray.push({
                            icon: brandsRoot.taxons[i].icon,
                            id: brandsRoot.taxons[i].id,
                            name: brandsRoot.taxons[i].name
                        });
                    }
                }
            }
            _taxonomiByName = brandArray;
            return state.merge({
                taxonomiByName: _taxonomiByName
            });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].SET_SEARCH_FILTER_ON:
            return state.merge({ searchFilter: true });
        case _search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"].SET_SEARCH_FILTER_OFF:
            return state.merge({ searchFilter: false });
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/app/home/reducers/search.state.ts":
/*!***********************************************!*\
  !*** ./src/app/home/reducers/search.state.ts ***!
  \***********************************************/
/*! exports provided: SearchStateRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchStateRecord", function() { return SearchStateRecord; });
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);
/** Search state
 * [{
 *   name: 'Bag',
 *   taxonId: 1
 * }, {
 *   name: 'T-shirts',
 *   taxonId: 9
 * }]
 *
*/

var SearchStateRecord = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Record"])({
    selectedFilters: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    selectedTaxonIds: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    productsByKeyword: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    getChildTaxons: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    categeoryLevel: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    taxonomiByName: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    paginationData: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])({}),
    searchFilter: false
});


/***/ }),

/***/ "./src/app/home/reducers/selectors.ts":
/*!********************************************!*\
  !*** ./src/app/home/reducers/selectors.ts ***!
  \********************************************/
/*! exports provided: getHomeState, getSearchState, getFilters, getSelectedTaxonIds, getProductsByKeyword, getPaginationData, getChildTaxons, categeoryLevel, taxonomiByName, searchFilterStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHomeState", function() { return getHomeState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchState", function() { return getSearchState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilters", function() { return getFilters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSelectedTaxonIds", function() { return getSelectedTaxonIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductsByKeyword", function() { return getProductsByKeyword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPaginationData", function() { return getPaginationData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getChildTaxons", function() { return getChildTaxons; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "categeoryLevel", function() { return categeoryLevel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taxonomiByName", function() { return taxonomiByName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchFilterStatus", function() { return searchFilterStatus; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");

/******************* Base Search State ******************/
var getHomeState = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createFeatureSelector"])('home');
var getSearchState = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getHomeState, function (state) { return state.search; });
/******************* Individual selectors ******************/
function fetchSelectedFilters(state) {
    return state.selectedFilters.toJS();
}
;
function fetchSelectedTaxonIds(state) {
    return state.selectedTaxonIds.toJS();
}
function fetchProductsByKeyword(state) {
    return state.productsByKeyword.toJS();
}
function fetchChildTaxons(state) {
    return state.getChildTaxons.toJS();
}
function fetchCategeoryLevel(state) {
    return state.categeoryLevel.toJS();
}
function fetchTaxonomiByName(state) {
    return state.taxonomiByName.toJS();
}
function fetchPaginationData(state) {
    return state.paginationData.toJS();
}
function fetchSearchFliterStatus(state) {
    return state.searchFilter;
}
/******************* Public Selector API's ******************/
var getFilters = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchSelectedFilters);
var getSelectedTaxonIds = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchSelectedTaxonIds);
var getProductsByKeyword = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchProductsByKeyword);
var getPaginationData = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchPaginationData);
var getChildTaxons = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchChildTaxons);
var categeoryLevel = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchCategeoryLevel);
var taxonomiByName = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchTaxonomiByName);
var searchFilterStatus = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getSearchState, fetchSearchFliterStatus);


/***/ }),

/***/ "./src/app/home/sidebar/brand-filter/brand-filter.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/home/sidebar/brand-filter/brand-filter.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!isFilterOn\" class=\"catgeory\">\n  <ul class=\"list-group\" *ngIf=\"taxonomiList[1]; let brands_taxonomy\">\n    <h4>Brands</h4>\n    <li *ngFor=\"let brandItem of brands_taxonomy.root.taxons\" (click)=\"brandFilter(brandItem.name)\">\n      <a class=\"list-group-item list-group-item-action\" [routerLink]=\"['.']\" [queryParams]=\"{'q[name_cont]': brandItem.name, id: brandItem.id}\">{{brandItem.name}}</a>\n    </li>\n  </ul>\n</div>\n<div *ngIf=\"isFilterOn\" class=\"catgeory\">\n  <div *ngIf=\"taxonomiList.length!=0\">\n    <ul class=\"list-group\" *ngIf=\"taxonomiList; let brands_taxonomy\">\n      <h4>Brands</h4>\n      <li *ngFor=\"let brandItem of brands_taxonomy\" (click)=\"brandFilter(brandItem.name)\">\n        <a class=\"list-group-item list-group-item-action\" [routerLink]=\"['.']\" [queryParams]=\"{'q[name_cont]': brandItem.name, id: brandItem.id}\">{{brandItem.name}}</a>\n      </li>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/home/sidebar/brand-filter/brand-filter.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/home/sidebar/brand-filter/brand-filter.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".catgeory {\n  padding: 4vh 0;\n  border-bottom: 1px solid #ccc;\n  background-color: #f8f9fa; }\n  .catgeory .list-group-item {\n    border: 0px solid red;\n    padding: 0.35rem 0rem;\n    background-color: transparent; }\n  .catgeory .list-group-item-action:hover,\n  .catgeory .list-group-item-action:focus {\n    background-color: transparent;\n    color: #dee2e6;\n    text-decoration: underline; }\n"

/***/ }),

/***/ "./src/app/home/sidebar/brand-filter/brand-filter.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/home/sidebar/brand-filter/brand-filter.component.ts ***!
  \*********************************************************************/
/*! exports provided: BrandFilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrandFilterComponent", function() { return BrandFilterComponent; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BrandFilterComponent = /** @class */ (function () {
    function BrandFilterComponent(searchActions, store, router) {
        var _this = this;
        this.searchActions = searchActions;
        this.store = store;
        this.router = router;
        this.router.queryParams
            .subscribe(function (params) {
            _this.queryParams = params;
        });
    }
    BrandFilterComponent.prototype.ngOnInit = function () {
    };
    BrandFilterComponent.prototype.brandFilter = function () {
        var search = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["URLSearchParams"]();
        search.set('id', this.queryParams.id);
        this.store.dispatch(this.searchActions.getProductsByTaxon(search.toString()));
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], BrandFilterComponent.prototype, "taxonomiList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], BrandFilterComponent.prototype, "isFilterOn", void 0);
    BrandFilterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-brand-filter',
            template: __webpack_require__(/*! ./brand-filter.component.html */ "./src/app/home/sidebar/brand-filter/brand-filter.component.html"),
            styles: [__webpack_require__(/*! ./brand-filter.component.scss */ "./src/app/home/sidebar/brand-filter/brand-filter.component.scss")]
        }),
        __metadata("design:paramtypes", [_reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__["SearchActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_0__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], BrandFilterComponent);
    return BrandFilterComponent;
}());



/***/ }),

/***/ "./src/app/home/sidebar/categories/categories.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/home/sidebar/categories/categories.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div><P>{{isFilterOn}}</P></div> -->\n<div *ngIf=\"!isFilterOn\" class=\"catgeory\">\n  <ul class=\"list-group\" *ngIf=\"taxonomiList[0]; let catgeory_taxonomy\">\n    <h4> Catgeories</h4>\n    <li *ngFor=\"let categoryItem of catgeory_taxonomy.root.taxons\" (click)=\"emitSelection(catgeory_taxonomy)\">\n      <a class=\"list-group-item list-group-item-action\" [routerLink]=\"['.']\" [queryParams]=\"{'q[name_cont]': categoryItem.name, id: categoryItem.id, page: 1}\">\n        {{categoryItem.name}}\n      </a>\n    </li>\n  </ul>\n</div>\n\n<div *ngIf=\"isFilterOn\" class=\"catgeory\">\n  <ul class=\"list-group\" *ngIf=\"taxonomiList; let child_taxonomy\">\n    <p>{{taxonomiList.pretty_name}}</p>\n    <h4 *ngIf=\"categoryLevel.length > 0\"> Catgeories</h4>\n    <li *ngIf=\"categoryLevel.length > 0\">\n\n      <a class=\"list-group-item list-group-item-action\" (click)=showAllCategory()>\n        <i class=\"fa fa-chevron-left mr-1\" aria-hidden=\"true\"></i>\n        All\n      </a>\n    </li>\n    <li *ngFor=\"let levels of categoryLevel; let index =i\" (click)=\"emitSelection()\">\n      <a [routerLink]=\"['.']\" class=\"list-group-item list-group-item-action\" [queryParams]=\"{'q[name_cont]': levels.name, id: levels.id}\">\n        <span class=\"maninname\">\n          {{levels.name}}\n        </span>\n      </a>\n    </li>\n    <li>\n      <ul *ngIf=\"child_taxonomy.taxons\">\n        <li *ngFor=\"let childs of child_taxonomy.taxons\" (click)=\"emitSelection(childs.id)\">\n          <a class=\"list-group-item list-group-item-action ml-3\" [routerLink]=\"['.']\" [queryParams]=\"{'q[name_cont]': childs.name, id: childs.id}\">{{childs.name}}</a>\n        </li>\n      </ul>\n    </li>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/home/sidebar/categories/categories.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/home/sidebar/categories/categories.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".catgeory {\n  padding: 4vh 0;\n  border-bottom: 1px solid #ccc;\n  background-color: #f8f9fa; }\n  .catgeory .list-group-item {\n    border: 0px solid red;\n    padding: 0.35rem 0rem;\n    background-color: transparent; }\n  .catgeory .list-group-item-action:hover,\n  .catgeory .list-group-item-action:focus {\n    background-color: transparent;\n    color: #dee2e6;\n    text-decoration: underline; }\n  .catgeory .maninname {\n    color: #212529;\n    font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/home/sidebar/categories/categories.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/home/sidebar/categories/categories.component.ts ***!
  \*****************************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CategoriesComponent = /** @class */ (function () {
    function CategoriesComponent(searchActions, store, router) {
        var _this = this;
        this.searchActions = searchActions;
        this.store = store;
        this.router = router;
        this.onSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        this.showAll = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        this.router.queryParams
            .subscribe(function (params) {
            _this.queryParams = params;
        });
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        if ('id' in this.queryParams) {
            this.catgeoryFilter();
        }
    };
    CategoriesComponent.prototype.showAllCategory = function () {
        this.showAll.emit();
        window.location.reload();
    };
    /**
     *
     *
     * @memberof CategoriesComponent
     */
    CategoriesComponent.prototype.catgeoryFilter = function () {
        var search = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["URLSearchParams"]();
        if ('page' in this.queryParams) {
            search.set('page', this.queryParams.page);
        }
        search.set('id', this.queryParams.id);
        this.store.dispatch(this.searchActions.getProductsByTaxon(search.toString()));
    };
    CategoriesComponent.prototype.emitSelection = function (root) {
        this.catgeoryFilter();
        this.onSelected.emit({ id: this.queryParams.id, name: this.queryParams['q[name_cont]'] });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesComponent.prototype, "taxonomiList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesComponent.prototype, "isFilterOn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesComponent.prototype, "categoryLevel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"])(),
        __metadata("design:type", Object)
    ], CategoriesComponent.prototype, "onSelected", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"])(),
        __metadata("design:type", Object)
    ], CategoriesComponent.prototype, "showAll", void 0);
    CategoriesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__(/*! ./categories.component.html */ "./src/app/home/sidebar/categories/categories.component.html"),
            styles: [__webpack_require__(/*! ./categories.component.scss */ "./src/app/home/sidebar/categories/categories.component.scss")]
        }),
        __metadata("design:paramtypes", [_reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__["SearchActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "./src/app/home/sidebar/filter/filter.component.html":
/*!***********************************************************!*\
  !*** ./src/app/home/sidebar/filter/filter.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  filter works!\n</p>\n"

/***/ }),

/***/ "./src/app/home/sidebar/filter/filter.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/home/sidebar/filter/filter.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/sidebar/filter/filter.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/home/sidebar/filter/filter.component.ts ***!
  \*********************************************************/
/*! exports provided: FilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterComponent", function() { return FilterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FilterComponent = /** @class */ (function () {
    function FilterComponent() {
    }
    FilterComponent.prototype.ngOnInit = function () {
    };
    FilterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filter',
            template: __webpack_require__(/*! ./filter.component.html */ "./src/app/home/sidebar/filter/filter.component.html"),
            styles: [__webpack_require__(/*! ./filter.component.scss */ "./src/app/home/sidebar/filter/filter.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FilterComponent);
    return FilterComponent;
}());



/***/ }),

/***/ "./src/app/home/sidebar/taxons/taxons.component.html":
/*!***********************************************************!*\
  !*** ./src/app/home/sidebar/taxons/taxons.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div *ngIf=\"taxonomies\">\n  <h4 class=\"heading\" *ngIf=\"taxonomies.length !=0\">{{taxonomies[0].name}}</h4>\n  <div class=\"filter-box\">\n    <ul class=\"taxonomy\" *ngFor=\"let taxonomy of taxonomies\">\n      <li *ngFor=\"let taxon of taxonomy.root.taxons\" class=\"filter\">\n        <label class=\"vertical-filters-label common-customCheckbox\">\n          <input type=\"checkbox\" [checked]=\"isChecked(taxon)\" (click)=\"taxonSelected(taxon, $event.target.checked)\"> {{taxon.name}}\n          <strong>{{taxon.name}}</strong>({{taxon.taxons.length}})\n          <div class=\"common-checkboxIndicator\"></div>\n        </label>\n      </li>\n    </ul>\n  </div>\n</div> -->\n"

/***/ }),

/***/ "./src/app/home/sidebar/taxons/taxons.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/home/sidebar/taxons/taxons.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".heading {\n  text-transform: uppercase;\n  font-weight: 900;\n  padding: 5px 22px 5px 2px;\n  color: #212529; }\n\n.filter-box {\n  margin-bottom: 20px;\n  padding-bottom: 20px;\n  border-bottom: 1px solid #e9ecef;\n  position: relative; }\n\n.filter-box ul.taxonomy {\n    margin-left: -37px;\n    list-style: none; }\n\n.filter-box ul.taxonomy li.filter {\n      font-size: 14px;\n      margin-bottom: 7px;\n      font-size: 18px; }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label {\n        display: block;\n        font-size: 16px;\n        width: 95%;\n        white-space: nowrap;\n        cursor: pointer;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        min-height: 17px;\n        color: #212529;\n        position: relative; }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label input {\n          margin: 0 10px 0 0;\n          visibility: hidden; }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label .common-checkboxIndicator {\n          box-sizing: border-box;\n          position: absolute;\n          top: 6px;\n          left: 0;\n          width: 16px;\n          height: 16px;\n          border: 1px solid #ccc;\n          background: #fff;\n          border-radius: 2px; }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label .common-checkboxIndicator:after {\n          display: block;\n          content: \"\";\n          position: absolute;\n          top: 4px;\n          left: 4px;\n          z-index: 1;\n          width: 8px;\n          height: 5px;\n          border: 2px solid #fff;\n          border-color: #fff;\n          border-top-style: none;\n          border-right-style: none;\n          transition: all .3s ease-in-out;\n          -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg); }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label input:checked ~ .common-checkboxIndicator {\n          border: 2px;\n          background: #007bff; }\n\n.filter-box ul.taxonomy li.filter label.vertical-filters-label input[type=checkbox]:checked {\n          content: \"\\2713\";\n          text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);\n          font-size: 15px;\n          color: #e9ecef;\n          text-align: center;\n          line-height: 15px; }\n\n.filter-box ul.taxonomy li.filter label:before {\n        border-radius: 3px; }\n"

/***/ }),

/***/ "./src/app/home/sidebar/taxons/taxons.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/home/sidebar/taxons/taxons.component.ts ***!
  \*********************************************************/
/*! exports provided: TaxonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaxonsComponent", function() { return TaxonsComponent; });
/* harmony import */ var _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../reducers/selectors */ "./src/app/home/reducers/selectors.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TaxonsComponent = /** @class */ (function () {
    function TaxonsComponent(store, actions, ref) {
        var _this = this;
        this.store = store;
        this.actions = actions;
        this.ref = ref;
        this.selectedFilters = [];
        this.searchFilters$ = this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_1__["getFilters"]);
        this.searchFilters$.subscribe(function (data) {
            _this.selectedFilters = data;
        });
    }
    TaxonsComponent.prototype.ngOnInit = function () {
    };
    TaxonsComponent.prototype.isChecked = function (taxon) {
        var result = false;
        this.selectedFilters.forEach(function (filter) {
            if (filter.id === taxon.id) {
                result = true;
            }
        });
        return result;
    };
    TaxonsComponent.prototype.taxonSelected = function (taxon, checked) {
        if (checked) {
            this.store.dispatch(this.actions.addFilter(taxon));
        }
        else {
            this.store.dispatch(this.actions.removeFilter(taxon));
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], TaxonsComponent.prototype, "taxonomies", void 0);
    TaxonsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-taxons',
            template: __webpack_require__(/*! ./taxons.component.html */ "./src/app/home/sidebar/taxons/taxons.component.html"),
            styles: [__webpack_require__(/*! ./taxons.component.scss */ "./src/app/home/sidebar/taxons/taxons.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"],
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"]])
    ], TaxonsComponent);
    return TaxonsComponent;
}());



/***/ }),

/***/ "./src/app/item-share.service.ts":
/*!***************************************!*\
  !*** ./src/app/item-share.service.ts ***!
  \***************************************/
/*! exports provided: ItemShareService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemShareService", function() { return ItemShareService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemShareService = /** @class */ (function () {
    function ItemShareService() {
        this.itemSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this.currentItems = this.itemSource.asObservable();
    }
    ItemShareService.prototype.changeItems = function (items) {
        this.itemSource.next(items);
    };
    ItemShareService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ItemShareService);
    return ItemShareService;
}());



/***/ }),

/***/ "./src/app/layout/checkout-footer/checkout-footer.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/layout/checkout-footer/checkout-footer.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"checkout-footer\">\n</div>"

/***/ }),

/***/ "./src/app/layout/checkout-footer/checkout-footer.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/checkout-footer/checkout-footer.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".checkout-footer {\n  background: #fff;\n  border-top: 1px solid #ccc; }\n"

/***/ }),

/***/ "./src/app/layout/checkout-footer/checkout-footer.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/checkout-footer/checkout-footer.component.ts ***!
  \*********************************************************************/
/*! exports provided: CheckoutFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutFooterComponent", function() { return CheckoutFooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CheckoutFooterComponent = /** @class */ (function () {
    function CheckoutFooterComponent() {
    }
    CheckoutFooterComponent.prototype.ngOnInit = function () {
    };
    CheckoutFooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-checkout-footer',
            template: __webpack_require__(/*! ./checkout-footer.component.html */ "./src/app/layout/checkout-footer/checkout-footer.component.html"),
            styles: [__webpack_require__(/*! ./checkout-footer.component.scss */ "./src/app/layout/checkout-footer/checkout-footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CheckoutFooterComponent);
    return CheckoutFooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/checkout-header/checkout-header.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/layout/checkout-header/checkout-header.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"checkout-header\">\n  <div class=\"full-container\">\n    <a class=\"navbar-brand\" routerLink=\"/\">\n      <img [src]=\"this.headerConfig.brand.logo\" [alt]=\"this.headerConfig.brand.name\" [width]=\"this.headerConfig.brand.width\" height=\"this.headerConfig.brand.height\"\n        class=\"d-inline-block align-top\" alt=\"\">\n    </a>\n    <ol class=\"checkout-steps\">\n      <li class=\"step step1\">\n        <a [ngClass]=\"{'can-click': checkIfClickable('cart'), 'active': isActiveRoute('cart')}\" [routerLink]=\"getRouterLink('cart')\">Bag</a>\n      </li>\n      <li class=\"divider\"></li>\n      <li class=\"step step2\">\n        <a [ngClass]=\"{'can-click': checkIfClickable('address'), 'active': isActiveRoute('address')}\" [routerLink]=\"getRouterLink('address')\">Delivery</a>\n      </li>\n      <li class=\"divider\"></li>\n      <li class=\"step step3\">\n        <a [ngClass]=\"{'can-click': checkIfClickable('payment'), 'active': isActiveRoute('payment')}\" [routerLink]=\"getRouterLink('payment')\">Payment</a>\n      </li>\n    </ol>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/checkout-header/checkout-header.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/checkout-header/checkout-header.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".checkout-header {\n  padding: 20px 0;\n  height: 78px;\n  border-bottom: 1px solid #ccc;\n  width: 100%;\n  z-index: 1;\n  background: #fff; }\n  .checkout-header .full-container {\n    max-width: 980px;\n    margin: 0 auto; }\n  .checkout-header .full-container .navbar-brand {\n      padding: 0px;\n      display: inline-block; }\n  .checkout-header .full-container .navbar-brand img {\n        height: 46px;\n        margin-top: -4px; }\n  .checkout-header .full-container .checkout-steps {\n      margin-left: 25%;\n      margin-top: 8px;\n      margin-bottom: 8px;\n      width: 40%;\n      display: inline-block;\n      padding: 0px; }\n  .checkout-header .full-container .checkout-steps .step {\n        display: inline-block;\n        background: #fff;\n        padding: 0px; }\n  .checkout-header .full-container .checkout-steps .step a {\n          color: #343a40;\n          text-transform: uppercase;\n          font-size: 12px;\n          font-weight: 600;\n          font-family: \"Whitney\";\n          letter-spacing: 3px;\n          text-decoration: none;\n          cursor: text; }\n  .checkout-header .full-container .checkout-steps .step .can-click {\n          cursor: pointer; }\n  .checkout-header .full-container .checkout-steps .step .active {\n          color: #ffe364;\n          border-bottom: 2px solid #ffe364;\n          padding-bottom: 5px; }\n  .checkout-header .full-container .checkout-steps .step1 {\n        margin-right: 5px; }\n  .checkout-header .full-container .checkout-steps .step2 {\n        margin: 0 5px; }\n  .checkout-header .full-container .checkout-steps .step3 {\n        margin-left: 5px; }\n  .checkout-header .full-container .checkout-steps .divider {\n        width: 10%;\n        border-top: 1px dashed #495057;\n        height: 5px;\n        display: inline-block !important;\n        background: #fff;\n        color: #343a40 !important;\n        padding: 0 !important; }\n"

/***/ }),

/***/ "./src/app/layout/checkout-header/checkout-header.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/checkout-header/checkout-header.component.ts ***!
  \*********************************************************************/
/*! exports provided: CheckoutHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutHeaderComponent", function() { return CheckoutHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CheckoutHeaderComponent = /** @class */ (function () {
    function CheckoutHeaderComponent(router) {
        this.router = router;
        this.checkoutStep = ['cart', 'address', 'payment'];
        this.headerConfig = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].config.header;
    }
    CheckoutHeaderComponent.prototype.ngOnInit = function () {
    };
    CheckoutHeaderComponent.prototype.isActiveRoute = function (step) {
        if (!this.currentStep) {
            return false;
        }
        if (step === this.currentStep) {
            return true;
        }
        else {
            return false;
        }
    };
    CheckoutHeaderComponent.prototype.checkIfClickable = function (clickStep) {
        return this.isLinkAccessible(clickStep);
    };
    CheckoutHeaderComponent.prototype.getRouterLink = function (step) {
        var isAccessible = this.isLinkAccessible(step);
        var link = [];
        if (isAccessible) {
            link = ['/checkout', step];
        }
        return link;
    };
    CheckoutHeaderComponent.prototype.isLinkAccessible = function (step) {
        if (!this.currentStep) {
            return false;
        }
        var currentStepIndex = this.checkoutStep.indexOf(this.currentStep);
        var stepIndex = this.checkoutStep.indexOf(step);
        if (stepIndex <= currentStepIndex) {
            return true;
        }
        else {
            return false;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CheckoutHeaderComponent.prototype, "currentStep", void 0);
    CheckoutHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-checkout-header',
            template: __webpack_require__(/*! ./checkout-header.component.html */ "./src/app/layout/checkout-header/checkout-header.component.html"),
            styles: [__webpack_require__(/*! ./checkout-header.component.scss */ "./src/app/layout/checkout-header/checkout-header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CheckoutHeaderComponent);
    return CheckoutHeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"contact-info text-left\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-12 col-md-5 col-sm-3 col-lg-3\">\n\n        Our experts are available 24/7:\n\n      </div>\n      <div class=\" col-12  col-md-6 col-sm-3 col-lg-2\">\n\n        <i class=\"fa fa-phone\" aria-hidden=\"true\"></i> {{contact_info.contact_no}}\n\n      </div>\n      <div class=\" col-12  col-md-4 col-sm-3 col-lg-2\">\n        <a href=\"#\">\n\n          <i class=\"fa fa-envelope\"></i> Email Us\n\n        </a>\n      </div>\n      <div class=\"col-12  col-md-4 col-sm-3 col-lg-2\">\n        <a href=\"#\">\n\n          <i class=\"fa fa-comments\" aria-hidden=\"true\"></i> Live Chat\n\n        </a>\n      </div>\n      <div class=\"col-12  col-md-4 col-sm-3 col-lg-3 text-right\">\n        <strong>\n          <a class=\"backtopbtn\" (click)=\"scollTop()\">\n            <span>Back to Top <i class=\"fa fa-angle-up pl-2\" aria-hidden=\"true\"></i></span>\n          </a>\n        </strong>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".line {\n  display: inline;\n  padding: 10px; }\n\n.col-md-3,\n.col-md-2 {\n  padding: 10px; }\n\n.contact-info {\n  background-color: #e9ecef;\n  padding: 1rem;\n  color: #6c757d;\n  font-size: 1em;\n  font-weight: normal; }\n\n.contact-info a {\n    padding: .7rem;\n    color: #6c757d; }\n\n.contact-info a:hover {\n    background-color: #c1c4c7;\n    border-radius: 4px; }\n\n.contact-info .backtopbtn {\n    background-color: #dee2e6;\n    border-radius: 4px;\n    text-decoration: none;\n    padding: 0.7rem 1.8rem;\n    color: #6c757d;\n    cursor: pointer; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n  .contact-info a {\n    padding: 0rem;\n    color: #6c757d; }\n  .contact-info .backtopbtn {\n    background-color: white;\n    border-radius: 4px;\n    text-align: center;\n    text-decoration: none;\n    padding: 0.7rem 1.8rem;\n    color: #6c757d; } }\n\n@media screen and (min-width: 320px) and (max-width: 480px) {\n  .contact-info .backtopbtn {\n    margin-top: 5px;\n    padding: 0.7rem 1.8rem;\n    display: block; } }\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: FooterContactInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterContactInfoComponent", function() { return FooterContactInfoComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterContactInfoComponent = /** @class */ (function () {
    function FooterContactInfoComponent() {
        this.contact_info = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.contact_info;
    }
    FooterContactInfoComponent.prototype.ngOnInit = function () {
    };
    FooterContactInfoComponent.prototype.scollTop = function () {
        window.scrollTo(0, 0);
    };
    FooterContactInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer-contact-info',
            template: __webpack_require__(/*! ./footer-contact-info.component.html */ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.html"),
            styles: [__webpack_require__(/*! ./footer-contact-info.component.scss */ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterContactInfoComponent);
    return FooterContactInfoComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"qlinks text-left\">\n  <ul class=\"p-0\">\n    <li class=\"line\" *ngFor=\"let page of footer_pages; let first = first; let last = last\" [class.first-item]=\"pl-0\" [class.last-item]=\"last\">\n      <a href=\"{{page.link_url}}\" target=\"_self\">{{page.name}}</a>\n    </li>`\n  </ul>\n</section>\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  color: #495057; }\n\n.line {\n  display: inline;\n  padding: 10px; }\n\nul {\n  list-style: none; }\n\n@media screen and (min-width: 320px) and (max-width: 767px) {\n  .qlinks li {\n    widows: 100%;\n    padding: 10px;\n    display: inline-block;\n    text-align: left; } }\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.ts ***!
  \********************************************************************************************/
/*! exports provided: FooterQuickLinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterQuickLinksComponent", function() { return FooterQuickLinksComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterQuickLinksComponent = /** @class */ (function () {
    function FooterQuickLinksComponent() {
        this.footer_pages = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.footer_page_links;
    }
    FooterQuickLinksComponent.prototype.ngOnInit = function () {
    };
    FooterQuickLinksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer-quick-links',
            template: __webpack_require__(/*! ./footer-quick-links.component.html */ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.html"),
            styles: [__webpack_require__(/*! ./footer-quick-links.component.scss */ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterQuickLinksComponent);
    return FooterQuickLinksComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-social-links/footer-social-links.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"social-link\">\n  <ul class=\" p-0\">\n    <li class=\"line\" *ngFor=\"let social of social_links\">\n      <a href={{social.link_url}} target=\"_blank\">\n        <i class=\"{{social.icon}} fa-2x\" aria-hidden=\"true\"></i>\n      </a>\n    </li>\n  </ul>\n</section>\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-social-links/footer-social-links.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  color: #495057; }\n\n.line {\n  display: inline;\n  padding: 10px; }\n\n.social-link {\n  text-align: right; }\n\n@media screen and (min-width: 320px) and (max-width: 767px) {\n  .social-link {\n    text-align: center; } }\n"

/***/ }),

/***/ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/footer/component/footer-social-links/footer-social-links.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: FooterSocialLinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterSocialLinksComponent", function() { return FooterSocialLinksComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterSocialLinksComponent = /** @class */ (function () {
    function FooterSocialLinksComponent() {
        this.social_links = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.footer_social_links;
    }
    FooterSocialLinksComponent.prototype.ngOnInit = function () {
    };
    FooterSocialLinksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer-social-links',
            template: __webpack_require__(/*! ./footer-social-links.component.html */ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.html"),
            styles: [__webpack_require__(/*! ./footer-social-links.component.scss */ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterSocialLinksComponent);
    return FooterSocialLinksComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer/footer.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"border-top\">\n  <!--<div class=\"container quicklinks \">\n    <div class=\"row\">\n      <div class=col-md-6>\n        <app-footer-quick-links></app-footer-quick-links>\n      </div>\n      <div class=\"col-md-6\">\n        <app-footer-social-links></app-footer-social-links>\n      </div>\n    </div>\n  </div>-->\n  <section class=\"cpy\">\n    <div class=\"container cpy1\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12 text-left\">\n          <ul class=\"p-0 copy\">\n            <li class=\"pl-0 pr-0\">{{contact_info.copyright}}</li>\n          </ul>\n        </div>\n        <!--<div class=\"col-md-6 col-xs-12 text-right\">\n          <ul class=\"text-right term\">\n            <li>\n              <a href=\"#\">Terms Of Use</a>\n            </li>\n            <li class=\"pr-0\">\n              <a href=\"#\">Privacy Policy</a>\n            </li>\n          </ul>\n        </div>-->\n      </div>\n    </div>\n  </section>\n</footer>"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "footer {\n  height: 50px;\n  position: absolute;\n  right: 0;\n  bottom: -60px;\n  left: 0;\n  background-color: #ccc;\n  text-align: center; }\n\n.quicklinks {\n  padding: 2rem 0; }\n\n.cpy {\n  border-top: 1px solid #a7a7a7; }\n\n.cpy1 ul {\n  padding: 0px;\n  margin: 0px;\n  list-style: none; }\n\n.cpy1 ul li {\n    display: inline-block;\n    padding: 1REM; }\n\n.cpy1 ul li a {\n      color: #6c757d;\n      font-size: 15PX; }\n\n@media screen and (min-width: 320px) and (max-width: 767px) {\n  .quicklinks .row {\n    margin: 0px; }\n  .cpy1 ul.copy li {\n    width: 100%;\n    text-align: center;\n    padding: 3px; }\n  .cpy1 ul.term li {\n    width: 50%;\n    text-align: center;\n    padding: 3px; } }\n"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.contact_info = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.contact_info;
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/layout/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layout/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/search']\" [queryParams]=\"{'q[name_cont]': taxons.name, id: taxons.id}\">\n  <img [src]=\"getBrandImageUrl(taxons.icon)\" class=\"img-fit\">\n</a>"

/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.scss":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.scss ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a a:focus {\n  color: #000;\n  -webkit-text-decoration: None;\n          text-decoration: None; }\n  a a:focus:hover {\n    color: #e9ecef;\n    text-decoration: underline; }\n  .img-fit {\n  max-width: 100%;\n  height: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down; }\n  .flex-container {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  display: -moz-flex;\n  display: flex; }\n  .wrap {\n  flex-wrap: wrap; }\n  .flex-item {\n  padding: 5px;\n  width: 100px;\n  height: 100px;\n  margin: 10px;\n  line-height: 100px;\n  color: #fff;\n  font-weight: bold;\n  font-size: 2em;\n  text-align: center; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: BrandListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrandListComponent", function() { return BrandListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BrandListComponent = /** @class */ (function () {
    function BrandListComponent() {
        // To do : Finding alternateway to show image.
        this.image = '../../../../../../../assets/default/no-image-available.png';
    }
    BrandListComponent.prototype.ngOnInit = function () {
    };
    BrandListComponent.prototype.getBrandImageUrl = function (url) {
        return url || this.image;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BrandListComponent.prototype, "taxons", void 0);
    BrandListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-brand-list',
            template: __webpack_require__(/*! ./brand-list.component.html */ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.html"),
            styles: [__webpack_require__(/*! ./brand-list.component.scss */ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], BrandListComponent);
    return BrandListComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"dpmenu\">\n  <div class=\"cust-menu-btn\" [ngClass]=\"[isOpen ? 'open':'']\" (mouseenter)=\"dropdown.show()\" (mouseleave)=\"dropdown.hide()\">\n    Shop By\n    <span *ngIf=\"taxonomies[0]; let brands_taxonomy\">{{brands_taxonomy.name}}\n      <i class=\"fa pl-3 fa-chevron-down\"></i>\n    </span>\n\n  </div>\n  <div dropdown #dropdown=\"bs-dropdown\" [autoClose]=\"false\" (isOpenChange)=\"onOpenChange($event)\" (mouseenter)=\"dropdown.show()\"\n    (mouseleave)=\"dropdown.hide()\">\n    <div id=\"dropdown-triggers-manual\" *dropdownMenu class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"button-triggers-manual\">\n      <div *ngIf=\"taxonomies[0]; let brands_taxonomy\">\n        <ul>\n          <li *ngFor=\"let taxon of brands_taxonomy.root.taxons\" class=\"col-2\" (click)=\"getBrands()\">\n            <app-brand-list [taxons]=\"taxon\"></app-brand-list>\n          </li>\n        </ul>\n\n      </div>\n    </div>\n  </div>\n</div> -->"

/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-menu {\n  width: 50rem;\n  padding: 0px;\n  left: -0.2rem !important;\n  top: 3rem !important;\n  min-height: 36.5vw;\n  z-index: 10;\n  padding: .7rem;\n  border-radius: 0px; }\n  .dropdown-menu:before {\n    content: \"\";\n    position: absolute;\n    width: 0;\n    height: 0;\n    top: -9.5px;\n    left: 9.9rem;\n    border-left: 10px solid transparent;\n    border-right: 10px solid transparent;\n    border-bottom: 10px solid #fff;\n    z-index: 3; }\n  .dropdown-menu .col-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 18.466667%; }\n  .dpmenu {\n  position: relative; }\n  .cust-menu-btn {\n  font-size: 1.2em;\n  padding: 0.7rem 1.2em 0.7em 0.7em;\n  border-radius: 7px 7px 0px 0px;\n  cursor: pointer;\n  margin-right: 2em;\n  position: relative;\n  left: -2px; }\n  .cust-menu-btn:hover {\n    background-color: #d5d8db;\n    box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n  .open {\n  background-color: #d5d8db;\n  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n  li {\n  padding: 5px;\n  margin: 5px;\n  line-height: 100px;\n  font-weight: bold;\n  font-size: 1em;\n  text-align: center;\n  float: left;\n  line-height: 1.5em;\n  height: 100px;\n  vertical-align: middle; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: BrandMenuDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrandMenuDropdownComponent", function() { return BrandMenuDropdownComponent; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BrandMenuDropdownComponent = /** @class */ (function () {
    function BrandMenuDropdownComponent(route, searchActions, store) {
        var _this = this;
        this.route = route;
        this.searchActions = searchActions;
        this.store = store;
        this.route.queryParams
            .subscribe(function (params) {
            _this.queryParams = params;
        });
    }
    BrandMenuDropdownComponent.prototype.ngOnInit = function () {
    };
    BrandMenuDropdownComponent.prototype.getBrands = function () {
        var search = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["URLSearchParams"]();
        search.set('id', this.queryParams.id);
        this.store.dispatch(this.searchActions.getProductsByTaxon(search.toString()));
    };
    BrandMenuDropdownComponent.prototype.onOpenChange = function (data) {
        this.isOpen = !this.isOpen;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], BrandMenuDropdownComponent.prototype, "taxonomies", void 0);
    BrandMenuDropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-brand-menu-dropdown',
            template: __webpack_require__(/*! ./brand-menu-dropdown.component.html */ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.html"),
            styles: [__webpack_require__(/*! ./brand-menu-dropdown.component.scss */ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_0__["Store"]])
    ], BrandMenuDropdownComponent);
    return BrandMenuDropdownComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.html":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.html ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row pl-3\" *ngIf=\"brandList\">\n  <div class=\"clearfix\"></div>\n  <div *ngFor=\"let brand of brandList\" class=\"menu-popular-brand-list-item mr-1\">\n    <a [routerLink]=\"['/search']\" [queryParams]=\"{'q[name_cont]': brand.name, 'id': brand.id}\">\n      <img [src]=\"getBrandImageUrl(brand.icon)\" class=\"img-fluid border p-1\">\n    </a>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.scss":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.scss ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".menu-popular-brand-list-item {\n  height: 5rem;\n  width: 5rem; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.ts":
/*!***********************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.ts ***!
  \***********************************************************************************************************************************************/
/*! exports provided: BrandLogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrandLogoComponent", function() { return BrandLogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BrandLogoComponent = /** @class */ (function () {
    function BrandLogoComponent() {
        // To do : Finding alternateway to show image.
        this.image = '/assets/default/no-image-available.png';
    }
    BrandLogoComponent.prototype.ngOnInit = function () { };
    BrandLogoComponent.prototype.getBrandImageUrl = function (url) {
        if (url) {
            return url;
        }
        else {
            return this.image;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BrandLogoComponent.prototype, "brandList", void 0);
    BrandLogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-brand-logo',
            template: __webpack_require__(/*! ./brand-logo.component.html */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.html"),
            styles: [__webpack_require__(/*! ./brand-logo.component.scss */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
        }),
        __metadata("design:paramtypes", [])
    ], BrandLogoComponent);
    return BrandLogoComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.html ***!
  \**********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container bgimg\" *ngIf=\"!screenwidth\" [ngStyle]=\"{'background': '#fff url(' + taxonImageLink + ') no-repeat 0 0'}\">\n  <div class=\"row mr-0\">\n    <div class=\"col-md-7 \">\n      <div class=\"row\">\n        <div class=\"col-md-3 p-0\" *ngFor=\"let taxon of taxons\">\n          <div>\n            <a [routerLink]=\"['/search']\" [queryParams]=\"{id: taxon.id}\">\n               <b class=\"sucathead\">{{taxon.name}}</b>\n            </a>\n            <app-categories-list [taxons]=\"taxon.taxons\"></app-categories-list>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-9 p-0 my-3\">\n          <h4>Popular Brands</h4>\n          <app-brand-logo [brandList]=\"brandLists$ |async\"></app-brand-logo>\n        </div>\n      </div>\n    </div>\n    <!-- to do -->\n    <!-- <div class=\"col-md-5 imgcon\">\n      <img class=\"w-100\" [src]=\"taxonImageLink\" alt=\"\">\n    </div> -->\n  </div>\n</div>\n<div class=\"container catcon\" *ngIf=\"screenwidth\">\n  <div class=\"row\">\n    <div class=\"col-md-8 catconinner\">\n\n      <div class=\"row catadjustwidth\">\n        <div class=\"col-6 col-sm-6   p-0 no-gutters brandsmenu\" [@popOverState]=\"stateName\">\n          <ul>\n            <li *ngFor=\"let taxon of taxons let i= index\" (click)=\"showCategoryonclick(taxon)\">\n              <a class=\"sucathead\" [routerLink]=\"['/search']\" [queryParams]=\"{id: taxon.id}\">\n                {{taxon.name}}\n              </a>\n            </li>\n          </ul>\n        </div>\n        <div class=\"col-6 col-sm-6  p-0 no-gutters brandsmenu\" [@popOverState]=\"stateName\">\n          <ul class=\"pb-0\">\n            <li (click)=\"backtolist()\">Back</li>\n          </ul>\n          <app-categories-list [screenwidth]=\"screenwidth\" [taxons]=\"menuTaxons\"></app-categories-list>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.scss":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.scss ***!
  \**********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sucathead {\n  color: #212529;\n  text-decoration: none;\n  font-size: 14px; }\n\n.brandsmenu a {\n  color: #fff;\n  -webkit-text-decoration: None;\n          text-decoration: None; }\n\n.brandsmenu a:hover {\n  color: #e9ecef;\n  text-decoration: underline; }\n\n.brandsmenu ul {\n  list-style: none;\n  padding: 0.5rem 0 0.5rem 1.5rem;\n  margin: 0px;\n  background-color: #ccc; }\n\n.brandsmenu ul li {\n    padding: 0.5rem 0.4rem;\n    background-color: #ccc;\n    border-bottom: 1px solid #ccc;\n    color: #ccc;\n    cursor: pointer; }\n\n.brandsmenu ul li::after {\n      font-family: 'FontAwesome';\n      content: \"\\f105\";\n      width: 20px;\n      height: 57px;\n      margin-right: 12px;\n      right: 0px;\n      position: absolute;\n      background-size: 10px; }\n\n.brandsmenu ul li:hover::after {\n      color: #e9ecef; }\n\n.brandsmenu ul li:hover {\n      background-color: #fff;\n      color: #e9ecef !important;\n      border-radius: 4px 0px 0px 4px; }\n\n.brandsmenu ul li uL > LI {\n      position: absolute;\n      left: 20%;\n      width: 78%;\n      top: 0PX;\n      display: NONE; }\n\n.catadjustwidth {\n  width: 220%; }\n\n@media screen and (min-width: 320px) and (max-width: 332x) {\n  .sucathead {\n    color: #fff; }\n  .catadjustwidth {\n    width: 220%; } }\n\n@media screen and (min-width: 375px) and (max-width: 413px) {\n  .sucathead {\n    color: #fff; }\n  .catadjustwidth {\n    width: 216%; } }\n\n@media screen and (min-width: 414px) and (max-width: 627px) {\n  .sucathead {\n    color: #fff; }\n  .catadjustwidth {\n    width: 215%; } }\n\n@media screen and (min-width: 628px) and (max-width: 767px) {\n  .sucathead {\n    color: #fff; }\n  .catadjustwidth {\n    width: 215%; } }\n\n.bgimg {\n  position: relative;\n  width: 96%;\n  left: -1%;\n  background-repeat: no-repeat !important;\n  background-position: 100% !important;\n  background-size: 300px !important; }\n\n@media screen and (min-width: 628px) and (max-width: 768px) {\n  .catadjustwidth {\n    width: 310%; }\n  .brandsmenu a {\n    color: #fff !important; }\n  .catcon {\n    padding: 0px; }\n  .catconinner {\n    padding: 0px; } }\n"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.ts":
/*!********************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.ts ***!
  \********************************************************************************************************************************************/
/*! exports provided: CategoriesDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesDetailsComponent", function() { return CategoriesDetailsComponent; });
/* harmony import */ var _home_reducers_selectors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../../../home/reducers/selectors */ "./src/app/home/reducers/selectors.ts");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CategoriesDetailsComponent = /** @class */ (function () {
    function CategoriesDetailsComponent(store, searchActions) {
        this.store = store;
        this.searchActions = searchActions;
        this.onSubCatClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
        this.show = false;
        this.taxon = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].config;
    }
    Object.defineProperty(CategoriesDetailsComponent.prototype, "stateName", {
        get: function () {
            return this.show ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    CategoriesDetailsComponent.prototype.showCategoryonclick = function (taxon) {
        this.show = !this.show;
        this.menuTaxons = taxon.taxons;
        this.onSubCatClicked.emit(true);
    };
    CategoriesDetailsComponent.prototype.backtolist = function () {
        this.show = !this.show;
        this.onSubCatClicked.emit(false);
    };
    CategoriesDetailsComponent.prototype.ngOnInit = function () {
    };
    CategoriesDetailsComponent.prototype.ngOnChanges = function () {
        this.store.dispatch(this.searchActions.getTaxonomiesByName('Brands', this.taxonName));
        this.brandLists$ = this.store.select(_home_reducers_selectors__WEBPACK_IMPORTED_MODULE_0__["taxonomiByName"]);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesDetailsComponent.prototype, "taxons", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesDetailsComponent.prototype, "taxonName", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesDetailsComponent.prototype, "screenwidth", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesDetailsComponent.prototype, "taxonImageLink", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"])
    ], CategoriesDetailsComponent.prototype, "onSubCatClicked", void 0);
    CategoriesDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-categories-details',
            template: __webpack_require__(/*! ./categories-details.component.html */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.html"),
            styles: [__webpack_require__(/*! ./categories-details.component.scss */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.scss")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewEncapsulation"].None,
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["trigger"])('popOverState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
                        left: -50 + '%'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
                        left: 0
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('200ms ease-in'))
                ])
            ],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"]])
    ], CategoriesDetailsComponent);
    return CategoriesDetailsComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.html":
/*!***********************************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.html ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!screenwidth\">\n  <div *ngFor=\"let taxon of taxons\">\n    <a [routerLink]=\"['/search']\" [queryParams]=\"{id: taxon.id}\">{{taxon.name}} </a>\n  </div>\n</div>\n<div class=\"subbrandmenu\" *ngIf=\"screenwidth\">\n  <ul>\n    <li *ngFor=\"let taxon of taxons\">\n      <a [routerLink]=\"['/search']\" [queryParams]=\"{id: taxon.id}\">{{taxon.name}} </a>\n    </li>\n  </ul>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.scss":
/*!***********************************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.scss ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  color: #6c757d;\n  font-size: 0.9rem; }\n  a:hover {\n    color: #ffb132;\n    text-decoration: underline; }\n  @media screen and (min-width: 320px) and (max-width: 767px) {\n    a {\n      color: #fff; } }\n  .subbrandmenu ul {\n  list-style: none;\n  padding: 0.5rem 0 0.5rem 1.5rem;\n  margin: 0px; }\n  .subbrandmenu ul li {\n    padding: 0.5rem 0.4rem;\n    border-bottom: 1px solid #ccc;\n    color: #fff;\n    cursor: pointer; }\n  .subbrandmenu ul li::after {\n      font-family: 'FontAwesome';\n      content: \"\\f105\";\n      width: 20px;\n      height: 57px;\n      margin-right: 12px;\n      right: 0px;\n      position: absolute;\n      background-size: 10px; }\n  .subbrandmenu ul li:hover::after {\n      color: #e9ecef; }\n  .subbrandmenu ul li:hover {\n      background-color: #fff;\n      color: #e9ecef;\n      border-radius: 4px 0px 0px 4px; }\n  .subbrandmenu ul li a:hover {\n      color: #e9ecef; }\n  .subbrandmenu ul li ul > li {\n      position: absolute;\n      left: 20%;\n      width: 78%;\n      top: 0PX;\n      display: none; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.ts":
/*!*********************************************************************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.ts ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: CategoriesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesListComponent", function() { return CategoriesListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CategoriesListComponent = /** @class */ (function () {
    function CategoriesListComponent() {
    }
    CategoriesListComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesListComponent.prototype, "taxons", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesListComponent.prototype, "screenwidth", void 0);
    CategoriesListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-categories-list',
            template: __webpack_require__(/*! ./categories-list.component.html */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.html"),
            styles: [__webpack_require__(/*! ./categories-list.component.scss */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], CategoriesListComponent);
    return CategoriesListComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"dpmenu\">\n  <div class=\"\" [ngClass]=\"[isOpen ? 'open main-nav-trigger-open':'',isScrolled ? 'main-nav-trigger':'cust-menu-btn']\" (mouseenter)=\"dropdown.show()\"\n    (mouseleave)=\"dropdown.hide()\">\n    <span *ngIf=\"isScrolled\"></span>\n    <strong *ngIf=\"!isScrolled\">Shop By\n      <a *ngIf=\"taxonomies[0]; let catgeory_taxonomy\">{{catgeory_taxonomy.name}}\n        <i class=\"fa pl-3 fa-chevron-down\"></i>\n      </a>\n    </strong>\n  </div>\n  <div dropdown #dropdown=\"bs-dropdown\" [autoClose]=\"false\" (isOpenChange)=\"onOpenChange($event)\" (mouseenter)=\"dropdown.show()\"\n    (mouseleave)=\"dropdown.hide()\" (click)=\"dropdown.hide()\">\n    <div id=\"dropdown-triggers-manual\" *dropdownMenu class=\"dropdown-menu container\" [ngClass]=\"[isScrolled ? '':'notscrolled']\"\n      role=\"menu\" aria-labelledby=\"button-triggers-manual\">\n      <div class=\"row\" *ngIf=\"taxonomies[0]; let catgeory_taxonomy\">\n        <div class=\"col-md-3\">\n          <ul class=\"h-100\">\n            <li *ngFor=\"let taxon of catgeory_taxonomy.root.taxons; let i= index\" \n            (click)=\"showCategoryonclick(i)\" \n            (mouseenter)=\"showCategory(i)\"\n            [ngClass]=\"{'active': selectedItem == i}\"\n            [routerLink]=\"['c' ,taxon.name]\">\n            {{taxon.name}}</li>\n          </ul>\n        </div>\n        <div class=\"col-md-9 no-gutters detailcontainer\" *ngIf=\"menuTaxons\">\n          <app-categories-details [taxons]=\"menuTaxons.taxons\" [taxonName]=\"menuTaxons.name\" [taxonImageLink]=\"menuTaxons.icon\" (click)=\"getCategeory()\"></app-categories-details>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>-->"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-menu {\n  width: 76vw;\n  padding: 0px;\n  left: -0.2rem !important;\n  top: 3rem !important;\n  z-index: 1;\n  border-radius: 0px; }\n\n.notscrolled:before {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 0;\n  top: -9.5px;\n  left: 8.6rem;\n  border-left: 10px solid transparent;\n  border-right: 10px solid transparent;\n  border-bottom: 10px solid #ccc;\n  z-index: 3; }\n\n.dpmenu {\n  position: relative; }\n\n.cust-menu-btn {\n  font-size: 1.2em;\n  padding: 0.7rem 1.2em 0.7em 0.7em;\n  border-radius: 7px 7px 0px 0px;\n  cursor: pointer;\n  margin-right: 2em;\n  position: relative;\n  left: -2px; }\n\n.cust-menu-btn:hover {\n    background-color: #d5d8db;\n    box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n\n.open {\n  background-color: #d5d8db;\n  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n\n.dpbtn .dropdown-toggle::after {\n  display: none; }\n\nul {\n  list-style: none;\n  padding: 1.3rem 0 0.5rem 1rem;\n  margin: 0px;\n  background-color: #ccc; }\n\nul li {\n    padding: 0.5rem 0.4rem;\n    background-color: #ccc;\n    border-bottom: 1px solid #ccc;\n    color: #fff;\n    cursor: pointer; }\n\nul li::after {\n      font-family: 'FontAwesome';\n      content: \"\\f105\";\n      width: 20px;\n      height: 57px;\n      margin-right: 12px;\n      right: 0px;\n      position: absolute;\n      background-size: 10px; }\n\nul li:hover::after {\n      color: #343a40; }\n\nul li ul > li {\n      position: relative;\n      left: 20%;\n      width: 78%;\n      top: 0PX;\n      display: NONE; }\n\nul .active {\n    background-color: #fff !important;\n    color: #6c757d !important;\n    border-radius: 4px 0px 0px 4px !important; }\n\nul .active::after {\n      color: #343a40; }\n\n.detailcontainer {\n  padding: 0.9em 0.5rem; }\n\n.main-nav-trigger {\n  touch-action: manipulation;\n  left: -3px;\n  position: relative;\n  -webkit-tap-highlight-color: transparent;\n  cursor: pointer; }\n\n.main-nav-trigger,\n.menu-items__links a {\n  display: block;\n  margin: 0;\n  width: 52px;\n  height: 50px; }\n\nlabel {\n  font-weight: 700;\n  text-transform: uppercase;\n  margin-bottom: 2px;\n  font-size: 12px; }\n\n.main-nav-trigger:after, .main-nav-trigger:before {\n  content: \"\";\n  width: 37px;\n  left: 40%;\n  -webkit-transform: translateX(-50%);\n  transform: translateX(-50%);\n  transition: -webkit-transform .4s ease-out, -webkit-transform-origin .4s ease-out;\n  transition: transform .4s ease-out, transform-origin .4s ease-out;\n  transition: transform .4s ease-out, transform-origin .4s ease-out, -webkit-transform .4s ease-out, -webkit-transform-origin .4s ease-out; }\n\n.main-nav-trigger:before {\n  top: 11px; }\n\n.main-nav-trigger:after {\n  bottom: 11px; }\n\n.main-nav-trigger span {\n  top: 24px;\n  left: 1px;\n  right: 11px;\n  opacity: 1;\n  width: 38px; }\n\n.main-nav-trigger:after, .main-nav-trigger:before,\n.main-nav-trigger span {\n  display: block;\n  height: 3px;\n  background: #fff;\n  border-radius: 1px;\n  position: absolute; }\n\n.main-nav-trigger-open {\n  background-color: #d5d8db;\n  box-shadow: none; }\n\n.main-nav-trigger-open:before {\n    -webkit-transform: translateX(-7px) rotate(45deg);\n    transform: translateX(-7px) rotate(45deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    background: #fff; }\n\n.main-nav-trigger-open:after {\n    -webkit-transform: translateX(-7px) rotate(-45deg);\n    transform: translateX(-7px) rotate(-45deg);\n    -webkit-transform-origin: bottom left;\n    transform-origin: bottom left;\n    background: #fff; }\n\n.main-nav-trigger-open span {\n    opacity: 0; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n  .dropup,\n  .dropright,\n  .dropdown,\n  .dropleft {\n    left: 20px !important; }\n  .dropdown-menu {\n    padding: 0px;\n    margin: 0px;\n    left: -2px;\n    background: transparent;\n    border: 0px;\n    margin-top: 10px; }\n  .petdropdown .btn {\n    padding: 0px 0 0 5px; } }\n\n@media screen and (min-width: 628px) and (max-width: 768px) {\n  .petdropdown {\n    left: -20px; }\n  .petdropdown .btn {\n    padding: 0px 0 0 5px; } }\n\n.petmenu {\n  width: 207%;\n  display: flex; }\n\n.petdropdown .btn-group > .btn:first-child {\n  margin-left: -17px; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: CategoriesMenuDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesMenuDropdownComponent", function() { return CategoriesMenuDropdownComponent; });
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CategoriesMenuDropdownComponent = /** @class */ (function () {
    function CategoriesMenuDropdownComponent(route, searchActions, store) {
        var _this = this;
        this.route = route;
        this.searchActions = searchActions;
        this.store = store;
        this.show = false;
        this.backBtnShow = false;
        this.route.queryParams
            .subscribe(function (params) {
            _this.queryParams = params;
        });
    }
    Object.defineProperty(CategoriesMenuDropdownComponent.prototype, "stateName", {
        get: function () {
            return this.show ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    CategoriesMenuDropdownComponent.prototype.ngOnInit = function () {
        if (this.screenwidth <= 1000) {
            this.dropdownWidth = this.screenwidth - 10 + 'px';
            this.autoclose = false;
        }
        else {
            this.autoclose = true;
        }
    };
    CategoriesMenuDropdownComponent.prototype.showCategory = function (i) {
        this.menuTaxons = this.taxonomies[0].root.taxons[i];
        this.selectedItem = i;
    };
    CategoriesMenuDropdownComponent.prototype.showCategoryonclick = function (i) {
        this.show = !this.show;
        if (this.screenwidth <= 1000) {
            this.menuTaxons = this.taxonomies[0].root.taxons[i];
        }
    };
    CategoriesMenuDropdownComponent.prototype.backtolist = function () {
        this.show = !this.show;
    };
    CategoriesMenuDropdownComponent.prototype.getCategeory = function () {
        var search = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["URLSearchParams"]();
        search.set('id', this.queryParams.id);
        this.store.dispatch(this.searchActions.getProductsByTaxon(search.toString()));
    };
    CategoriesMenuDropdownComponent.prototype.childCatLoaded = function (status) {
        this.backBtnShow = status;
    };
    CategoriesMenuDropdownComponent.prototype.onOpenChange = function (data) {
        this.isOpen = !this.isOpen;
        this.menuTaxons = this.taxonomies[0].root.taxons[0];
        this.selectedItem = 0;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesMenuDropdownComponent.prototype, "taxonomies", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesMenuDropdownComponent.prototype, "isScrolled", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", Object)
    ], CategoriesMenuDropdownComponent.prototype, "screenwidth", void 0);
    CategoriesMenuDropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-categories-menu-dropdown',
            template: __webpack_require__(/*! ./categories-menu-dropdown.component.html */ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.html"),
            styles: [__webpack_require__(/*! ./categories-menu-dropdown.component.scss */ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.scss")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('popOverState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({
                        left: -100 + '%'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({
                        left: 0
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('200ms ease-in'))
                ])
            ],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_0__["SearchActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"]])
    ], CategoriesMenuDropdownComponent);
    return CategoriesMenuDropdownComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex  my-flex-container\" *ngIf=\"taxonomies[0]; let catgeory_taxonomy\">\n  <div class=\"p-0 no-gutters\" class=\"parrent-catgory\" [@popOverState]=\"getparrentState\">\n    <ul class=\"bg-color\">\n      <li *ngFor=\"let taxon of catgeory_taxonomy.root.taxons; let i= index\" (click)=\"showCategory(i)\">{{taxon.name}}</li>\n    </ul>\n    <ul class=\"otherlinks\">\n      <li>\n        <a href=\"#\">Account</a>\n      </li>\n      <li>\n        <a href=\"#\">FAQs</a>\n      </li>\n      <li>\n        <a href=\"#\">Blogs</a>\n      </li>\n    </ul>\n  </div>\n  <div class=\"p-0 no-gutters\" class=\"sub-catgory\" [@popOverState]=\"getparrentState\" [@subCatgory]=\"getChildState\">\n    <ul class=\"bg-color\" *ngIf=\"menuTaxons\">\n      <li (click)=\"parrantBack()\" class=\"backbtn\"><i class=\"fa fa-chevron-left mr-2\"></i>Back</li>\n      <div class=\"subhead\">{{menuTaxons.name}}</div>\n      <li *ngFor=\"let taxon of menuTaxons.taxons; let i= index\" (click)=\"showSubCategory(i)\">{{taxon.name}}</li>\n    </ul>\n  </div>\n  <div class=\"p-0 no-gutters\" class=\"sub-sub-catgory\" [@subCatgory]=\"getChildState\">\n    <ul class=\"bg-color\" *ngIf=\"subChild\">\n      <li (click)=\"childBack()\" class=\"backbtn\"> <i class=\"fa fa-chevron-left mr-2\"></i> Back</li>\n      <div class=\"subhead\">{{subChild.name}}</div>\n      <li *ngFor=\"let taxon of subChild.taxons; let i= index\">{{taxon.name}}</li>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\n  width: 100vw;\n  list-style: none;\n  margin: 0px; }\n  ul li {\n    cursor: pointer;\n    padding: 0.8rem 1.1rem;\n    border-bottom: 1px solid #ccc;\n    color: #212529;\n    cursor: pointer; }\n  ul li::after {\n      font-family: 'FontAwesome';\n      content: \"\\f105\";\n      width: 20px;\n      height: 57px;\n      margin-right: 12px;\n      right: 0px;\n      position: absolute;\n      background-size: 10px; }\n  ul li uL > LI {\n      position: absolute;\n      left: 20%;\n      width: 78%;\n      top: 0PX;\n      display: NONE; }\n  .otherlinks li {\n  border-bottom: 1px solid #ccc; }\n  .otherlinks li a {\n    color: #212529; }\n  .otherlinks li::after {\n    font-family: 'FontAwesome';\n    content: \" \"; }\n  .parrent-catgory, .sub-catgory, .sub-sub-catgory {\n  width: 100vw;\n  left: 0vw;\n  position: relative; }\n  .sub-catgory {\n  left: -100vw; }\n  .bg-color {\n  background-color: #fff; }\n  .subhead {\n  padding: 16px;\n  font-size: 1.4em;\n  font-weight: 700;\n  line-height: 1.3;\n  background-color: whitesmoke;\n  border-bottom: 1px solid #ccc; }\n  .backbtn {\n  padding-left: 10px; }\n  .backbtn::after {\n    font-family: 'FontAwesome';\n    content: \"\";\n    width: 20px;\n    height: 57px;\n    margin-right: 12px;\n    left: 11px;\n    top: 7px;\n    display: none;\n    position: absolute;\n    background-size: 10px;\n    font-size: 20px; }\n"

/***/ }),

/***/ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: CategoryMobileMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryMobileMenuComponent", function() { return CategoryMobileMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryMobileMenuComponent = /** @class */ (function () {
    function CategoryMobileMenuComponent() {
        this.showParrent = false;
        this.showChild = false;
        this.backBtnShow = false;
    }
    CategoryMobileMenuComponent.prototype.showCategory = function (i) {
        this.menuTaxons = this.taxonomies[0].root.taxons[i];
        this.showParrent = !this.showParrent;
    };
    Object.defineProperty(CategoryMobileMenuComponent.prototype, "getparrentState", {
        get: function () {
            return this.showParrent ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CategoryMobileMenuComponent.prototype, "getChildState", {
        get: function () {
            return this.showChild ? 'show' : 'hide';
        },
        enumerable: true,
        configurable: true
    });
    CategoryMobileMenuComponent.prototype.showSubCategory = function (i) {
        this.showChild = !this.showChild;
        this.subChild = this.menuTaxons.taxons[i];
    };
    CategoryMobileMenuComponent.prototype.parrantBack = function () {
        this.showParrent = !this.showParrent;
    };
    CategoryMobileMenuComponent.prototype.childBack = function () {
        this.showChild = !this.showChild;
    };
    CategoryMobileMenuComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CategoryMobileMenuComponent.prototype, "taxonomies", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CategoryMobileMenuComponent.prototype, "isScrolled", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CategoryMobileMenuComponent.prototype, "screenwidth", void 0);
    CategoryMobileMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category-mobile-menu',
            template: __webpack_require__(/*! ./category-mobile-menu.component.html */ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.html"),
            styles: [__webpack_require__(/*! ./category-mobile-menu.component.scss */ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.scss")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('popOverState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -100 + 'vw'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: 0
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in'))
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('subCatgory', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -200 + 'vw'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        left: -100
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('show => hide', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('100ms ease-out')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hide => show', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in'))
                ])
            ],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], CategoryMobileMenuComponent);
    return CategoryMobileMenuComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/header-cart/header-cart.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/header/components/header-cart/header-cart.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header-cart btn\" [routerLink]=\"['/checkout/cart']\" *ngIf=\"!isMobile\">\n  <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i> Cart\n  <span class=\"badge badge-pill shadow-sm\">{{totalCartItems}}</span>\n</div>\n\n<div class=\"header-cart btn\" [routerLink]=\"['/checkout/cart']\" *ngIf=\"isMobile\">\n  <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\n  <span class=\"badge badge-pill shadow-sm\">{{totalCartItems}}</span>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/header-cart/header-cart.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/header/components/header-cart/header-cart.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-cart {\n  background-color: #cbced1 !important; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n  .header-cart {\n    font-size: 20px !important;\n    padding: 0px !important;\n    background-color: transparent !important; }\n  .btn .badge {\n    position: relative;\n    top: -10px;\n    left: -5px; }\n  .badge-pill {\n    padding-right: 0.5em;\n    padding-left: 0.4em;\n    border-radius: 11.2rem;\n    font-size: 0.6em; } }\n"

/***/ }),

/***/ "./src/app/layout/header/components/header-cart/header-cart.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/header/components/header-cart/header-cart.component.ts ***!
  \*******************************************************************************/
/*! exports provided: HeaderCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderCartComponent", function() { return HeaderCartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderCartComponent = /** @class */ (function () {
    function HeaderCartComponent() {
    }
    HeaderCartComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], HeaderCartComponent.prototype, "totalCartItems", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderCartComponent.prototype, "isMobile", void 0);
    HeaderCartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-cart',
            template: __webpack_require__(/*! ./header-cart.component.html */ "./src/app/layout/header/components/header-cart/header-cart.component.html"),
            styles: [__webpack_require__(/*! ./header-cart.component.scss */ "./src/app/layout/header/components/header-cart/header-cart.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], HeaderCartComponent);
    return HeaderCartComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"btn helpbtn\" [ngClass]=\"[isOpen ? 'openhelp':'']\" (mouseenter)=\"dropdown.show()\" (mouseleave)=\"dropdown.hide()\">\n    <strong>24/7 help</strong>\n    <i class=\"fa pl-3 fa-chevron-down\"></i>\n  </div>\n  <div class=\"dpmenu\" dropdown #dropdown=\"bs-dropdown\" [autoClose]=\"false\" (isOpenChange)=\"onOpenChange($event)\" (mouseenter)=\"dropdown.show()\"\n    (mouseleave)=\"dropdown.hide()\" (click)=\"dropdown.hide()\">\n    <div id=\"dropdown-triggers-manual\" *dropdownMenu class=\"dropdown-menu text-center\" [ngClass]=\"[isScrolled ? '':'notscrolled']\"\n      role=\"menu\" aria-labelledby=\"button-triggers-manual\">\n      <ul>\n        <li role=\"menuitem\">\n          <h6>Get help from our experts 24/7</h6>\n          <h2 class=\"text-danger\">1-800-xxx-xxx</h2>\n        </li>\n        <li class=\"divider dropdown-divider\"></li>\n        <li></li>\n        <li role=\"menuitem\">\n          <a class=\"dropdown-item\" [routerLink]=\"['/']\">Contact Us</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-menu {\n  width: 20rem;\n  top: -0.2rem !important;\n  border-radius: 0 0 5px 5px;\n  left: -2px !important; }\n  .dropdown-menu:before {\n    content: \"\";\n    position: absolute;\n    width: 0;\n    height: 0;\n    top: -6.9px;\n    left: 6.1rem;\n    border-left: 7px solid transparent;\n    border-right: 7px solid transparent;\n    border-bottom: 7px solid #ccc;\n    z-index: 3; }\n  .dropdown-toggle::after {\n  vertical-align: 0.055em !important; }\n  .dpmenu {\n  position: relative; }\n  .helpbtn {\n  font-size: 1.2em;\n  padding: 0.7rem 1.2em 0.7em 0.7em;\n  border-radius: 7px 7px 0px 0px;\n  cursor: pointer;\n  margin-right: 1em;\n  position: relative; }\n  .helpbtn:hover {\n    box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6);\n    background-color: #d5d8db; }\n  .openhelp {\n  background-color: #d5d8db !important;\n  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n"

/***/ }),

/***/ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: HeaderHelpDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderHelpDropdownComponent", function() { return HeaderHelpDropdownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderHelpDropdownComponent = /** @class */ (function () {
    function HeaderHelpDropdownComponent() {
    }
    HeaderHelpDropdownComponent.prototype.ngOnInit = function () {
    };
    HeaderHelpDropdownComponent.prototype.onOpenChange = function (data) {
        this.isOpen = !this.isOpen;
    };
    HeaderHelpDropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-help-dropdown',
            template: __webpack_require__(/*! ./header-help-dropdown.component.html */ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.html"),
            styles: [__webpack_require__(/*! ./header-help-dropdown.component.scss */ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], HeaderHelpDropdownComponent);
    return HeaderHelpDropdownComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/header-search/header-search.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/header/components/header-search/header-search.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"desktop-query form-inline my-2 my-lg-0\" *ngIf=\"!isMobile\">\n  <input class=\"desktop-search-bar bg-white\" type=\"search\" placeholder=\"{{searchPlaceholder}}\" aria-label=\"Search\" #box (keyup.enter)=\"onSearch(box.value)\">\n  <a class=\"desktop-submit bg-white\">\n    <span class=\"web-sprite desktop-iconSearch sprites-search\" (click)=\"onSearch(box.value)\"></span>\n  </a>\n</form>\n<form class=\"desktop-query form-inline  my-lg-0\" *ngIf=\"isMobile\" [ngClass]=\"{\n  'bg-white desktop-query-updated':isSearchopen }\">\n  <a class=\"desktop-submit\" [ngClass]=\"{\n    'themecolor':isSearchopen }\">\n    <i class=\"fa fa-search\" (click)=\"showsearch()\"></i>\n  </a>\n  <div (click)=\"onSearch(box.value)\" *ngIf=\"isSearchopen\" class=\"searchinput\">\n    <div class=\"search-container\">\n      <input class=\"desktop-search-bar bg-white\" type=\"search\" placeholder=\"{{searchPlaceholder}}\" aria-label=\"Search\" #box (keyup.enter)=\"onSearch(box.value)\"\n        (focus)=\"onFoucs()\" (blur)=\"onFoucsOut()\">\n      <button *ngIf=\"showGo\" class=\"gobtn\">Go</button>\n    </div>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/layout/header/components/header-search/header-search.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/header/components/header-search/header-search.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  width: 100%; }\n\n.desktop-query {\n  line-height: 0;\n  margin: 4px 10px 20px 0; }\n\n@media screen and (width: 768px) {\n    .desktop-query {\n      margin-left: -45px !important;\n      margin-right: 26px !important; } }\n\n.desktop-query input.desktop-search-bar {\n    width: 82%;\n    font-family: \"Whitney\";\n    display: inline-block;\n    font-size: 16px;\n    height: 20px;\n    line-height: 24px;\n    color: #212529;\n    box-sizing: content-box;\n    padding: 9px 10px 9px;\n    margin: 0;\n    outline: 0;\n    border: 1px solid #e9ecef;\n    border-radius: 4px 0 0 4px;\n    border-right: 0;\n    background: #e9ecef; }\n\n@media screen and (width: 768px) {\n      .desktop-query input.desktop-search-bar {\n        width: 97.2% !important; } }\n\n.desktop-query input.desktop-search-bar:focus,\n    .desktop-query input.desktop-search-bar:focus + a.desktop-submit {\n      background-color: #fff;\n      box-shadow: 0 0 3px -1px white; }\n\n.desktop-query a.desktop-submit {\n    box-sizing: content-box;\n    display: inline-block;\n    height: 28px;\n    width: 5%;\n    text-align: center;\n    padding: 8px 10px 2px;\n    background: #e9ecef;\n    border: 1px solid #e9ecef;\n    border-left: none;\n    border-radius: 0 4px 4px 0; }\n\n.desktop-query a.desktop-submit .desktop-iconSearch {\n      display: inline-block; }\n\n.desktop-query a.desktop-submit .sprites-search {\n      width: 21px;\n      height: 21px; }\n\n.desktop-query a.desktop-submit .web-sprite {\n      background: url(/assets/default/search-sprite.png);\n      background-position: -209px -148px;\n      display: inline-block; }\n\n.themecolor {\n  color: #e9ecef !important; }\n\n.desktop-query-updated {\n  background-color: #fff; }\n\n@media screen and (min-width: 769px) and (max-width: 1024px) {\n  input.desktop-search-bar {\n    width: 75%; } }\n\n.searchinput {\n  position: absolute;\n  width: 100%;\n  left: 0;\n  bottom: -47px;\n  background-color: #e9ecef;\n  padding: 0px 7px 10px 7px; }\n\n.searchinput .search-container {\n    border-radius: 4px;\n    background-color: #fff; }\n\n.searchinput .search-container .input.desktop-search-bar {\n      width: 100%;\n      border: 1px solid #e9ecef; }\n\n.searchinput .search-container .gobtn {\n      padding: 13px 9px 12px;\n      border-radius: 4px;\n      border: 0px;\n      background-color: #ffb132; }\n\n@media screen and (min-width: 320px) and (max-width: 768px) {\n  .desktop-query input.desktop-search-bar {\n    width: 78%;\n    border: 0px solid #e9ecef;\n    border-radius: 4px 0px 0px 4px; }\n  .desktop-query {\n    padding: 0px 7px 5.9px 4px;\n    margin-right: 4px;\n    margin-top: 2.2px;\n    margin-bottom: 0px;\n    margin-left: 0px;\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n    border: 1px solid #e9ecef; }\n    .desktop-query a.desktop-submit {\n      color: #fff;\n      height: 28px;\n      width: 21 px;\n      text-align: center;\n      font-size: 20px;\n      padding: 5px 5px 1px 5px;\n      background: transparent !important;\n      border: 0px solid #ccc;\n      border-left: none;\n      border-radius: none; }\n      .desktop-query a.desktop-submit .desktop-iconSearch {\n        display: inline-block; } }\n"

/***/ }),

/***/ "./src/app/layout/header/components/header-search/header-search.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/header/components/header-search/header-search.component.ts ***!
  \***********************************************************************************/
/*! exports provided: HeaderSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderSearchComponent", function() { return HeaderSearchComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/index.js");
/* harmony import */ var _item_share_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../item-share.service */ "./src/app/item-share.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HeaderSearchComponent = /** @class */ (function () {
    function HeaderSearchComponent(store, searchActions, router, activatedRouter, renderer, el, socket, itemsShare) {
        var _this = this;
        this.store = store;
        this.searchActions = searchActions;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.renderer = renderer;
        this.el = el;
        this.socket = socket;
        this.itemsShare = itemsShare;
        this.onSubCatClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
        this.searchPlaceholder = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.header.searchPlaceholder;
        this.showGo = false;
        this.activatedRouter.queryParams.subscribe(function (params) {
            _this.queryParams = params;
            _this.loadPage();
        });
    }
    HeaderSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemsShare.currentItems.subscribe(function (items) { return _this.items = items; });
        this.onSearch("");
    };
    HeaderSearchComponent.prototype.showsearch = function () {
        this.isSearchopen = !this.isSearchopen;
        this.onSubCatClicked.emit(false);
        if (this.isSearchopen) {
            this.renderer.addClass(document.body, 'issearchopen');
        }
        else {
            this.renderer.removeClass(document.body, 'issearchopen');
        }
    };
    HeaderSearchComponent.prototype.onSearch = function (keyword) {
        var _this = this;
        if (keyword == '') {
            keyword = 'all';
        }
        keyword = keyword.trim();
        this.socket.emit('query', { query: keyword });
        this.socket.on('result', function (result) {
            _this.itemsShare.changeItems(result.results);
        });
        /*const search = new URLSearchParams();
        search.set('q[name_cont_any]', keyword);
    
        if ('page' in this.queryParams) {
          search.set('page', this.queryParams.page);
        }
        if ('q[s]' in this.queryParams) {
          search.set('q[s]', this.queryParams['q[s]']);
        }
        this.store.dispatch(
          this.searchActions.getproductsByKeyword(search.toString())
        );
        this.router.navigate(['/search'], {
          queryParams: {
            'q[name_cont_any]': keyword,
            page: this.queryParams.page,
            'q[s]': this.queryParams['q[s]']
          }
        });
        this.store.dispatch(this.searchActions.clearCategeoryLevel());*/
    };
    HeaderSearchComponent.prototype.catgeoryFilter = function () {
        var search = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["URLSearchParams"]();
        search.set('id', this.queryParams.id);
        search.set('page', this.queryParams.page);
        if ('q[s]' in this.queryParams) {
            search.set('q[s]', this.queryParams['q[s]']);
        }
        this.store.dispatch(this.searchActions.getProductsByTaxon(search.toString()));
    };
    HeaderSearchComponent.prototype.loadPage = function () {
        if ('q[name_cont_any]' in this.queryParams && 'page' in this.queryParams) {
            this.onSearch(this.queryParams['q[name_cont_any]']);
        }
        else if ('q[name_cont_any]' in this.queryParams) {
            this.onSearch(this.queryParams['q[name_cont_any]']);
        }
        if ('id' in this.queryParams && 'page' in this.queryParams) {
            this.catgeoryFilter();
        }
        else if ('id' in this.queryParams && 'q[s]' in this.queryParams) {
            this.catgeoryFilter();
        }
        else if ('id' in this.queryParams) {
            this.catgeoryFilter();
        }
        if ('q[s]' in this.queryParams && 'q[name_cont_any]' in this.queryParams) {
            this.onSearch(this.queryParams['q[name_cont_any]']);
        }
    };
    HeaderSearchComponent.prototype.onFoucs = function () {
        this.showGo = true;
    };
    HeaderSearchComponent.prototype.onFoucsOut = function () {
        this.showGo = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderSearchComponent.prototype, "isMobile", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderSearchComponent.prototype, "isSearchopen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"])
    ], HeaderSearchComponent.prototype, "onSubCatClicked", void 0);
    HeaderSearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-header-search',
            template: __webpack_require__(/*! ./header-search.component.html */ "./src/app/layout/header/components/header-search/header-search.component.html"),
            styles: [__webpack_require__(/*! ./header-search.component.scss */ "./src/app/layout/header/components/header-search/header-search.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_2__["SearchActions"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"],
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"],
            ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__["Socket"],
            _item_share_service__WEBPACK_IMPORTED_MODULE_7__["ItemShareService"]])
    ], HeaderSearchComponent);
    return HeaderSearchComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!isMobile\">\n\n  <div *ngIf=\"!isAuthenticated\">\n    <div class=\"btn  helpbtn\" [ngClass]=\"[isOpen ? 'openhelp':'']\" (mouseenter)=\"dropdown.show()\" (mouseleave)=\"dropdown.hide()\">\n      <strong>Login & SignUp</strong>\n      <i class=\"fa pl-3 fa-chevron-down\"></i>\n    </div>\n    <div class=\"dpmenu\" dropdown #dropdown=\"bs-dropdown\" [autoClose]=\"false\" (isOpenChange)=\"onOpenChange($event)\" (mouseenter)=\"dropdown.show()\"\n      (mouseleave)=\"dropdown.hide()\" (click)=\"dropdown.hide()\">\n      <div id=\"dropdown-triggers-manual\" *dropdownMenu class=\"dropdown-menu text-center\" [ngClass]=\"[isScrolled ? '':'notscrolled']\"\n        role=\"menu\" aria-labelledby=\"button-triggers-manual\">\n        <ul>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" (click)=\"login()\">Login</a>\n          </li>\n          <li class=\"divider dropdown-divider\"></li>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" [routerLink]=\"['/auth/signup']\">Sign Up</a>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n  <div *ngIf=\"isAuthenticated\">\n    <div class=\"btn  helpbtn\" [ngClass]=\"[isOpen ? 'openhelp':'']\" (mouseenter)=\"dropdown.show()\" (mouseleave)=\"dropdown.hide()\">\n      <i class=\"fa fa-user mr-2\" aria-hidden=\"true\"></i>\n      <strong class=\"text-capitalize\">{{email}}\n        <i class=\"fa pl-3 fa-chevron-down\"></i>\n      </strong>\n    </div>\n    <div class=\"dpmenu\" dropdown #dropdown=\"bs-dropdown\" [autoClose]=\"false\" (isOpenChange)=\"onOpenChange($event)\" (mouseenter)=\"dropdown.show()\"\n      (mouseleave)=\"dropdown.hide()\" (click)=\"dropdown.hide()\">\n      <div id=\"dropdown-triggers-manual\" *dropdownMenu class=\"dropdown-menu text-center\" [ngClass]=\"[isAuthenticated ? 'arrowalignement':'']\"\n        [ngClass]=\"[isScrolled ? '':'notscrolled']\" role=\"menu\" aria-labelledby=\"button-triggers-manual\">\n        <ul>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" routerLink=\"/user\">My Profile</a>\n          </li>\n          <li class=\"divider dropdown-divider\"></li>\n\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" routerLink=\"/user/orders\">My Orders</a>\n          </li>\n          <li class=\"divider dropdown-divider\"></li>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" routerLink=\"/user/addresses\">Saved Addresses</a>\n          </li>\n          <li class=\"divider dropdown-divider\"></li>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" href=\"#\">Edit Profile</a>\n          </li>\n          <li class=\"divider dropdown-divider\"></li>\n          <li role=\"menuitem\">\n            <a class=\"dropdown-item\" (click)=\"logout()\">Logout</a>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n</div>\n<div *ngIf=\"isMobile\" class=\"mobileusericon\">\n  <i class=\"fa fa-user mr-2\" (click)=\"login()\" aria-hidden=\"true\" *ngIf=\"!isAuthenticated\"></i>\n  <div class=\"btn-group\" dropdown *ngIf=\"isAuthenticated\">\n    <button id=\"button-basic\" dropdownToggle type=\"button\" class=\"btn pr-0 pl-0\" aria-controls=\"dropdown-basic\">\n      <i class=\"fa fa-user mr-2\" aria-hidden=\"true\"></i>\n    </button>\n    <ul id=\"dropdown-split\" *dropdownMenu class=\"dropdown-menu mobile-profile\" role=\"menu\" aria-labelledby=\"button-split\">\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item\" routerLink=\"/user\">My Profile</a>\n      </li>\n      <li class=\"divider dropdown-divider\"></li>\n\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item\" routerLink=\"/user/orders\">My Orders</a>\n      </li>\n      <li class=\"divider dropdown-divider\"></li>\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item\" routerLink=\"/user/addresses\">Saved Addresses</a>\n      </li>\n      <li class=\"divider dropdown-divider\"></li>\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item\" href=\"#\">Edit Profile</a>\n      </li>\n\n      <li class=\"divider dropdown-divider\"></li>\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item\" (click)=\"logout()\">Logout</a>\n      </li>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-toggle::after {\n  vertical-align: 0.055em !important; }\n\n.dropdown-menu {\n  top: -0.2rem !important;\n  border-radius: 0 0 5px 5px;\n  width: 12em;\n  left: 0.9em !important; }\n\n.dropdown-menu:before {\n    content: \"\";\n    position: absolute;\n    width: 0;\n    height: 0;\n    top: -6.9px;\n    left: 8.7rem;\n    border-left: 7px solid transparent;\n    border-right: 7px solid transparent;\n    border-bottom: 7px solid #ccc;\n    z-index: 3; }\n\n.arrowalignement:before {\n  left: 6.2rem; }\n\n.border-left {\n  border-left: 1px solid #dee2e6 !important; }\n\n.mobileusericon {\n  margin-top: -16px;\n  font-size: 20px; }\n\n.mobile-profile {\n  width: 100vw;\n  min-width: 101.7vw;\n  left: -76.6vw !important;\n  height: calc(100vh - 9vw);\n  top: 10vw !important; }\n\n.mobile-profile:before {\n    left: 15.3rem;\n    border-left: 7px solid transparent;\n    border-right: 7px solid transparent;\n    border-bottom: 7px solid #fff; }\n\n.dpmenu {\n  position: relative; }\n\n.helpbtn {\n  font-size: 1.2em;\n  padding: 0.7rem 1.2em 0.7em 0.7em;\n  border-radius: 7px 7px 0px 0px;\n  cursor: pointer;\n  margin: 0 1em;\n  position: relative; }\n\n.helpbtn:hover {\n    background-color: #d5d8db;\n    box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n\n.openhelp {\n  background-color: #d5d8db !important;\n  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.6); }\n"

/***/ }),

/***/ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ProfileDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileDropdownComponent", function() { return ProfileDropdownComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileDropdownComponent = /** @class */ (function () {
    function ProfileDropdownComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.email = '';
    }
    ProfileDropdownComponent.prototype.ngOnInit = function () {
    };
    ProfileDropdownComponent.prototype.onOpenChange = function (data) {
        this.isOpen = !this.isOpen;
    };
    ProfileDropdownComponent.prototype.ngOnChanges = function () {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        if (this.currentUser) {
            this.email = this.currentUser.email.split('@')[0];
        }
    };
    ProfileDropdownComponent.prototype.logout = function () {
        var _this = this;
        this.subnav = !this.subnav;
        this.authService.logout().
            subscribe(function (res) {
            _this.router.navigate(['auth', 'login']);
        });
    };
    ProfileDropdownComponent.prototype.login = function () {
        this.router.navigate(['/auth/login']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ProfileDropdownComponent.prototype, "isAuthenticated", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ProfileDropdownComponent.prototype, "isMobile", void 0);
    ProfileDropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-dropdown',
            template: __webpack_require__(/*! ./profile-dropdown.component.html */ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.html"),
            styles: [__webpack_require__(/*! ./profile-dropdown.component.scss */ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]])
    ], ProfileDropdownComponent);
    return ProfileDropdownComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/header.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\n  <nav class=\"navbar navbar-expand-lg\" id=\"main\" [ngClass]=\"{'page-scroll-header': isScrolled}\">\n    <div class=\"container\">\n      <app-categories-menu-dropdown [taxonomies]=\"taxonomies$| async\" [screenwidth]=\"screenwidth\" [isScrolled]=\"isScrolled\" *ngIf=\"!isMobile && isScrolled\"></app-categories-menu-dropdown>\n      <a class=\"navbar-brand\" routerLink=\"/\" *ngIf=\"!isMobile\">\n        <!--<img [src]=\"this.headerConfig.brand.logo\" [alt]=\"this.headerConfig.brand.name\" [width]=\"this.headerConfig.brand.width\" [height]=\"this.headerConfig.brand.height\"\n          class=\"d-inline-block align-top\" alt=\"\"> -->\n          <h1 style=\"font: Acionica Regulat; color: black\">pasala</h1>\n      </a>\n      <a (click)=\"showModal()\" for=\"main-nav-state\" class=\"main-nav-trigger\" *ngIf=\"isMobile\" [ngClass]=\"{'main-nav-trigger-open':isModalShown}\">\n        <span></span>\n      </a>\n      <a *ngIf=\"this.headerConfig.showGithubRibon\" href=\"https://github.com/aviabird/angularspree\">\n        <img style=\"position: absolute; top: 0; right: 0; border: 0; height: 130px; z-index: 9999;\" src=\"https://camo.githubusercontent.com/38ef81f8aca64bb9a64448d0d70f1308ef5341ab/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6461726b626c75655f3132313632312e706e67\"\n          alt=\"Fork me on GitHub\" data-canonical-src=\"https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png\">\n      </a>\n      <!-- Collect the nav links, forms, and other content for toggling -->\n      <div class=\"collapse navbar-collapse justify-content-end\" id=\"navbarNavDropdown\">\n        <app-header-search></app-header-search>\n        <app-profile-dropdown class=\"pr-2 border-left\" [isAuthenticated]=\"isAuthenticated | async\"></app-profile-dropdown>\n        <app-header-cart class=\"pl-2 border-left\" [totalCartItems]=\"totalCartItems | async\"></app-header-cart>\n      </div>\n      <div class=\"resheader row m-0 hidden-md hidden-lg\" *ngIf=\"isMobile\">\n        <div class=\"search\">\n          <app-header-search [isMobile]=\"isMobile\" (onSubCatClicked)=\"childCatLoaded($event)\" [isSearchopen]=\"isSearchopen\"></app-header-search>\n        </div>\n        <div class=\"mlogo\">\n          <a class=\"navbar-brand small-logo\" routerLink=\"/\" *ngIf=\"isMobile\">\n            <img [src]=\"this.headerConfig.brand.logo\" [alt]=\"this.headerConfig.brand.name\" [width]=\"this.headerConfig.brand.width\" height=\"this.headerConfig.brand.height\"\n              class=\"d-inline-block align-top\" alt=\"\">\n          </a>\n        </div>\n        <div class=\"profile\">\n          <app-profile-dropdown [isMobile]=\"isMobile\" class=\"pr-2 \" [isAuthenticated]=\"isAuthenticated | async\"></app-profile-dropdown>\n        </div>\n        <div class=\"cart\">\n          <app-header-cart class=\"pl-2 border-left\" [isMobile]=\"isMobile\" [totalCartItems]=\"totalCartItems | async\"></app-header-cart>\n        </div>\n      </div>\n    </div>\n  </nav>\n  <nav class=\"navbar navbar-expand-lg\">\n    <div class=\"container\">\n      <div class=\"collapse navbar-collapse left-content-start\">\n        <app-categories-menu-dropdown [screenwidth]=\"screenwidth\" [taxonomies]=\"taxonomies$| async\"></app-categories-menu-dropdown>\n        <app-brand-menu-dropdown [taxonomies]=\"taxonomies$| async | slice:1:2\"></app-brand-menu-dropdown>\n        <div class=\"col text-right\">\n\n          <!--<button type=\"button\" class=\"btn shippingbtn\"> FREE 1-2 DAY SHIPPING OVER &#8377;699!\n            <i class=\"fa ml-2 fa-truck\" aria-hidden=\"true\"></i>\n          </button>-->\n        </div>\n      </div>\n    </div>\n  </nav>\n</div>\n<div *ngIf=\"isModalShown\" [config]=\"{ show: true ,backdrop:false}\" (onHidden)=\"onHidden()\" bsModal #autoShownModal=\"bs-modal\"\n  class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-auto-name\">\n  <div class=\"modal-dialog  cat-mobile \">\n    <div class=\"modal-content\">\n      <div class=\"modal-body\">\n        <app-category-mobile-menu [taxonomies]=\"taxonomies$| async\"></app-category-mobile-menu>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/header/header.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar {\n  transition: all 500ms;\n  padding: 0px;\n  padding-top: 1rem; }\n  .navbar.page-scroll-header {\n    position: fixed;\n    z-index: 100;\n    width: 100%; }\n  .navbar:not(#main), .navbar.page-scroll-header {\n    box-shadow: 0 8px 6px -8px #212529;\n    z-index: 1; }\n  .shippingbtn {\n  font-size: 1.3em !important; }\n  .shippingbtn i {\n    font-size: 20px;\n    color: #fff;\n    -webkit-transform: scaleX(-1);\n    transform: scaleX(-1);\n    -webkit-filter: FlipH;\n            filter: FlipH;\n    -ms-filter: \"FlipH\"; }\n  .shippingbtn:hover {\n    background-color: #d5d8db !important; }\n  .page-scroll-header {\n  padding-top: 0px;\n  padding-bottom: 0px; }\n  :host /deep/ .page-scroll-header .cust-menu-btn {\n  border-radius: 0px;\n  margin-right: 10px; }\n  :host /deep/ .dropdown-menu {\n  top: 3.1em !important; }\n  .selected-theme .navbar {\n  box-shadow: none !important; }\n  .header {\n  background-color: #e9ecef; }\n  .header .resheader {\n    width: calc(100vw - 15vw); }\n  @media screen and (width: 768px) {\n      .header .resheader {\n        width: calc(100vw - 12vw); } }\n  .profile {\n  padding-top: 15px;\n  flex: 0.2;\n  display: flex;\n  justify-content: flex-end;\n  align-items: center; }\n  .mlogo {\n  flex: 4;\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n  .cart {\n  padding-top: 6px;\n  padding-right: 10px;\n  flex: .5;\n  display: flex;\n  justify-content: flex-end;\n  align-items: center; }\n  .search {\n  padding-top: 2px;\n  flex: 0.2;\n  display: flex;\n  justify-content: flex-start;\n  align-items: center; }\n  .small-logo img {\n  width: 120px;\n  margin-top: 0; }\n  @media screen and (min-width: 321px) and (max-width: 768px) {\n  .navbar {\n    padding: 0px; }\n  .cart {\n    padding-top: 0;\n    padding-right: 0; }\n  :host /deep/ .page-scroll-header .cust-menu-btn {\n    display: none; }\n  .navbar .btn {\n    padding: 0px; }\n  .selected-theme .navbar .btn {\n    padding: 0px; } }\n  @media screen and (min-width: 310px) and (max-width: 320px) {\n  .navbar {\n    padding: 0px; }\n  .cart {\n    padding-top: 0;\n    padding-right: 0; }\n  :host /deep/ .page-scroll-header .cust-menu-btn {\n    display: none; }\n  .search {\n    flex: 1; }\n  .cart {\n    flex: 1; }\n  .mlogo {\n    flex: 39; }\n  .profile {\n    flex: 1; } }\n  .modal {\n  top: 2.7rem; }\n  .modal .cat-mobile {\n    border-radius: 0px;\n    margin: 0px;\n    width: 100vw;\n    height: 100vh;\n    max-width: 100vw !important; }\n  .modal .cat-mobile .modal-content {\n      border-radius: 0px;\n      border: 0px solid transparent;\n      height: 100vh; }\n  .modal .cat-mobile .modal-body {\n      padding: 0px;\n      background-color: whitesmoke; }\n  .main-nav-trigger {\n  ms-touch-action: manipulation;\n  touch-action: manipulation;\n  left: 3px;\n  position: relative;\n  border-radius: 5px 5px 0 0;\n  -webkit-tap-highlight-color: transparent; }\n  .main-nav-trigger,\n.menu-items__links a {\n  display: block;\n  width: 40px;\n  height: 37px;\n  margin: 0;\n  top: -4.5px;\n  left: 6px; }\n  label {\n  font-weight: 700;\n  text-transform: uppercase;\n  margin-bottom: 2px;\n  font-size: 12px; }\n  .main-nav-trigger:after, .main-nav-trigger:before {\n  content: \"\";\n  width: 20px;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n  transform: translateX(-50%);\n  transition: -webkit-transform .4s ease-out, -webkit-transform-origin .4s ease-out;\n  transition: transform .4s ease-out, transform-origin .4s ease-out;\n  transition: transform .4s ease-out, transform-origin .4s ease-out, -webkit-transform .4s ease-out, -webkit-transform-origin .4s ease-out; }\n  .main-nav-trigger:before {\n  top: 13px; }\n  .main-nav-trigger:after {\n  bottom: 8px; }\n  .main-nav-trigger span {\n  top: 20px;\n  left: 10px;\n  right: 11px;\n  opacity: 1;\n  width: 20px; }\n  .main-nav-trigger:after, .main-nav-trigger:before,\n.main-nav-trigger span {\n  display: block;\n  height: 2px;\n  background: #fff;\n  border-radius: 1px;\n  position: absolute; }\n  .main-nav-trigger-open {\n  top: 1.5px;\n  background: #fff;\n  box-shadow: 0 1px 0 #fff; }\n  .main-nav-trigger-open:before {\n    -webkit-transform: translateX(-7px) rotate(45deg);\n    transform: translateX(-7px) rotate(45deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    background: #e9ecef; }\n  .main-nav-trigger-open:after {\n    -webkit-transform: translateX(-7px) rotate(-45deg);\n    transform: translateX(-7px) rotate(-45deg);\n    -webkit-transform-origin: bottom left;\n    transform-origin: bottom left;\n    background: #e9ecef; }\n  .main-nav-trigger-open span {\n    opacity: 0; }\n"

/***/ }),

/***/ "./src/app/layout/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../product/actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _product_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../product/reducers/selectors */ "./src/app/product/reducers/selectors.ts");
/* harmony import */ var _checkout_reducers_selectors__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../checkout/reducers/selectors */ "./src/app/checkout/reducers/selectors.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../auth/reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../auth/actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(store, authService, authActions, searchActions, actions, router, modalService, renderer) {
        this.store = store;
        this.authService = authService;
        this.authActions = authActions;
        this.searchActions = searchActions;
        this.actions = actions;
        this.router = router;
        this.modalService = modalService;
        this.renderer = renderer;
        this.isModalShown = false;
        this.isSearchopen = true;
        this.headerConfig = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].config.header;
        this.isScrolled = false;
        this.currPos = 0;
        this.startPos = 0;
        this.changePos = 100;
        this.isMobile = false;
        this.config = {
            backdrop: false,
            ignoreBackdropClick: false
        };
        this.taxonList = [
            {
                id: 4,
                name: 'Mugs',
                pretty_name: 'Categories -> Mugs',
                permalink: 'categories/mugs',
                parent_id: 1,
                taxonomy_id: 1,
                taxons: null
            },
            {
                id: 3,
                name: 'Bags',
                pretty_name: 'Categories -> Bags',
                permalink: 'categories/bags',
                parent_id: 1,
                taxonomy_id: 1,
                taxons: null
            },
            {
                id: 8,
                name: 'Ruby',
                pretty_name: 'Brand -> Ruby',
                permalink: 'brand/ruby',
                parent_id: 2,
                taxonomy_id: 2,
                taxons: null
            },
            {
                id: 9,
                name: 'Apache',
                pretty_name: 'Brand -> Apache',
                permalink: 'brand/apache',
                parent_id: 2,
                taxonomy_id: 2,
                taxons: null
            },
            {
                id: 10,
                name: 'Spree',
                pretty_name: 'Brand -> Spree',
                permalink: 'brand/spree',
                parent_id: 2,
                taxonomy_id: 2,
                taxons: null
            },
            {
                id: 11,
                name: 'Rails',
                pretty_name: 'Brand -> Rails',
                permalink: 'brand/rails',
                parent_id: 2,
                taxonomy_id: 2,
                taxons: null
            }
        ];
        this.taxonomies$ = this.store.select(_product_reducers_selectors__WEBPACK_IMPORTED_MODULE_4__["getTaxonomies"]);
        this.store.dispatch(this.actions.getAllTaxonomies());
        if (this.isSearchopen) {
            this.renderer.addClass(document.body, 'issearchopen');
        }
        else {
            this.renderer.removeClass(document.body, 'issearchopen');
        }
    }
    HeaderComponent.prototype.openModalWithClass = function (template) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'cat-mobile' }, this.config));
    };
    HeaderComponent.prototype.ngOnInit = function () {
        this.store.dispatch(this.authActions.authorize());
        this.store.dispatch(this.authActions.login());
        this.isAuthenticated = this.store.select(_auth_reducers_selectors__WEBPACK_IMPORTED_MODULE_8__["getAuthStatus"]);
        this.totalCartItems = this.store.select(_checkout_reducers_selectors__WEBPACK_IMPORTED_MODULE_5__["getTotalCartItems"]);
        this.screenwidth = window.innerWidth;
        this.calculateInnerWidth();
    };
    HeaderComponent.prototype.calculateInnerWidth = function () {
        if (this.screenwidth <= 1000) {
            this.isScrolled = false;
            this.isMobile = this.screenwidth;
        }
    };
    HeaderComponent.prototype.selectTaxon = function (taxon) {
        this.router.navigateByUrl('/');
        this.store.dispatch(this.searchActions.addFilter(taxon));
    };
    HeaderComponent.prototype.showModal = function () {
        this.isModalShown = !this.isModalShown;
        this.isSearchopen = !this.isSearchopen;
        if (this.isModalShown) {
            this.renderer.addClass(document.body, 'isModalShown');
        }
        else {
            this.renderer.removeClass(document.body, 'isModalShown');
        }
        if (this.isSearchopen) {
            this.renderer.addClass(document.body, 'issearchopen');
        }
        else {
            this.renderer.removeClass(document.body, 'issearchopen');
        }
    };
    HeaderComponent.prototype.hideModal = function () {
        this.autoShownModal.hide();
    };
    HeaderComponent.prototype.onHidden = function () {
        this.isModalShown = false;
    };
    HeaderComponent.prototype.updateHeader = function (evt) {
        if (this.screenwidth >= 1000) {
            this.currPos =
                (window.pageYOffset || evt.target.scrollTop) -
                    (evt.target.clientTop || 0);
            if (this.currPos >= this.changePos) {
                this.isScrolled = true;
            }
            else {
                this.isScrolled = false;
            }
        }
    };
    HeaderComponent.prototype.childCatLoaded = function (status) {
        this.isModalShown = status;
        this.isSearchopen = !status;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ViewChild"])('autoShownModal'),
        __metadata("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["ModalDirective"])
    ], HeaderComponent.prototype, "autoShownModal", void 0);
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/layout/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/header/header.component.scss")],
            // tslint:disable-next-line:use-host-property-decorator
            host: {
                '(window:scroll)': 'updateHeader($event)'
            },
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"],
            _auth_actions_auth_actions__WEBPACK_IMPORTED_MODULE_10__["AuthActions"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_3__["SearchActions"],
            _product_actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["BsModalService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_6__["Renderer2"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/index.ts":
/*!*********************************!*\
  !*** ./src/app/layout/index.ts ***!
  \*********************************/
/*! exports provided: LayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var _footer_component_footer_quick_links_footer_quick_links_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer/component/footer-quick-links/footer-quick-links.component */ "./src/app/layout/footer/component/footer-quick-links/footer-quick-links.component.ts");
/* harmony import */ var _footer_component_footer_social_links_footer_social_links_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer/component/footer-social-links/footer-social-links.component */ "./src/app/layout/footer/component/footer-social-links/footer-social-links.component.ts");
/* harmony import */ var _footer_component_footer_contact_info_footer_contact_info_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer/component/footer-contact-info/footer-contact-info.component */ "./src/app/layout/footer/component/footer-contact-info/footer-contact-info.component.ts");
/* harmony import */ var _header_components_categories_menu_dropdown_categories_components_categories_details_brand_logo_brand_logo_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/brand-logo/brand-logo.component.ts");
/* harmony import */ var _header_components_brand_menu_dropdown_brand_componant_brand_list_brand_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component */ "./src/app/layout/header/components/brand-menu-dropdown/brand-componant/brand-list/brand-list.component.ts");
/* harmony import */ var _header_components_categories_menu_dropdown_categories_components_categories_details_categories_list_categories_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-list/categories-list.component.ts");
/* harmony import */ var _header_components_categories_menu_dropdown_categories_components_categories_details_categories_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component */ "./src/app/layout/header/components/categories-menu-dropdown/categories-components/categories-details/categories-details.component.ts");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/index.js");
/* harmony import */ var _header_components_brand_menu_dropdown_brand_menu_dropdown_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./header/components/brand-menu-dropdown/brand-menu-dropdown.component */ "./src/app/layout/header/components/brand-menu-dropdown/brand-menu-dropdown.component.ts");
/* harmony import */ var _header_components_categories_menu_dropdown_categories_menu_dropdown_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header/components/categories-menu-dropdown/categories-menu-dropdown.component */ "./src/app/layout/header/components/categories-menu-dropdown/categories-menu-dropdown.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./header/header.component */ "./src/app/layout/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/layout/footer/footer.component.ts");
/* harmony import */ var _header_components_profile_dropdown_profile_dropdown_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./header/components/profile-dropdown/profile-dropdown.component */ "./src/app/layout/header/components/profile-dropdown/profile-dropdown.component.ts");
/* harmony import */ var _header_components_header_search_header_search_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./header/components/header-search/header-search.component */ "./src/app/layout/header/components/header-search/header-search.component.ts");
/* harmony import */ var _header_components_header_cart_header_cart_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./header/components/header-cart/header-cart.component */ "./src/app/layout/header/components/header-cart/header-cart.component.ts");
/* harmony import */ var _header_components_header_help_dropdown_header_help_dropdown_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./header/components/header-help-dropdown/header-help-dropdown.component */ "./src/app/layout/header/components/header-help-dropdown/header-help-dropdown.component.ts");
/* harmony import */ var _header_components_category_mobile_menu_category_mobile_menu_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./header/components/category-mobile-menu/category-mobile-menu.component */ "./src/app/layout/header/components/category-mobile-menu/category-mobile-menu.component.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// tslint:disable-next-line:max-line-length


// tslint:disable-next-line:max-line-length

// tslint:disable-next-line:max-line-length






// Components







// Modules


var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_10__["NgModule"])({
            declarations: [
                // components
                _header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
                _header_components_header_search_header_search_component__WEBPACK_IMPORTED_MODULE_15__["HeaderSearchComponent"],
                _header_components_header_cart_header_cart_component__WEBPACK_IMPORTED_MODULE_16__["HeaderCartComponent"],
                _header_components_header_help_dropdown_header_help_dropdown_component__WEBPACK_IMPORTED_MODULE_17__["HeaderHelpDropdownComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_13__["FooterComponent"],
                _header_components_categories_menu_dropdown_categories_menu_dropdown_component__WEBPACK_IMPORTED_MODULE_9__["CategoriesMenuDropdownComponent"],
                _header_components_brand_menu_dropdown_brand_menu_dropdown_component__WEBPACK_IMPORTED_MODULE_8__["BrandMenuDropdownComponent"],
                _header_components_categories_menu_dropdown_categories_components_categories_details_categories_details_component__WEBPACK_IMPORTED_MODULE_6__["CategoriesDetailsComponent"],
                _header_components_categories_menu_dropdown_categories_components_categories_details_categories_list_categories_list_component__WEBPACK_IMPORTED_MODULE_5__["CategoriesListComponent"],
                _header_components_brand_menu_dropdown_brand_componant_brand_list_brand_list_component__WEBPACK_IMPORTED_MODULE_4__["BrandListComponent"],
                _header_components_categories_menu_dropdown_categories_components_categories_details_brand_logo_brand_logo_component__WEBPACK_IMPORTED_MODULE_3__["BrandLogoComponent"],
                _footer_component_footer_contact_info_footer_contact_info_component__WEBPACK_IMPORTED_MODULE_2__["FooterContactInfoComponent"],
                _footer_component_footer_social_links_footer_social_links_component__WEBPACK_IMPORTED_MODULE_1__["FooterSocialLinksComponent"],
                _footer_component_footer_quick_links_footer_quick_links_component__WEBPACK_IMPORTED_MODULE_0__["FooterQuickLinksComponent"],
                _header_components_category_mobile_menu_category_mobile_menu_component__WEBPACK_IMPORTED_MODULE_18__["CategoryMobileMenuComponent"],
                // sub components
                _header_components_profile_dropdown_profile_dropdown_component__WEBPACK_IMPORTED_MODULE_14__["ProfileDropdownComponent"]
                // pipes
            ],
            exports: [
                _header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_13__["FooterComponent"],
            ],
            imports: [
                _shared_index__WEBPACK_IMPORTED_MODULE_19__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_20__["RouterModule"],
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_7__["TabsModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_11__["ModalModule"].forRoot()
            ]
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ }),

/***/ "./src/app/oauth_config.ts":
/*!*********************************!*\
  !*** ./src/app/oauth_config.ts ***!
  \*********************************/
/*! exports provided: myAuthConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "myAuthConfig", function() { return myAuthConfig; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");

var myAuthConfig = {
    providers: {
        google: {
            clientId: "682466898339-lh2psagi1s3pgo9h5unri91ogptom1os.apps.googleusercontent.com",
            url: _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].config.prodApiEndpoint + "/auth/google"
        }
    }
};


/***/ }),

/***/ "./src/app/product/actions/product-actions.ts":
/*!****************************************************!*\
  !*** ./src/app/product/actions/product-actions.ts ***!
  \****************************************************/
/*! exports provided: ProductActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductActions", function() { return ProductActions; });
var ProductActions = /** @class */ (function () {
    function ProductActions() {
    }
    ProductActions.prototype.getAllProducts = function (pageNumber) {
        if (pageNumber === void 0) { pageNumber = 1; }
        return {
            type: ProductActions.GET_ALL_PRODUCTS,
            payload: pageNumber
        };
    };
    ProductActions.prototype.getProductDetail = function (id) {
        return {
            type: ProductActions.GET_PRODUCT_DETAIL,
            payload: id
        };
    };
    // change products type to Product[]
    ProductActions.prototype.getAllProductsSuccess = function (products) {
        return {
            type: ProductActions.GET_ALL_PRODUCTS_SUCCESS,
            payload: products
        };
    };
    ProductActions.prototype.getProductDetailSuccess = function (data) {
        return {
            type: ProductActions.GET_PRODUCT_DETAIL_SUCCESS,
            payload: data
        };
    };
    ProductActions.prototype.clearSelectedProduct = function () {
        return { type: ProductActions.CLEAR_SELECTED_PRODUCT };
    };
    ProductActions.prototype.getAllTaxonomies = function () {
        return { type: ProductActions.GET_ALL_TAXONOMIES };
    };
    ProductActions.prototype.getAllTaxonomiesSuccess = function (taxonomies) {
        return {
            type: ProductActions.GET_ALL_TAXONOMIES_SUCCESS,
            payload: taxonomies
        };
    };
    ProductActions.prototype.getRelatedProduct = function (product_id) {
        return {
            type: ProductActions.GET_RELATED_PRODUCT,
            payload: product_id
        };
    };
    ProductActions.prototype.getRelatedProductSuccess = function (products) {
        return {
            type: ProductActions.GET_RELATED_PRODUCT_SUCCESS,
            payload: products
        };
    };
    ProductActions.prototype.getProductReviews = function (product_id) {
        return {
            type: ProductActions.GET_REVIEWS,
            payload: product_id
        };
    };
    ProductActions.prototype.getProductReviewsSuccess = function (reviews) {
        return {
            type: ProductActions.GET_REVIEWS_SUCCESS,
            payload: reviews
        };
    };
    ProductActions.GET_ALL_PRODUCTS = 'GET_ALL_PRODUCTS';
    ProductActions.GET_ALL_PRODUCTS_SUCCESS = 'GET_ALL_PRODUCTS_SUCCESS';
    ProductActions.GET_PRODUCT_DETAIL = 'GET_PRODUCT_DETAIL';
    ProductActions.GET_PRODUCT_DETAIL_SUCCESS = 'GET_PRODUCT_DETAIL_SUCCESS';
    ProductActions.CLEAR_SELECTED_PRODUCT = 'CLEAR_SELECTED_PRODUCT';
    ProductActions.GET_ALL_TAXONOMIES = 'GET_ALL_TAXONOMIES';
    ProductActions.GET_ALL_TAXONOMIES_SUCCESS = 'GET_ALL_TAXONOMIES_SUCCESS';
    ProductActions.GET_ALL_PRODUCTS_SEARCH_SUCCESS = 'GET_ALL_PRODUCTS_SEARCH_SUCCESS';
    ProductActions.GET_RELATED_PRODUCT = 'GET_RELATED_PRODUCT';
    ProductActions.GET_RELATED_PRODUCT_SUCCESS = 'GET_RELATED_PRODUCT_SUCCESS';
    ProductActions.GET_REVIEWS = 'GET_REVIEWS';
    ProductActions.GET_REVIEWS_SUCCESS = 'GET_REVIEWS_SUCCESS';
    return ProductActions;
}());



/***/ }),

/***/ "./src/app/product/effects/product.effects.ts":
/*!****************************************************!*\
  !*** ./src/app/product/effects/product.effects.ts ***!
  \****************************************************/
/*! exports provided: ProductEffects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductEffects", function() { return ProductEffects; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../home/reducers/search.actions */ "./src/app/home/reducers/search.actions.ts");
/* harmony import */ var _actions_product_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../core/services/product.service */ "./src/app/core/services/product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProductEffects = /** @class */ (function () {
    function ProductEffects(actions$, productService, productActions, searchActions) {
        var _this = this;
        this.actions$ = actions$;
        this.productService = productService;
        this.productActions = productActions;
        this.searchActions = searchActions;
        // tslint:disable-next-line:member-ordering
        this.GetAllProducts$ = this.actions$
            .ofType(_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"].GET_ALL_PRODUCTS)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getProducts(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            return _this.productActions.getAllProductsSuccess({ products: data });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetAllTaxonomies$ = this.actions$
            .ofType(_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"].GET_ALL_TAXONOMIES)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) { return _this.productService.getTaxonomies(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            return _this.productActions.getAllTaxonomiesSuccess({ taxonomies: data });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetProductDetail$ = this.actions$
            .ofType(_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"].GET_PRODUCT_DETAIL)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getProduct(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (product) {
            return _this.productActions.getProductDetailSuccess({ product: product });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetProductsByKeyword$ = this.actions$
            .ofType(_home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"].GET_PRODUCTS_BY_KEYWORD)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getproductsByKeyword(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (_a) {
            var products = _a.products, pagination = _a.pagination;
            return _this.searchActions.getProducsByKeywordSuccess({ products: products, pagination: pagination });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetProductsByTaxons$ = this.actions$
            .ofType(_home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"].GET_PRODUCTS_BY_TAXON)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getProductsByTaxon(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (_a) {
            var products = _a.products, pagination = _a.pagination;
            return _this.searchActions.getProducsByKeywordSuccess({ products: products, pagination: pagination });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetChildTaxons$ = this.actions$
            .ofType(_home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"].GET_CHILD_TAXONS)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getChildTaxons(action.payload.taxonomiesId, action.payload.taxonId);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            return _this.searchActions.getChildTaxonsSuccess({ taxonList: data });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetTaxonomiByName$ = this.actions$
            .ofType(_home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"].GET_TAXONOMIES_BY_NAME)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getTaxonByName(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            return _this.searchActions.getTaxonomiesByNameSuccess({ taxonomiList: data });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetRelatedProducts$ = this.actions$
            .ofType(_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"].GET_RELATED_PRODUCT)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getRelatedProducts(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (products) {
            return _this.productActions.getRelatedProductSuccess({ products: products });
        }));
        // tslint:disable-next-line:member-ordering
        this.GetReview$ = this.actions$
            .ofType(_actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"].GET_REVIEWS)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) {
            return _this.productService.getProductReviews(action.payload);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (data) {
            return _this.productActions.getProductReviewsSuccess({ reviews: data });
        }));
    }
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetAllProducts$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetAllTaxonomies$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetProductDetail$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetProductsByKeyword$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetProductsByTaxons$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetChildTaxons$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetTaxonomiByName$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetRelatedProducts$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProductEffects.prototype, "GetReview$", void 0);
    ProductEffects = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])(),
        __metadata("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["Actions"],
            _core_services_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"],
            _actions_product_actions__WEBPACK_IMPORTED_MODULE_2__["ProductActions"],
            _home_reducers_search_actions__WEBPACK_IMPORTED_MODULE_1__["SearchActions"]])
    ], ProductEffects);
    return ProductEffects;
}());



/***/ }),

/***/ "./src/app/product/reducers/product-reducer.ts":
/*!*****************************************************!*\
  !*** ./src/app/product/reducers/product-reducer.ts ***!
  \*****************************************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../actions/product-actions */ "./src/app/product/actions/product-actions.ts");
/* harmony import */ var _product_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-state */ "./src/app/product/reducers/product-state.ts");


var initialState = new _product_state__WEBPACK_IMPORTED_MODULE_1__["ProductStateRecord"]();
function reducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"].GET_PRODUCT_DETAIL_SUCCESS:
            return state.merge({
                selectedProduct: payload
            });
        case _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"].GET_ALL_PRODUCTS_SUCCESS:
            var _products = payload.products;
            var _showAllProducts = payload.products;
            var productIds = _products.map(function (product) { return product.id; });
            var productEntities = _products.reduce(function (products, product) {
                return Object.assign(products, (_a = {},
                    _a[product.id] = product,
                    _a));
                var _a;
            }, {});
            return state.merge({
                productIds: productIds,
                productEntities: productEntities,
                showAllProducts: _showAllProducts
            });
        case _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"].GET_ALL_TAXONOMIES_SUCCESS:
            var _taxonomies = payload.taxonomies.taxonomies;
            return state.merge({
                taxonomies: _taxonomies,
                rootTaxonomyId: payload.taxonomies.taxonomies[0].id,
            });
        case _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"].GET_RELATED_PRODUCT_SUCCESS:
            var relatedProducts = payload.products;
            return state.merge({
                relatedProducts: relatedProducts
            });
        case _actions_product_actions__WEBPACK_IMPORTED_MODULE_0__["ProductActions"].GET_REVIEWS_SUCCESS:
            var _productReviews = payload.reviews;
            return state.merge({
                productReviews: _productReviews
            });
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/app/product/reducers/product-state.ts":
/*!***************************************************!*\
  !*** ./src/app/product/reducers/product-state.ts ***!
  \***************************************************/
/*! exports provided: ProductStateRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductStateRecord", function() { return ProductStateRecord; });
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);

var ProductStateRecord = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Record"])({
    productIds: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    productEntities: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])({}),
    selectedProductId: null,
    selectedProduct: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])({}),
    taxonomies: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    showAllProducts: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    relatedProducts: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    productReviews: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    rootTaxonomyId: 0
});


/***/ }),

/***/ "./src/app/product/reducers/selectors.ts":
/*!***********************************************!*\
  !*** ./src/app/product/reducers/selectors.ts ***!
  \***********************************************/
/*! exports provided: getProductState, fetchProducts, fetchAllTaxonomies, getSelectedProduct, getProducts, getTaxonomies, showAllProducts, relatedProducts, productReviews, rootTaxonomyId */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductState", function() { return getProductState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchProducts", function() { return fetchProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchAllTaxonomies", function() { return fetchAllTaxonomies; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSelectedProduct", function() { return getSelectedProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProducts", function() { return getProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTaxonomies", function() { return getTaxonomies; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showAllProducts", function() { return showAllProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "relatedProducts", function() { return relatedProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productReviews", function() { return productReviews; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rootTaxonomyId", function() { return rootTaxonomyId; });
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reselect */ "./node_modules/reselect/lib/index.js");
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reselect__WEBPACK_IMPORTED_MODULE_0__);

// Base product state selector function
function getProductState(state) {
    return state.products;
}
// ******************** Individual selectors ***************************
function fetchProducts(state) {
    var ids = state.productIds.toJS();
    var productEntities = state.productEntities.toJS();
    return ids.map(function (id) { return productEntities[id]; });
}
function fetchAllTaxonomies(state) {
    return state.taxonomies.toJS();
}
var fetchSelectedProduct = function (state) {
    return state.selectedProduct;
};
var fetchAllProductSearch = function (state) {
    return state.showAllProducts.toJS();
};
var fetchReletedProducts = function (state) {
    return state.relatedProducts.toJS();
};
var fetchProductReviews = function (state) {
    return state.productReviews.toJS();
};
var fetchRootTaxonId = function (state) {
    return state.rootTaxonomyId;
};
// *************************** PUBLIC API's ****************************
var getSelectedProduct = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchSelectedProduct);
var getProducts = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchProducts);
var getTaxonomies = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchAllTaxonomies);
var showAllProducts = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchAllProductSearch);
var relatedProducts = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchReletedProducts);
var productReviews = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchProductReviews);
var rootTaxonomyId = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getProductState, fetchRootTaxonId);


/***/ }),

/***/ "./src/app/shared/components/product-slider/inner-product/inner-product.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/inner-product/inner-product.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"product-tile\">\n  <a [routerLink]=\"['/', product.slug]\">\n    <img width=\"150\" height=\"200\" [src]=\"product.product_url\">\n    <p> {{ product.name }} </p>\n  </a>\n  <ngx-input-star-rating *ngIf=\"product.avg_rating === 0\" value=\"{{product.avg_rating}}\" disabled=\"true\"></ngx-input-star-rating>\n</div>\n"

/***/ }),

/***/ "./src/app/shared/components/product-slider/inner-product/inner-product.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/inner-product/inner-product.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".product-box {\n  border-radius: 5px;\n  margin-top: 1rem;\n  border: 1px solid transparent; }\n  @media screen and (min-width: 320px) and (max-width: 768px) {\n    .product-box {\n      margin-right: 1rem;\n      border: 1px solid #ccc; } }\n  .product-box:hover {\n    box-shadow: 0 1px 0 #ccc;\n    border-color: #ccc; }\n  img {\n  width: 100%;\n  -o-object-fit: contain;\n     object-fit: contain; }\n"

/***/ }),

/***/ "./src/app/shared/components/product-slider/inner-product/inner-product.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/inner-product/inner-product.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: InnerIproductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InnerIproductComponent", function() { return InnerIproductComponent; });
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InnerIproductComponent = /** @class */ (function () {
    function InnerIproductComponent() {
    }
    InnerIproductComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_0__["Product"])
    ], InnerIproductComponent.prototype, "product", void 0);
    InnerIproductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inner-product',
            template: __webpack_require__(/*! ./inner-product.component.html */ "./src/app/shared/components/product-slider/inner-product/inner-product.component.html"),
            styles: [__webpack_require__(/*! ./inner-product.component.scss */ "./src/app/shared/components/product-slider/inner-product/inner-product.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        }),
        __metadata("design:paramtypes", [])
    ], InnerIproductComponent);
    return InnerIproductComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/product-slider/product-slider.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/product-slider.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"mt-3\" *ngIf=\"productlist?.length\">\n  <div class=\"slider-section\">\n    <div class=\"slider-control\">\n      <button button color=\"primary \" (click)=\"moveLeft() \" class=\"slider-btn prv\">left</button>\n      <button button color=\"primary \" (click)=\"moveRight() \" class=\"slider-btn next\">right</button>\n    </div>\n  </div>\n  <div class=\"d-flex scrollbar-hidden\" dragScroll drag-scroll-y-disabled=\"true\" scrollbar-hidden=\"true\" #nav>\n    <app-inner-product class=\"col-6  col-sm-2  col-md-3 col-lg-2 product-box\" *ngFor=\"let product of productlist \" [product]=\"product\"></app-inner-product>\n  </div>\n  <hr>\n</section>\n"

/***/ }),

/***/ "./src/app/shared/components/product-slider/product-slider.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/product-slider.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host /deep/ .section-title {\n  color: #000;\n  text-transform: capitalize;\n  font-weight: 600; }\n\n:host /deep/ .scrollbar-hidden {\n  overflow: hidden !important; }\n\n:host /deep/ .slider-section {\n  position: relative; }\n\n:host /deep/ .slider-section .slider-control {\n    position: absolute;\n    right: 0;\n    top: -60px; }\n\n:host /deep/ .slider-section .slider-control .slider-btn {\n      margin: 0 3px;\n      border: 1.5px solid #ffe364;\n      border-radius: 50%;\n      width: 28px;\n      height: 28px;\n      display: block;\n      text-indent: -9999px;\n      float: left;\n      cursor: pointer;\n      background: #fff; }\n\n:host /deep/ .slider-section .slider-control .slider-btn.next {\n        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAjCAYAAAB7NEEmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM1REJCQTFCNDk2NTExRThCRDk2QUJCRDVCMTMzNTEwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjM1REJCQTFDNDk2NTExRThCRDk2QUJCRDVCMTMzNTEwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzVEQkJBMTk0OTY1MTFFOEJEOTZBQkJENUIxMzM1MTAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzVEQkJBMUE0OTY1MTFFOEJEOTZBQkJENUIxMzM1MTAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6aVdl9AAAArUlEQVR42sTVUQ6DMAwD0Man3Y220+IhJKSJQRsHV+SX8iSE3cTybmznEy0x8fp/HZ3zbMXB4DlnoCUYyXOcgUowsn9ZgaHEJwtDzWUGRiXwIxjVJvVg3Knopn6CSqTK8CinJTgTfhnONkqClZrGjO6nYRWlG6X789PgurMCbtC6o3bQtk1/wR5aBq9Q3s0t3OARpatZcIM7yie7b7+llGXovfntO8q+TY/zFWAAfrEuQ1kWh00AAAAASUVORK5CYII=);\n        background-position: 10px;\n        background-repeat: no-repeat;\n        background-size: 8px; }\n\n:host /deep/ .slider-section .slider-control .slider-btn.prv {\n        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAjCAYAAAB7NEEmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMxREIzMTdCNDk2NTExRTg5QkIzQzgwMjM4QTJFMTBBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMxREIzMTdDNDk2NTExRTg5QkIzQzgwMjM4QTJFMTBBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzFEQjMxNzk0OTY1MTFFODlCQjNDODAyMzhBMkUxMEEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzFEQjMxN0E0OTY1MTFFODlCQjNDODAyMzhBMkUxMEEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7MZuafAAAAqUlEQVR42qzWWQqAMAwEUCen9UZ6WiMKBRGXmWT6WeRRm6XBtkydlfcNzIlwgufmigwnOFa4wQqazEfhBpVA0SB7UglkUBn8Q0vgF5rkXYONfgt8QtvgHbWAV9QGDtQKKmUKpUGEG7SWaeX3U0XhhkO8N7lJ2+AoRlp++NpwNHMz1ZSi4GN4UPO0BNvK9ApbG8qAlWGChtWxB86GUnpOrHBn6MXbcLELMAAyLDND8usrZgAAAABJRU5ErkJggg==);\n        background-position: 7px;\n        background-repeat: no-repeat;\n        background-size: 8px; }\n\n:host /deep/ .slider-section .slider-control .slider-btn:hover {\n      background-color: #ffbb3c;\n      color: #fff; }\n\n:host /deep/ .product-tile {\n  text-align: center; }\n\n:host /deep/ .product-tile a {\n    color: #212529; }\n\n:host /deep/ .product-tile a:hover {\n      text-decoration: none; }\n\n:host /deep/ .product-box {\n  border-radius: 5px;\n  border: 1px solid transparent; }\n\n:host /deep/ .product-box:hover {\n    box-shadow: 0 1px 0 #ccc;\n    border-color: #ccc; }\n\n:host /deep/.product-tile p {\n  margin-bottom: 0px;\n  white-space: normal;\n  margin-top: 10px; }\n\n:host /deep/.dropdown-toggle::after {\n  vertical-align: 0.055em !important; }\n\n@media screen and (min-width: 320px) and (max-width: 767px) {\n  :host /deep/ .section-title {\n    font-size: 1.4em; } }\n"

/***/ }),

/***/ "./src/app/shared/components/product-slider/product-slider.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/shared/components/product-slider/product-slider.component.ts ***!
  \******************************************************************************/
/*! exports provided: ProductSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductSliderComponent", function() { return ProductSliderComponent; });
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngx-drag-scroll */ "./node_modules/ngx-drag-scroll/index.js");
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductSliderComponent = /** @class */ (function () {
    function ProductSliderComponent() {
        this.productlist = new Array(10);
    }
    ProductSliderComponent.prototype.ngOnInit = function () {
    };
    ProductSliderComponent.prototype.moveLeft = function () {
        this.ds.moveLeft();
    };
    ProductSliderComponent.prototype.moveRight = function () {
        this.ds.moveRight();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ProductSliderComponent.prototype, "productlist", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('nav', { read: ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_0__["DragScrollDirective"] }),
        __metadata("design:type", ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_0__["DragScrollDirective"])
    ], ProductSliderComponent.prototype, "ds", void 0);
    ProductSliderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-slider',
            template: __webpack_require__(/*! ./product-slider.component.html */ "./src/app/shared/components/product-slider/product-slider.component.html"),
            styles: [__webpack_require__(/*! ./product-slider.component.scss */ "./src/app/shared/components/product-slider/product-slider.component.scss")],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], ProductSliderComponent);
    return ProductSliderComponent;
}());



/***/ }),

/***/ "./src/app/shared/directives/zoomable.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/directives/zoomable.directive.ts ***!
  \*********************************************************/
/*! exports provided: ZoomableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZoomableDirective", function() { return ZoomableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ZoomableDirective = /** @class */ (function () {
    function ZoomableDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.listeners = [];
        this.options = {
            hoverView: {
                background: 'transparent none',
                position: 'absolute',
                zIndex: '999'
            },
            peepView: {
                borderColor: '#fff',
                borderWidth: '2px',
                borderStyle: 'solid',
                cursor: 'zoom-in',
                position: 'fixed'
            },
            zoomView: {
                position: 'absolute',
                zIndex: '999'
            },
            settings: {
                zoom: 3,
                gap: 20
            }
        };
        this.sourceImage = '';
    }
    // directives starts its work each time element it moused over
    ZoomableDirective.prototype.onMouseEnter = function () {
        this.imgRect = this.el.nativeElement.getBoundingClientRect();
        this.createHoverView();
        this.updateHoverViewPosition();
        this.positionHoverView();
        this.styleHoverView();
        this.assignHoverListeners();
    };
    ZoomableDirective.prototype.ngOnInit = function () {
        var _a = this.customOptions || {}, _b = _a.peepView, peepViewCustomOptions = _b === void 0 ? {} : _b, _c = _a.zoomView, zoomViewCustomOptions = _c === void 0 ? {} : _c, _d = _a.settings, customSettings = _d === void 0 ? {} : _d;
        // extend default options
        Object.assign(this.options.peepView, peepViewCustomOptions);
        Object.assign(this.options.zoomView, zoomViewCustomOptions);
        Object.assign(this.options.settings, customSettings);
    };
    /**
     * Creates element that acts as wrapper for peep box
     */
    ZoomableDirective.prototype.createHoverView = function () {
        this.hoverView = this.renderer.createElement(this.el.nativeElement.parentNode, 'div');
    };
    /**
     * Creates peep view element that moves with the mouse over the image
     */
    ZoomableDirective.prototype.createPeepView = function () {
        this.peepView = this.renderer.createElement(this.hoverView, 'div');
    };
    /**
     * Creates the zoom view element
     */
    ZoomableDirective.prototype.createZoomView = function () {
        this.zoomView = this.renderer.createElement(this.el.nativeElement.parentNode, 'div');
    };
    // TODO: Try doing this with observables
    ZoomableDirective.prototype.assignHoverListeners = function () {
        var _this = this;
        this.listeners.push(this.renderer.listen(this.hoverView, 'mouseenter', function (event) {
            // peep view related tasks
            _this.createPeepView();
            _this.stylePeepView();
            _this.positionPeepView(event);
            // zoom view related tasks
            _this.createZoomView();
            _this.styleZoomView();
            _this.positionZoomView();
        }), this.renderer.listen(this.hoverView, 'mouseleave', function () { return _this.destroyViews(); }), this.renderer.listen(this.hoverView, 'mousemove', function (event) {
            _this.positionPeepView(event);
            _this.positionZoomBackground(event);
        }));
    };
    ZoomableDirective.prototype.styleHoverView = function () {
        var _this = this;
        Object.keys(this.options.hoverView)
            .forEach(function (key) {
            _this.renderer.setElementStyle(_this.hoverView, key, _this.options.hoverView[key]);
        });
    };
    ZoomableDirective.prototype.stylePeepView = function () {
        var _this = this;
        var dimensions = this.getPeepViewDimensions(), props = Object.assign({
            width: dimensions.width + "px",
            height: dimensions.height + "px"
        }, this.options.peepView);
        Object.keys(props)
            .forEach(function (key) {
            _this.renderer.setElementStyle(_this.peepView, key, props[key]);
        });
    };
    ZoomableDirective.prototype.styleZoomView = function () {
        var _this = this;
        Object.keys(this.options.zoomView)
            .forEach(function (key) {
            _this.renderer.setElementStyle(_this.zoomView, key, _this.options.zoomView[key]);
        });
    };
    ZoomableDirective.prototype.updateHoverViewPosition = function () {
        this.hoverViewPosition = {
            top: this.imgRect.top + window.scrollY,
            left: this.imgRect.left + window.scrollX,
            width: this.imgRect.width,
            height: this.imgRect.height
        };
    };
    ZoomableDirective.prototype.positionHoverView = function () {
        var _this = this;
        Object.keys(this.hoverViewPosition)
            .forEach(function (key) {
            _this.renderer.setElementStyle(_this.hoverView, key, _this.hoverViewPosition[key] + "px");
        });
    };
    ZoomableDirective.prototype.getPeepViewDimensions = function () {
        return {
            width: Math.round(this.imgRect.width / this.options.settings.zoom),
            height: Math.round(this.imgRect.height / this.options.settings.zoom)
        };
    };
    ZoomableDirective.prototype.getPeepViewCoords = function (event) {
        var dimensions = this.getPeepViewDimensions(), mouseX = event.clientX, mouseY = event.clientY, borderWidth = parseInt(this.options.peepView.borderWidth, 10) * 2, 
        // ensure top is not above the box
        top = Math.max(this.imgRect.top, mouseY - (dimensions.height / 2) - borderWidth), 
        // ensure left is not outside the box
        left = Math.max(this.imgRect.left, mouseX - (dimensions.width / 2) - borderWidth);
        // ensure both top and left will not leak from bottom or right
        return {
            top: Math.min(top, this.imgRect.bottom - dimensions.height - borderWidth),
            left: Math.min(left, this.imgRect.right - dimensions.width - borderWidth)
        };
    };
    ZoomableDirective.prototype.positionPeepView = function (event) {
        var coords = this.getPeepViewCoords(event);
        this.renderer.setElementStyle(this.peepView, 'top', coords.top + "px");
        this.renderer.setElementStyle(this.peepView, 'left', coords.left + "px");
    };
    ZoomableDirective.prototype.positionZoomView = function () {
        var _this = this;
        var props = Object.assign({}, this.hoverViewPosition, {
            left: this.hoverViewPosition.left + this.imgRect.width + this.options.settings.gap
        });
        Object.keys(props)
            .forEach(function (key) {
            if (key === 'top') {
                props[key] = -20;
            }
            // if (key == 'width') {
            //   props[key] = 500;
            // }
            // if (key == 'height') {
            //   props[key] = 600;
            // }
            _this.renderer.setElementStyle(_this.zoomView, key, props[key] + "px");
        });
    };
    ZoomableDirective.prototype.positionZoomBackground = function (event) {
        var coords = this.getPeepViewCoords(event), zoom = this.options.settings.zoom, bgWidth = this.imgRect.width * zoom, bgHeight = this.imgRect.height * zoom, imgSrc = this.sourceImage || this.el.nativeElement.getAttribute('src'), xRatio = Math.max(0, (coords.left + window.scrollX - this.hoverViewPosition.left) / this.imgRect.width), yRatio = Math.max(0, (coords.top + window.scrollY - this.hoverViewPosition.top) / this.imgRect.height);
        this.renderer.setElementStyle(this.zoomView, 'backgroundImage', "url(\"" + imgSrc + "\")");
        this.renderer.setElementStyle(this.zoomView, 'backgroundPosition', "-" + xRatio * bgWidth + "px -" + yRatio * bgHeight + "px");
        this.renderer.setElementStyle(this.zoomView, 'backgroundSize', bgWidth + "px " + bgHeight + "px");
    };
    ZoomableDirective.prototype.destroyViews = function () {
        // remove all event listeners
        for (var _i = 0, _a = this.listeners; _i < _a.length; _i++) {
            var listener = _a[_i];
            listener();
        }
        this.listeners = [];
        this.hoverView.parentNode.removeChild(this.hoverView);
        this.zoomView.parentNode.removeChild(this.zoomView);
        this.hoverView = null;
        this.zoomView = null;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('zoomable'),
        __metadata("design:type", Object)
    ], ZoomableDirective.prototype, "customOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('zoomableSrc'),
        __metadata("design:type", Object)
    ], ZoomableDirective.prototype, "sourceImage", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ZoomableDirective.prototype, "onMouseEnter", null);
    ZoomableDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[zoomable]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"]])
    ], ZoomableDirective);
    return ZoomableDirective;
}());



/***/ }),

/***/ "./src/app/shared/index.ts":
/*!*********************************!*\
  !*** ./src/app/shared/index.ts ***!
  \*********************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngx-lite/input-star-rating */ "./node_modules/@ngx-lite/input-star-rating/fesm5/ngx-lite-input-star-rating.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-drag-scroll */ "./node_modules/ngx-drag-scroll/index.js");
/* harmony import */ var ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _pipes_keys_pipe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pipes/keys.pipe */ "./src/app/shared/pipes/keys.pipe.ts");
/* harmony import */ var _core_pipes_humanize_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../core/pipes/humanize.pipe */ "./src/app/core/pipes/humanize.pipe.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_product_slider_product_slider_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/product-slider/product-slider.component */ "./src/app/shared/components/product-slider/product-slider.component.ts");
/* harmony import */ var _components_product_slider_inner_product_inner_product_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/product-slider/inner-product/inner-product.component */ "./src/app/shared/components/product-slider/inner-product/inner-product.component.ts");
/* harmony import */ var _directives_zoomable_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./directives/zoomable.directive */ "./src/app/shared/directives/zoomable.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// Pipes


// components
// imports




// Directives

var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                // components
                // pipes
                _pipes_keys_pipe__WEBPACK_IMPORTED_MODULE_5__["KeysPipe"],
                _core_pipes_humanize_pipe__WEBPACK_IMPORTED_MODULE_6__["HumanizePipe"],
                _directives_zoomable_directive__WEBPACK_IMPORTED_MODULE_11__["ZoomableDirective"],
                _components_product_slider_product_slider_component__WEBPACK_IMPORTED_MODULE_9__["ProductSliderComponent"],
                _components_product_slider_inner_product_inner_product_component__WEBPACK_IMPORTED_MODULE_10__["InnerIproductComponent"],
            ],
            exports: [
                // components
                // modules
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_7__["BsDropdownModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                // pipes
                _pipes_keys_pipe__WEBPACK_IMPORTED_MODULE_5__["KeysPipe"],
                _core_pipes_humanize_pipe__WEBPACK_IMPORTED_MODULE_6__["HumanizePipe"],
                ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4__["DragScrollModule"],
                _directives_zoomable_directive__WEBPACK_IMPORTED_MODULE_11__["ZoomableDirective"],
                _components_product_slider_product_slider_component__WEBPACK_IMPORTED_MODULE_9__["ProductSliderComponent"],
                ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4__["DragScrollModule"]
            ],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_7__["BsDropdownModule"].forRoot(),
                ngx_drag_scroll__WEBPACK_IMPORTED_MODULE_4__["DragScrollModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"],
                _ngx_lite_input_star_rating__WEBPACK_IMPORTED_MODULE_0__["NgxInputStarRatingModule"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/pipes/keys.pipe.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/pipes/keys.pipe.ts ***!
  \*******************************************/
/*! exports provided: KeysPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeysPipe", function() { return KeysPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                // keys.push(key);
                keys.push({ key: key, value: value[key] });
            }
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'keys'
        })
    ], KeysPipe);
    return KeysPipe;
}());



/***/ }),

/***/ "./src/app/user/actions/user.actions.ts":
/*!**********************************************!*\
  !*** ./src/app/user/actions/user.actions.ts ***!
  \**********************************************/
/*! exports provided: UserActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserActions", function() { return UserActions; });
var UserActions = /** @class */ (function () {
    function UserActions() {
    }
    UserActions.prototype.getUserOrders = function (email, page) {
        return {
            type: UserActions.GET_USER_ORDERS,
            payload: { email: email, page: page }
        };
    };
    UserActions.prototype.getUserOrdersSuccess = function (orders) {
        return { type: UserActions.GET_USER_ORDERS_SUCCESS, payload: orders };
    };
    UserActions.prototype.getUserFavoriteProducts = function () {
        return { type: UserActions.GET_USER_FAVORITE_PRODUCTS };
    };
    UserActions.prototype.getUserFavoriteProductsSuccess = function (products) {
        return { type: UserActions.GET_USER_FAVORITE_PRODUCTS_SUCCESS, payload: products };
    };
    UserActions.prototype.removeFromFavoriteProducts = function (id) {
        return { type: UserActions.REMOVE_FROM_FAVORITE_PRODUCTS, payload: id };
    };
    UserActions.GET_USER_ORDERS = 'GET_USER_ORDERS';
    UserActions.GET_USER_ORDERS_SUCCESS = 'GET_USER_ORDERS_SUCCESS';
    UserActions.GET_USER_FAVORITE_PRODUCTS = 'GET_USER_FAVORITE_PRODUCTS';
    UserActions.GET_USER_FAVORITE_PRODUCTS_SUCCESS = 'GET_USER_FAVORITE_PRODUCTS_SUCCESS';
    UserActions.REMOVE_FROM_FAVORITE_PRODUCTS = 'REMOVE_FROM_FAVORITE_PRODUCTS';
    return UserActions;
}());



/***/ }),

/***/ "./src/app/user/effects/user.effects.ts":
/*!**********************************************!*\
  !*** ./src/app/user/effects/user.effects.ts ***!
  \**********************************************/
/*! exports provided: UserEffects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEffects", function() { return UserEffects; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../core/services/product.service */ "./src/app/core/services/product.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/user.service */ "./src/app/user/services/user.service.ts");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../actions/user.actions */ "./src/app/user/actions/user.actions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserEffects = /** @class */ (function () {
    function UserEffects(actions$, userService, userActions, productService) {
        var _this = this;
        this.actions$ = actions$;
        this.userService = userService;
        this.userActions = userActions;
        this.productService = productService;
        // tslint:disable-next-line:member-ordering
        this.GetUserOrders$ = this.actions$
            .ofType(_actions_user_actions__WEBPACK_IMPORTED_MODULE_6__["UserActions"].GET_USER_ORDERS).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function (action) { return _this.userService.getOrders(action.payload.email, action.payload.page); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (orders) { return _this.userActions.getUserOrdersSuccess(orders); }));
        // tslint:disable-next-line:member-ordering
        this.GetUserFavoriteProducts$ = this.actions$
            .ofType(_actions_user_actions__WEBPACK_IMPORTED_MODULE_6__["UserActions"].GET_USER_FAVORITE_PRODUCTS).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["switchMap"])(function () { return _this.productService.getUserFavoriteProducts(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(function (products) { return _this.userActions.getUserFavoriteProductsSuccess(products); }));
    }
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], UserEffects.prototype, "GetUserOrders$", void 0);
    __decorate([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], UserEffects.prototype, "GetUserFavoriteProducts$", void 0);
    UserEffects = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        __metadata("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Actions"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _actions_user_actions__WEBPACK_IMPORTED_MODULE_6__["UserActions"],
            _core_services_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"]])
    ], UserEffects);
    return UserEffects;
}());



/***/ }),

/***/ "./src/app/user/reducers/user.reducer.ts":
/*!***********************************************!*\
  !*** ./src/app/user/reducers/user.reducer.ts ***!
  \***********************************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var _user_state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.state */ "./src/app/user/reducers/user.state.ts");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actions/user.actions */ "./src/app/user/actions/user.actions.ts");


var initialState = new _user_state__WEBPACK_IMPORTED_MODULE_0__["UserStateRecord"]();
function reducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__["UserActions"].GET_USER_ORDERS_SUCCESS:
            return state.merge({ orders: payload });
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__["UserActions"].GET_USER_FAVORITE_PRODUCTS_SUCCESS:
            return state.merge({ favorite_products: payload });
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__["UserActions"].REMOVE_FROM_FAVORITE_PRODUCTS:
            var deletedId_1 = payload;
            var products = state.toJS().favorite_products;
            products = products.filter(function (product) { return product.id !== deletedId_1; });
            return state.merge({ favorite_products: products });
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/app/user/reducers/user.state.ts":
/*!*********************************************!*\
  !*** ./src/app/user/reducers/user.state.ts ***!
  \*********************************************/
/*! exports provided: UserStateRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserStateRecord", function() { return UserStateRecord; });
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);

var UserStateRecord = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Record"])({
    user: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])({}),
    orders: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([]),
    favorite_products: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["List"])([])
});


/***/ }),

/***/ "./src/app/user/services/user.service.ts":
/*!***********************************************!*\
  !*** ./src/app/user/services/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../actions/user.actions */ "./src/app/user/actions/user.actions.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserService = /** @class */ (function () {
    function UserService(http, actions, store) {
        this.http = http;
        this.actions = actions;
        this.store = store;
    }
    /**
     *
     *
     * @returns {Observable<Order[]>}
     *
     * @memberof UserService
     */
    UserService.prototype.getOrders = function (email, page) {
        return new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](); /*this.http.get<Array<Order>>(`/api/v1/orders.json?q[email_cont]=${email}&per_page=10&q[s]=id%20desc&page=${page}`)
          .pipe(
            map(data => data)
          )*/
    };
    /**
     *
     *
     * @param {string} orderNumber
     * @returns {Observable<Order>}
     *
     * @memberof UserService
     */
    UserService.prototype.getOrderDetail = function (orderNumber) {
        return this.http.get("/" + orderNumber);
    };
    /**
     *
     *
     * @returns {Observable<User>}
     *
     * @memberof UserService
     */
    UserService.prototype.getUser = function () {
        var user_id = JSON.parse(localStorage.getItem('user')).id;
        return this.http.get("api/v1/users/" + user_id);
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"],
            _actions_user_actions__WEBPACK_IMPORTED_MODULE_2__["UserActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/config/default/app-data.ts":
/*!****************************************!*\
  !*** ./src/config/default/app-data.ts ***!
  \****************************************/
/*! exports provided: DEFAULT_APP_DATA */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_APP_DATA", function() { return DEFAULT_APP_DATA; });
var cdn_url = 'https://res.cloudinary.com/aviabird/image/upload/q_60/v1529433193/angularspree/';
var DEFAULT_APP_DATA = {
    landing_page_banner: [
        {
            image_link: cdn_url + "banner-1.jpg",
            link_url: '#'
        },
        {
            image_link: cdn_url + "banner-2.jpg",
            link_url: '#'
        },
        {
            image_link: cdn_url + "banner-3.jpg",
            link_url: '#'
        },
        {
            image_link: cdn_url + "banner-4.jpg",
            link_url: '#'
        }
    ],
    promo_banner: {
        image_link: cdn_url + "secondary-banner-1.jpg",
        link_url: '#'
    },
    category_banner: {
        Living: {
            image_link: cdn_url + "banner-1.jpg",
            link_url: '#'
        },
        Bedroom: {
            image_link: cdn_url + "banner-2.jpg",
            link_url: '#'
        },
        Dining: {
            image_link: cdn_url + "banner-3.jpg",
            link_url: '#'
        },
        Study: {
            image_link: cdn_url + "banner-4.jpg",
            link_url: '#'
        }
    },
    Deals: {
        type: 'Today\'s Deals'
    },
    footer_page_links: [
        {
            name: 'About Us',
            link_url: '#'
        },
        {
            name: 'Blog',
            link_url: '#'
        },
        {
            name: 'Return Policy',
            link_url: '#'
        },
        {
            name: 'FAQs',
            link_url: '#'
        },
        {
            name: 'Testimonials',
            link_url: '#'
        }
    ],
    footer_social_links: [
        {
            link_url: 'https://twitter.com/angularspree',
            name: 'Twitter',
            icon: 'fa fa-twitter-square'
        },
        {
            link_url: 'https://www.instagram.com/angularspree/',
            name: 'Instagram',
            icon: 'fa fa-instagram'
        },
        {
            link_url: 'https://plus.google.com/b/110371544800340671090/110371544800340671090',
            name: 'Google +',
            icon: 'fa fa-google-plus-square'
        },
        {
            link_url: 'https://in.pinterest.com/angularspree/',
            name: 'Pinterest',
            icon: 'fa fa-pinterest-square'
        },
        {
            link_url: 'https://www.facebook.com/angularspree/',
            name: 'Facebook',
            icon: 'fa fa-facebook-square'
        },
        {
            link_url: 'https://www.youtube.com/channel/UCFBvY3QxKAKBAI_chAzRpjQ',
            name: 'Youtube',
            icon: 'fa fa-youtube-square'
        }
    ],
    contact_info: {
        contact_no: '917-6031-568',
        copyright: 'Copyright © 2018'
    }
};


/***/ }),

/***/ "./src/config/default/default.ts":
/*!***************************************!*\
  !*** ./src/config/default/default.ts ***!
  \***************************************/
/*! exports provided: DEFAULT_CONFIG */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_CONFIG", function() { return DEFAULT_CONFIG; });
/* harmony import */ var _app_data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app-data */ "./src/config/default/app-data.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var DEFAULT_CONFIG = __assign({ 
    // Add Your custom configs hereh
    prodApiEndpoint: 'https://ngspree-api.herokuapp.com/', 
    // prodApiEndpoint: 'http://localhost:3000/',
    appName: 'Angularspree', fevicon: 'http://via.placeholder.com/350x150', header: {
        brand: {
            logo: '/assets/default/logo.svg',
            name: 'Angularspree',
            height: '42',
            width: '140'
        },
        searchPlaceholder: 'Find the latest fashion items for me',
        showGithubRibon: false
    }, 
    // Following are the test crediantials for payubiz payment gateway.
    payuBizSalt: 'eCwWELxi', payuBizKey: 'gtKFFx', payuBizUrl: 'https://test.payu.in/_payment', free_shipping_order_amount: 10, currency_symbol: '$' }, _app_data__WEBPACK_IMPORTED_MODULE_0__["DEFAULT_APP_DATA"]);


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var _config_default_default__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/default/default */ "./src/config/default/default.ts");

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    apiEndpoint: 'http://localhost:8080/',
    appName: _config_default_default__WEBPACK_IMPORTED_MODULE_0__["DEFAULT_CONFIG"].appName,
    config: _config_default_default__WEBPACK_IMPORTED_MODULE_0__["DEFAULT_CONFIG"]
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/ivan/Documents/angularspree/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map