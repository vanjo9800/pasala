(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-index"],{

/***/ "./src/app/core/models/order.ts":
/*!**************************************!*\
  !*** ./src/app/core/models/order.ts ***!
  \**************************************/
/*! exports provided: Order, LightOrder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LightOrder", function() { return LightOrder; });
/*
 * Order model
 * Detailed info http://guides.spreecommerce.org/developer/orders.html
 * Public API's http://guides.spreecommerce.org/api/orders.html
 */
var Order = /** @class */ (function () {
    function Order() {
    }
    return Order;
}());

// NOTE: This just mimics the serializer exposed in the API
// Not sure if it is required, review it in APRIL
var LightOrder = /** @class */ (function () {
    function LightOrder() {
    }
    return LightOrder;
}());



/***/ }),

/***/ "./src/app/user/components/addresses/addresses.component.html":
/*!********************************************************************!*\
  !*** ./src/app/user/components/addresses/addresses.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  addresses works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/components/addresses/addresses.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/user/components/addresses/addresses.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/components/addresses/addresses.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/user/components/addresses/addresses.component.ts ***!
  \******************************************************************/
/*! exports provided: AddressesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressesComponent", function() { return AddressesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddressesComponent = /** @class */ (function () {
    function AddressesComponent() {
    }
    AddressesComponent.prototype.ngOnInit = function () {
    };
    AddressesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-addresses',
            template: __webpack_require__(/*! ./addresses.component.html */ "./src/app/user/components/addresses/addresses.component.html"),
            styles: [__webpack_require__(/*! ./addresses.component.scss */ "./src/app/user/components/addresses/addresses.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AddressesComponent);
    return AddressesComponent;
}());



/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.html":
/*!************************************************************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table\">\n  <thead class=\"thead-light\">\n    <tr>\n      <th scope=\"col\">Product Image</th>\n      <th scope=\"col\">Product Name</th>\n      <th scope=\"col\">Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let productinfo of product\">\n      <td>\n        <img [src]=\"productinfo.product_url\" class=\"img-fluid fimg\">\n      </td>\n      <td>{{ productinfo.name }}</td>\n      <td class=\"text-right\">\n        <button (click)=\"removeFromFavorite(productinfo.id)\" class=\"btn\">\n          <i class=\"fa fa-trash\"></i>\n        </button>\n      </td>\n    </tr>\n  </tbody>\n</table>"

/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.scss":
/*!************************************************************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.scss ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fimg {\n  -o-object-fit: contain;\n     object-fit: contain;\n  width: 5.6em; }\n"

/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: FavoriteProductListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoriteProductListItemComponent", function() { return FavoriteProductListItemComponent; });
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../actions/user.actions */ "./src/app/user/actions/user.actions.ts");
/* harmony import */ var _core_services_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../core/services/product.service */ "./src/app/core/services/product.service.ts");
/* harmony import */ var _core_models_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../core/models/product */ "./src/app/core/models/product.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FavoriteProductListItemComponent = /** @class */ (function () {
    function FavoriteProductListItemComponent(productService, store, userActions) {
        this.productService = productService;
        this.store = store;
        this.userActions = userActions;
    }
    FavoriteProductListItemComponent.prototype.ngOnInit = function () {
    };
    FavoriteProductListItemComponent.prototype.removeFromFavorite = function (id) {
        var _this = this;
        this.productService.removeFromFavorite(id).subscribe(function (status) {
            _this.store.dispatch(_this.userActions.removeFromFavoriteProducts(id));
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        __metadata("design:type", _core_models_product__WEBPACK_IMPORTED_MODULE_2__["Product"])
    ], FavoriteProductListItemComponent.prototype, "product", void 0);
    FavoriteProductListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-favorite-product-list-item',
            template: __webpack_require__(/*! ./favorite-product-list-item.component.html */ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.html"),
            styles: [__webpack_require__(/*! ./favorite-product-list-item.component.scss */ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_services_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["UserActions"]])
    ], FavoriteProductListItemComponent);
    return FavoriteProductListItemComponent;
}());



/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-products.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-products.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"products$ | async; let products\">\n\n  <div class=\"table-responsive\" *ngIf=\"products.length; else elseBlock\">\n    <app-favorite-product-list-item [product]=\"products\"></app-favorite-product-list-item>\n  </div>\n  <ng-template #elseBlock>\n    <div>You have'nt favorited anything yet.</div>\n  </ng-template>\n</div>"

/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-products.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-products.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/components/favorite-products/favorite-products.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/user/components/favorite-products/favorite-products.component.ts ***!
  \**********************************************************************************/
/*! exports provided: FavoriteProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoriteProductsComponent", function() { return FavoriteProductsComponent; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../actions/user.actions */ "./src/app/user/actions/user.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _reducers_selector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../reducers/selector */ "./src/app/user/reducers/selector.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FavoriteProductsComponent = /** @class */ (function () {
    function FavoriteProductsComponent(store, userActions) {
        this.store = store;
        this.userActions = userActions;
        this.products$ = this.store.select(_reducers_selector__WEBPACK_IMPORTED_MODULE_3__["getUserFavoriteProducts"]);
    }
    FavoriteProductsComponent.prototype.ngOnInit = function () {
        this.store.dispatch(this.userActions.getUserFavoriteProducts());
    };
    FavoriteProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-favorite-products',
            template: __webpack_require__(/*! ./favorite-products.component.html */ "./src/app/user/components/favorite-products/favorite-products.component.html"),
            styles: [__webpack_require__(/*! ./favorite-products.component.scss */ "./src/app/user/components/favorite-products/favorite-products.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["Store"],
            _actions_user_actions__WEBPACK_IMPORTED_MODULE_1__["UserActions"]])
    ], FavoriteProductsComponent);
    return FavoriteProductsComponent;
}());



/***/ }),

/***/ "./src/app/user/components/orders/order-detail/order-detail.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/user/components/orders/order-detail/order-detail.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"order\">\n  <div class=\"row box  m-1\">\n    <div class=\"col-12 col-sm-6\">\n      <h3>Order Details</h3>\n      <table>\n        <tr>\n          <td>Order ID:</td>\n          <td> {{order.number}}</td>\n        </tr>\n        <tr>\n          <td>Order Date:</td>\n          <td>{{order.updated_at | date}}</td>\n        </tr>\n        <tr>\n          <td>Item price:</td>\n          <td> {{order.display_item_total}}</td>\n        </tr>\n        <tr>\n          <tr>\n            <td>Shipping:</td>\n            <td> {{order.display_ship_total}}</td>\n          </tr>\n          <tr>\n            <td>Total Amount:</td>\n            <td> {{order.display_total}} </td>\n          </tr>\n          <tr>\n            <td>Payment Status:</td>\n            <td>\n              <b class=\"text-uppercase\">{{order.payment_state | humanize | uppercase}}</b>\n            </td>\n          </tr>\n      </table>\n    </div>\n    <div class=\"col-12 col-sm-6 \">\n      <h3>Address</h3>\n      <table>\n        <tr>\n          <td>Name: </td>\n          <td>{{order.ship_address.full_name}}</td>\n        </tr>\n        <tr>\n          <td>Address:</td>\n          <td>{{order.ship_address.address1}}, {{order.ship_address.address2}}, {{order.ship_address.city}}</td>\n        </tr>\n        <tr>\n          <td>Pincode:</td>\n          <td>{{order.ship_address.zipcode}}</td>\n        </tr>\n        <tr>\n          <td>Phone:</td>\n          <td>{{order.ship_address.phone}}</td>\n        </tr>\n        <tr>\n          <td>Email:</td>\n          <td>{{order.email}}</td>\n        </tr>\n      </table>\n\n    </div>\n  </div>\n\n  <div class='row box m-1 my-4' *ngFor=\"let line_item of order.line_items\">\n    <br>\n    <div class=\"col-md-2\">\n      <img class=\"line_item_image\" [src]=\"getProductImageUrl(line_item)\" alt=\"Line Item\">\n    </div>\n\n    <div class=\"col-md-3\">\n      <h5 class=\"ptitle\">\n        <a [routerLink]=\"['/', line_item.variant.slug]\">\n          {{line_item.variant.name}}\n        </a>\n      </h5>\n    </div>\n\n    <div class=\"col-md-3\">\n      <h5 class=\"strong\">\n\n        {{line_item.display_amount}}\n      </h5>\n\n\n    </div>\n    <div class=\"col-md-4\">\n      <p>\n        <strong class=\"text-warning\">Please note:</strong> You can return or exchange this within 30 days.\n      </p>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/user/components/orders/order-detail/order-detail.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/user/components/orders/order-detail/order-detail.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".status {\n  color: #ffe364; }\n\n.line_item_image {\n  border: 1px solid #ccc; }\n\n.box {\n  background-color: #f8f9fa;\n  padding: 1rem;\n  box-shadow: 0px 0px 4px #ccc;\n  border-radius: 2px;\n  margin-bottom: 10px; }\n\n.box a {\n    color: #212529;\n    text-decoration: none;\n    font-size: 1em; }\n\ntable tr td:first-child {\n  font-weight: bold;\n  padding-right: 1rem; }\n"

/***/ }),

/***/ "./src/app/user/components/orders/order-detail/order-detail.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/user/components/orders/order-detail/order-detail.component.ts ***!
  \*******************************************************************************/
/*! exports provided: OrderDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailComponent", function() { return OrderDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/user/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderDetailComponent = /** @class */ (function () {
    function OrderDetailComponent(userService, route) {
        this.userService = userService;
        this.route = route;
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSubscription$ = this.route.params.subscribe(function (params) {
            _this.orderNumber = params['number'];
            _this.orderSubscription$ =
                _this.userService
                    .getOrderDetail(_this.orderNumber)
                    .subscribe(function (order) { return _this.order = order; });
        });
    };
    OrderDetailComponent.prototype.getProductImageUrl = function (line_item) {
        return line_item.variant.images[0].small_url;
    };
    OrderDetailComponent.prototype.ngOnDestroy = function () {
        this.routeSubscription$.unsubscribe();
        this.orderSubscription$.unsubscribe();
    };
    OrderDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-detail',
            template: __webpack_require__(/*! ./order-detail.component.html */ "./src/app/user/components/orders/order-detail/order-detail.component.html"),
            styles: [__webpack_require__(/*! ./order-detail.component.scss */ "./src/app/user/components/orders/order-detail/order-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], OrderDetailComponent);
    return OrderDetailComponent;
}());



/***/ }),

/***/ "./src/app/user/components/orders/order-list-item/order-list-item.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/user/components/orders/order-list-item/order-list-item.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=container *ngIf=\"order.completed_at!=null\">\n  <table class=\"table\">\n    <tr>\n      <th>\n        Order No\n      </th>\n      <th>Status:</th>\n      <th>Ordered On:</th>\n      <th>Order Total:</th>\n      <th>Payment Status</th>\n    </tr>\n    <tr *ngFor=\"let order of order.orders\">\n      <td>\n        <a [routerLink]=\"['detail', order.number]\" class=\"view-details-link\"> {{order.number}} </a>\n      </td>\n      <td>{{order.shipment_state}}</td>\n      <td>{{order.completed_at | date:'fullDate'}}</td>\n      <td>{{order.display_total}}</td>\n      <td>{{order.payment_state}}</td>\n    </tr>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/user/components/orders/order-list-item/order-list-item.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/user/components/orders/order-list-item/order-list-item.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".line_item_image {\n  float: left; }\n\n.view-details-link {\n  color: #eb7f00;\n  font-weight: bold;\n  font-size: 1em;\n  text-decoration: underline; }\n"

/***/ }),

/***/ "./src/app/user/components/orders/order-list-item/order-list-item.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/user/components/orders/order-list-item/order-list-item.component.ts ***!
  \*************************************************************************************/
/*! exports provided: OrderListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListItemComponent", function() { return OrderListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/models/order */ "./src/app/core/models/order.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderListItemComponent = /** @class */ (function () {
    function OrderListItemComponent() {
    }
    OrderListItemComponent.prototype.ngOnInit = function () {
    };
    OrderListItemComponent.prototype.getProductImageUrl = function (url) {
        return url;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _core_models_order__WEBPACK_IMPORTED_MODULE_1__["Order"])
    ], OrderListItemComponent.prototype, "order", void 0);
    OrderListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-list-item',
            template: __webpack_require__(/*! ./order-list-item.component.html */ "./src/app/user/components/orders/order-list-item/order-list-item.component.html"),
            styles: [__webpack_require__(/*! ./order-list-item.component.scss */ "./src/app/user/components/orders/order-list-item/order-list-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderListItemComponent);
    return OrderListItemComponent;
}());



/***/ }),

/***/ "./src/app/user/components/orders/orders.component.html":
/*!**************************************************************!*\
  !*** ./src/app/user/components/orders/orders.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"orders$ | async;let orders\">\n  <div *ngIf=\"orders.total_count > 0; else elseBlock\">\n    <div *ngFor=\"let order of orders.orders\"></div>\n    <app-order-list-item [order]=\"orders\"></app-order-list-item>\n  </div>\n\n\n  <div class=\"col-12\">\n    <div class=\"row\">\n      <pagination class=\"m-auto\" *ngIf=\"orders.total_count > orders.per_page\" [totalItems]=\"orders.total_count\" [itemsPerPage]=\"orders.per_page\"\n        [(ngModel)]=\"orders.current_page\" (pageChanged)=\"pageChanged($event)\">\n      </pagination>\n    </div>\n  </div>\n\n\n  <ng-template #elseBlock>\n    <div class=\"cart-empty\">\n      <div class=\"empty-cart-icon\"></div>\n      <div class=\"empty-cart-message\">You have'nt ordered anything yet.</div>\n      <button type=\"button\" class=\"btn btn-primary\" [routerLink]=\"['/']\">Start Shopping</button>\n    </div>\n  </ng-template>\n</div>"

/***/ }),

/***/ "./src/app/user/components/orders/orders.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/user/components/orders/orders.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cart-empty {\n  min-height: 500px;\n  text-align: center; }\n  .cart-empty .empty-cart-icon {\n    height: 85px;\n    width: 85px;\n    margin: 0 auto;\n    background-image: url(/assets/shoppingbag.png); }\n  .cart-empty .empty-cart-message {\n    font-size: 20px;\n    color: #495057;\n    font-weight: 600;\n    font-family: \"Whitney\";\n    line-height: 120px;\n    margin: 15px 0; }\n  .cart-empty .empty-wishlist-link {\n    color: #ffe364;\n    font-size: 16px;\n    font-weight: 500; }\n"

/***/ }),

/***/ "./src/app/user/components/orders/orders.component.ts":
/*!************************************************************!*\
  !*** ./src/app/user/components/orders/orders.component.ts ***!
  \************************************************************/
/*! exports provided: OrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersComponent", function() { return OrdersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../actions/user.actions */ "./src/app/user/actions/user.actions.ts");
/* harmony import */ var _reducers_selector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../reducers/selector */ "./src/app/user/reducers/selector.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrdersComponent = /** @class */ (function () {
    function OrdersComponent(store, userActions) {
        this.store = store;
        this.userActions = userActions;
        this.orders$ = this.store.select(_reducers_selector__WEBPACK_IMPORTED_MODULE_3__["getUserOrders"]);
    }
    OrdersComponent.prototype.ngOnInit = function () {
        if (JSON.parse(localStorage.getItem('user'))) {
            this.email = JSON.parse(localStorage.getItem('user')).email;
            this.store.dispatch(this.userActions.getUserOrders(this.email, 1));
        }
    };
    OrdersComponent.prototype.pageChanged = function (event) {
        this.page = event.page;
        this.store.dispatch(this.userActions.getUserOrders(this.email, this.page));
    };
    OrdersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orders',
            template: __webpack_require__(/*! ./orders.component.html */ "./src/app/user/components/orders/orders.component.html"),
            styles: [__webpack_require__(/*! ./orders.component.scss */ "./src/app/user/components/orders/orders.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"],
            _actions_user_actions__WEBPACK_IMPORTED_MODULE_2__["UserActions"]])
    ], OrdersComponent);
    return OrdersComponent;
}());



/***/ }),

/***/ "./src/app/user/components/overview/overview.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/user/components/overview/overview.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/components/overview/overview.component.html":
/*!******************************************************************!*\
  !*** ./src/app/user/components/overview/overview.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  overview works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/components/overview/overview.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/user/components/overview/overview.component.ts ***!
  \****************************************************************/
/*! exports provided: OverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverviewComponent", function() { return OverviewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OverviewComponent = /** @class */ (function () {
    function OverviewComponent() {
    }
    OverviewComponent.prototype.ngOnInit = function () {
    };
    OverviewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-overview',
            template: __webpack_require__(/*! ./overview.component.html */ "./src/app/user/components/overview/overview.component.html"),
            styles: [__webpack_require__(/*! ./overview.component.css */ "./src/app/user/components/overview/overview.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], OverviewComponent);
    return OverviewComponent;
}());



/***/ }),

/***/ "./src/app/user/components/returns/return-list-item/return-list-item.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/user/components/returns/return-list-item/return-list-item.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/components/returns/return-list-item/return-list-item.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/user/components/returns/return-list-item/return-list-item.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  return-list-item works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/components/returns/return-list-item/return-list-item.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/user/components/returns/return-list-item/return-list-item.component.ts ***!
  \****************************************************************************************/
/*! exports provided: ReturnListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReturnListItemComponent", function() { return ReturnListItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReturnListItemComponent = /** @class */ (function () {
    function ReturnListItemComponent() {
    }
    ReturnListItemComponent.prototype.ngOnInit = function () {
    };
    ReturnListItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-return-list-item',
            template: __webpack_require__(/*! ./return-list-item.component.html */ "./src/app/user/components/returns/return-list-item/return-list-item.component.html"),
            styles: [__webpack_require__(/*! ./return-list-item.component.css */ "./src/app/user/components/returns/return-list-item/return-list-item.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ReturnListItemComponent);
    return ReturnListItemComponent;
}());



/***/ }),

/***/ "./src/app/user/components/returns/returns.component.css":
/*!***************************************************************!*\
  !*** ./src/app/user/components/returns/returns.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/components/returns/returns.component.html":
/*!****************************************************************!*\
  !*** ./src/app/user/components/returns/returns.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  returns works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/components/returns/returns.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user/components/returns/returns.component.ts ***!
  \**************************************************************/
/*! exports provided: ReturnsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReturnsComponent", function() { return ReturnsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReturnsComponent = /** @class */ (function () {
    function ReturnsComponent() {
    }
    ReturnsComponent.prototype.ngOnInit = function () {
    };
    ReturnsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-returns',
            template: __webpack_require__(/*! ./returns.component.html */ "./src/app/user/components/returns/returns.component.html"),
            styles: [__webpack_require__(/*! ./returns.component.css */ "./src/app/user/components/returns/returns.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ReturnsComponent);
    return ReturnsComponent;
}());



/***/ }),

/***/ "./src/app/user/index.ts":
/*!*******************************!*\
  !*** ./src/app/user/index.ts ***!
  \*******************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var _components_favorite_products_favorite_product_list_item_favorite_product_list_item_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/favorite-products/favorite-product-list-item/favorite-product-list-item.component */ "./src/app/user/components/favorite-products/favorite-product-list-item/favorite-product-list-item.component.ts");
/* harmony import */ var _components_favorite_products_favorite_products_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/favorite-products/favorite-products.component */ "./src/app/user/components/favorite-products/favorite-products.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/index.js");
/* harmony import */ var _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/overview/overview.component */ "./src/app/user/components/overview/overview.component.ts");
/* harmony import */ var _components_orders_orders_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/orders/orders.component */ "./src/app/user/components/orders/orders.component.ts");
/* harmony import */ var _components_orders_order_list_item_order_list_item_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/orders/order-list-item/order-list-item.component */ "./src/app/user/components/orders/order-list-item/order-list-item.component.ts");
/* harmony import */ var _components_returns_returns_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/returns/returns.component */ "./src/app/user/components/returns/returns.component.ts");
/* harmony import */ var _components_returns_return_list_item_return_list_item_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/returns/return-list-item/return-list-item.component */ "./src/app/user/components/returns/return-list-item/return-list-item.component.ts");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _user_routes__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./user.routes */ "./src/app/user/user.routes.ts");
/* harmony import */ var _components_addresses_addresses_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/addresses/addresses.component */ "./src/app/user/components/addresses/addresses.component.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _components_orders_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/orders/order-detail/order-detail.component */ "./src/app/user/components/orders/order-detail/order-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// components






// services
// import { UserService } from './services/user.service';




var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                // components
                _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_5__["OverviewComponent"],
                _components_orders_order_list_item_order_list_item_component__WEBPACK_IMPORTED_MODULE_7__["OrderListItemComponent"],
                _components_orders_orders_component__WEBPACK_IMPORTED_MODULE_6__["OrdersComponent"],
                _components_returns_returns_component__WEBPACK_IMPORTED_MODULE_8__["ReturnsComponent"],
                _components_returns_return_list_item_return_list_item_component__WEBPACK_IMPORTED_MODULE_9__["ReturnListItemComponent"],
                _user_component__WEBPACK_IMPORTED_MODULE_10__["UserComponent"],
                _components_addresses_addresses_component__WEBPACK_IMPORTED_MODULE_12__["AddressesComponent"],
                _components_orders_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_14__["OrderDetailComponent"],
                _components_favorite_products_favorite_products_component__WEBPACK_IMPORTED_MODULE_1__["FavoriteProductsComponent"],
                _components_favorite_products_favorite_product_list_item_favorite_product_list_item_component__WEBPACK_IMPORTED_MODULE_0__["FavoriteProductListItemComponent"]
                // pipes
            ],
            exports: [],
            providers: [],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_user_routes__WEBPACK_IMPORTED_MODULE_11__["UserRoutes"]),
                _shared_index__WEBPACK_IMPORTED_MODULE_13__["SharedModule"],
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_4__["PaginationModule"],
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "./src/app/user/reducers/selector.ts":
/*!*******************************************!*\
  !*** ./src/app/user/reducers/selector.ts ***!
  \*******************************************/
/*! exports provided: getUserOrders, getUserFavoriteProducts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserOrders", function() { return getUserOrders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserFavoriteProducts", function() { return getUserFavoriteProducts; });
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reselect */ "./node_modules/reselect/lib/index.js");
/* harmony import */ var reselect__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reselect__WEBPACK_IMPORTED_MODULE_0__);

// Base product state function
/**
 *
 *
 * @param {AppState} state
 * @returns {UserState}
 */
function getUserState(state) {
    return state.users;
}
// ******************** Individual selectors ***************************
/**
 *
 *
 * @param {UserState} state
 * @returns {Order[]}
 */
var fetchUserOrders = function (state) {
    return state.orders.toJS();
};
var fetchUserFavoriteProducts = function (state) {
    return state.favorite_products.toJS();
};
// *************************** PUBLIC API's ****************************
var getUserOrders = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getUserState, fetchUserOrders);
var getUserFavoriteProducts = Object(reselect__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getUserState, fetchUserFavoriteProducts);


/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-12 my-2\">\n      <h4> My Account </h4>\n      <hr>\n    </div>\n\n    <div class=\"sidebar col-md-3 d-none d-lg-block\">\n      <!-- <p><a routerLink=\"/user/overview\" routerLinkActive=\"active\">Overview</a></p> -->\n      <!-- <hr> -->\n      <p>\n        <a routerLink=\"/user/orders\" routerLinkActive=\"active\">My Orders</a>\n      </p>\n      <hr>\n      <!-- <p><a routerLink=\"/user/addresses\" routerLinkActive=\"active\">My Address</a></p> -->\n      <!-- <hr> -->\n      <p>\n        <a routerLink=\"/user/favorite-products\" routerLinkActive=\"active\">My Favorite Products</a>\n      </p>\n    </div>\n\n    <div class=\"col-md-9\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/user/user.component.scss":
/*!******************************************!*\
  !*** ./src/app/user/user.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p a {\n  color: #6c757d;\n  text-decoration: none; }\n  p a.active {\n    font-family: \"Whitney\";\n    color: #eb7f00;\n    font-weight: 900;\n    background-color: transparent; }\n  .sidebar {\n  border-right: 1px solid #ccc; }\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.scss */ "./src/app/user/user.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/user/user.routes.ts":
/*!*************************************!*\
  !*** ./src/app/user/user.routes.ts ***!
  \*************************************/
/*! exports provided: UserRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutes", function() { return UserRoutes; });
/* harmony import */ var _components_favorite_products_favorite_products_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/favorite-products/favorite-products.component */ "./src/app/user/components/favorite-products/favorite-products.component.ts");
/* harmony import */ var _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/overview/overview.component */ "./src/app/user/components/overview/overview.component.ts");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _components_orders_orders_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/orders/orders.component */ "./src/app/user/components/orders/orders.component.ts");
/* harmony import */ var _components_addresses_addresses_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/addresses/addresses.component */ "./src/app/user/components/addresses/addresses.component.ts");
/* harmony import */ var _components_orders_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/orders/order-detail/order-detail.component */ "./src/app/user/components/orders/order-detail/order-detail.component.ts");






var UserRoutes = [
    {
        path: '',
        component: _user_component__WEBPACK_IMPORTED_MODULE_2__["UserComponent"],
        children: [
            { path: '', redirectTo: 'orders' },
            { path: 'overview', component: _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_1__["OverviewComponent"], redirectTo: 'orders' },
            { path: 'orders', component: _components_orders_orders_component__WEBPACK_IMPORTED_MODULE_3__["OrdersComponent"] },
            { path: 'orders/detail/:number', component: _components_orders_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_5__["OrderDetailComponent"] },
            { path: 'addresses', component: _components_addresses_addresses_component__WEBPACK_IMPORTED_MODULE_4__["AddressesComponent"], redirectTo: 'orders' },
            { path: 'favorite-products', component: _components_favorite_products_favorite_products_component__WEBPACK_IMPORTED_MODULE_0__["FavoriteProductsComponent"] }
        ]
    },
];


/***/ })

}]);
//# sourceMappingURL=user-index.js.map