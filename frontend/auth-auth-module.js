(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"],{

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/auth/components/login/login.component.ts");
/* harmony import */ var _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/sign-up/sign-up.component */ "./src/app/auth/components/sign-up/sign-up.component.ts");
/* harmony import */ var _auth_routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.routes */ "./src/app/auth/auth.routes.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _components_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/forget-password/forget-password.component */ "./src/app/auth/components/forget-password/forget-password.component.ts");
/* harmony import */ var _components_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/update-password/update-password.component */ "./src/app/auth/components/update-password/update-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_auth_routes__WEBPACK_IMPORTED_MODULE_5__["AuthRoutes"]),
                _shared_index__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]
            ],
            declarations: [
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
                _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_4__["SignUpComponent"],
                _components_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_7__["ForgetPasswordComponent"],
                _components_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_8__["UpdatePasswordComponent"]
            ]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.routes.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.routes.ts ***!
  \*************************************/
/*! exports provided: AuthRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutes", function() { return AuthRoutes; });
/* harmony import */ var _components_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/update-password/update-password.component */ "./src/app/auth/components/update-password/update-password.component.ts");
/* harmony import */ var _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/sign-up/sign-up.component */ "./src/app/auth/components/sign-up/sign-up.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/auth/components/login/login.component.ts");
/* harmony import */ var _components_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/forget-password/forget-password.component */ "./src/app/auth/components/forget-password/forget-password.component.ts");




var AuthRoutes = [
    { path: '', redirectTo: 'signup', pathMatch: 'full' },
    { path: 'signup', component: _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_1__["SignUpComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'recover', component: _components_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_3__["ForgetPasswordComponent"] },
    { path: 'updatePassword', component: _components_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_0__["UpdatePasswordComponent"] }
];


/***/ }),

/***/ "./src/app/auth/components/forget-password/forget-password.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/auth/components/forget-password/forget-password.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <div class=\"col-12 col-md-7 col-sm-7 col-lg-5 loginwindow\">\n\n    <div class=\"row\">\n      <div class=\"col-12 col-md-12 col-sm-12 col-lg-12\">\n        <div class=\"row login-container\" data-hook=\"\">\n          <div id=\"content\" class=\"col-sm-12\" data-hook=\"\">\n            <div class=\"col-md-12 col-12 col-centered\">\n              <div class=\"panel panel-default\" *ngIf=\"emailSent==false\">\n                <div class=\"panel-heading\">\n                  <h3 class=\"panel-title\">Forget Password?</h3>\n                </div>\n                <p class=\"login-info-text\">Please enter your valid email</p>\n                <div id=\"existing-customer\" class=\"panel-body\" data-hook=\"login\">\n                  <form class=\"login-login-form\" [formGroup]=\"forgetPasswordForm\" (ngSubmit)=\"onSubmit()\">\n                    <fieldset class=\"login-input-container\">\n                      <div class=\"login-input-item\">\n                        <input type=\"email\" class=\"login-user-input-email login-user-input\" name=\"email\" placeholder=\"Your Email Address\" formControlName=\"email\"\n                          autocomplete=\"off\">\n                        <div *ngIf=\"forgetPasswordForm.get('email').errors && forgetPasswordForm.get('email').touched\">\n                          <span class=\"login-error-icon text-danger\">!</span>\n                          <p class=\"login-error-message text-danger\">{{forgetPasswordForm.get('email').errors.msg || 'Please enter a valid email id'}}</p>\n                        </div>\n                      </div>\n                    </fieldset>\n                    <fieldset class=\"login-login-button-container\">\n                      <button type=\"submit\" class=\"btn btn-danger login-login-button\">Reset Password</button>\n                    </fieldset>\n                  </form>\n                </div>\n              </div>\n              <div class=\"panel panel-default\" *ngIf=\"emailSent==true\">\n                <div class=\"panel-heading\">\n                  <h3 class=\"panel-title\">Email Sent!</h3>\n                  <p>Reset Password instruction has been sent sucessfully on your mail</p>\n                  <strong>{{sentEmail}}!</strong>\n                </div>\n\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/auth/components/forget-password/forget-password.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/auth/components/forget-password/forget-password.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loginwindow {\n  margin: 6em auto 0;\n  overflow: hidden;\n  padding: 10px 0; }\n\n.panel {\n  margin-bottom: 20px;\n  background-color: #e9ecef;\n  border: none;\n  border-radius: 4px;\n  border: 1px solid #ccc;\n  box-shadow: none; }\n\n.panel .panel-heading {\n    padding: 30px 0 0;\n    border-bottom: 1px solid transparent;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    text-align: center;\n    background-color: transparent; }\n\n.panel .panel-heading .panel-title {\n      margin-top: 0;\n      margin-bottom: 0;\n      font-size: 28px;\n      color: #212529;\n      font-weight: 400; }\n\n.login-input-container {\n  margin: 20px;\n  border: 1px solid #ccc;\n  border-radius: 5px;\n  padding: 0;\n  background-color: #fff; }\n\nfieldset {\n  display: block;\n  -webkit-margin-start: 2px;\n  -webkit-margin-end: 2px;\n  -webkit-padding-before: 0.35em;\n  -webkit-padding-start: 0.75em;\n  -webkit-padding-end: 0.75em;\n  -webkit-padding-after: 0.625em;\n  min-width: -webkit-min-content;\n  border-width: 2px;\n  border-style: groove;\n  border-color: threedface;\n  -o-border-image: initial;\n     border-image: initial; }\n\n.login-input-item {\n  position: relative; }\n\n.login-container {\n  box-sizing: border-box;\n  text-align: center;\n  position: relative; }\n\n.login-user-input-email {\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px; }\n\n.login-user-input-password {\n  border-top: 1px solid #ccc !important;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.login-user-input {\n  display: block;\n  color: #212529;\n  padding-right: 40px;\n  font-size: 15px;\n  width: 100%;\n  border: 0;\n  padding: 15px; }\n\ninput {\n  -webkit-appearance: textfield;\n  background-color: #fff;\n  -webkit-rtl-ordering: logical;\n  -webkit-user-select: text;\n     -moz-user-select: text;\n      -ms-user-select: text;\n          user-select: text;\n  cursor: auto;\n  padding: 1px;\n  border-width: 2px;\n  border-style: inset;\n  border-color: initial;\n  -o-border-image: initial;\n     border-image: initial;\n  text-rendering: auto;\n  color: initial;\n  letter-spacing: normal;\n  word-spacing: normal;\n  text-transform: none;\n  text-indent: 0px;\n  text-shadow: none;\n  display: inline-block;\n  text-align: start;\n  margin: 0em 0em 0em 0em;\n  font: 11px system-ui; }\n\nfieldset {\n  border: 1px solid #ccc;\n  margin: 0 2px;\n  padding: .35em .625em .75em; }\n\n.login-login-button-container {\n  padding: 10px 20px;\n  margin: 0;\n  border: 0; }\n\n.login-login-button {\n  font-size: 14px;\n  font-weight: bold;\n  letter-spacing: 2px;\n  padding: 15px;\n  display: block;\n  width: 100%;\n  border: 0;\n  text-transform: uppercase;\n  border-radius: 3px;\n  background-color: #ffb132;\n  color: #fff; }\n\n.login-link-container {\n  text-align: right;\n  padding: 0px 20px 12px;\n  font-size: 14px; }\n\n.login-link {\n  text-decoration: none;\n  font-size: 15px;\n  color: #ffe364 !important; }\n\n.login-right-links {\n  float: right; }\n\n.login-info-text {\n  color: #495057;\n  font-size: 12px; }\n\n.login-create-account-link {\n  margin-left: 5px; }\n\na {\n  background-color: transparent; }\n\n.login-third-party-login {\n  margin-top: 30px; }\n\n.login-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n\n.login-info-text {\n  color: #495057;\n  font-size: 12px; }\n\n.login-button-container {\n  margin-top: 10px;\n  margin-bottom: 40px; }\n\n@media (min-width: 360px) {\n  .login-facebook {\n    margin-right: 15px;\n    margin-bottom: 0; } }\n\n@media (min-width: 360px) {\n  .login-button {\n    width: 49%;\n    display: inline-block; } }\n\n.login-facebook {\n  margin-bottom: 0px; }\n\n.login-button {\n  width: 100%;\n  padding-left: 18%;\n  position: relative;\n  font-size: 13px;\n  font-weight: 500;\n  height: 50px;\n  border: 1px solid #ccc;\n  background-color: #fff;\n  border-radius: 5px;\n  text-align: left; }\n\n[type=reset],\n[type=submit],\nbutton,\nhtml [type=button] {\n  -webkit-appearance: button; }\n\n[type=button],\n[type=reset],\n[type=submit],\nbutton {\n  cursor: pointer; }\n\nbutton,\nselect {\n  text-transform: none; }\n\nbutton,\ninput,\nselect,\ntextarea {\n  margin: 0; }\n\nbutton,\ninput,\nselect {\n  overflow: visible; }\n\nbutton,\ninput,\nselect,\ntextarea {\n  font: inherit; }\n\n.header-sprite {\n  background: url(http://res.cloudinary.com/mally/image/upload/v1489480940/148948096411965_w5wbjb.png) no-repeat 0 0;\n  background-size: 159px 48px;\n  display: inline-block; }\n\n.login-fb-logo,\n.login-gplus-logo {\n  height: 29px;\n  position: absolute;\n  left: 15px; }\n\n.login-fb-logo {\n  background-position: -93px 0;\n  width: 28px;\n  top: 10px; }\n\n.login-gplus-logo {\n  background-position: -122px 0;\n  width: 23px;\n  top: 13px; }\n\n.login-error-icon {\n  border: 2px solid #dc3545;\n  padding: 0 7px;\n  display: inline-block;\n  position: absolute;\n  top: 12px;\n  right: 10px;\n  font-weight: 500;\n  border-radius: 21px; }\n\n.login-error-message {\n  font-size: 11px;\n  margin-left: 15px;\n  text-align: left;\n  margin-top: -9px;\n  max-height: 500px;\n  transition-property: all;\n  transition-duration: .5s;\n  transition-timing-function: cubic-bezier(0, 1, 0.5, 1); }\n\np {\n  display: block;\n  -webkit-margin-before: 1em;\n  -webkit-margin-after: 1em;\n  -webkit-margin-start: 0px;\n  -webkit-margin-end: 0px; }\n\n.login-input-container:hover {\n  border: 1px solid #e9ecef;\n  box-shadow: 0.1px 0.2px 0.4px #85888b; }\n\n.createacc {\n  display: flex;\n  align-items: center;\n  padding-left: 3rem; }\n"

/***/ }),

/***/ "./src/app/auth/components/forget-password/forget-password.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/auth/components/forget-password/forget-password.component.ts ***!
  \******************************************************************************/
/*! exports provided: ForgetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetPasswordComponent", function() { return ForgetPasswordComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ForgetPasswordComponent = /** @class */ (function () {
    function ForgetPasswordComponent(fb, store, route, router, actions, authService) {
        this.fb = fb;
        this.store = store;
        this.route = route;
        this.router = router;
        this.actions = actions;
        this.authService = authService;
        this.emailSent = false;
        this.sentEmail = '';
    }
    ForgetPasswordComponent.prototype.ngOnInit = function () {
        this.initForm();
    };
    ForgetPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        var values = this.forgetPasswordForm.value;
        var keys = Object.keys(values);
        if (this.forgetPasswordForm.valid) {
            this.forgetPasswordSubs = this.authService
                .forgetPassword(values).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (_) {
                _this.emailSent = true,
                    _this.sentEmail = values.email;
            }, function (user) {
                var errors = user.error.error || 'Something went wrong';
                keys.forEach(function (val) {
                    _this.pushErrorFor(val, errors);
                });
            })).subscribe();
        }
        else {
            keys.forEach(function (val) {
                var ctrl = _this.forgetPasswordForm.controls[val];
                if (!ctrl.valid) {
                    _this.pushErrorFor(val, null);
                    ctrl.markAsTouched();
                }
                ;
            });
        }
    };
    ForgetPasswordComponent.prototype.pushErrorFor = function (ctrl_name, msg) {
        this.forgetPasswordForm.controls[ctrl_name].setErrors({ 'msg': msg });
    };
    ForgetPasswordComponent.prototype.initForm = function () {
        var email = '';
        this.forgetPasswordForm = this.fb.group({
            'email': [email, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
        });
    };
    ForgetPasswordComponent.prototype.ngOnDestroy = function () {
        if (this.forgetPasswordSubs) {
            this.forgetPasswordSubs.unsubscribe();
        }
    };
    ForgetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
            selector: 'app-forget-password',
            template: __webpack_require__(/*! ./forget-password.component.html */ "./src/app/auth/components/forget-password/forget-password.component.html"),
            styles: [__webpack_require__(/*! ./forget-password.component.scss */ "./src/app/auth/components/forget-password/forget-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__["AuthActions"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], ForgetPasswordComponent);
    return ForgetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/auth/components/login/login.component.html":
/*!************************************************************!*\
  !*** ./src/app/auth/components/login/login.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <div class=\"col-12 col-md-10 col-sm-10 col-lg-8 loginwindow\">\n    <h2>Sign In or Regsister</h2>\n    <div class=\"row\">\n      <div class=\"col-12 0col-md-6 col-sm-6 col-lg-6\">\n        <div class=\"row login-container\" data-hook=\"\">\n          <div id=\"content\" class=\"col-sm-12\" data-hook=\"\">\n            <div class=\"col-centered\">\n              <div class=\"panel panel-default\">\n                <div id=\"existing-customer\" class=\"panel-body\" data-hook=\"login\">\n                  <div class=\"login-third-party-login col-12\">\n                    <p class=\"login-button-info-text login-info-text\">EASILY USING</p>\n                    <div class=\"login-button-container col-12\">\n                      <div class=\"row\">\n                        <button class=\"login-google login-button\" id=\"gPlusLogin\" (click)=\"socialLogin('google')\">\n                          <span class=\"header-sprite login-gplus-logo\"></span>\n                          GOOGLE\n                        </button>\n                      </div>\n                    </div>\n                  </div>\n                  <p class=\"login-info-text\">- OR USING EMAIL -</p>\n                  <form class=\"login-login-form\" [formGroup]=\"signInForm\" (ngSubmit)=\"onSubmit()\">\n                    <fieldset class=\"login-input-container\">\n                      <div class=\"login-input-item\">\n                        <input type=\"email\" class=\"login-user-input-email login-user-input\" name=\"email\" placeholder=\"Your Email Address\" formControlName=\"email\"\n                          autocomplete=\"off\">\n                        <div *ngIf=\"signInForm.get('email').errors && signInForm.get('email').touched\">\n                          <span class=\"login-error-icon text-danger\">!</span>\n                          <p class=\"login-error-message text-danger\">{{signInForm.get('email').errors.msg || 'Please enter a valid email id'}}</p>\n                        </div>\n                      </div>\n                      <div class=\"login-input-item\">\n                        <input type=\"password\" class=\"login-user-input-password login-user-input\" name=\"password\" placeholder=\"Enter Password\" formControlName=\"password\"\n                          autocomplete=\"off\">\n                        <div *ngIf=\"signInForm.get('password').errors && signInForm.get('password').touched\">\n                          <span class=\"login-error-icon text-danger\">!</span>\n                          <p class=\"login-error-message text-danger\">{{signInForm.get('password').errors.msg || 'Password must be at least 6 characters'}}</p>\n                        </div>\n                      </div>\n                    </fieldset>\n                    <fieldset class=\"login-login-button-container\">\n                      <button type=\"submit\" class=\"btn login-login-button\">Log in</button>\n                    </fieldset>\n                  </form>\n                  <div class=\"login-link-container\">\n                    <small>\n                      <a class=\"login-link text-danger\" [routerLink]=\"['/auth/recover']\">Forget Password</a>\n\n                    </small>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-12 0col-md-6 col-sm-6 col-lg-6 createacc\">\n        <div class=\"login-right-links\">\n          <h2 class=\"text-default\">For New User</h2>\n          <p>Creating an account is fast, easy, and free. You'll be able to manage your autoships, track your orders, write\n            reviews, and more!\n            <p>\n              <a class=\"btn login-login-button\" [routerLink]=\"['/auth']\">Create Account</a>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n</div>"

/***/ }),

/***/ "./src/app/auth/components/login/login.component.scss":
/*!************************************************************!*\
  !*** ./src/app/auth/components/login/login.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loginwindow {\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 10px 0; }\n\n.panel {\n  margin-bottom: 20px;\n  background-color: #e9ecef;\n  border: none;\n  border-radius: 4px;\n  border: 1px solid #ccc;\n  box-shadow: none; }\n\n.panel .panel-heading {\n    padding: 30px 0 0;\n    border-bottom: 1px solid transparent;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    text-align: center;\n    background-color: transparent; }\n\n.panel .panel-heading .panel-title {\n      margin-top: 0;\n      margin-bottom: 0;\n      font-size: 28px;\n      color: #212529;\n      font-weight: 400; }\n\n.login-input-container {\n  margin: 20px;\n  border: 1px solid #ccc;\n  border-radius: 5px;\n  padding: 0; }\n\nfieldset {\n  display: block;\n  -webkit-margin-start: 2px;\n  -webkit-margin-end: 2px;\n  -webkit-padding-before: 0.35em;\n  -webkit-padding-start: 0.75em;\n  -webkit-padding-end: 0.75em;\n  -webkit-padding-after: 0.625em;\n  min-width: -webkit-min-content;\n  border-width: 2px;\n  border-style: groove;\n  border-color: threedface;\n  -o-border-image: initial;\n     border-image: initial; }\n\n.login-input-item {\n  position: relative; }\n\n.login-container {\n  box-sizing: border-box;\n  text-align: center;\n  position: relative; }\n\n.login-user-input-email {\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px; }\n\n.login-user-input-password {\n  border-top: 1px solid #ccc !important;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.login-user-input {\n  display: block;\n  color: #212529;\n  padding-right: 40px;\n  font-size: 15px;\n  width: 100%;\n  border: 0;\n  padding: 15px; }\n\ninput {\n  -webkit-appearance: textfield;\n  background-color: #fff;\n  -webkit-rtl-ordering: logical;\n  -webkit-user-select: text;\n     -moz-user-select: text;\n      -ms-user-select: text;\n          user-select: text;\n  cursor: auto;\n  padding: 1px;\n  border-width: 2px;\n  border-style: inset;\n  border-color: initial;\n  -o-border-image: initial;\n     border-image: initial;\n  text-rendering: auto;\n  color: initial;\n  letter-spacing: normal;\n  word-spacing: normal;\n  text-transform: none;\n  text-indent: 0px;\n  text-shadow: none;\n  display: inline-block;\n  text-align: start;\n  margin: 0em 0em 0em 0em;\n  font: 11px system-ui; }\n\nfieldset {\n  border: 1px solid #ccc;\n  margin: 0 2px;\n  padding: .35em .625em .75em; }\n\n.login-login-button-container {\n  padding: 10px 20px;\n  margin: 0;\n  border: 0; }\n\n.login-login-button {\n  font-size: 14px;\n  font-weight: bold;\n  letter-spacing: 2px;\n  padding: 15px;\n  display: block;\n  width: 100%;\n  border: 0;\n  text-transform: uppercase;\n  border-radius: 3px;\n  background-color: #ffb132;\n  color: #fff; }\n\n.login-link-container {\n  text-align: right;\n  padding: 0px 20px 12px;\n  font-size: 14px; }\n\n.login-link {\n  text-decoration: none;\n  font-size: 15px;\n  color: #ffe364 !important; }\n\n.login-right-links {\n  float: right; }\n\n.login-info-text {\n  color: #495057;\n  font-size: 12px; }\n\n.login-create-account-link {\n  margin-left: 5px; }\n\na {\n  background-color: transparent; }\n\n.login-third-party-login {\n  margin-top: 30px; }\n\n.login-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n\n.login-info-text {\n  color: #495057;\n  font-size: 12px; }\n\n.login-button-container {\n  margin-top: 10px;\n  margin-bottom: 40px; }\n\n@media (min-width: 360px) {\n  .login-facebook {\n    margin-right: 15px;\n    margin-bottom: 0; } }\n\n@media (min-width: 360px) {\n  .login-button {\n    width: 49%;\n    display: inline-block; } }\n\n.login-facebook {\n  margin-bottom: 0px; }\n\n.login-button {\n  width: 100%;\n  padding-left: 18%;\n  position: relative;\n  font-size: 13px;\n  font-weight: 500;\n  height: 50px;\n  border: 1px solid #ccc;\n  background-color: #fff;\n  border-radius: 5px;\n  text-align: left; }\n\n[type=reset],\n[type=submit],\nbutton,\nhtml [type=button] {\n  -webkit-appearance: button; }\n\n[type=button],\n[type=reset],\n[type=submit],\nbutton {\n  cursor: pointer; }\n\nbutton,\nselect {\n  text-transform: none; }\n\nbutton,\ninput,\nselect,\ntextarea {\n  margin: 0; }\n\nbutton,\ninput,\nselect {\n  overflow: visible; }\n\nbutton,\ninput,\nselect,\ntextarea {\n  font: inherit; }\n\n.header-sprite {\n  background: url(http://res.cloudinary.com/mally/image/upload/v1489480940/148948096411965_w5wbjb.png) no-repeat 0 0;\n  background-size: 159px 48px;\n  display: inline-block; }\n\n.login-fb-logo,\n.login-gplus-logo {\n  height: 29px;\n  position: absolute;\n  left: 15px; }\n\n.login-fb-logo {\n  background-position: -93px 0;\n  width: 28px;\n  top: 10px; }\n\n.login-gplus-logo {\n  background-position: -122px 0;\n  width: 23px;\n  top: 13px; }\n\n.login-error-icon {\n  border: 2px solid #dc3545;\n  padding: 0 7px;\n  display: inline-block;\n  position: absolute;\n  top: 12px;\n  right: 10px;\n  font-weight: 500;\n  border-radius: 21px; }\n\n.login-error-message {\n  font-size: 11px;\n  margin-left: 15px;\n  text-align: left;\n  margin-top: -9px;\n  max-height: 500px;\n  transition-property: all;\n  transition-duration: .5s;\n  transition-timing-function: cubic-bezier(0, 1, 0.5, 1); }\n\np {\n  display: block;\n  -webkit-margin-before: 1em;\n  -webkit-margin-after: 1em;\n  -webkit-margin-start: 0px;\n  -webkit-margin-end: 0px; }\n\n.login-input-container {\n  background-color: #fff; }\n\n.login-input-container:hover {\n    border: 1px solid #e9ecef;\n    box-shadow: 0.1px 0.2px 0.4px #85888b; }\n\n.createacc {\n  display: flex;\n  align-items: center;\n  padding-left: 3rem; }\n"

/***/ }),

/***/ "./src/app/auth/components/login/login.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/auth/components/login/login.component.ts ***!
  \**********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, store, route, router, actions, authService) {
        this.fb = fb;
        this.store = store;
        this.route = route;
        this.router = router;
        this.actions = actions;
        this.authService = authService;
        this.title = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appName;
        this.redirectIfUserLoggedIn();
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.initForm();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        var values = this.signInForm.value;
        var keys = Object.keys(values);
        if (this.signInForm.valid) {
            this.loginSubs = this.authService
                .login(values).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (_) { return _; }, function (user) {
                var errors = user.error.error || 'Something went wrong';
                keys.forEach(function (val) {
                    _this.pushErrorFor(val, errors);
                });
            })).subscribe();
        }
        else {
            keys.forEach(function (val) {
                var ctrl = _this.signInForm.controls[val];
                if (!ctrl.valid) {
                    _this.pushErrorFor(val, null);
                    ctrl.markAsTouched();
                }
                ;
            });
        }
    };
    LoginComponent.prototype.pushErrorFor = function (ctrl_name, msg) {
        this.signInForm.controls[ctrl_name].setErrors({ 'msg': msg });
    };
    LoginComponent.prototype.initForm = function () {
        var email = '';
        var password = '';
        this.signInForm = this.fb.group({
            'email': [email, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            'password': [password, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    };
    LoginComponent.prototype.redirectIfUserLoggedIn = function () {
        var _this = this;
        this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_8__["getAuthStatus"]).subscribe(function (data) {
            if (data === true) {
                _this.router.navigate([_this.returnUrl]);
            }
        });
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        if (this.loginSubs) {
            this.loginSubs.unsubscribe();
        }
    };
    LoginComponent.prototype.socialLogin = function (provider) {
        this.store.dispatch(this.actions.oAuthLogin(provider));
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/auth/components/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_6__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _actions_auth_actions__WEBPACK_IMPORTED_MODULE_1__["AuthActions"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/components/sign-up/sign-up.component.html":
/*!****************************************************************!*\
  !*** ./src/app/auth/components/sign-up/sign-up.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"col-12 col-md-7 col-sm-7 col-lg-5 loginwindow\">\n    <h3 class=\"panel-title\">Signup to {{title}}</h3>\n    <div class=\"row register-container\" data-hook=\"\">\n      <div id=\"content\" class=\"col-sm-12\" data-hook=\"\">\n        <div>\n          <div class=\"panel panel-default\">\n            <div class=\"panel-heading\">\n\n            </div>\n            <div id=\"existing-customer\" class=\"panel-body\" data-hook=\"login\">\n              <div class=\"register-third-party-register\">\n                <p class=\"register-button-info-text register-info-text\">EASILY USING</p>\n                <div class=\"register-button-container\">\n                  <button class=\"register-facebook register-button coming-soon\">\n                    <span class=\"register-fb-logo register-sprite\"></span>\n                    FACEBOOK\n                  </button>\n                  <button class=\"register-google register-button\" id=\"gPlusLogin\" (click)=\"socialLogin('google')\">\n                    <span class=\"register-gplus-logo register-sprite\"></span>\n                    GOOGLE\n                  </button>\n                </div>\n              </div>\n              <p class=\"register-button-info-text register-info-text\">- OR USING EMAIL -</p>\n              <form class=\"register-register-form\" novalidate=\"\" [formGroup]=\"signUpForm\" (ngSubmit)=\"onSubmit()\">\n                <fieldset class=\"register-input-container\">\n\n                  <div class=\"register-input-item\">\n                    <input type=\"email\" class=\"register-user-input-email register-user-input\" name=\"email\" formControlName=\"email\" placeholder=\"Your Email Address\"\n                      autocomplete=\"off\">\n                    <div *ngIf=\"signUpForm.get('email').errors && signUpForm.get('email').touched\">\n                      <span class=\"register-error-icon\">!</span>\n                      <p class=\"register-error-message\">{{signUpForm.get('email').errors.msg || 'Please enter a valid email id'}}</p>\n                    </div>\n                  </div>\n\n                  <div class=\"register-input-item\">\n                    <input type=\"password\" class=\"register-user-input-password register-user-input\" name=\"password\" formControlName=\"password\"\n                      placeholder=\"Choose Password\" autocomplete=\"off\">\n                    <div *ngIf=\"signUpForm.get('password').errors && signUpForm.get('password').touched\">\n                      <span class=\"register-error-icon\">!</span>\n                      <p class=\"register-error-message\">{{signUpForm.get('password').errors.msg || 'Password must be at least 6 characters'}}</p>\n                    </div>\n                  </div>\n\n                  <div class=\"register-input-item\">\n                    <input type=\"password\" class=\"register-user-input-password register-user-input\" name=\"password_confirmation\" formControlName=\"password_confirmation\"\n                      placeholder=\"Confirm Password\" autocomplete=\"off\">\n                    <div *ngIf=\"(signUpForm.get('password_confirmation').errors || signUpForm.hasError('mismatchedPasswords')) && signUpForm.get('password_confirmation').touched\">\n                      <span class=\"register-error-icon\">!</span>\n                      <p class=\"register-error-message\">{{ 'Password must match'}}</p>\n                    </div>\n                  </div>\n\n                  <div class=\"register-input-item\">\n                    <input type=\"number\" class=\"register-user-input-mobile register-user-input\" name=\"mobile\" formControlName=\"mobile\" placeholder=\"Mobile Number (For order status updates)\">\n                    <div *ngIf=\"signUpForm.get('mobile').errors && signUpForm.get('mobile').touched\">\n                      <span class=\"register-error-icon\">!</span>\n                      <p class=\"register-error-message\">{{signUpForm.get('mobile').errors.msg || 'Please enter a valid mobile number (10 digits)'}}</p>\n                    </div>\n                  </div>\n\n                  <fieldset data-type=\"horizontal\" class=\"register-gender\">\n                    <legend class=\"register-gender-title\">I'm a</legend>\n\n                    <input type=\"radio\" class=\"register-gender-radio\" id=\"male\" name=\"gender\" value=\"M\" formControlName=\"gender\">\n                    <label class=\"register-gender-label\" for=\"male\">Male</label>\n\n                    <input type=\"radio\" class=\"register-gender-radio\" id=\"female\" name=\"gender\" value=\"F\" formControlName=\"gender\">\n                    <label class=\"register-gender-label\" for=\"female\">Female</label>\n\n                    <div *ngIf=\"signUpForm.get('gender').errors && signUpForm.get('gender').touched\">\n                      <span class=\"register-gender-error-icon register-error-icon\">!</span>\n                      <p class=\"register-gender-error-message register-error-message\">{{signUpForm.get('gender').errors.msg || 'Please select your gender'}}</p>\n                    </div>\n                  </fieldset>\n                </fieldset>\n                <fieldset class=\"register-register-button-container\">\n                  <button class=\"btn register-register-button\">REGISTER</button>\n                </fieldset>\n              </form>\n              <div class=\"register-link-container\">\n                <small>\n                  <div class=\"register-login-link\">\n                    <span class=\"register-info-text\">Already have an account?</span>\n                    <a class=\"register-create-account-link register-link text-danger\" [routerLink]=\"['/auth','login']\">Login!</a>\n                  </div>\n                </small>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/components/sign-up/sign-up.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/auth/components/sign-up/sign-up.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loginwindow {\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 10px 0; }\n\n.panel {\n  margin-bottom: 20px;\n  background-color: #f8f9fa;\n  border: none;\n  border-radius: 4px;\n  border: 1px solid #ccc;\n  box-shadow: none; }\n\n.panel .panel-heading {\n    padding: 30px 0 0;\n    border-bottom: 1px solid transparent;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    text-align: center;\n    background-color: transparent; }\n\n.panel .panel-heading .panel-title {\n      margin-top: 0;\n      margin-bottom: 0;\n      font-size: 28px;\n      color: #ccc;\n      font-weight: 400; }\n\n.register-input-container {\n  margin: 20px;\n  border: 1px solid #ccc;\n  border-radius: 5px;\n  padding: 0; }\n\n.register-register-button-container {\n  padding: 10px 20px;\n  margin: 0;\n  border: 0; }\n\n.register-register-button {\n  font-size: 14px;\n  font-weight: BOLD;\n  color: #fff;\n  letter-spacing: 2px;\n  padding: 15px;\n  display: block;\n  width: 100%;\n  border: 0;\n  text-transform: uppercase;\n  border-radius: 3px;\n  background-color: #ffb132; }\n\n.register-input-item {\n  position: relative; }\n\n.register-user-input-email {\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px; }\n\n.register-user-input {\n  display: block;\n  color: #212529;\n  font-size: 15px;\n  padding-right: 40px;\n  width: 100%;\n  border: 0;\n  padding: 15px; }\n\n.register-user-input-password {\n  border-top: 1px solid #ccc; }\n\n.register-user-input-mobile {\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.register-gender {\n  position: relative;\n  border: 0;\n  font-size: 13px;\n  color: #343a40;\n  text-align: left;\n  padding: 15px;\n  margin: 0;\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.register-gender {\n  position: relative;\n  border: 0;\n  font-size: 13px;\n  color: #343a40;\n  text-align: left;\n  padding: 15px;\n  margin: 0;\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.register-gender-title {\n  display: inline;\n  font-weight: 500; }\n\nlegend {\n  box-sizing: border-box;\n  color: inherit;\n  display: table;\n  max-width: 100%;\n  padding: 0;\n  white-space: normal;\n  margin-bottom: 0;\n  font-size: inherit;\n  width: auto;\n  border: none; }\n\n.register-gender-radio {\n  margin-left: 15px;\n  margin-right: 5px; }\n\n[type=checkbox],\n[type=radio] {\n  box-sizing: border-box;\n  padding: 0; }\n\n.register-gender-label {\n  cursor: pointer;\n  font-weight: 500; }\n\nfieldset {\n  display: block;\n  -webkit-margin-start: 2px;\n  -webkit-margin-end: 2px;\n  -webkit-padding-before: 0.35em;\n  -webkit-padding-start: 0.75em;\n  -webkit-padding-end: 0.75em;\n  -webkit-padding-after: 0.625em;\n  min-width: -webkit-min-content;\n  border-width: 2px;\n  border-style: groove;\n  border-color: threedface;\n  -o-border-image: initial;\n     border-image: initial; }\n\n.register-link-container {\n  text-align: left;\n  padding: 20px; }\n\n.register-login-link {\n  text-align: center; }\n\n.register-info-text {\n  color: #343a40;\n  font-size: 12px; }\n\n.register-create-account-link {\n  margin-left: 5px; }\n\n.register-link {\n  text-decoration: none;\n  color: #ffe364 !important;\n  font-size: 15px; }\n\na {\n  background-color: transparent; }\n\n.register-third-party-register {\n  margin-top: 30px; }\n\n.register-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n\n.register-info-text {\n  color: #343a40;\n  font-size: 12px; }\n\n.register-button-container {\n  margin-top: 10px;\n  margin-bottom: 40px; }\n\n.register-button {\n  margin: 0 auto;\n  max-width: 162px;\n  padding-left: 18%;\n  position: relative;\n  font-size: 13px;\n  font-weight: 500;\n  color: #343a40;\n  height: 50px;\n  border: 1px solid #ccc;\n  background-color: #fff;\n  border-radius: 5px;\n  text-align: left; }\n\n[type=reset],\n[type=submit],\nbutton,\nhtml [type=button] {\n  -webkit-appearance: button; }\n\n[type=button],\n[type=reset],\n[type=submit],\nbutton {\n  cursor: pointer; }\n\n/* For Firefox */\n\ninput[type='number'] {\n  -moz-appearance: textfield; }\n\n/* Webkit browsers like Safari and Chrome */\n\ninput[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0; }\n\nbutton,\nselect {\n  text-transform: none; }\n\nbutton,\ninput,\nselect,\ntextarea {\n  font: inherit; }\n\n@media (min-width: 360px) {\n  .register-facebook {\n    margin-right: 15px;\n    margin-bottom: 0; }\n  .register-button {\n    width: 49%;\n    display: inline-block; } }\n\n.register-container {\n  box-sizing: border-box;\n  text-align: center;\n  position: relative;\n  padding-bottom: 40px; }\n\n.register-sprite {\n  background: url(http://res.cloudinary.com/mally/image/upload/v1489480940/148948096411965_w5wbjb.png) no-repeat 0 0;\n  background-size: 159px 48px;\n  display: inline-block; }\n\n.register-fb-logo {\n  background-position: -93px 0;\n  width: 28px;\n  top: 10px; }\n\n.register-fb-logo,\n.register-gplus-logo {\n  height: 29px;\n  position: absolute;\n  left: 15px; }\n\n.register-gplus-logo {\n  background-position: -122px 0;\n  width: 23px;\n  top: 13px; }\n\n.register-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n\n.register-info-text {\n  color: #343a40;\n  font-size: 12px; }\n\n.register-error-icon {\n  border: 2px solid #ff9314;\n  padding: 0 7px;\n  color: #ff9314;\n  display: inline-block;\n  position: absolute;\n  top: 12px;\n  right: 10px;\n  font-weight: 500;\n  border-radius: 21px; }\n\n.register-error-message {\n  font-size: 11px;\n  margin-left: 15px;\n  color: #ff9314;\n  text-align: left;\n  margin-top: -9px;\n  max-height: 500px;\n  transition-property: all;\n  transition-duration: .5s;\n  transition-timing-function: cubic-bezier(0, 1, 0.5, 1); }\n\n.register-gender-error-icon {\n  font-size: 16px; }\n\n.register-gender-error-message {\n  margin-left: 0;\n  margin-top: 10px;\n  margin-bottom: 0; }\n\n.register-input-container:hover {\n  border: 1px solid #e9ecef;\n  box-shadow: 0.1px 0.2px 0.4px #85888b; }\n"

/***/ }),

/***/ "./src/app/auth/components/sign-up/sign-up.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/auth/components/sign-up/sign-up.component.ts ***!
  \**************************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _reducers_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../reducers/selectors */ "./src/app/auth/reducers/selectors.ts");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(fb, actions, store, router, authService) {
        this.fb = fb;
        this.actions = actions;
        this.store = store;
        this.router = router;
        this.authService = authService;
        this.formSubmit = false;
        this.title = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appName;
        this.redirectIfUserLoggedIn();
    }
    SignUpComponent.prototype.ngOnInit = function () {
        this.initForm();
    };
    SignUpComponent.prototype.onSubmit = function () {
        var _this = this;
        var values = this.signUpForm.value;
        var keys = Object.keys(values);
        this.formSubmit = true;
        if (this.signUpForm.valid) {
            this.registerSubs = this.authService
                .register(values).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (_) { return _; }, function (user) {
                var errors = user.error.errors || {};
                keys.forEach(function (val) {
                    if (errors[val]) {
                        _this.pushErrorFor(val, errors[val][0]);
                    }
                    ;
                });
            })).subscribe();
        }
        else {
            keys.forEach(function (val) {
                var ctrl = _this.signUpForm.controls[val];
                if (!ctrl.valid) {
                    _this.pushErrorFor(val, null);
                    ctrl.markAsTouched();
                }
                ;
            });
        }
    };
    SignUpComponent.prototype.pushErrorFor = function (ctrl_name, msg) {
        this.signUpForm.controls[ctrl_name].setErrors({ 'msg': msg });
    };
    SignUpComponent.prototype.initForm = function () {
        var email = '';
        var password = '';
        var password_confirmation = '';
        var mobile = '';
        var gender = '';
        this.signUpForm = this.fb.group({
            'email': [email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
            'password': [password, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)])],
            'password_confirmation': [password_confirmation, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)])],
            'mobile': [mobile, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[0-9]{10}')])],
            'gender': [gender, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, { validator: this.matchingPasswords('password', 'password_confirmation') });
    };
    SignUpComponent.prototype.redirectIfUserLoggedIn = function () {
        var _this = this;
        this.store.select(_reducers_selectors__WEBPACK_IMPORTED_MODULE_7__["getAuthStatus"]).subscribe(function (data) {
            if (data === true) {
                _this.router.navigateByUrl('/');
            }
        });
    };
    SignUpComponent.prototype.ngOnDestroy = function () {
        if (this.registerSubs) {
            this.registerSubs.unsubscribe();
        }
    };
    SignUpComponent.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    SignUpComponent.prototype.socialLogin = function (provider) {
        this.store.dispatch(this.actions.oAuthLogin(provider));
    };
    SignUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/auth/components/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.scss */ "./src/app/auth/components/sign-up/sign-up.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _actions_auth_actions__WEBPACK_IMPORTED_MODULE_8__["AuthActions"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/app/auth/components/update-password/update-password.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/auth/components/update-password/update-password.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"register-container\" data-hook=\"\">\n  <div id=\"content\" class=\"col-sm-12\" data-hook=\"\">\n    <div class=\"col-md-5 col-centered\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n          <h3 class=\"panel-title\" *ngIf=\"passwordReset===false\">Update Password</h3>\n          <h3 class=\"panel-title\" *ngIf=\"passwordReset===true\">Password Changed!</h3>\n        </div>\n        <div id=\"existing-customer\" class=\"panel-body\" data-hook=\"login\" *ngIf=\"passwordReset===false\">\n          <form class=\"register-register-form\" novalidate=\"\" [formGroup]=\"updatePasswordForm\" (ngSubmit)=\"onSubmit()\">\n            <fieldset class=\"register-input-container\">\n\n              <div class=\"register-input-item\">\n                <input type=\"password\" class=\"register-user-input-password register-user-input\" name=\"password\" formControlName=\"password\"\n                  placeholder=\"Choose New Password\" autocomplete=\"off\">\n                <div *ngIf=\"updatePasswordForm.get('password').errors && updatePasswordForm.get('password').touched\">\n                  <span class=\"register-error-icon\">!</span>\n                  <p class=\"register-error-message\">{{updatePasswordForm.get('password').errors.msg || 'Password must be at least 6 characters'}}</p>\n                </div>\n              </div>\n\n              <div class=\"register-input-item\">\n                <input type=\"password\" class=\"register-user-input-password register-user-input\" name=\"password_confirmation\" formControlName=\"password_confirmation\"\n                  placeholder=\"Confirm Password\" autocomplete=\"off\">\n                <div *ngIf=\"(updatePasswordForm.get('password_confirmation').errors || updatePasswordForm.hasError('mismatchedPasswords')) && updatePasswordForm.get('password_confirmation').touched\">\n                  <span class=\"register-error-icon\">!</span>\n                  <p class=\"register-error-message\">{{ 'Password must match'}}</p>\n                </div>\n              </div>\n            </fieldset>\n            <fieldset class=\"register-register-button-container\">\n              <button class=\"btn  register-register-button\">Update Password!</button>\n            </fieldset>\n          </form>\n        </div>\n        <div id=\"existing-customer\" class=\"panel-body\" data-hook=\"login\" *ngIf=\"passwordReset===true\">\n          <span>Password has been changed sucessfully!</span>\n          <span>\n            Click\n            <a [routerLink]=\"['/auth/login']\">here</a>for login.\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/auth/components/update-password/update-password.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/auth/components/update-password/update-password.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel {\n  margin-bottom: 20px;\n  background-color: #e9ecef;\n  border: none;\n  border-radius: 4px;\n  border: 1px solid #ccc;\n  box-shadow: none; }\n  .panel .panel-heading {\n    padding: 30px 0 0;\n    border-bottom: 1px solid transparent;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    text-align: center;\n    background-color: transparent; }\n  .panel .panel-heading .panel-title {\n      margin-top: 0;\n      margin-bottom: 0;\n      font-size: 28px;\n      color: #343a40;\n      font-weight: 400; }\n  .register-input-container {\n  margin: 20px;\n  border: 1px solid #ccc;\n  border-radius: 5px;\n  padding: 0;\n  background-color: #fff; }\n  .register-input-container:hover {\n    border: 1px solid #e9ecef;\n    box-shadow: 0.1px 0.2px 0.4px #85888b; }\n  .register-register-button-container {\n  padding: 10px 20px;\n  margin: 0;\n  border: 0; }\n  .register-register-button {\n  font-size: 13px;\n  font-weight: 500;\n  letter-spacing: 2px;\n  padding: 15px;\n  display: block;\n  width: 100%;\n  border: 0;\n  text-transform: uppercase;\n  border-radius: 3px;\n  color: #fff;\n  background-color: #ffb132; }\n  .register-input-item {\n  position: relative; }\n  .register-user-input-email {\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px; }\n  .register-user-input {\n  display: block;\n  color: #212529;\n  font-size: 15px;\n  padding-right: 40px;\n  width: 100%;\n  border: 0;\n  padding: 15px; }\n  .register-user-input-password {\n  border-top: 1px solid #ccc; }\n  .register-user-input-mobile {\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n  .register-gender {\n  position: relative;\n  border: 0;\n  font-size: 13px;\n  color: #343a40;\n  text-align: left;\n  padding: 15px;\n  margin: 0;\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n  .register-gender {\n  position: relative;\n  border: 0;\n  font-size: 13px;\n  color: #343a40;\n  text-align: left;\n  padding: 15px;\n  margin: 0;\n  border-top: 1px solid #ccc;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px; }\n  .register-gender-title {\n  display: inline;\n  font-weight: 500; }\n  legend {\n  box-sizing: border-box;\n  color: inherit;\n  display: table;\n  max-width: 100%;\n  padding: 0;\n  white-space: normal;\n  margin-bottom: 0;\n  font-size: inherit;\n  width: auto;\n  border: none; }\n  .register-gender-radio {\n  margin-left: 15px;\n  margin-right: 5px; }\n  [type=checkbox],\n[type=radio] {\n  box-sizing: border-box;\n  padding: 0; }\n  .register-gender-label {\n  cursor: pointer;\n  font-weight: 500; }\n  fieldset {\n  display: block;\n  -webkit-margin-start: 2px;\n  -webkit-margin-end: 2px;\n  -webkit-padding-before: 0.35em;\n  -webkit-padding-start: 0.75em;\n  -webkit-padding-end: 0.75em;\n  -webkit-padding-after: 0.625em;\n  min-width: -webkit-min-content;\n  border-width: 2px;\n  border-style: groove;\n  border-color: threedface;\n  -o-border-image: initial;\n     border-image: initial; }\n  .register-link-container {\n  text-align: left;\n  padding: 20px; }\n  .register-login-link {\n  text-align: center; }\n  .register-info-text {\n  color: #495057;\n  font-size: 12px; }\n  .register-create-account-link {\n  margin-left: 5px; }\n  .register-link {\n  text-decoration: none; }\n  a {\n  background-color: transparent; }\n  .register-third-party-register {\n  margin-top: 30px; }\n  .register-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n  .register-info-text {\n  color: #495057;\n  font-size: 12px; }\n  .register-button-container {\n  margin-top: 10px;\n  margin-bottom: 40px; }\n  .register-button {\n  margin: 0 auto;\n  max-width: 162px;\n  padding-left: 18%;\n  position: relative;\n  font-size: 13px;\n  font-weight: 500;\n  color: #343a40;\n  height: 50px;\n  border: 1px solid #ccc;\n  background-color: #fff;\n  border-radius: 5px;\n  text-align: left; }\n  [type=reset],\n[type=submit],\nbutton,\nhtml [type=button] {\n  -webkit-appearance: button; }\n  [type=button],\n[type=reset],\n[type=submit],\nbutton {\n  cursor: pointer; }\n  /* For Firefox */\n  input[type='number'] {\n  -moz-appearance: textfield; }\n  /* Webkit browsers like Safari and Chrome */\n  input[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0; }\n  button,\nselect {\n  text-transform: none; }\n  button,\ninput,\nselect,\ntextarea {\n  font: inherit; }\n  @media (min-width: 360px) {\n  .register-facebook {\n    margin-right: 15px;\n    margin-bottom: 0; }\n  .register-button {\n    width: 49%;\n    display: inline-block; } }\n  .register-container {\n  box-sizing: border-box;\n  text-align: center;\n  position: relative;\n  padding-bottom: 40px; }\n  .register-sprite {\n  background: url(http://res.cloudinary.com/mally/image/upload/v1489480940/148948096411965_w5wbjb.png) no-repeat 0 0;\n  background-size: 159px 48px;\n  display: inline-block; }\n  .register-fb-logo {\n  background-position: -93px 0;\n  width: 28px;\n  top: 10px; }\n  .register-fb-logo,\n.register-gplus-logo {\n  height: 29px;\n  position: absolute;\n  left: 15px; }\n  .register-gplus-logo {\n  background-position: -122px 0;\n  width: 23px;\n  top: 13px; }\n  .register-button-info-text {\n  margin-top: 0;\n  margin-bottom: 20px; }\n  .register-info-text {\n  color: #495057;\n  font-size: 12px; }\n  .register-error-icon {\n  border: 2px solid #ff9314;\n  padding: 0 7px;\n  color: #ff9314;\n  display: inline-block;\n  position: absolute;\n  top: 12px;\n  right: 10px;\n  font-weight: 500;\n  border-radius: 21px; }\n  .register-error-message {\n  font-size: 11px;\n  margin-left: 15px;\n  color: #ff9314;\n  text-align: left;\n  margin-top: -9px;\n  max-height: 500px;\n  transition-property: all;\n  transition-duration: .5s;\n  transition-timing-function: cubic-bezier(0, 1, 0.5, 1); }\n  .register-gender-error-icon {\n  font-size: 16px; }\n  .register-gender-error-message {\n  margin-left: 0;\n  margin-top: 10px;\n  margin-bottom: 0; }\n"

/***/ }),

/***/ "./src/app/auth/components/update-password/update-password.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/auth/components/update-password/update-password.component.ts ***!
  \******************************************************************************/
/*! exports provided: UpdatePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePasswordComponent", function() { return UpdatePasswordComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../actions/auth.actions */ "./src/app/auth/actions/auth.actions.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdatePasswordComponent = /** @class */ (function () {
    function UpdatePasswordComponent(fb, store, route, router, actions, authService) {
        this.fb = fb;
        this.store = store;
        this.route = route;
        this.router = router;
        this.actions = actions;
        this.authService = authService;
        this.passwordReset = false;
    }
    UpdatePasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.email = params['email'];
            _this.token = params['reset_password_token'];
            _this.id = params['id'];
        });
        this.initForm();
    };
    UpdatePasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        var values = this.updatePasswordForm.value;
        var keys = Object.keys(values);
        if (this.updatePasswordForm.valid) {
            this.updatePasswordSubs = this.authService
                .updatePassword(values).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (_) { return _this.passwordReset = true; }, function (user) {
                var errors = user.error.error || 'Something went wrong';
                keys.forEach(function (val) {
                    _this.pushErrorFor(val, errors);
                });
            })).subscribe();
        }
        else {
            keys.forEach(function (val) {
                var ctrl = _this.updatePasswordForm.controls[val];
                if (!ctrl.valid) {
                    _this.pushErrorFor(val, null);
                    ctrl.markAsTouched();
                }
                ;
            });
        }
    };
    UpdatePasswordComponent.prototype.pushErrorFor = function (ctrl_name, msg) {
        this.updatePasswordForm.controls[ctrl_name].setErrors({ 'msg': msg });
    };
    UpdatePasswordComponent.prototype.initForm = function () {
        var password = '';
        var password_confirmation = '';
        this.updatePasswordForm = this.fb.group({
            'password': [password, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(6)])],
            'password_confirmation': [password_confirmation, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(6)])],
            'email': this.email,
            'reset_password_token': this.token,
            'id': this.id,
        }, { validator: this.matchingPasswords('password', 'password_confirmation') });
    };
    UpdatePasswordComponent.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    UpdatePasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
            selector: 'app-update-password',
            template: __webpack_require__(/*! ./update-password.component.html */ "./src/app/auth/components/update-password/update-password.component.html"),
            styles: [__webpack_require__(/*! ./update-password.component.scss */ "./src/app/auth/components/update-password/update-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__["AuthActions"],
            _core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], UpdatePasswordComponent);
    return UpdatePasswordComponent;
}());



/***/ })

}]);
//# sourceMappingURL=auth-auth-module.js.map