var mongoose = require('mongoose')

var productSchema = new mongoose.Schema({
	name: String,
	type: String,
	price: Number,
	seller: String,
	image: String
	// location: String
});

module.exports = mongoose.model('Product', productSchema, 'products');