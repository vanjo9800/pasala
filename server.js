var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);
var mongoose = require('mongoose')
var Product = require('./models/orderModel.js')
var cors = require('cors')
 
app.use(cors())

mongoose.connect('mongodb://localhost/shop');

app.use(express.static(__dirname + '/bower_components'));  
app.use('/', express.static(__dirname + '/frontend/'));

app.get('/', function(req, res,next) {  
    res.sendFile(__dirname + '/index.html');
});
app.use(cors())


// app.post('/addProduct', (req,res)=>{
// 	console.log('GOT REQ')
// 	// console.log(req)
// 	var productToAdd = new Product;
// 	productToAdd.name = req.query.name
// 	productToAdd.price = req.query.price
// 	productToAdd.type = req.query.type
// 	productToAdd.seller = req.query.seller
// 	productToAdd.image = req.query.image
// 	console.log(productToAdd)

// 	productToAdd.save((err)=>{
// 		if(err){
// 			console.log(err)
// 		}else{
// 			res.send('OK')
// 		}
// 	})
// })

io.on('connection', function(socket){
	socket.on('query', (queryData)=>{
			if(queryData.query == 'all'){
				mongoose.model('Product').find({},(err,results)=>{
					if(err){console.log(err)}
						else{
							socket.emit('result', {results: results})

						}
				})
				return
			}
			var apiai = require('apiai');

var app = apiai("4cf90f7f2fc44cc88614b095bedb9ac8");

var request = app.textRequest(queryData.query, {
    sessionId: 'ebaloSiEMaikata'
});

request.on('response', function(response) {
    tag = response.result.parameters.fashion || ""
    comparisson = response.result.parameters.comparison || "over"
    gender = response.result.parameters.gender || ""
    otherValue = response.result.parameters.number || ""
    currencyValue = response.result.parameters['unit-currency'].amount || ( otherValue != '' ? 	otherValue : 0 )
    searchText = "\"" + gender + "\" \"" + tag + "\""
    if( gender == "men") {
    	searchText += "\"-women\""
    }
    if(comparisson == "less") {
    	mongoose.model("Product").find( { $and: [{ $text: { $search:  searchText } }, { price: {$lt: currencyValue }} ] }, (err, results)=>{
    		if(err){
    			console.log(err)
    		}else{
    			//console.log(results)
    			socket.emit('result', {results: results})
    		}	
    	} )
    } else {
    	mongoose.model("Product").find( { $and: [{ $text: { $search:  searchText } }, { price: {$gt: currencyValue }} ] }, (err, results)=>{
    		if(err){console.log(err)}
    		else{
    			//2console.log(results)
    			socket.emit('result', {results: results})
    		}	
    	} )
    }
    // console.log(tag, comparisson, gender, currencyValue)

});

request.on('error', function(error) {
    console.log(error);
});

request.end();


})
})


server.listen(8080); 
