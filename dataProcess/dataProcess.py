import os
import pandas as pd
import requests

df = pd.read_csv('fashion.csv')

df2 = df[['Title', 'Vendor', 'Type', 'Variant Price', 'Variant Image']]
df2 = df2.dropna()
df2 = df2.drop_duplicates()
print(df2.shape)

for row in df2.iterrows():
	print(row[1][2])
	name = str(row[1][0])
	seller = str(row[1][1])
	itemType = str(row[1][2])
	price = int(row[1][3])
	image = str(row[1][4])
	#0 - title 1 - vendor 2 - type 3 - price 4 -variant image
	# r = requests.post("http://localhost:8080/addProduct", data={'name': title, 'price': price,'type': str(itemType).lower(), 'seller': vendor,'image': image})
	reqURL = "http://localhost:8080/addProduct?name={}&type={}&price={}&seller={}&image={}".format(name,itemType,price,seller,image)
	r = requests.post(reqURL)
